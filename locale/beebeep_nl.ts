<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="nl_BE" sourcelanguage="en_US">
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="292"/>
        <source>Header</source>
        <translation>Rubriek</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="293"/>
        <source>System</source>
        <translation>Systeem</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="294"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="295"/>
        <source>Connection</source>
        <translation>Verbinding</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="296"/>
        <source>User Status</source>
        <translation>Gebruikers Status</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="297"/>
        <source>User Information</source>
        <translation>Gebruikers Informatie</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="298"/>
        <source>File Transfer</source>
        <translation>Bestandsoverdracht</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="299"/>
        <source>History</source>
        <translation>Geschiedenis</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="300"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../src/core/Core.cpp" line="127"/>
        <source>%1 Unable to connect to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Kan niet verbinden met %2 netwerk. Controleer uw firewall instellingen.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="152"/>
        <source>%1 Unable to broadcast to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Kan niet verzenden naar %2 netwerk. Controleer uw firewall instellingen.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="165"/>
        <source>%1 You are connected to %2 Network.</source>
        <translation>%1 U bent verbonden met %2 netwerk.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="277"/>
        <source>%1 You are disconnected from %2 Network.</source>
        <translation>%1 U bent niet langer verbonden met het %2 netwerk.</translation>
    </message>
    <message>
        <source>%1 %2 will search users in these IP addresses: %3</source>
        <translation type="obsolete">%1 %2 zal zoeken naar gebruikers in deze ip adressen: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="230"/>
        <source>%1 Zero Configuration started with service name: %2</source>
        <translation>%1 Nul configuratie start altijd met service naam: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="81"/>
        <source>%1 User %2 cannot save settings in path: %3</source>
        <translation>%1 Gebruiker %2 kan instellingen niet in pad opslaan:%3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="97"/>
        <source>%1 User %2 cannot save chat messages in path: %3</source>
        <translation>%1 Gebruiker %2 kan chat berichten niet in pad opslaan:%3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="176"/>
        <source>%1 You have selected to join only in these workgroups: %2</source>
        <translation>%1 Je hebt gekozen om alleen deze werkgroepen deel te nemen: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="245"/>
        <source>%1 Zero Configuration service closed.</source>
        <translation>%1 Nul configuratie service gesloten.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="300"/>
        <source>%1 Zero Configuration is browsing network for service: %2</source>
        <translation>%1 Nul configuratie is netwerk voor service: %2 aan het doorbladeren</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="307"/>
        <source>%1 Zero Configuration cannot browse network for service: %2</source>
        <translation>%1 Nul configuratie kan niet netwerk voor service: %2 door bladeren</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="325"/>
        <source>%1 Broadcasting to the %2 Network...</source>
        <translation>%1 Versturen naar %2 netwerk...</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="331"/>
        <source>%1 You are not connected to %2 Network.</source>
        <translation>%1 U bent niet met het %2 netwerk verbonden.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="337"/>
        <source>%1 %2 has found a filter on UDP port %3. Please check your firewall settings.</source>
        <translation>%1 %2 heeft een filter op de UDP poort ontdekt %3. Controleer uw firewall instellingen.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="341"/>
        <source>View the log messages for more informations</source>
        <translation>Kijk in log bestand voor meer informatie</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="511"/>
        <source>New version is available</source>
        <translation>Nieuwe versie beschikbaar</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="512"/>
        <source>Click here to download</source>
        <translation>Klik hier voor de download</translation>
    </message>
    <message>
        <source>%1 The following networks appears as filtered: %2</source>
        <translation type="obsolete">%1 De volgende netwerken wordt weergegeven als gefilterd: %2</translation>
    </message>
    <message>
        <source>%1 You cannot reach %2 Network.</source>
        <translation type="obsolete">Voor %1 is het %2 netwerk onbereikbaar.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="389"/>
        <source>is connected from external network (the new subnet is added to your broadcast address list).</source>
        <translation>is verbonden met een extern netwerk (het nieuwe subnet is aan uw verspreidingsadreslijst toegevoegd).</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="401"/>
        <source>%1 Checking %2 more addresses...</source>
        <translation>%1 Controleer %2 meer adressen...</translation>
    </message>
    <message>
        <source>%1 Contacting %2 ...</source>
        <translation type="obsolete">%1 Contact opnemen met %2...</translation>
    </message>
    <message>
        <source>%1 Contacting %2 host addresses previously saved...</source>
        <translation type="obsolete">%1 Contact met %2 en hostadressen eerder opgeslagen...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="46"/>
        <source>%1 Chat with all local users.</source>
        <translation>%1 chat met alle lokale gebruikers.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="53"/>
        <source>Help me to know how many people are really using BeeBEEP.</source>
        <translation>Wil graag weten hoeveel mensen werkelijk BeeBEEP gebruiken.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="55"/>
        <source>Please add a like on Facebook.</source>
        <translation>Voeg een like toe op Facebook.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="83"/>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to you!</source>
        <translation>Gelukkige verjaardag!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to %1!</source>
        <translation>We wensen %1 een gelukkige verjaardag!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="64"/>
        <source>Happy birthday to Marco Mastroddi: %1 years old today! Cheers!!!</source>
        <translation>Gelukkige verjaardag Marco Mastroddi: %1 jaar oud vandaag! Schol!!!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="76"/>
        <source>Happy New Year!</source>
        <translation>Gelukkig Nieuw Jaar!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="99"/>
        <location filename="../src/core/CoreChat.cpp" line="164"/>
        <location filename="../src/core/CoreChat.cpp" line="244"/>
        <source>%1 Chat with %2.</source>
        <translation>%1 Chat met %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="147"/>
        <source>%1 You have created the group %2.</source>
        <translation>%1 U hebt de groep %2 aangemaakt.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="153"/>
        <source>%1 Welcome to the group %2.</source>
        <translation>%1 Welkom bij groep %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="198"/>
        <source>%1 The group has a new name: %2.</source>
        <translation>%1 De groep heeft een nieuwe naam: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="230"/>
        <source>%1 Members removed from the group: %2.</source>
        <translation>%1 Leden uit de groep %2 verwijderd.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="237"/>
        <source>%1 Members added to the group: %2.</source>
        <translation>%1 Leden aan de groep %2 toegevoegd.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="262"/>
        <source>Unable to send the message: you are not connected.</source>
        <translation>Kan bericht niet verzenden: u bent niet verbonden.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="268"/>
        <source>Unable to send the message: this chat is disabled.</source>
        <translation>Kan bericht niet verzenden: de chat is uitgeschakeld.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="302"/>
        <source>Unable to send the message to %1.</source>
        <translation>Kan bericht niet verzenden aan %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="330"/>
        <source>The message will be delivered to %1.</source>
        <translation>Dit bericht wordt verstuurd naar %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="335"/>
        <source>Nobody has received the message.</source>
        <translation>Niemand heeft het bericht ontvangen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="394"/>
        <source>%1 %2 can not join to the group.</source>
        <translation>%1 %2 kan niet toetreden tot de groep.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="457"/>
        <source>%1 saved chats are added to history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="573"/>
        <source>Offline messages sent to %2.</source>
        <translation>Offline bericht verzonden naar%2.</translation>
    </message>
    <message>
        <source>%1 %2 can not be invited to the group.</source>
        <translation type="obsolete">%1 %2 kan niet uitgenodigd worden voor de groep.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="410"/>
        <source>%1 %2 cannot be informed that you have left the group.</source>
        <translation>%1 %2 kan niet verwittigd worden dat u de groep verlaten hebt.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="502"/>
        <source>%1 You have left the group.</source>
        <translation>%1 U hebt de groep verlaten.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="504"/>
        <source>%1 %2 has left the group.</source>
        <translation>%1 %2 heeft de groep verlaten.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="218"/>
        <source>%1 (%2) is disconnected from %3 network.</source>
        <translation>%1 (%2) is niet verbonden met %3 netwerk.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="338"/>
        <source>%1 (%2) is connected to %3 network.</source>
        <translation>%1 (%2) is verbonden met het %3 netwerk.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="435"/>
        <source>%1 Network interface %2 is gone down.</source>
        <translation>%1 Netwerkkaart %2 is verdwenen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="48"/>
        <source>%1 Unable to start file transfer server: bind address/port failed.</source>
        <translation>%1 Kan de bestandsoverdrachtserver niet starten: adres/poort reserveren mislukt.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="94"/>
        <source>%1 Unable to download %2 from %3: user is offline.</source>
        <translation>%1 Onmogelijk %2 te downloaden van %3: gebruiker is offline.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="109"/>
        <source>%1 Unable to download %2 from %3: folder %4 cannot be created.</source>
        <translation>%1 Onmogelijk %2 te downloaden van %3 map %4 kan niet worden gemaakt.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="121"/>
        <source>%1 Downloading %2 from %3.</source>
        <translation>%1 is %2 aan het downloaden van %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>from</source>
        <translation>van</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>to</source>
        <translation>naar</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="184"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="238"/>
        <source>%1 Unable to send %2 to %3: user is offline.</source>
        <translation>%1 Onmogelijk %2 te verzenden naar %3:gebruiker is offline.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="215"/>
        <source>%1 Unable to send %2. File transfer is disabled.</source>
        <translation>%1 Kan %2 niet versturen. Bestandsoverdracht is uitgeschakeld.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Upload</source>
        <translation>Upload</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Download</source>
        <translation>Downloaden</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="187"/>
        <source>folder</source>
        <translation>map</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="247"/>
        <source>%1 %2: file not found.</source>
        <translation>%1 %2: bestand niet gevonden.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="257"/>
        <source>%1 %2 is a folder. You can share it.</source>
        <translation>%1 %2 is een map. U kunt het delen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="278"/>
        <source>%1 Unable to send %2: %3 is not connected.</source>
        <translation>%1 Kan %2 niet versturen: %3 is niet aangemeld.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="272"/>
        <source>%1 You send %2 to %3.</source>
        <translation>%1 U bent %2 naar %3 aan het versturen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="303"/>
        <source>%1 You have refused to download %2 from %3.</source>
        <translation>%1 U hebt de download van %2 geweigerd van %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="331"/>
        <source>%1 You have refused to download folder %2 from %3.</source>
        <translation>%1 Je hebt geweigerd om de map %2 te downloaden van %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="394"/>
        <source>Adding to file sharing</source>
        <translation>Toe aan het voegen aan bestandsdeling</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="449"/>
        <source>%1 is added to file sharing (%2)</source>
        <translation>%1 is toegevoegd aan bestandsdeling (%2)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="451"/>
        <source>%1 is added to file sharing with %2 files, %3 (elapsed time: %4)</source>
        <translation>%1 is toegevoegd aan bestandsdeling met %2 bestanden, %3 (verlopen tijd: %4)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="487"/>
        <source>All paths are removed from file sharing</source>
        <translation>Alle paden zijn verwijderd van gedeelde bestanden</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="509"/>
        <source>%1 is removed from file sharing</source>
        <translation>%1 is verwijderd uit de bestandsdeling</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="511"/>
        <source>%1 is removed from file sharing with %2 files</source>
        <translation>%1 is verwijderd uit de bestandsdeling met %2 bestanden</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="559"/>
        <source>%1 You are about to send %2 to %3. Checking folder...</source>
        <translation>%1 Je staat op het punt om %2 te zenden naar %3. Controleer map...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="594"/>
        <source>%1 Unable to send folder %2</source>
        <translation>%1 Kan niet verzenden naar map %2</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="599"/>
        <source>the folder is empty.</source>
        <translation>de map is leeg.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="606"/>
        <source>file transfer is not working.</source>
        <translation>Bestandsoverdracht werkt niet.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="614"/>
        <source>%1 is not connected.</source>
        <translation>%1 is niet verbonden.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="628"/>
        <source>internal error.</source>
        <translation>interne fout.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="633"/>
        <source>%1 You send folder %2 to %3.</source>
        <translation>%1 Je zend map %2 naar %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="148"/>
        <source>%1 %2 has refused to download %3.</source>
        <translation>%1 %2 heeft de download van %3 geweigerd.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="175"/>
        <source>%1 %2 is sending to you the file: %3.</source>
        <translation>%1 %2 stuurt bestand %3 aan u.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="245"/>
        <source>%1 An error occurred when %2 tries to add you to the group chat: %3.</source>
        <translation>%1 Er is een fout opgetreden toen %2 u aan de %3 chatgroep wou toevoegen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="251"/>
        <source>%1 %2 adds you to the group chat: %3.</source>
        <translation>%1 %2 voegt u toe aan de chatgroep %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="285"/>
        <source>%1 %2 has not shared files.</source>
        <translation>%1 %2 heeft geen gedeelde bestanden.</translation>
    </message>
    <message>
        <source>%1 %2 has not file shared.</source>
        <translation type="obsolete">%1 %2 heeft geen bestanden gedeeld.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="290"/>
        <source>%1 %2 has shared %3 files.</source>
        <translation>%1 %2 heeft %3 bestanden gedeeld.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="319"/>
        <source>%1 %2 has refused to download folder %3.</source>
        <translation>%1 %2 hebben geweigerd om map %3 te downloaden.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="326"/>
        <source>unknown folder</source>
        <translation>onbekende map</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="334"/>
        <source>%1 %2 is sending to you the folder: %3.</source>
        <translation>%1 %2 zend naar jou de map %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="86"/>
        <source>You are</source>
        <translation>U bent</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="88"/>
        <source>%1 is</source>
        <translation>%1 is</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="101"/>
        <source>You have changed your nickname from %1 to %2.</source>
        <translation>U hebt uw nickname van %1 in %2 veranderd.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="103"/>
        <source>%1 has changed the nickname in %2.</source>
        <translation>%1 heeft zijn/haar nickname gewijzigd in %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="117"/>
        <source>The %1&apos;s profile has been received.</source>
        <translation>Het profiel van %1 werd ontvangen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>You share this information</source>
        <translation>U deelt deze informatie</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>%1 shares this information</source>
        <translation>%1 deelt deze informatie</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="235"/>
        <source>%1 You have created group from chat: %2.</source>
        <translation>%1 Je hebt een groep van chat: %2 gemaakt.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="290"/>
        <source>%1 You have deleted group: %2.</source>
        <translation>%1 Je hebt de groep:%2 verwijderd.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="377"/>
        <source>is removed from favorites</source>
        <translation>is verwijderd van favorieten</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="383"/>
        <source>is added to favorites</source>
        <translation>is toegevoegd aan favorieten</translation>
    </message>
</context>
<context>
    <name>File</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="274"/>
        <source>Audio</source>
        <translation>Geluid</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="275"/>
        <source>Video</source>
        <translation>Beeld</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="276"/>
        <source>Image</source>
        <translation>Afbeelding</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="277"/>
        <source>Document</source>
        <translation>Document</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="278"/>
        <source>Other</source>
        <translation>Andere</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="279"/>
        <source>Executable</source>
        <translation>Uitvoerbaar bestand</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="280"/>
        <source>MacOSX</source>
        <translation>MacOSX</translation>
    </message>
</context>
<context>
    <name>FileTransferPeer</name>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="77"/>
        <source>invalid file header</source>
        <translation>Ongeldig bestandstitel</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="105"/>
        <source>Unable to open file</source>
        <translation>Kan bestand niet openen</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="112"/>
        <source>Unable to write in the file</source>
        <translation>Kan niet schrijven in het bestand</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="119"/>
        <source>%1 bytes downloaded but the file size is only %2 bytes</source>
        <translation>%1 bytes gedownload maar grootte van het bestand is maar %2 bytes</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="52"/>
        <source>Transfer cancelled</source>
        <translation>Overdracht annuleren</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="113"/>
        <source>Transfer completed in %1</source>
        <translation>Overdracht voltooid in %1</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="171"/>
        <source>Connection timeout</source>
        <translation>Kon niet tijdig verbinden</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="179"/>
        <source>Transfer timeout</source>
        <translation>Overdrachts timeout</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="101"/>
        <source>unable to send file header</source>
        <translation>Kan bestandstitel niet verzenden</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="116"/>
        <source>%1 bytes uploaded but the file size is only %2 bytes</source>
        <translation>%1 bytes geupload maar grootte van het bestand is maar %2 bytes</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="123"/>
        <source>%1 bytes sent not confirmed (%2 bytes confirmed)</source>
        <translation>%1 bytes verzonden niet bevestigd(%2 bytes bevestigd)</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="154"/>
        <source>Unable to upload data</source>
        <translation>Kan data niet uploaden</translation>
    </message>
</context>
<context>
    <name>GuiAddUser</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="36"/>
        <source>Add user</source>
        <translation>Gebruiker toevoegen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="39"/>
        <source>your IP is %1 in LAN %2</source>
        <translation>je IP is %1 in LAN %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="104"/>
        <source>Please insert a valid IP address.</source>
        <translation>Geef aub een geldig IP adres op.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="114"/>
        <source>Please insert a valid port or use the default one %1.</source>
        <translation>Geef aub een geldige poort op of gebruik de standaard poort %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="148"/>
        <source>These IP address and port are already inserted in list.</source>
        <translation>Deze IP adres en poort staan al in de opgegeven lijst.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="182"/>
        <source>Remove user path</source>
        <translation>Verwijder gebruikerspad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="184"/>
        <source>Clear all</source>
        <translation>Alles wissen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="209"/>
        <source>Please select an user path in the list.</source>
        <translation>Selecteer aub een gebruikerspad van de lijst.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="234"/>
        <source>auto added</source>
        <translation>Automatisch toegevoegd</translation>
    </message>
</context>
<context>
    <name>GuiAddUserDialog</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="17"/>
        <source>Dialog</source>
        <translation>Venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="73"/>
        <source>Add an IP address and port of the user you want to connect</source>
        <translation>Voeg Ip adres en poort toe van de gebruiker waarmee je wilt verbinden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="89"/>
        <source>IP Address</source>
        <translation>IP adres</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="121"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="169"/>
        <source>Split in IPv4 addresses</source>
        <translation>Deel in IPV4 adressen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="179"/>
        <source>Add</source>
        <translation>Toevoegen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="176"/>
        <source>Click here to add user path</source>
        <translation>Klik hier om gebruikerspad toe tevoegen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="156"/>
        <source>Comment</source>
        <translation>Commentaar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="25"/>
        <source>Auto add from LAN</source>
        <translation>Auto toevoegen van LAN</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="45"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="52"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>GuiAskPassword</name>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="23"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="30"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="62"/>
        <source>Use standard session (encrypted but authentication is not required)</source>
        <translation>Standaardsessie gebruiken (versleuteld maar authenticatie is niet vereist)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="69"/>
        <source>Use authentication password (spaces are removed) *</source>
        <translation>Gebruik aanmeldingswachtwoord (spaties worden verwijderd) *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="124"/>
        <source>* Password must be the same for all user you want to connect</source>
        <translation>* Wachtwoord dient hetzelfde te zijnvoor alle gebruikers waarmee u wenst te verbinden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="153"/>
        <source>Remember password (not recommended)</source>
        <translation>Wachtwoord onthouden (niet aanbevolen)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="160"/>
        <source>Show this dialog at connection startup</source>
        <translation>Toon dit venster bij het opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="33"/>
        <source>Chat Password - %1</source>
        <translation>Chat wachtwoord - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="103"/>
        <source>Password is empty. Please enter a valid one (spaces are removed).</source>
        <translation>Wachtwoord is leeg. Geef aub een geldig wachtwoord in (spaties worden verwijderd).</translation>
    </message>
</context>
<context>
    <name>GuiChat</name>
    <message>
        <source>%1 Secure Mode</source>
        <translation type="obsolete">%1 Veilige Modus</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation type="obsolete">Emoticons</translation>
    </message>
    <message>
        <source>Add your preferred emoticon to the message</source>
        <translation type="obsolete">Voeg uw favoriete emoticon toe aan het bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="139"/>
        <source>Change font style</source>
        <translation>Wijzig stijl lettertype</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="140"/>
        <source>Select your favourite chat font style</source>
        <translation>Selecteer uw favoriet lettertype</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="141"/>
        <source>Change font color</source>
        <translation>Wijzig tekstkleur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="142"/>
        <source>Select your favourite font color for the chat messages</source>
        <translation>Selecteer uw favoriete tekstkleur voor chatberichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="143"/>
        <source>Change background color</source>
        <translation>Wijzig achtergrond kleur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="144"/>
        <source>Select your favourite background color for the chat window</source>
        <translation>Selecteer uw favoriete achtergrondkleur voor chatberichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="145"/>
        <source>Filter message</source>
        <translation>Bericht filteren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="146"/>
        <source>Select the message types which will be showed in chat</source>
        <translation>Selecteer een bericht soort welke in chat worden getoond</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="147"/>
        <source>Chat settings</source>
        <translation>Chat instellingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="148"/>
        <source>Click to show the settings menu of the chat</source>
        <translation>Klik om de chat instellingen te tonen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="149"/>
        <source>Spell checking</source>
        <translation>Spellingscontrole</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="151"/>
        <source>Word completer</source>
        <translation>Volledig woord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="153"/>
        <source>Use Return key to send message</source>
        <translation>Gebruik enter toets om bericht te verzenden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="158"/>
        <source>Members</source>
        <translation>Leden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="159"/>
        <source>Show the members of the chat</source>
        <translation>Toon de leden van de chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="165"/>
        <location filename="../src/desktop/GuiChat.cpp" line="1076"/>
        <source>Find text in chat</source>
        <translation>Vind text in chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="166"/>
        <source>Send file</source>
        <translation>Stuur bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="167"/>
        <source>Send a file to a user or a group</source>
        <translation>Stuur een bestand naar gebruiker of groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="168"/>
        <source>Send folder</source>
        <translation>Stuur map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="169"/>
        <source>Save chat</source>
        <translation>Sla chat op</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="170"/>
        <source>Save the messages of the current chat to a file</source>
        <translation>Sla berichten uit deze chat op in een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="171"/>
        <source>Print...</source>
        <translation>Print...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="229"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="415"/>
        <source>unread messages</source>
        <translation>Ongelezen berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="948"/>
        <source>Use key Return to send message</source>
        <translation>Gebruik enter toets om bericht te verzenden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="950"/>
        <source>Use key Return to make a carriage return</source>
        <translation>Gebruik enter toest</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="964"/>
        <source>Spell checking is enabled</source>
        <translation>Spellingscontrole aanzetten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="971"/>
        <location filename="../src/desktop/GuiChat.cpp" line="995"/>
        <source>There is not a valid dictionary</source>
        <translation>Er is geen geldige map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="1114"/>
        <source>%1 not found in chat.</source>
        <translation>%1 niet gevonden in chat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="966"/>
        <source>Spell checking is disabled</source>
        <translation>Spellingscontrole uitzetten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="988"/>
        <source>Word completer is enabled</source>
        <translation>Volledig woord is uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="990"/>
        <source>Word completer is disabled</source>
        <translation>Volledig woord is ingeschakeld</translation>
    </message>
    <message>
        <source>Create group chat</source>
        <translation type="obsolete">Maak een groepschat aan</translation>
    </message>
    <message>
        <source>Create a group chat with two or more users</source>
        <translation type="obsolete">Maak een groepschat met 2 of meer gebruikers</translation>
    </message>
    <message>
        <source>Edit group chat</source>
        <translation type="obsolete">Bewerk groepschat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="180"/>
        <source>Change the name of the group or add and remove users</source>
        <translation>Wijzig de groepsnaam of beheer gebruikers in deze groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="173"/>
        <source>Clear messages</source>
        <translation>Berichten wissen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="174"/>
        <source>Clear all the messages of the chat</source>
        <translation>Alle chatberichten wissen</translation>
    </message>
    <message>
        <source>Create chat</source>
        <translation type="obsolete">Chat aanmaken</translation>
    </message>
    <message>
        <source>Create a chat with two or more users</source>
        <translation type="obsolete">Chat aanmaken met een of twee of meerdere gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="177"/>
        <source>Create group from chat</source>
        <translation>Maak een groepschat aan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="178"/>
        <source>Create a group from this chat</source>
        <translation>Maak een groep aan van deze chat</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation type="obsolete">Groep aanmaken</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation type="obsolete">Groep aanmaken met 2 of meer gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="179"/>
        <source>Edit group</source>
        <translation>Bewerk groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="181"/>
        <location filename="../src/desktop/GuiChat.cpp" line="182"/>
        <source>Leave the group</source>
        <translation>Verlaat de groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="223"/>
        <source>Copy to clipboard</source>
        <translation>Naar klembord kopiëren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="225"/>
        <source>Select All</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="257"/>
        <source>Show only messages in default chat</source>
        <translation>Toon alleen berichten in de standaard chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="297"/>
        <source>Last message %1</source>
        <translation>Laatste bericht %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="395"/>
        <source>All Lan Users</source>
        <translation>Alle lan-gebruikers</translation>
    </message>
    <message>
        <source>(You have left)</source>
        <translation type="obsolete">(U bent weggegaan)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="411"/>
        <source>You</source>
        <translation>U</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="437"/>
        <source>offline</source>
        <translation>afgemeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="445"/>
        <source>Show profile</source>
        <translation>Toon profiel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="455"/>
        <source>Show members</source>
        <translation>Toon leden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>Nobody</source>
        <translation>Niemand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>and</source>
        <translation>en</translation>
    </message>
    <message>
        <source>To</source>
        <translation type="obsolete">naar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="537"/>
        <source>last %1 messages</source>
        <translation>laatste %1 berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="706"/>
        <source>Please select a file to save the messages of the chat.</source>
        <translation>Selecteer aub een bestand om de chatberichten in op te slaan.</translation>
    </message>
    <message>
        <source>%1: unable to save the messages.
Please check the file or the directories write permissions.</source>
        <translation type="obsolete">%1: kan de berichten niet opslaan.
Controleer de schrijfrechten op de betrokken map of het bestand.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>%1: save completed.</source>
        <translation>%1: opslaan voltooid.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="771"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Kan tijdelijk bestand: %1 niet opslaan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="819"/>
        <source>Do you really want to send %1 %2 to the members of this chat?</source>
        <translation>Wilt u %1 %2 echt versturen naar de leden van deze chat?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>file</source>
        <translation>bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>files</source>
        <translation>bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="834"/>
        <source>Qt library for this OS doesn&apos;t support Drag and Drop for files. You have to select again the file to send.</source>
        <translation>Qt bibliotheek voor dit besturingssysteem ondersteunt geen Drag en drop voor bestanden. U dient het bestand dat u wilt versturen opnieuw te selecteren.</translation>
    </message>
</context>
<context>
    <name>GuiChatList</name>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="49"/>
        <source>Show</source>
        <translation>Toon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="52"/>
        <source>Clear</source>
        <translation>Wis</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="53"/>
        <source>Clear all chat messages</source>
        <translation>Wis alle chatberichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="55"/>
        <source>Delete</source>
        <translation>Verwijder</translation>
    </message>
</context>
<context>
    <name>GuiChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="20"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>*</source>
        <translation type="obsolete">*</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="238"/>
        <source>Write to: &lt;b&gt;ALL&lt;/b&gt;</source>
        <translation>Schrijf naar: &lt;b&gt; ALLEN&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="260"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="288"/>
        <source>Save window&apos;s geometry</source>
        <translation>Sla vensters geometry op</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="326"/>
        <source>Detach chat</source>
        <translation>Chat losmaken</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;.Helvetica Neue DeskInterface&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta naam=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="136"/>
        <source>Click to send message or just hit enter</source>
        <translation>Klik om bericht te sturen of druk op enter</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroup</name>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="14"/>
        <source>Dialog</source>
        <translation>Venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="26"/>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="42"/>
        <source>TextLabel</source>
        <translation>Tekstlabel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="74"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="81"/>
        <source>Cancel</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="37"/>
        <source>Group name</source>
        <translation>Groepsnaam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="38"/>
        <source>Please add member in the group:</source>
        <translation>Voeg aub toe aan de groep:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="41"/>
        <source>Users</source>
        <translation>Gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="65"/>
        <source>Create Group - %1</source>
        <translation>Maak groep - %1 aan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="67"/>
        <source>Create Chat - %1</source>
        <translation>Maak Chat - %1 aan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="73"/>
        <source>Edit Group - %1</source>
        <translation>Bewerk Groep - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="75"/>
        <source>Edit Chat - %1</source>
        <translation>Bewerk Chat - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="113"/>
        <source>Please select two or more member for the group.</source>
        <translation>Selecteer 2 of meer leden voor de groep.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="121"/>
        <source>Please insert a group name.</source>
        <translation>Geef aub een groepsnaam in.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="130"/>
        <source>%1 already exists as group name or chat name.
Please select a different name.</source>
        <translation>%1 bestaat al als chat- of groepsnaam.
Gebruik aub een andere naam.</translation>
    </message>
</context>
<context>
    <name>GuiEditVCard</name>
    <message>
        <source>Profile - %1</source>
        <translation type="obsolete">Profiel - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="103"/>
        <source>%1 - Select your profile photo</source>
        <translation>%1 - Selecteer uw profielfoto</translation>
    </message>
    <message>
        <source>Images (*.png *.xpm *.jpg *.jpeg)</source>
        <translation type="obsolete">Afbeeldingen (*.png *.xpm *.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="35"/>
        <source>Edit your profile</source>
        <translation>Bewerk uw profiel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="104"/>
        <source>Images</source>
        <translation>Afbeeldingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <source>Unable to load image %1.</source>
        <translation>Kan afbeelding %1 niet laden.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="147"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="156"/>
        <source>Please insert your nickname.</source>
        <translation>Geef aub uw nickname in.</translation>
    </message>
</context>
<context>
    <name>GuiEmoticons</name>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="49"/>
        <source>Recent</source>
        <translation>Laatst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="53"/>
        <source>Smiley</source>
        <translation>Smiley</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="57"/>
        <source>Objects</source>
        <translation>Objecten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="61"/>
        <source>Nature</source>
        <translation>Aard</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="65"/>
        <source>Places</source>
        <translation>Plaatsen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="69"/>
        <source>Symbols</source>
        <translation>Symbolen</translation>
    </message>
</context>
<context>
    <name>GuiFileInfoList</name>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Shared folders and files</source>
        <translation>Deel mappen en bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
</context>
<context>
    <name>GuiFloatingChat</name>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="43"/>
        <source>Show the bar of chat</source>
        <translation>Toon de chatbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="51"/>
        <source>Emoticons</source>
        <translation>Emoticons</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="63"/>
        <source>Show the emoticon panel</source>
        <translation>Toon de emoticon paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="64"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Voeg uw favoriete emoticon toe aan het bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="244"/>
        <source>Window&apos;s geometry and state saved</source>
        <translation>Venster geometrie en staat opgeslagen</translation>
    </message>
</context>
<context>
    <name>GuiGroupList</name>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="48"/>
        <source>Create group</source>
        <translation>Maak groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="51"/>
        <source>Edit group</source>
        <translation>Bewerk groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="54"/>
        <source>Open chat</source>
        <translation>Chat openen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="57"/>
        <source>Enable notifications</source>
        <translation>Notificaties inschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="60"/>
        <source>Disable notifications</source>
        <translation>Notificaties uitschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="63"/>
        <source>Delete group</source>
        <translation>Verwijder groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="143"/>
        <source>Waiting for two or more connected user</source>
        <translation>Aan het wachten tot 2 of meer gebruikers verbinden</translation>
    </message>
</context>
<context>
    <name>GuiHome</name>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="39"/>
        <source>Home</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="49"/>
        <source>Select a user you want to chat with or</source>
        <translation>Selecteer een gebruiker waarmee je wilt chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="94"/>
        <source>Copy to clipboard</source>
        <translation>Naar klembord kopiëren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="96"/>
        <source>Select All</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="98"/>
        <source>Print...</source>
        <translation>Print...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="105"/>
        <source>Show the datestamp</source>
        <translation>Toon datum</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="109"/>
        <source>Show the timestamp</source>
        <translation>Toon tijd</translation>
    </message>
</context>
<context>
    <name>GuiHomeWidget</name>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <source>Show this page at startup</source>
        <translation type="obsolete">Toon deze pagina tijdens opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="107"/>
        <source>chat with all users</source>
        <translation>Chat met alle gebruikers</translation>
    </message>
</context>
<context>
    <name>GuiLanguage</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="35"/>
        <source>Select language</source>
        <translation>Selecteer taal</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="50"/>
        <source>For the latest language translations please visit the %1</source>
        <translation>Voor de laatste vertaling bezoek %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="51"/>
        <source>official website</source>
        <translation>Officiele website</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="94"/>
        <source>Select a language folder</source>
        <translation>Selecteer de map met taalbestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="121"/>
        <source>Language &apos;%1&apos;&apos; not found.</source>
        <translation>Taal &apos;%1&apos; niet gevonden.</translation>
    </message>
</context>
<context>
    <name>GuiLanguageDialog</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="17"/>
        <source>Dialog</source>
        <translation>Venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="45"/>
        <source>Language</source>
        <translation>Taal</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="96"/>
        <source>Path</source>
        <translation>Map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="127"/>
        <source>Select language folder</source>
        <translation>Selecteer de map met taalbestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="169"/>
        <source>Restore to default language</source>
        <translation>De standaardtaal(EN) herstellen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="172"/>
        <source>Restore</source>
        <translation>Herstellen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="195"/>
        <source>Select</source>
        <translation>Selecteer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="205"/>
        <source>Cancel</source>
        <translation>Afbreken</translation>
    </message>
</context>
<context>
    <name>GuiLife</name>
    <message>
        <source>Paused (press space bar to continue)</source>
        <translation type="obsolete">Gepauzeerd (druk op de spatiebalk om voort te gaan)</translation>
    </message>
    <message>
        <source>Alive: %1 - Died: %2 - Spaces: %3 - Step: %4</source>
        <translation type="obsolete">Levens: %1 - Dood: %2 - Spaties: %3 - Stap: %4</translation>
    </message>
    <message>
        <source>BeeLife</source>
        <translation type="obsolete">BeeLife</translation>
    </message>
    <message>
        <source>Do you really want to randomize and restart?</source>
        <translation type="obsolete">Wilt u echt herinstellen en herstarten?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nee</translation>
    </message>
    <message>
        <source>Do you really want to clear all?</source>
        <translation type="obsolete">Wilt u echt alles wissen?</translation>
    </message>
    <message>
        <source>??? Evolution Completed ??? ... (or press space bar to continue)</source>
        <translation type="obsolete">??? Evolutie voltooid ??? ... (of druk op spatiebalk om voort te gaan)</translation>
    </message>
</context>
<context>
    <name>GuiLifeWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulier</translation>
    </message>
    <message>
        <source>#</source>
        <translation type="obsolete">#</translation>
    </message>
    <message>
        <source>Press space bar to start</source>
        <translation type="obsolete">Druk op spatiebalk om te starten</translation>
    </message>
    <message>
        <source>BeeLife</source>
        <translation type="obsolete">BeeLife</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="obsolete">Herstart</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Wis</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="obsolete">Snel</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="obsolete">Traag</translation>
    </message>
</context>
<context>
    <name>GuiLog</name>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="36"/>
        <source>System Log</source>
        <translation>Systeem log</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="55"/>
        <source>Save log as</source>
        <translation>Log opslaan als</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="56"/>
        <source>Save the log in a file</source>
        <translation>Log opslaan in een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="72"/>
        <source>Search</source>
        <translation>Zoek</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="78"/>
        <source>keyword</source>
        <translation>Sleutelwoord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="84"/>
        <source>Find</source>
        <translation>Vind</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="85"/>
        <source>Find keywords in the log</source>
        <translation>Vind een sleutelwoord in log</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="97"/>
        <source>Case sensitive</source>
        <translation>Hoofdlettergevoelig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="102"/>
        <source>Whole word</source>
        <translation>Gehele woord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="60"/>
        <source>Log to file</source>
        <translation>Log naar bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="109"/>
        <source>Block scrolling</source>
        <translation>Blok schrollen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="116"/>
        <source>Please select a file to save the log.</source>
        <translation>Selecteer een bestand om log op te slaan.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <source>Unable to save log in the file: %1</source>
        <translation>Kan log bestand:%1 niet opslaan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="151"/>
        <source>%1: save log completed.</source>
        <translation>%1: Opslaan log voltooid.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open file</source>
        <translation>Open bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open folder</source>
        <translation>Open map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="193"/>
        <source>%1 not found</source>
        <translation>%1 niet gevonden</translation>
    </message>
</context>
<context>
    <name>GuiLogWidget</name>
    <message>
        <location filename="../src/desktop/GuiLog.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
</context>
<context>
    <name>GuiMain</name>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="86"/>
        <source>Show the main tool bar</source>
        <translation>Toon de hoofdmenubalk</translation>
    </message>
    <message>
        <source>Show the bar of plugins</source>
        <translation type="obsolete">Toon de plugin balk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="283"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3291"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3292"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3293"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3294"/>
        <source>offline</source>
        <translation>afgemeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="326"/>
        <source>Do you really want to quit %1?</source>
        <translation>Wilt u %1 echt afsluiten ?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="349"/>
        <source>&lt;b&gt;Settings can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Instellingen kunnen niet bewaard worden&lt;/b&gt;. Map:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="351"/>
        <location filename="../src/desktop/GuiMain.cpp" line="368"/>
        <source>&lt;b&gt;is not writable&lt;/b&gt; by user:</source>
        <translation>&lt;b&gt;is niet schrijfbaar&lt;/b&gt; voor user:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="353"/>
        <location filename="../src/desktop/GuiMain.cpp" line="370"/>
        <source>Do you want to close anyway?</source>
        <translation>Wilt u toch afsluiten?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="414"/>
        <source>No new message available</source>
        <translation>Geen nieuwe berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="498"/>
        <source>Disconnect from %1 network</source>
        <translation>Verbinding met %1 netwerk verbreken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="504"/>
        <source>Connect to %1 network</source>
        <translation>Verbinden met %1 netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="587"/>
        <source>Secure Lan Messenger</source>
        <translation>Secure Lan Messenger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="588"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="590"/>
        <source>for</source>
        <translation>voor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="592"/>
        <source>developed by</source>
        <translation>ontwikkeld door</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="601"/>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br /&gt;it under the terms of the GNU General Public License as published&lt;br /&gt;by the Free Software Foundation, either version 3 of the License&lt;br /&gt;or (at your option) any later version.&lt;br /&gt;&lt;br /&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br /&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br /&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br /&gt;See the GNU General Public License for more details.</source>
        <translation>BeeBEEP is gratis software: u kunt het verspreiden en wijzigen &lt;br /&gt; onder de voorwaarden van de &apos;GNU General Public License&apos; gepubliceerd door de Free Software Foundation, ofwel versie 3 of een latere versie.&lt;br /&gt;&lt;br /&gt;BeeBEEP wordt verspreid in de hoop nuttig te zijn, ZONDER ENIGE GARANTIE;zonder zelfs ook maar enige geïmpliceerde garantie&lt;br /&gt;van GESCHIKTHEID VOOR EEN BEPAALD DOEL&lt;br /&gt;Zie ook de algemene voorwaarden van de &apos;GNU General Public License&apos; voor meer details.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="617"/>
        <source>Search for users...</source>
        <translation>Zoeken op gebruikers...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="623"/>
        <source>Close the chat and quit %1</source>
        <translation>Sluit de chat af en sluit het programma %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="631"/>
        <source>Show the main tool bar with settings</source>
        <translation>Toon de algemene toolbar met instellingen</translation>
    </message>
    <message>
        <source>Show the tool bar with plugin shortcuts</source>
        <translation type="obsolete">Toon de toolbar met plugins en snelkoppelingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="635"/>
        <source>Show the informations about %1</source>
        <translation>Toon de informatie betreffende %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="638"/>
        <source>Create chat</source>
        <translation>Chat aanmaken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="639"/>
        <source>Create a chat with two or more users</source>
        <translation>Chat aanmaken met een of twee of meerdere gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="642"/>
        <source>Create group</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="643"/>
        <source>Create a group with two or more users</source>
        <translation>Groep aanmaken met 2 of meer gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="663"/>
        <source>Add users manually...</source>
        <translation>Gebruikers handmatig toevoegen...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="664"/>
        <source>Add the IP address and the port of the users you want to connect</source>
        <translation>Voeg Ip adres en poort toe van de gebruikers waarmee je wilt verbinden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="667"/>
        <source>Select language...</source>
        <translation>Selecteer taal...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="668"/>
        <source>Select your preferred language</source>
        <translation>Selecteer uw voorkeurstaal</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="669"/>
        <source>Download folder...</source>
        <translation>Download map...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="670"/>
        <source>Select the download folder</source>
        <translation>Selecteer de download map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="673"/>
        <source>Dictionary...</source>
        <translation>Woordenboek...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="674"/>
        <source>Select your preferred dictionary for spell checking</source>
        <translation>Selecteer de gewenste woordenboek voor spellingcontrole</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="677"/>
        <source>Select beep file...</source>
        <translation>Selecteer beep bestand...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="678"/>
        <source>Select the file to play on new message arrived</source>
        <translation>Selecteer het bestand dat moet afgespeeld worden bij ontvangst van een bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="679"/>
        <source>Play beep</source>
        <translation>Speel beep af</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="680"/>
        <source>Test the file to play on new message arrived</source>
        <translation>Test het bestand dat moet afgespeeld worden bij ontvangst nieuw bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="684"/>
        <source>Open your resource folder</source>
        <translation>Open je bron map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="685"/>
        <source>Click to open your resource folder</source>
        <translation>Klik om je bronmap te openen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="687"/>
        <source>Open your data folder</source>
        <translation>Open je data map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="688"/>
        <source>Click to open your data folder</source>
        <translation>Klik om je data map te openen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="721"/>
        <source>Show the timestamp</source>
        <translation>Toon datum en tijd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="727"/>
        <source>Show the datestamp</source>
        <translation>Toon datum</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="728"/>
        <source>If enabled the message shows its datestamp in the chat window</source>
        <translation>Indien ingeschakeld toont het bericht de datum in het chat venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="733"/>
        <source>Show preview of the images</source>
        <translation>Toon voorbeeld afbeeldingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="734"/>
        <source>If enabled the preview of the downloaded images will be showed in the chat window</source>
        <translation>Indien ingeschakeld toon voorbeeld van gedownloade afbeeldingen in het chatvenster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="739"/>
        <source>Parse Unicode and ASCII emoticons</source>
        <translation>Verwerk Unicode en ASCII emoticons</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="740"/>
        <source>If enabled the ASCII emoticons will be recognized and shown as images</source>
        <translation>Indien ingeschakeld worden de ASCI emoticons herkend en getoond als afbeeldingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="745"/>
        <source>Use native emoticons</source>
        <translation>Gebruik standaard emoticons</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="746"/>
        <source>If enabled the emoticons will be parsed by your system font</source>
        <translation>Indien ingeschakeld zullen de emoticons worden verwerkt door uw systeem lettertype</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="763"/>
        <source>Use your name instead of &apos;You&apos;</source>
        <translation>Gebruik je naam in plaats van &apos;Jij&apos;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="785"/>
        <source>Prompts for nickname on startup</source>
        <translation>Vraag voor een nicknaam bij opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="814"/>
        <source>Check for new version at startup</source>
        <translation>Controleer nieuwe versie bij het opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="840"/>
        <source>If the file to be downloaded already exists it is automatically overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="849"/>
        <source>Ask me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="874"/>
        <source>Always open a new floating chat window</source>
        <translation>Open altijd een nieuw zwevend chatvenster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="875"/>
        <source>If enabled when you always open chat in a new floating window</source>
        <translation>Indien ingeschakeld open altijd een nieuw zwevend chatvenster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="884"/>
        <source>When the chat is not visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="892"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="951"/>
        <source>Show status color in background</source>
        <translation>Toon status kleur in achtergrond</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="952"/>
        <source>If enabled the user in list has colored backrgound as status</source>
        <translation>Indien ingeschakeld heeft gebruikers lijst gekleurde achtergrond als status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="978"/>
        <source>Recently used</source>
        <translation>Onlangs gebruikt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="983"/>
        <source>Change your status description...</source>
        <translation>Verander je statusbeschrijving...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="984"/>
        <source>Clear all status descriptions</source>
        <translation>Wis alle statusbeschrijvingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="996"/>
        <source>Save main window geometry</source>
        <translation>Sla vensters geometry op</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1018"/>
        <source>Show new message</source>
        <translation>Toon nieuw bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1048"/>
        <source>Add +1 user to anonymous usage statistics</source>
        <translation>Voeg 1 gebruiker voor anonieme gebruiksstatistieken toe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1049"/>
        <source>Help me to know how many users have BeeBEEP</source>
        <translation>Wil graag weten hoeveel gebruikers BeeBEEP gebruiken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1066"/>
        <source>Network</source>
        <translation>Netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1100"/>
        <source>Show only message notifications</source>
        <translation>Toon alleen bericht meldingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1101"/>
        <source>If enabled tray icon shows only message notifications</source>
        <translation>Indien taakbalkicoon ingeschakeld toon alleen bericht meldingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1106"/>
        <source>Show chat message preview</source>
        <translation>Toon voorbeeld chat bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1639"/>
        <source>Show only last %1 messages</source>
        <translation>Toon alleen laatste %1 berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1640"/>
        <source>If enabled only the last %1 messages will be shown in chat</source>
        <translation>Indien ingeschakeld worden alleen de laatste %1 berichten getoond in de chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1954"/>
        <source>File transfer is not enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1960"/>
        <source>You are not connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3589"/>
        <source>at lunch</source>
        <translation>Lunchtijd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3598"/>
        <source>in a meeting</source>
        <translation>Vergadering</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3634"/>
        <source>Do you really want to clear all saved status descriptions?</source>
        <translation>Wil je echt alle opgeslagen statusbeschrijvingen wissen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3811"/>
        <source>Select your dictionary path</source>
        <translation>Selecteer je woordenboek pad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3819"/>
        <source>Dictionary selected: %1</source>
        <translation>Woordenboek geselecteerd: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3821"/>
        <source>Unable to set dictionary: %1</source>
        <translation>Kan woordenboek niet instellen: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3982"/>
        <source>Window geometry and state saved</source>
        <translation>Venster geometrie en staat opgeslagen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="783"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="802"/>
        <source>Show minimized at startup</source>
        <translation>Toon geminimaliseerd met opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="803"/>
        <source>If enabled %1 is showed minimized at startup</source>
        <translation>Indien ingeschakeld %1 wordt geminimaliseerd getoond met opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="858"/>
        <source>Prompt before downloading file</source>
        <translation>Melding voor het downloaden van een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="859"/>
        <source>If enabled you have to confirm the action before downloading a file</source>
        <translation>Indien ingeschakeld moet je de actie bevestigen voor het downloaden van een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="808"/>
        <source>Reset window geometry at startup</source>
        <translation>Herstel venster geometrie bij het opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="671"/>
        <source>Shortcuts...</source>
        <translation>Snelkoppelingen...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="672"/>
        <source>Enable and edit your custom shortcuts</source>
        <translation>Eigen snelkoppelingen aanzetten en bewerken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="796"/>
        <source>Show activities home page at startup</source>
        <translation>Toon activiteiten van homepage bij het opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="797"/>
        <source>If enabled the activities home page instead of chat page will be showed at startup</source>
        <translation>Indien ingeschakeld activiteiten van homepage in plaats van chat-pagina zal worden getoond bij het opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="959"/>
        <source>Change size of the user&apos;s picture</source>
        <translation>Verander de grootte van gebruikersafbeelding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="960"/>
        <source>Click to change the picture size of the users in the list</source>
        <translation>Klik om de afbeeldings grootte van gebruiker in lijst te veranderen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1303"/>
        <source>Show the bar of log</source>
        <translation>Toon de logbalk</translation>
    </message>
    <message>
        <source>When do you want %1 to play beep?</source>
        <translation type="obsolete">Wanneer wil je dat %1 beep afspeeld?</translation>
    </message>
    <message>
        <source>If it not visible</source>
        <translation type="obsolete">Als het niet zictbaar is</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="888"/>
        <source>Always</source>
        <translation>Altijd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>or more</source>
        <translation>of meer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2389"/>
        <source>Show the bar of games</source>
        <translation>Toon de bar van spellen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2815"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2822"/>
        <source>Unable to add this key in the registry: permission denied.</source>
        <translation>Kan deze sleutel niet aan register toevoegen: Niet toegestaan.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3037"/>
        <source>All the members of this chat are not online. The changes may not be permanent. Do you wish to continue?</source>
        <translation>Alle leden van deze chat zijn offline. De veranderingen zijn niet blijvend. Wil je toch doorgaan?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3175"/>
        <source>Do you really want to delete chat with %1?</source>
        <translation>Wilt u echt de chat met %1 verwijderen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3296"/>
        <source>inactive</source>
        <translation>Inactief</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3323"/>
        <source>active</source>
        <translation>actief</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3396"/>
        <source>%1 is online</source>
        <translation>%1 is online</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3398"/>
        <source>%1 is offline</source>
        <translation>%1 is offline</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3410"/>
        <source>Please select the new size of the user picture</source>
        <translation>Selecteer een nieuwe grootte voor gebruikers foto</translation>
    </message>
    <message>
        <source>If enabled the window geometry will be reset to default value at next startup</source>
        <translation type="obsolete">Indien ingeschakeld wordt de venster geometrie teruggezet naar de standaardwaarde bij de volgende startup</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="923"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="933"/>
        <source>Show only the online users</source>
        <translation>Toon de aangemelde gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="934"/>
        <source>If enabled only the online users are shown in the list</source>
        <translation>Indien ingeschakeld worden alleen aangemelde gebruikers getoond in de lijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="940"/>
        <source>If enabled you can see a picture of the users in the list (if they have)</source>
        <translation>Indien ingeschakeld wordt een afbeelding getoond van de gebruikers (indien beschikbaar)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="945"/>
        <source>Show the user&apos;s vCard on right click</source>
        <translation>Toon de vCards gebruikers met rechtermuis klik</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="946"/>
        <source>If enabled you can see the user&apos;s vCard when right click on it</source>
        <translation>Indien ingeschakeld kun je de vCard gebruikers zien met rechtermuis klik</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1082"/>
        <source>Close button minimize to tray icon</source>
        <translation>Sluitknop minimaliseren naar taakbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1083"/>
        <source>If enabled when the close button is clicked the window will be minimized to the system tray icon</source>
        <translation>Indien ingeschakeld: Er wordt op knop sluiten geklikt het venster wordt geminimaliseerd in de taakbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1088"/>
        <source>Escape key minimize to tray icon</source>
        <translation>Escape knop minimaliseren naar taakbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1089"/>
        <source>If enabled when the escape button is clicked the window will be minimized to the system tray icon</source>
        <translation>Indien ingeschakeld: Als op escape knop wordt geklikt het venster wordt geminimaliseerd in de taakbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1223"/>
        <source>Emoticons</source>
        <translation>Emoticons</translation>
    </message>
    <message>
        <source>Smiley</source>
        <translation type="obsolete">Smiley</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1235"/>
        <source>Show the emoticon panel</source>
        <translation>Toon de emoticon-paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1236"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Voeg uw favoriete emoticon toe aan het bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1543"/>
        <source>Please select the maximum number of messages to be showed</source>
        <translation>Selecteer het maximale aantal berichten  dat word getoond</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2737"/>
        <source>Group chat will be deleted when all members goes offline.</source>
        <translation>Groep-chat word verwijderd wanneer alle leden offline gaan.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2738"/>
        <source>If you want a persistent chat please consider to make a Group instead.</source>
        <translation>Als je een blijfende chat wilt, iverweeg een groep aan te maken.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2739"/>
        <source>Do you wish to continue or create group?</source>
        <translation>Wil je doorgaan of een groep aanmaken?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Continue</source>
        <translation>Doorgaan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Create Group</source>
        <translation>Groep aanmaken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3316"/>
        <source>disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="866"/>
        <source>Set status to away automatically</source>
        <translation>Zet status automatisch in &apos;weg&apos;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="867"/>
        <source>If enabled %1 change your status to away after an idle of %2 minutes</source>
        <translation>Indien ingeschakeld %1 wijzig de status in &apos;weg&apos; na een tijdspanne van %2 minuten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="881"/>
        <source>If enabled when a new message is arrived a sound is emitted</source>
        <translation>Indien ingeschakeld wordt een geluid afgespeeld bij ontvangen van een bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="902"/>
        <source>If enabled when a new message is arrived %1 is shown on top of all other windows</source>
        <translation>Indien ingeschakeld wordt bij ontvangen bericht %1 een popup getoond</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="844"/>
        <source>Generate automatic filename</source>
        <translation>Genereer automatische bestandsnaam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="845"/>
        <source>If the file to be downloaded already exists a new filename is automatically generated</source>
        <translation>Indien het te downloaden bestand reeds bestaat, wordt een bestandsnaam automatisch gegenereerd</translation>
    </message>
    <message>
        <source>Close to tray icon</source>
        <translation type="obsolete">Minimaliseren naar de taakbalk ipv afsluiten</translation>
    </message>
    <message>
        <source>If enabled when the close button is clicked the window minimized to the system tray icon</source>
        <translation type="obsolete">Indien ingeschakeld wordt het ventster geminimaliseerd ipv gesloten bij het sluiten van het venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1094"/>
        <source>Enable tray icon notification</source>
        <translation>Taakbalkicoon tonen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1095"/>
        <source>If enabled tray icon shows some notification about status and message</source>
        <translation>Indien ingeschakeld worden status en berichtwaarschuwingen getoond wanneer de muis over het taakbalkicoon beweegt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="908"/>
        <source>If enabled %1 stays on top of the other windows</source>
        <translation>Indien ingeschakeld %1 wordt het boven andere vensters getoond</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="829"/>
        <source>Enable file transfer</source>
        <translation>Schakel bestandsoverdracht in</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="830"/>
        <source>If enabled you can transfer files with the other users</source>
        <translation>Indien ingeschakeld kunnen bestanden gedeeld worden met andere gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="790"/>
        <source>Prompts for network password on startup</source>
        <translation>Vraag netwerkwachtwoord bij opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="791"/>
        <source>If enabled the password dialog will be shown on connection startup</source>
        <translation>Indien ingeschakeld wordt het wachtwoordvenster getoond bij het opstarten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="820"/>
        <source>Load %1 on Windows startup</source>
        <translation>Laad %1 bij het opstarten van Windows</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="821"/>
        <source>If enabled you can automatically load %1 at system startup</source>
        <translation>Indien ingeschakeld kun je %1 automatisch laden bij het opstarten van Windows</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="709"/>
        <source>Enable the compact mode in chat window</source>
        <translation>Schakel de compacte modus in in het chatvenster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="710"/>
        <source>If enabled the sender&apos;s nickname and his message are in the same line</source>
        <translation>Indien ingeschakeld verschijnen de nickname en het bericht op de dezelfde lijn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="715"/>
        <source>Add a blank line between the messages</source>
        <translation>Voeg een witregel toe tussen berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="716"/>
        <source>If enabled the messages in the chat window are separated by a blank line</source>
        <translation>Indien ingeschakeld worden berichten in het chatvenster gescheiden door een witregel</translation>
    </message>
    <message>
        <source>Show the messages&apos; timestamp</source>
        <translation type="obsolete">Toon tijd bij berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="722"/>
        <source>If enabled the message shows its timestamp in the chat window</source>
        <translation>Indien ingeschakeld wordt de tijd getoond bij de berichten in het chatvenster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="704"/>
        <source>If enabled the user&apos;s nickname in chat and in list is colored</source>
        <translation>Indien ingeschakeld is de nickname gekleurd in het chatvenster en de lijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="770"/>
        <source>Use HTML tags</source>
        <translation>Gebruik HTML tags</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="771"/>
        <source>If enabled HTML tags are not removed from the message</source>
        <translation>Indien ingeschakeld worden HTML tags niet verwijderd uit de berichten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="776"/>
        <source>Use clickable links</source>
        <translation>Gebruik klikbare links</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="777"/>
        <source>If enabled the links in the message are recognized and made clickable</source>
        <translation>Indien ingeschakeld worden links in de berichten herkend en worden ze klikbaar gemaakt</translation>
    </message>
    <message>
        <source>Show emoticons</source>
        <translation type="obsolete">Toon emoticons</translation>
    </message>
    <message>
        <source>If enabled the emoticons will be recognized and shown as images</source>
        <translation type="obsolete">Indien ingeschakeld worden emoticons herkend en getoond als afbeeldingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="751"/>
        <source>Show messages grouped by user</source>
        <translation>Toon berichten gegroepeerd per gebruiker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="752"/>
        <source>If enabled the messages will be shown grouped by user</source>
        <translation>Indien ingeschakeld worden de berichten gegroepeerd getoond per gebruiker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="695"/>
        <source>Save messages</source>
        <translation>Sla berichten op</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="366"/>
        <source>&lt;b&gt;Chat messages can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Chat bericht kan niet worden opgeslagen&lt;/b&gt;. Pad:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="696"/>
        <source>If enabled the messages are saved when the program is closed</source>
        <translation>Indien ingeschakeld worden de berichten opgeslagen wanneer het programma wordt afgesloten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="965"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="966"/>
        <source>Select your status</source>
        <translation>Selecteer uw status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="972"/>
        <location filename="../src/desktop/GuiMain.cpp" line="988"/>
        <source>Your status will be %1</source>
        <translation>Uw status zal %1 zijn</translation>
    </message>
    <message>
        <source>Add a status description...</source>
        <translation type="obsolete">Voeg een statusbeschrijving toe...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1006"/>
        <source>Show %1 home</source>
        <translation>Toon %1 Home</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1007"/>
        <source>Show the homepage with %1 activity</source>
        <translation>Toon startpagina met %1 activiteit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1008"/>
        <source>Show the chat</source>
        <translation>Toon de chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1009"/>
        <source>Show the chat view</source>
        <translation>Toon de chatview</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1010"/>
        <source>Show my shared files</source>
        <translation>Toon de gedeelde bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1011"/>
        <source>Show the list of the files which I have shared</source>
        <translation>Toon de lijst van bestanden die ik deel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1012"/>
        <source>Show the network shared files</source>
        <translation>Toon de bestanden die gedeeld worden op het netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1013"/>
        <source>Show the list of the network shared files</source>
        <translation>Toon een lijst van bestanden die op het netwerk gedeeld worden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1022"/>
        <source>Plugins</source>
        <translation>Plugins</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1026"/>
        <source>Tip of the day</source>
        <translation>Tip van de dag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1027"/>
        <source>Show me the tip of the day</source>
        <translation>Toon mij de tip van de dag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1028"/>
        <source>Fact of the day</source>
        <translation>Feit van de dag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1029"/>
        <source>Show me the fact of the day</source>
        <translation>Toon me het feit van de dag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1032"/>
        <source>Show %1&apos;s license...</source>
        <translation>Toon de %1 licentie...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1033"/>
        <source>Show the informations about %1&apos;s license</source>
        <translation>Toon informatie van de %1 licentie</translation>
    </message>
    <message>
        <source>About &amp;Qt...</source>
        <translation type="obsolete">Over &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1035"/>
        <source>Show the informations about Qt library</source>
        <translation>Toon informatie over de Qt library</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1037"/>
        <source>Open %1 official website...</source>
        <translation>Open de officiële website van %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1038"/>
        <source>Explore %1 official website</source>
        <translation>Verken de officiële %1 website</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1039"/>
        <source>Check for new version...</source>
        <translation>Controleer op nieuwe versie...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1040"/>
        <source>Open %1 website and check if a new version exists</source>
        <translation>Open de %1 website en kijk of er een nieuwe versie beschikbaar is</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1041"/>
        <source>Download plugins...</source>
        <translation>Plugins downloaden...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1042"/>
        <source>Open %1 website and download your preferred plugin</source>
        <translation>Open de %1 website en download uw favoriete plugin</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1043"/>
        <source>Help online...</source>
        <translation>Online help...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1044"/>
        <source>Open %1 website to have online support</source>
        <translation>Open de %1 website om beroep te doen op online ondersteuning</translation>
    </message>
    <message>
        <source>Please donate for %1 :-)</source>
        <translation type="obsolete">Doneer aub voor %1 :-)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1055"/>
        <source>I&apos;m so grateful and pleased about that</source>
        <translation>Ik ben daar zo dankbaar en blij om</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="181"/>
        <source>Ready</source>
        <translation>Klaar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1163"/>
        <source>Users</source>
        <translation>Gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="497"/>
        <source>Disconnect</source>
        <translation>Verbreek</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="503"/>
        <source>Connect</source>
        <translation>Verbind</translation>
    </message>
    <message>
        <source>Configure network...</source>
        <translation type="obsolete">Netwerk instellen...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="618"/>
        <source>Configure %1 network to search a user who is not in your local subnet</source>
        <translation>Stel %1 netwerk in om te zoeken naar een gebruiker die niet in het lokale subnet zit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="621"/>
        <source>Quit</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="626"/>
        <source>Edit your profile...</source>
        <translation>Bewerk uw profiel...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="627"/>
        <source>Change your profile information like your picture or your email or phone number</source>
        <translation>Wijzig uw profielinformatie (zoals afbeelding, telefoon of email)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="634"/>
        <source>About %1...</source>
        <translation>Over %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="653"/>
        <source>Main</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="659"/>
        <source>Broadcast to network</source>
        <translation>Verzend naar netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="660"/>
        <source>Broadcast a message in your network to find available users</source>
        <translation>Verzend een bericht naar het netwerk om beschikbare gebruikers te vinden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="703"/>
        <source>Show colored nickname</source>
        <translation>Toon gekleurde nickname</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="836"/>
        <source>If a file already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="939"/>
        <source>Show the user&apos;s picture</source>
        <translation>Toon de gebruikersafbeelding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="693"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Show send message icon</source>
        <translation type="obsolete">Toon icoon voor &apos;verzend bericht&apos;</translation>
    </message>
    <message>
        <source>If enabled the icon of send message is shown in chat window</source>
        <translation type="obsolete">Indien ingeschakeld wordt een icoon getoond voor &apos;zend bericht&apos; in het chatvenster</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="obsolete">Systeem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="880"/>
        <source>Enable BEEP alert on new message</source>
        <translation>Schakel BEEP-waarschuwing in bij nieuw bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="901"/>
        <source>Raise on top on new message</source>
        <translation>Breng programma op voorgrond bij nieuw bericht</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="907"/>
        <source>Always stay on top</source>
        <translation>Altijd boven</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="995"/>
        <source>View</source>
        <translation>Toon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1025"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <source>Show online users</source>
        <translation type="obsolete">Toon aangemelde gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1172"/>
        <source>Show the list of the connected users</source>
        <translation>Toon de lijst van verbonden gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1175"/>
        <source>Groups</source>
        <translation>Groepen</translation>
    </message>
    <message>
        <source>Show your groups</source>
        <translation type="obsolete">Toon uw groepen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1184"/>
        <source>Show the list of your groups</source>
        <translation>Toon de lijst van uw groepen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1187"/>
        <source>Chats</source>
        <translation>Chatgesprekken</translation>
    </message>
    <message>
        <source>Show the chat list</source>
        <translation type="obsolete">Toon de gesprekkenlijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1196"/>
        <source>Show the list of the chats</source>
        <translation>Toon de lijst met chatgesprekken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1199"/>
        <source>History</source>
        <translation>Geschiedenis</translation>
    </message>
    <message>
        <source>Show the saved chat list</source>
        <translation type="obsolete">Toon de lijst met opgeslagen chatgesprekken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1208"/>
        <source>Show the list of the saved chats</source>
        <translation>Toon de lijst met opgeslagen chatgesprekken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1211"/>
        <source>File Transfers</source>
        <translation>Bestandsoverdrachten</translation>
    </message>
    <message>
        <source>Show the file transfers</source>
        <translation type="obsolete">Toon de bestandsoverdrachten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1220"/>
        <source>Show the list of the file transfers</source>
        <translation>Toon de lijst van bestandsoverdrachten</translation>
    </message>
    <message>
        <source>Show the chat tool bar</source>
        <translation type="obsolete">Toon de chat werkbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1281"/>
        <source>Show the bar of local file sharing</source>
        <translation>Toon de werkbalk voor lokale bestandsdeling</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1292"/>
        <source>Show the bar of network file sharing</source>
        <translation>Toon de werkbalk voor bestandsdeling op het netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1317"/>
        <source>Show the bar of screenshot plugin</source>
        <translation>Toon de werkbalk voor schermafdrukken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1342"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2406"/>
        <source>Play %1</source>
        <translation>Speel %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1346"/>
        <source>is a game developed by</source>
        <translation>is een spel ontworpen door</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1349"/>
        <source>About %1</source>
        <translation>Over %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1485"/>
        <source>Please save the network password in the next dialog.</source>
        <translation>Sla het netwerkwachtwoord aub op in het volgende venster.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1507"/>
        <source>How many minutes of idle %1 can wait before changing status to away?</source>
        <translation>Hoeveel minuten moet %1 wachten voor de status in &apos;weg&apos; wordt gewijzigd?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1722"/>
        <source>New message from %1</source>
        <translation>Nieuw bericht van %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1725"/>
        <source>New message arrived</source>
        <translation>Nieuw bericht ontvangen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1829"/>
        <source>%1 is writing...</source>
        <translation>%1 is aan het typen...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1842"/>
        <source>Do you want to disconnect from %1 network?</source>
        <translation>Wilt u de verbinding met het %1 netwerk verbreken?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1875"/>
        <source>You are %1%2</source>
        <translation>U bent %1%2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1886"/>
        <source>Please insert the new status description</source>
        <translation>Voeg aub een nieuwe statusbeschrijving toe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>%1 - Select a file</source>
        <translation>%1 - Selecteer een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1971"/>
        <source>There is no user connected.</source>
        <translation>Er is geen enkele gebruiker aangesloten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1977"/>
        <source>Please select the user to whom you would like to send a file.</source>
        <translation>Selecteer de gebruiker aan wie u een bestand wilt sturen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1986"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2993"/>
        <source>User not found.</source>
        <translation>Gebruiker niet gevonden.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2009"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2159"/>
        <source>File transfer is disabled. You cannot download %1.</source>
        <translation>Bestandsoverdracht is uitgeschakeld. U kunt niet downloaden %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <source>Yes, and don&apos;t ask anymore</source>
        <translation>Ja en vraag er mij niet meer naar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2063"/>
        <source>%1 already exists. Please select a new filename.</source>
        <translation>%1 bestaat al. Geef een nieuwe bestandsnaam in.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2102"/>
        <source>You cannot download all these files at once. Do you want to download the first %1 files of the list?</source>
        <translation>Je kan niet alle bestanden in een keer downloaden. Wil je het eerste bestand %1 van de lijst downloaden?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2110"/>
        <source>Downloading %1 files is a hard duty. Maybe you have to wait a lot of minutes. Do yo want to continue?</source>
        <translation>%1 bestanden downloaden wordt moeilijk. Je moet waarschijnlijk vele minuten wachten.Wil je doorgaan?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2128"/>
        <source>%1 files are scheduled for download</source>
        <translation>%1 bestanden staan klaar om te downloaden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2167"/>
        <source>Do you want to download folder %1 (%2 files) from %3?</source>
        <translation>Wil je gedownloade map %1 (%2 bestanden) van %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2208"/>
        <source>%1 - Select the download folder</source>
        <translation>%1 - Selecteer de downloadmap</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2361"/>
        <source>Plugin Manager...</source>
        <translation>Pluginbeheerder...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2362"/>
        <source>Open the plugin manager dialog and manage the installed plugins</source>
        <translation>Open het pluginbeheerder venster en beheer de geïnstalleerde plugins</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1014"/>
        <source>Show the %1 log</source>
        <translation>Toon het %1 logbestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1015"/>
        <source>Show the application log to see if an error occurred</source>
        <translation>Toon het logbestand van de toepassing om eventuele foutmeldingen te zien</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1016"/>
        <source>Make a screenshot</source>
        <translation>Maak een schermafdruk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1017"/>
        <source>Show the utility to capture a screenshot</source>
        <translation>Toon de schermafdruktoepassing</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2364"/>
        <source>is a plugin developed by</source>
        <translation>is een plugin ontwikkeld door</translation>
    </message>
    <message>
        <source>%1 will keep running in the background mode</source>
        <translation type="obsolete">%1 zal actief blijven op de achtergrond</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2636"/>
        <source>Do you really want to open the file %1?</source>
        <translation>Wilt u echt het bestand %1 openen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Unable to open %1</source>
        <translation>Kan %1 niet openen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2661"/>
        <source>Sound files (*.wav)</source>
        <translation>Geluidsbestanden (*.wav)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <source>Sound is not enabled on a new message. Do you want to enable it?</source>
        <translation>Geluid bij een nieuw bericht is niet ingeschakeld. Wilt u het geluid inschakelen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2685"/>
        <source>Sound module is not working. The default BEEP will be used.</source>
        <translation>Geluidsmodule werkt niet. De standaard BEEP zal gebruikt worden.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2690"/>
        <source>Sound file not found</source>
        <translation>Geluidsbestand niet gevonden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2691"/>
        <source>The default BEEP will be used</source>
        <translation>De standaard BEEP zal gebruikt worden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2766"/>
        <source>Unable to add users in this chat. Please select a group one.</source>
        <translation>Kan geen gebruikers toevoegen aan deze chat. Selecteer aub een groepschat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2813"/>
        <source>Now %1 will start on windows boot.</source>
        <translation>Nu zal %1 starten bij het opstarten van Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2820"/>
        <source>%1 will not start on windows boot.</source>
        <translation>%1 zal niet starten bij het opstarten van Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2877"/>
        <source>Please select a chat you would like to link the saved text.</source>
        <translation>Select aub een chat die u wilt opslaan in een tekstbestand.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2886"/>
        <source>The chat &apos;%1&apos; selected has already a saved text.&lt;br /&gt;What do you want to do with the selected saved text?</source>
        <translation>De chat %1 werd eerder al opgeslagen in een tekstbestand.&lt;br /&gt;Wat wilt u doen met het bestaande tekstbestand?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="839"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Overwrite</source>
        <translation>Overschrijven</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Add in the head</source>
        <translation>Bovenaan toevoegen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Cancel</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="809"/>
        <source>If enabled the window geometry will be reset to default value at the next startup</source>
        <translation>Indien ingeschakeld venster geometrie wordt teruggezet naar de standaardwaarde bij de volgende startup</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="915"/>
        <source>Prompt on quit (only when connected)</source>
        <translation>Vragen om af te sluiten (alleen online)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="916"/>
        <source>If enabled you will be asked if you really want to close %1</source>
        <translation>Indien ingeschakeld wordt je gevraagd als je echt wilt sluiten %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="925"/>
        <source>Save the users on exit</source>
        <translation>Sla gebruikers op bij afsluiten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="926"/>
        <source>If enabled the user list will be save on exit</source>
        <translation>Indien ingeschakeld de gebruikerslijst wordt opgeslagen tijdens afsluiten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1034"/>
        <source>Qt Library...</source>
        <translation>Qt dll...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1046"/>
        <source>Like %1 on Facebook</source>
        <translation>Like %1 op Facebook</translation>
    </message>
    <message>
        <source>Help me to know how many people use BeeBEEP</source>
        <translation type="obsolete">Wil graag weten hoeveel mensen werkelijk BeeBEEP gebruiken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1054"/>
        <source>Donate for %1</source>
        <translation>Doneer voor %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1059"/>
        <source>Show</source>
        <translation>Toon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1076"/>
        <source>Load on system tray at startup</source>
        <translation>Tijdens opstarten de systeemvak laden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1077"/>
        <source>If enabled %1 will be start hided in system tray</source>
        <translation>Indien ingeschakeld %1 is verborgen in systeemvak</translation>
    </message>
    <message>
        <source>Used Ports</source>
        <translation type="obsolete">Gebruikte poorten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1171"/>
        <source>Show the user panel</source>
        <translation>Toon gebruikers paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1183"/>
        <source>Show the group panel</source>
        <translation>Toon groep paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1195"/>
        <source>Show the chat panel</source>
        <translation>Toon chat paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1207"/>
        <source>Show the history panel</source>
        <translation>Toon geschiedenis paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1219"/>
        <source>Show the file transfer panel</source>
        <translation>Toon bestandsoverdrachts paneel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1268"/>
        <source>Show the bar of chat</source>
        <translation>Toon de chatbalk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2030"/>
        <source>Do you want to download %1 (%2) from %3?</source>
        <translation>Wilt u %1(%2) downloaden van %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2143"/>
        <source>File is not available for download.</source>
        <translation>Bestand is niet beschikbaar voor download.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2145"/>
        <source>%1 is not connected.</source>
        <translation>%1 is niet verbonden.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2146"/>
        <source>Please reload the list of shared files.</source>
        <translation>Herlaad aub de lijst met gedeelde bestanden.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <source>Reload file list</source>
        <translation>Herlaad de bestandenlijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3122"/>
        <source>Chat with %1 is empty.</source>
        <translation>Chat met %1 is leeg.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3126"/>
        <source>Do you really want to clear messages with %1?</source>
        <translation>Wilt u echt alle berichten met %1 wissen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3129"/>
        <source>Yes and delete history</source>
        <translation>Ja en verwijder geschiedenis</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3059"/>
        <source>%1 is a your group. You can not leave the chat.</source>
        <translation>%1 is uw groep. U kunt de chat niet verlaten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Delete this group</source>
        <translation>Verwijder deze groep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3084"/>
        <source>You cannot leave this chat.</source>
        <translation>U kunt deze chat niet verlaten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3096"/>
        <source>Do you really want to delete group &apos;%1&apos;?</source>
        <translation>Wilt u de groep &apos;%1&apos; echt verwijderen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3197"/>
        <source>Unable to delete this chat.</source>
        <translation>Kan deze chat niet verwijderen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3219"/>
        <source>%1 has shared %2 files</source>
        <translation>%1 heeft %2 bestanden gedeeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3241"/>
        <source>Default language is restored.</source>
        <translation>Standaardtaal werd heringesteld.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3243"/>
        <source>New language &apos;%1&apos; is selected.</source>
        <translation>Nieuwe taal &apos;%1&apos; is geselecteerd.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3246"/>
        <source>You must restart %1 to apply these changes.</source>
        <translation>Uw moet %1 herstarten om de wijzigingen door te voeren.</translation>
    </message>
</context>
<context>
    <name>GuiMessageEdit</name>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="322"/>
        <source>Undo</source>
        <translation>Maak ongedaan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="324"/>
        <source>Redo</source>
        <translation>Herhaal</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="327"/>
        <source>Cut</source>
        <translation>Knip</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="328"/>
        <source>Copy</source>
        <translation>Kopiëren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="329"/>
        <source>Paste</source>
        <translation>Plakken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="332"/>
        <source>Select All</source>
        <translation>Alles selecteren</translation>
    </message>
</context>
<context>
    <name>GuiPluginManager</name>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="14"/>
        <source>Dialog</source>
        <translation>Venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="100"/>
        <source>Enable All</source>
        <translation>Alles inschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="110"/>
        <source>Disable All</source>
        <translation>Alles uitschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="133"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="36"/>
        <source>Plugin Manager - %1</source>
        <translation>Pluginbeheerder - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Author</source>
        <translation>Auteur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Disable %1</source>
        <translation>%1 uitschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Enable %1</source>
        <translation>%1 inschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is enabled</source>
        <translation>%1 is ingeschakeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is disabled</source>
        <translation>%1 is uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="129"/>
        <source>Please select a plugin in the list.</source>
        <translation>Selecteer aub een plugin uit de lijst.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="152"/>
        <source>Text Markers</source>
        <translation>Tekstmarkeringen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="171"/>
        <source>Games</source>
        <translation>Spelletjes</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="192"/>
        <source>%1 - Select the plugin folder</source>
        <translation>%1 - Selecteer de pluginmap</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="208"/>
        <source>Folder %1 not found.</source>
        <translation>Map %1 niet gevonden.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChat</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="56"/>
        <source>Saved chat</source>
        <translation>Opgeslagen chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="58"/>
        <source>Empty</source>
        <translation>Leeg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="72"/>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="143"/>
        <source>Find text in chat</source>
        <translation type="unfinished">Vind text in chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="77"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Naar klembord kopiëren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="79"/>
        <source>Select All</source>
        <translation type="unfinished">Alles selecteren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="83"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="86"/>
        <source>Print...</source>
        <translation type="unfinished">Print...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="181"/>
        <source>%1 not found in chat.</source>
        <translation type="unfinished">%1 niet gevonden in chat.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatList</name>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="47"/>
        <source>Show</source>
        <translation>Toon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="50"/>
        <source>Link to chat</source>
        <translation>Link naar chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="52"/>
        <source>Delete</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Do you really want to delete this saved chat?</source>
        <translation>Wilt u deze opgeslagen chat echt verwijderen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
</context>
<context>
    <name>GuiScreenShot</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="35"/>
        <source>Make a Screenshot</source>
        <translation>Maak een schermafdruk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="44"/>
        <source>Delay</source>
        <translation>Uitstel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="45"/>
        <source>Delay screenshot for selected seconds</source>
        <translation>Stel schermafdruk uit voor geselecteerd aantal seconden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="53"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="61"/>
        <source>Hide this window</source>
        <translation>Verberg dit venster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="62"/>
        <source>Hide this window before capture screenshot</source>
        <translation>Verberg dit venster voor het nemen van de schermafdruk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="73"/>
        <source>Enable high dpi</source>
        <translation>Schakel hoge DPI in</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="74"/>
        <source>Enable high dpi support to manage, for example, Apple Retina display</source>
        <translation>Schakel hoge DPI in om bv tegemoet te komen aan Apple&apos;s Retina scherm</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="80"/>
        <source>Capture</source>
        <translation>Neem schermafdruk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="81"/>
        <source>Capture a screenshot of your desktop</source>
        <translation>Neem een schermafdruk van uw bureaublad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="82"/>
        <source>Send</source>
        <translation>Verstuur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="83"/>
        <source>Send the captured screenshot to an user</source>
        <translation>Verstuurt de schermafdruk naar een gebruiker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="84"/>
        <source>Save</source>
        <translation>Opslaan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="85"/>
        <source>Save the captured screenshot as file</source>
        <translation>Sla de schermafdruk op al bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="86"/>
        <source>Delete</source>
        <translation>Verwijder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="87"/>
        <source>Delete the captured screenshot</source>
        <translation>Verwijder de geselecteerde schermafdruk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="107"/>
        <source>No screenshot available</source>
        <translation>Geen schermafdruk beschikbaar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="179"/>
        <source>/beesshot-%1.</source>
        <translation>/beesshot-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="182"/>
        <source>Save As</source>
        <translation>Opslaan als</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="184"/>
        <source>%1 Files (*.%2)</source>
        <translation>%1 Bestanden (*.%2)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="199"/>
        <source>/beesshottmp-%1.</source>
        <translation>/beesshottmp-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="206"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Kan tijdelijk bestand %1 niet opslaan</translation>
    </message>
</context>
<context>
    <name>GuiScreenShotWidget</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="84"/>
        <source>No Screenshot Available</source>
        <translation>Geen schermafdruk beschikbaar</translation>
    </message>
</context>
<context>
    <name>GuiSearchUser</name>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="77"/>
        <source>Local subnet address *</source>
        <translation>Lokaal subnet adres *</translation>
    </message>
    <message>
        <source>Enter the IP addresses or subnet of your local area network separed by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation type="obsolete">Geef de ip adressen of het subnet van uw lokaal netwerk in gescheiden door comma&apos;s (vb: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="204"/>
        <source>Addresses in beehosts.ini *</source>
        <translation>Adressen in beehosts.ini *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="35"/>
        <source>UDP Port in beebeep.rc *</source>
        <translation>UDP Poort in beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="64"/>
        <source>(the same for all clients)</source>
        <translation>(zelfde voor alle cliënten)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="106"/>
        <source>(search users here by default)</source>
        <translation>(zoek alle gebruikers standaard hier)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="119"/>
        <source>Multicast group in beebeep.rc *</source>
        <translation>Multicast groep in beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="155"/>
        <source>Enable broadcast interval</source>
        <translation>Zet uitgezonden interval aan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="184"/>
        <source>seconds (0=disabled, 10=default)</source>
        <translation>seconden (0=uit, 10=standaard)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="351"/>
        <source>Workgroups (enter your network groups separated by comma)</source>
        <translation>Werkgroepen (voert u netwerkgroepen in gescheiden door een komma)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="239"/>
        <source>Accept connections only from your workgroups</source>
        <translation>Accepteer verbindingen alleen van je werkgroepen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="258"/>
        <source>Enter the IP addresses or subnet of your local area network separated by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation>Geef de ip adressen of het subnet van uw lokaal netwerk in gescheiden door comma&apos;s (vb: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="284"/>
        <source>Split subnet to IPV4 addresses</source>
        <translation>Deel subnet op in IPV4 adressen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="297"/>
        <source>Verbose</source>
        <translation>Uitgebreidt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="328"/>
        <source>Max users to contact in a tick</source>
        <translation>Maximale gebruikers te contacten in een tick</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="337"/>
        <source>Automatically add external subnet</source>
        <translation>Voeg automatisch een externe subnet toe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="344"/>
        <source>Enable Zero Configuration Networking</source>
        <translation>Nul configuratie netwerken inschakelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="367"/>
        <source>* (read only section)</source>
        <translation>* (alleen lezen sectie)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="374"/>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="381"/>
        <source>Cancel</source>
        <translation>Annuleer</translation>
    </message>
    <message>
        <source>Configure network</source>
        <translation type="obsolete">Netwerk configureren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="36"/>
        <source>Search for users</source>
        <translation>Zoeken op gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="54"/>
        <source>Unknown address</source>
        <translation>Onbekend adres</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="62"/>
        <source>File is empty</source>
        <translation>Bestand is leeg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="116"/>
        <source>Warning</source>
        <translation>Opgepast</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>You have inserted an invalid host address:
%1 is removed from the list.</source>
        <translation>U hebt een ongeldig host adres ingegeven:
%1 is uit de lijst verwijderd.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocal</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Path</source>
        <translation>Pad</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation type="obsolete">Bestandsnaam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="37"/>
        <source>Share your folders or files</source>
        <translation>Deel je mappen en bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="77"/>
        <source>Share a file</source>
        <translation>Deel een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="78"/>
        <source>Add a file to your local share</source>
        <translation>Voeg een bestand toe aan uw lokale share</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="80"/>
        <source>Share a folder</source>
        <translation>Deel een map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="81"/>
        <source>Add a folder to your local share</source>
        <translation>Voeg een map toe aan je lokale share</translation>
    </message>
    <message>
        <source>Load shared files</source>
        <translation type="obsolete">Laad gedeelde bestanden</translation>
    </message>
    <message>
        <source>Remove shared files from the selected path</source>
        <translation type="obsolete">Verwijder gedeelde bestanden uit de geselecteerde map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="83"/>
        <source>Update shares</source>
        <translation>Update gedeelde bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="84"/>
        <source>Update shared folders and files</source>
        <translation>Update gedeelde mappen en bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="86"/>
        <source>Remove shared path</source>
        <translation>Verwijder gedeelde map</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="87"/>
        <source>Remove shared path from the list</source>
        <translation>Verwijder gedeelde map uit de lijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="89"/>
        <source>Clear all shares</source>
        <translation>Verwijder alle gedeelde bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="90"/>
        <source>Clear all shared paths from the list</source>
        <translation>Verwijder alle gedeelde paden van de lijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="99"/>
        <source>Shared files</source>
        <translation>Gedeelde bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="101"/>
        <source>File transfer is disabled</source>
        <translation>Bestandsoverdracht is uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="119"/>
        <source>Select a file to share</source>
        <translation>Selecteer een bestand om te delen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="132"/>
        <source>Select a folder to share</source>
        <translation>Selecteer een map om te delen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="150"/>
        <source>Please select a shared path.</source>
        <translation>Selecteer een gedeelde map.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="167"/>
        <source>Do you really want to remove all shared paths?</source>
        <translation>Wil je echt alle gedeelde paden verwijderen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>No</source>
        <translation>Nee</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="214"/>
        <source>Click to open %1</source>
        <translation>Klik om %1 te openen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="334"/>
        <source>%1 shared files</source>
        <translation>%1 gedeelde bestand</translation>
    </message>
    <message>
        <source>Double click to open %1</source>
        <translation type="obsolete">Dubbelklik op te openen %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="229"/>
        <source>%1 is already shared.</source>
        <translation>%1 is al geopend.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="270"/>
        <source>File transfer is disabled. Open the option menu to enable it.</source>
        <translation>Bestandsoverdracht is uitgeschakeld. Open het optie menu om het in te schakelen.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocalWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="87"/>
        <source>2</source>
        <translation>2</translation>
    </message>
</context>
<context>
    <name>GuiShareNetwork</name>
    <message>
        <source>File</source>
        <translation type="obsolete">Bestand</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Grootte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="98"/>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="39"/>
        <source>Folder and Files shared in your network</source>
        <translation>Mappen en bestanden delen in je netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="52"/>
        <source>Scan network</source>
        <translation>Scan netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="53"/>
        <source>Search shared files in your network</source>
        <translation>Zoek gedeelde bestanden in uw netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="56"/>
        <source>Reload list</source>
        <translation>Herlaad lijst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="57"/>
        <source>Clear and reload list</source>
        <translation>Leegmaken en lijst herladen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="68"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="74"/>
        <source>Search</source>
        <translation>Zoek</translation>
    </message>
    <message>
        <source>Apply Filter</source>
        <translation type="obsolete">Filter toepassen</translation>
    </message>
    <message>
        <source>Filter the files in list using some keywords</source>
        <translation type="obsolete">Filter de bestanden in de lijst op basis van bepaalde kernwoorden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="83"/>
        <source>File Type</source>
        <translation>Bestandstype</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="89"/>
        <source>All Files</source>
        <translation>Alle bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="61"/>
        <source>Download</source>
        <translation>Downloaden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="62"/>
        <source>Download single or multiple files simultaneously</source>
        <translation>Download een of meerdere bestanden tegelijk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="116"/>
        <source>All Users</source>
        <translation>Alle gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="141"/>
        <source>%1 is searching shared files in your network</source>
        <translation>%1 is gedeelde bestanden aan het zoeken in uw netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="195"/>
        <source>Double click to download %1</source>
        <translation>Dubbelklik om te downloaden %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="234"/>
        <source>%1 has shared %2 files (%3)</source>
        <translation>%1 heeft %2 bestanden gedeeld (%3)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="315"/>
        <source>Double click to open %1</source>
        <translation>Dubbelklik om te openen %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="317"/>
        <source>Transfer completed</source>
        <translation>Overdracht voltooid</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="331"/>
        <source>%1 files are shown in list (%2 are available in your network)</source>
        <translation>%1 bestanden worden getoond in lijst (%2 bestanden beschikbaar in je netwerk)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="333"/>
        <source>%1 files shared in your network</source>
        <translation>%1 bestanden gedeeld in je netwerk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download single file</source>
        <translation>Download een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download %1 selected files</source>
        <translation>Dowload %1 geselecteerde bestanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="377"/>
        <source>MAX</source>
        <translation>Maximaal</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="380"/>
        <source>Clear selection</source>
        <translation>Selectie wissen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="384"/>
        <source>Expand all items</source>
        <translation>Alles uitvouwen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="385"/>
        <source>Collapse all items</source>
        <translation>Alles invouwen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="396"/>
        <source>Please select one or more files to download.</source>
        <translation>Selecteer aub een of meerdere bestanden om te downloaden.</translation>
    </message>
</context>
<context>
    <name>GuiShareNetworkWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
</context>
<context>
    <name>GuiShortcut</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="34"/>
        <source>Shortcuts</source>
        <translation>Snelkoppelingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Key</source>
        <translation>Toets</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Action</source>
        <translation>Actie</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="114"/>
        <source>Insert shorcut for the action: %1</source>
        <translation>Voeg snelkoppeling toe voor de actie: %1</translation>
    </message>
</context>
<context>
    <name>GuiShortcutDialog</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <source>Restore to default language</source>
        <translation type="obsolete">De standaardtaal(EN) herstellen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="23"/>
        <source>Use custom shortcuts</source>
        <translation>Gebruik eigen snelkoppelingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="50"/>
        <source>Restore default shortcuts</source>
        <translation>Hestel standaard snelkoppelingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="53"/>
        <source>Restore</source>
        <translation>Herstel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="76"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="89"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>GuiSystemTray</name>
    <message>
        <source>1 new message</source>
        <translation type="obsolete">1 nieuwe melding</translation>
    </message>
    <message>
        <source>%1 new messages</source>
        <translation type="obsolete">%1 nieuwe meldingen</translation>
    </message>
</context>
<context>
    <name>GuiTetris</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulier</translation>
    </message>
    <message>
        <source>BeeTetris</source>
        <translation type="obsolete">BeeTetris</translation>
    </message>
    <message>
        <source>Next Piece</source>
        <translation type="obsolete">Volgend stuk</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Score</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation type="obsolete">Lijnen</translation>
    </message>
    <message>
        <source>Level</source>
        <translation type="obsolete">Level</translation>
    </message>
    <message>
        <source>Record</source>
        <translation type="obsolete">Record</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Start</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Pause</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Doorgaan</translation>
    </message>
</context>
<context>
    <name>GuiTransferFile</name>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>User</source>
        <translation>Gebruiker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="116"/>
        <source>Completed</source>
        <translation>Voltooid</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="122"/>
        <source>Cancel Transfer</source>
        <translation>Overdracht annuleren</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="128"/>
        <source>Not Completed</source>
        <translation>Niet voltooid</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="161"/>
        <source>Transfer completed</source>
        <translation>Overdracht voltooid</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Downloading</source>
        <translation>Aan het downloaden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Uploading</source>
        <translation>Aan het uploaden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="218"/>
        <source>Do you really want to cancel the transfer of %1?</source>
        <translation>Wilt u de overdracht van bestand %1 echt annuleren?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="251"/>
        <source>Remove all transfers</source>
        <translation>Verwijder alle bestandsoverdrachten</translation>
    </message>
</context>
<context>
    <name>GuiUserList</name>
    <message>
        <location filename="../src/desktop/GuiUserList.cpp" line="55"/>
        <source>Search</source>
        <translation>Zoek</translation>
    </message>
</context>
<context>
    <name>GuiUserListWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Formulier</translation>
    </message>
</context>
<context>
    <name>GuiVCard</name>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="59"/>
        <source>Birthday: %1</source>
        <translation>Verjaardag: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="85"/>
        <source>unknown</source>
        <translation>onbekend</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="87"/>
        <source>old %1</source>
        <translation>oud %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="89"/>
        <source>new %1</source>
        <translation>nieuw %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="105"/>
        <source>Remove from favorites</source>
        <translation>Verwijderd van favorieten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="110"/>
        <source>Add to favorites</source>
        <translation>Toegevoegd aan favorieten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="115"/>
        <source>Chat with all</source>
        <translation>Met iedereen chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="121"/>
        <source>Open chat</source>
        <translation>Chat openen</translation>
    </message>
</context>
<context>
    <name>GuiVCardDialog</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="22"/>
        <source>User ID</source>
        <translation>GebruikersID</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="150"/>
        <source>Nickname</source>
        <translation>Nickname</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="207"/>
        <source>First name</source>
        <translation>Voornaam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="224"/>
        <source>Last name</source>
        <translation>Familienaam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="241"/>
        <source>Birthday</source>
        <translation>Verjaardag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="274"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="291"/>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="356"/>
        <source>Add or change photo</source>
        <translation>Foto toewijzen of wijzigen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="385"/>
        <source>Remove photo</source>
        <translation>Foto verwijderen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="417"/>
        <source>Change your nickname color</source>
        <translation>Wijzig nickname kleur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="456"/>
        <source>Other informations</source>
        <translation>Andere gegevens</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="499"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="509"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
</context>
<context>
    <name>GuiVCardWidget</name>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="81"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="291"/>
        <source>User Path</source>
        <translation>Gebruikerspad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="307"/>
        <source>User Name</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="323"/>
        <source>Birthday</source>
        <translation>Verjaardag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="339"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="355"/>
        <source>Phone</source>
        <translation>Telefoon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="417"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="452"/>
        <source>Open chat</source>
        <translation>Chat openen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="490"/>
        <source>Send a file</source>
        <translation>Stuur een bestand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="525"/>
        <source>Change the nickname color</source>
        <translation>Wijzig nickname kleur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="583"/>
        <source>Remove user</source>
        <translation>Verwijder gebruiker</translation>
    </message>
</context>
<context>
    <name>GuiWizard</name>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="36"/>
        <source>Welcome to &lt;b&gt;%1 Network&lt;/b&gt;.</source>
        <translation>Welkom op het &lt;b&gt;%1 netwerk&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="42"/>
        <source>Your system account is</source>
        <translation>Uw systeem account is</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="59"/>
        <source>Your nickname can not be empty.</source>
        <translation>Uw nickname mag niet leeg zijn.</translation>
    </message>
</context>
<context>
    <name>GuiWizardDialog</name>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialoog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="56"/>
        <source>Welcome</source>
        <translation>Welkom</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>LifeGame</name>
    <message>
        <source>BeeLife</source>
        <translation type="obsolete">BeeLife</translation>
    </message>
    <message>
        <source>&lt;b&gt;The Game of Life&lt;/b&gt;, also known simply as Life, is a cellular automaton devised by the British mathematician &lt;b&gt;John Horton Conway&lt;/b&gt; in 1970. The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead. Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:&lt;ul&gt;&lt;li&gt;Any live cell with less than two live neighbours dies, as if caused by under-population.&lt;/li&gt;&lt;li&gt;Any live cell with two or three live neighbours lives on to the next generation.&lt;/li&gt;&lt;li&gt;Any live cell with more than three live neighbours dies, as if by overcrowding.&lt;/li&gt;&lt;li&gt;Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.&lt;/li&gt;&lt;/ul&gt;The rules continue to be applied repeatedly to create further generations.&lt;br /&gt;For more info please visit &lt;a href=http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life&gt;Conway&apos;s Game of Life&lt;/a&gt;.</source>
        <translatorcomment>Sorry not going to translate this whole text word for word. Copied explanation from dutch wikipedia website.</translatorcomment>
        <translation type="obsolete">&lt;b&gt;Game of Life&lt;/b&gt;, soms kortweg Life genoemd, is een in 1970 door de Britse wiskundige &lt;b&gt;John Conway&lt;/b&gt; bedachte cellulaire automaat, een tweedimensionaal raster met vierkante &apos;cellen&apos; die &apos;levend&apos; of &apos;dood&apos; kunnen zijn, en die zich volgens vastgestelde regels ontwikkelen en daarbij allerlei patronen kunnen vormen.

Het &quot;spel&quot; werd bekend na publicatie in de rubriek Mathematical Games van Martin Gardner in Scientific American. Conway heeft veel geëxperimenteerd met verschillende sets met regels om een goede balans te vinden. Met de verkeerde regels zouden namelijk meer cellen &apos;geboren&apos; worden dan &apos;dood&apos; gaan waardoor het hele raster &apos;levend&apos; wordt, of er gaan juist zoveel cellen dood dat het hele gebied &apos;dood&apos; wordt.
Voor meer info, ga naar: &lt;a href=http://nl.wikipedia.org/wiki/Game_of_Life&gt;http://nl.wikipedia.org/wiki/Game_of_Life&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>NumberTextMarker</name>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="33"/>
        <source>Number Text Marker</source>
        <translation>Nummer tekst markeerder</translation>
    </message>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="48"/>
        <source>If you want to encode your message with numbers write a #text to encode# .</source>
        <translation>Als u uw bericht wilt versleutelen met nummers, zet u uw tekst tussen hekjes #versleutelde tekst# .</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="63"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3119"/>
        <location filename="../src/desktop/GuiUserItem.cpp" line="84"/>
        <source>All Lan Users</source>
        <translation>Alle netwerkgebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="64"/>
        <source>Open chat with all local users</source>
        <translation>Chat openen met alle lokale gebruikers</translation>
    </message>
    <message>
        <source>Nobody</source>
        <translation type="obsolete">Niemand</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="84"/>
        <source>Open chat with %1</source>
        <translation>Open chat met %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatMessage.cpp" line="61"/>
        <source>You</source>
        <translation>U</translation>
    </message>
    <message>
        <source>Double click to open chat with all local users</source>
        <translation type="obsolete">Dubbelklik om chat te openen met alle lokale gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="148"/>
        <source>Click to open chat with all local users</source>
        <translation>Klik om chat te openen met alle lokale gebruikers</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="152"/>
        <source>%1 is %2</source>
        <translation>%1 is %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="167"/>
        <source>Click to send a private message</source>
        <translation>Klik om privé-bericht te verzenden</translation>
    </message>
    <message>
        <source>Double click to send a private message</source>
        <translation type="obsolete">Dubbelklik om privé-bericht te verzenden</translation>
    </message>
    <message>
        <source>Saved in</source>
        <translation type="obsolete">Opgeslagen in</translation>
    </message>
    <message>
        <source>Started in</source>
        <translation type="obsolete">Gestart in</translation>
    </message>
    <message>
        <source>Double click to send message to group: %1</source>
        <translation type="obsolete">Dubbelklik om bericht naar groep te zenden: %1</translation>
    </message>
    <message>
        <source>Double click to view chat history: %1</source>
        <translation type="obsolete">Dubbelklik om chatgeschiedenis te tonen: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupItem.cpp" line="67"/>
        <source>Click to send message to group: %1</source>
        <translation>Klik om bericht naar groep te zenden: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="129"/>
        <source>Click to view chat history: %1</source>
        <translation>Klik om chatgeschiedenis te tonen: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="100"/>
        <source>Empty</source>
        <translation type="unfinished">Leeg</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="102"/>
        <source>Send file</source>
        <translation type="unfinished">Stuur bestand</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="104"/>
        <source>Show file transfers</source>
        <translation type="unfinished">Toon bestandsoverdracht</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="106"/>
        <source>Set focus in message box</source>
        <translation type="unfinished">Stel focus op berichten box in</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="108"/>
        <source>Minimize all chats</source>
        <translation type="unfinished">Maak alle chats klein</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="110"/>
        <source>Show the next unread message</source>
        <translation type="unfinished">Toon volgend ongelezen bericht</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="112"/>
        <source>Send chat message</source>
        <translation type="unfinished">Stuur een chatbericht</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="114"/>
        <source>Print</source>
        <translation type="unfinished">Print</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="116"/>
        <source>Broadcast</source>
        <translation type="unfinished">Uitgezonden</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="118"/>
        <source>Find text in chat</source>
        <translation type="unfinished">Vind text in chat</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="120"/>
        <source>Find next text in chat</source>
        <translation type="unfinished">Vind volgende text in chat</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="122"/>
        <source>Send folder</source>
        <translation type="unfinished">Stuur map</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="124"/>
        <source>Show emoticons panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="126"/>
        <source>Show all chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show emoticons</source>
        <translation type="obsolete">Toon emoticons</translation>
    </message>
</context>
<context>
    <name>RainbowTextMarker</name>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="37"/>
        <source>Rainbow Text Marker</source>
        <translation>Regenboog tekst markeerder</translation>
    </message>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="52"/>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~ .</source>
        <translation>Als u &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;e&lt;/font&gt;&lt;font color=#FFff00&gt;g&lt;/font&gt;&lt;font color=#7Fff00&gt;e&lt;/font&gt;&lt;font color=#00ff00&gt;n&lt;/font&gt;&lt;font color=#00ff80&gt;b&lt;/font&gt;&lt;font color=#00ffff&gt;o&lt;/font&gt;&lt;font color=#0080ff&gt;o&lt;/font&gt;&lt;font color=#0000ff&gt;g&lt;/font&gt;&lt;font color=#7F00ff&gt;t&lt;/font&gt;&lt;font color=#FF00ff&gt;e&lt;/font&gt;&lt;font color=#FF0080&gt;k&lt;/font&gt;&lt;font color=#FF0000&gt;s&lt;/font&gt;&lt;font color=#FF8000&gt;t&lt;/font&gt; wilt, schrijf de tekst dan tussen liggende krullen ~regenboogtekst~ .</translation>
    </message>
</context>
<context>
    <name>RegularBoldTextMarker</name>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="32"/>
        <source>Regular Bold Text Marker</source>
        <translation>Normale vette tekst markeerder</translation>
    </message>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="47"/>
        <source>If you want to format your message with words in regular and bold font write a [text to format] .</source>
        <translation>Als u in uw bericht woorden normaal en vet wilt schrijven, schrijf ze dan tussen vierkante haken [op te maken tekst] .</translation>
    </message>
</context>
<context>
    <name>TetrisBoard</name>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Pause</translation>
    </message>
    <message>
        <source>Game Over</source>
        <translation type="obsolete">Spel afgelopen</translation>
    </message>
</context>
<context>
    <name>TetrisGame</name>
    <message>
        <source>BeeTetris</source>
        <translation type="obsolete">BeeTetris</translation>
    </message>
    <message>
        <source>Use &lt;b&gt;Left&lt;/b&gt; and &lt;b&gt;Right Arrow&lt;/b&gt; to move the pieces on the board. &lt;b&gt;Up&lt;/b&gt; and &lt;b&gt;Down Arrow&lt;/b&gt; to rotate left and right. &lt;b&gt;Space&lt;/b&gt; to drop down the piece. &lt;b&gt;D&lt;/b&gt; to drop the piece only one line. &lt;b&gt;P&lt;/b&gt; to pause the game.</source>
        <translation type="obsolete">Gebruik &lt;b&gt;Links&lt;/b&gt; en &lt;b&gt;Rechts&lt;/b&gt; om de stukken op het bord te bewegen. &lt;b&gt;Op&lt;/b&gt; en &lt;b&gt;Neer&lt;/b&gt; om lins en rechts om te draaien. &lt;b&gt;Spatie&lt;/b&gt; om het stukje te laten vallen. &lt;b&gt;D&lt;/b&gt; om het stukje 1 lijntje te laten zakken. &lt;b&gt;P&lt;/b&gt; om het spel te pauzeren.</translation>
    </message>
</context>
<context>
    <name>Tips</name>
    <message>
        <location filename="../src/Tips.h" line="31"/>
        <source>You can switch between chats with CTRL+TAB if there are new messages availables.</source>
        <translation type="unfinished">U kunt tussen chats overschakelen met CTRL+TAB als er nieuwe berichten zijn.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="32"/>
        <source>If you want a &lt;b&gt;bold text&lt;/b&gt; write a *bold text*.</source>
        <translation type="unfinished">Als u &lt;b&gt;vette tekst&lt;/b&gt; wilt, typt u *vette tekst*.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="33"/>
        <source>If you want an &lt;i&gt;italic text&lt;/i&gt; write a /italic text/.</source>
        <translation type="unfinished">Als u &lt;i&gt;cursieve tekst&lt;/i&gt; wilt, typ dan /cursieve tekst/.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="34"/>
        <source>If you want an &lt;u&gt;underlined text&lt;/u&gt; write a _underlined text_.</source>
        <translation type="unfinished">Als u &lt;u&gt;onderlijnde tekst&lt;/u&gt; wilt, typ dan _onderlijnde tekst_.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="35"/>
        <source>You can search previous sent message in the history using the CTRL+Up e CTRL+Down keys.</source>
        <translation type="unfinished">U kunt zoeken in verstuurde berichten door gebruik te maken van de CTRL + omhoog en CTRL+omlaag-knoppen.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="37"/>
        <source>You can drop files to active chat and send them to members.</source>
        <translation type="unfinished">U kunt bestand in de vensters van chatgesprekken laten vallen om ze te versturen naar de deelnemers in het chatgesprek.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="38"/>
        <source>You can select multiple files from network shares and download them simultaneously with a right click.</source>
        <translation type="unfinished">Je kan verschillende bestanden van netwerk delen en tegelijk downloaden met een rechtermuisknop klik.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="39"/>
        <source>You can disable message notification from a group right clicking on its name on the list.</source>
        <translation type="unfinished">Je kan bericht melding uitschakelen voor een groep door rechts te klikken op een naam in de lijst.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="47"/>
        <source>&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</source>
        <translatorcomment>sorry not translating quotes...</translatorcomment>
        <translation type="unfinished">&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="48"/>
        <source>&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</source>
        <translation type="unfinished">&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="49"/>
        <source>&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</source>
        <translation type="unfinished">&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="50"/>
        <source>&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</source>
        <translation type="unfinished">&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="51"/>
        <source>&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</source>
        <translation type="unfinished">&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="53"/>
        <source>&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</source>
        <translation type="unfinished">&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="54"/>
        <source>&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</source>
        <translation type="unfinished">&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="55"/>
        <source>&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</source>
        <translation type="unfinished">&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="56"/>
        <source>&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</source>
        <translation type="unfinished">&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="57"/>
        <source>&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</source>
        <translation type="unfinished">&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="59"/>
        <source>&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</source>
        <translation type="unfinished">&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="60"/>
        <source>&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</source>
        <translation type="unfinished">&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="61"/>
        <source>&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</source>
        <translation type="unfinished">&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="62"/>
        <source>&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</source>
        <translation type="unfinished">&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="63"/>
        <source>&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</source>
        <translation type="unfinished">&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="65"/>
        <source>&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation type="unfinished">&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="66"/>
        <source>&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</source>
        <translation type="unfinished">&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="67"/>
        <source>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</source>
        <translation type="unfinished">&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="68"/>
        <source>&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</source>
        <translation type="unfinished">&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="73"/>
        <source>&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</source>
        <translation type="unfinished">&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="75"/>
        <source>&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</source>
        <translation type="unfinished">&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="76"/>
        <source>&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</source>
        <translation type="unfinished">&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="77"/>
        <source>&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</source>
        <translation type="unfinished">&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="78"/>
        <source>&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</source>
        <translation type="unfinished">&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="79"/>
        <source>&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</source>
        <translation type="unfinished">&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="81"/>
        <source>&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</source>
        <translation type="unfinished">&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="82"/>
        <source>&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</source>
        <translation type="unfinished">&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="83"/>
        <source>&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</source>
        <translation type="unfinished">&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="84"/>
        <source>&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</source>
        <translation type="unfinished">&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="85"/>
        <source>&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</source>
        <translation type="unfinished">&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="87"/>
        <source>&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</source>
        <translation type="unfinished">&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="88"/>
        <source>&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</source>
        <translation type="unfinished">&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="89"/>
        <source>&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</source>
        <translation type="unfinished">&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="90"/>
        <source>&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</source>
        <translation type="unfinished">&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="91"/>
        <source>&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</source>
        <translation type="unfinished">&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="93"/>
        <source>&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</source>
        <translation type="unfinished">&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="62"/>
        <source>offline</source>
        <translation>offline</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="63"/>
        <source>available</source>
        <translation>beschikbaar</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="64"/>
        <source>busy</source>
        <translation>bezig</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="65"/>
        <source>away</source>
        <translation>weg</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="66"/>
        <source>status error</source>
        <translation>foutmelding status</translation>
    </message>
</context>
</TS>
