<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>ChatMessage</name>
    <message>
        <source>Undefined</source>
        <translation type="obsolete">Indefinito</translation>
    </message>
    <message>
        <source>System</source>
        <translation>Sistema</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>Connection</source>
        <translation>Connessione</translation>
    </message>
    <message>
        <source>User Status</source>
        <translation>Stato Utente</translation>
    </message>
    <message>
        <source>User Information</source>
        <translation>Informazioni Utente</translation>
    </message>
    <message>
        <source>File Transfer</source>
        <translation>Trasferimento file</translation>
    </message>
    <message>
        <source>History</source>
        <translation>Storia</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Altri</translation>
    </message>
    <message>
        <source>Header</source>
        <translation>Intestazione</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <source>%1 Unable to connect to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Impossibile connttersi nella rete %2. Controlla le impostazioni del tuo firewall.</translation>
    </message>
    <message>
        <source>%1 Unable to broadcast to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Impossibile inviare messaggi nella rete %2. Controlla le impostazioni del tuo firewall.</translation>
    </message>
    <message>
        <source>%1 You are connected to %2 Network.</source>
        <translation>%1 Connessione alla rete %2 riuscita.</translation>
    </message>
    <message>
        <source>%1 You are disconnected from %2 Network.</source>
        <translation>%1 Disconnessione dalla rete %2.</translation>
    </message>
    <message>
        <source>%1 You are disconnected.</source>
        <translation type="obsolete">%1 Disconnesso.</translation>
    </message>
    <message>
        <source>%1 Chat with all users.</source>
        <translation type="obsolete">%1 Parla con tutti.</translation>
    </message>
    <message>
        <source>%1 Chat with %2.</source>
        <translation>%1 Parla con %2.</translation>
    </message>
    <message>
        <source>Unable to send the message: you are not connected.</source>
        <translation>Impossibile inviare il messaggio: non sei connesso.</translation>
    </message>
    <message>
        <source>Unable to send the message to %1.</source>
        <translation>Impossibile inviare il messaggio a %1.</translation>
    </message>
    <message>
        <source>Nobody has received the message.</source>
        <translation>Nessuno ha ricevuto il tuo messaggio.</translation>
    </message>
    <message>
        <source>All users</source>
        <translation type="obsolete">Tutti</translation>
    </message>
    <message>
        <source>Nobody</source>
        <translation type="obsolete">Nessuno</translation>
    </message>
    <message>
        <source>%1 Downloading %2 from %3.</source>
        <translation>%1 Inizi a scaricare %2 da %3.</translation>
    </message>
    <message>
        <source>from</source>
        <translation>da</translation>
    </message>
    <message>
        <source>to</source>
        <translation>a</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <source>%1 %2: file not found.</source>
        <translation>%1 %2: file non trovato.</translation>
    </message>
    <message>
        <source>%1 Unable to send file: user is not connected.</source>
        <translation type="obsolete">%1 Impossibile inviare il file: l&apos;utente non è connesso.</translation>
    </message>
    <message>
        <source>%1 Unable to send file: bind address/port failed.</source>
        <translation type="obsolete">%1 Impossibile inviare il file: controlla le impostazioni del tuo firewall.</translation>
    </message>
    <message>
        <source>%1 You send the file to %2: %3.</source>
        <translation type="obsolete">%1 Stai inviando il file a %2: %3.</translation>
    </message>
    <message>
        <source>%1 You have refused to download %2 from %3.</source>
        <translation>%1 Hai rifiutato di scaricare %2 da %3.</translation>
    </message>
    <message>
        <source>%1 %2 has refused to download %3.</source>
        <translation>%1 %2 ha rifiutato di scaricare %3.</translation>
    </message>
    <message>
        <source>%1 %2 is sending to you the file: %3.</source>
        <translation>%1 %2 ti sta inviando il seguente file: %3.</translation>
    </message>
    <message>
        <source>You are</source>
        <translation>Il tuo stato è</translation>
    </message>
    <message>
        <source>is</source>
        <translation type="obsolete">è nello stato</translation>
    </message>
    <message>
        <source>%1 is</source>
        <translation>Lo stato di %1 è</translation>
    </message>
    <message>
        <source>You have changed your nickname from %1 to %2.</source>
        <translation>Hai cambiato il tuo soprannome da %1 a %2.</translation>
    </message>
    <message>
        <source>%1 has changed the nickname in %2.</source>
        <translation>%1 ha cambiato il suo soprannome in %2.</translation>
    </message>
    <message>
        <source>You have changed your profile.</source>
        <translation type="obsolete">Hai cambiato il tuo profilo.</translation>
    </message>
    <message>
        <source>%1 has changed the profile.</source>
        <translation type="obsolete">%1 ha cambiato il suo profilo.</translation>
    </message>
    <message>
        <source>%1 Looking for the available users in the network address %2...</source>
        <translation type="obsolete">%1 Hai iniziato a cercare nuovi utenti all&apos;indirizzo di rete %2...</translation>
    </message>
    <message>
        <source>You are already connected to the network account.</source>
        <translation type="obsolete">La connessione al servizio di rete è già attiva.</translation>
    </message>
    <message>
        <source>Unable to connect to the network account. Username is empty.</source>
        <translation type="obsolete">Impossibile connettersi al servizio di rete. Il nome utente non è stato specificato.</translation>
    </message>
    <message>
        <source>Unable to connect to the network account. Password is empty.</source>
        <translation type="obsolete">Impossibile connettersi al servizio di rete. La password non è valida.</translation>
    </message>
    <message>
        <source>Connecting to %1...</source>
        <translation type="obsolete">Connessione a %1...</translation>
    </message>
    <message>
        <source>%1 has updated the profile.</source>
        <translation type="obsolete">%1 ha aggiornato il suo profilo.</translation>
    </message>
    <message>
        <source>%1 Unable to send the file: user is not connected.</source>
        <translation type="obsolete">%1 Impossibile inviare il file: l&apos;utente non è connesso.</translation>
    </message>
    <message>
        <source>%1 Unable to send the file: bind address/port failed.</source>
        <translation type="obsolete">%1 Impossibile inviare il file: non è stato possibile creare una porta di comunicazione.</translation>
    </message>
    <message>
        <source>%1 Unable to send the file %2 to %3.</source>
        <translation type="obsolete">%1 Impossibile inviare il file %2 a %3.</translation>
    </message>
    <message>
        <source>%1 You send the file %2 to %3.</source>
        <translation type="obsolete">%1 Stai inviando il file %2 a %3.</translation>
    </message>
    <message>
        <source>%1 is connected to %2 network.</source>
        <translation type="obsolete">%1 entra nella tua rete %2.</translation>
    </message>
    <message>
        <source>The %1&apos;s profile has been received.</source>
        <translation>E&apos; stato ricevuto il profilo di %1.</translation>
    </message>
    <message>
        <source>Happy Birthday to you!</source>
        <translation>Buon Compleanno!</translation>
    </message>
    <message>
        <source>Happy Birthday to %1!</source>
        <translation>Buon Compleanno a %1!</translation>
    </message>
    <message>
        <source>%1 Chat with all local users.</source>
        <translation>%1 Parla con tutti gli utenti locali.</translation>
    </message>
    <message>
        <source>%1 Looking for the available users in %2...</source>
        <translation type="obsolete">%1 Ricerca utenti disponibili in %2 in corso...</translation>
    </message>
    <message>
        <source>%1 Broadcasting to the %2 Network...</source>
        <translation>%1 Ricerca in corso di altri %2 connessi alla rete...</translation>
    </message>
    <message>
        <source>%1 You are not connected to %2 Network.</source>
        <translation>%1 %2 non e&apos; conensso alla rete.</translation>
    </message>
    <message>
        <source>%1 Unable to start file transfer server: bind address/port failed.</source>
        <translation>%1 Impossibile avviare il server per il trasferimento dei file: la porta tcp è già in uso.</translation>
    </message>
    <message>
        <source>%1 %2 has updated the list of the file shared.</source>
        <translation type="obsolete">%1 %2 ha aggiornato la sua lista di file condivisi.</translation>
    </message>
    <message>
        <source>%1 %2 has shared some files.</source>
        <translation type="obsolete">%1 %2 ha condiviso alcuni file.</translation>
    </message>
    <message>
        <source>%1 Unable to send the file: user is not found.</source>
        <translation type="obsolete">%1 Impossibile inviare il file: l&apos;utente non è stato trovato.</translation>
    </message>
    <message>
        <source>Unable to send the group chat request message to %1.</source>
        <translation type="obsolete">Impossibile recapitare il messaggio di richiesta creazione gruppo a %1.</translation>
    </message>
    <message>
        <source>%1 You have created the group %2.</source>
        <translation>%1 Hai creato il gruppo %2.</translation>
    </message>
    <message>
        <source>%1 Welcome to the group %2.</source>
        <translation>%1 Benvenuti nel gruppo %2.</translation>
    </message>
    <message>
        <source>%1 The group has a new name: %2.</source>
        <translation>%1 Il gruppo ha un nuovo nome: %2.</translation>
    </message>
    <message>
        <source>%1 Adding in group: %2.</source>
        <translation type="obsolete">%1 Aggiunti al gruppo: %2.</translation>
    </message>
    <message>
        <source>%1 cannot be invited to the group.</source>
        <translation type="obsolete">%1 non può essere invitato nel gruppo.</translation>
    </message>
    <message>
        <source>%1 An error occurred when %2 tries to add you to the group: %3.</source>
        <translation type="obsolete">%1 Si è verificato un errore quando %2 ha provato ad aggiungerti al gruppo: %3.</translation>
    </message>
    <message>
        <source>%1 %2 adds you to the group: %3.</source>
        <translation type="obsolete">%1 %2 ti ha aggiunto al gruppo: %3.</translation>
    </message>
    <message>
        <source>%1 Chat with %2 (%3).</source>
        <translation type="obsolete">%1 Parla con %2 (%3).</translation>
    </message>
    <message>
        <source>%1 is unable to add a new user from %2 (invalid protocol or password).</source>
        <translation type="obsolete">%1 non può aggiungere un nuovo utente da %2 (protocollo o password non validi).</translation>
    </message>
    <message>
        <source>Adding to file sharing</source>
        <translation>Aggiungo ai file condivisi in rete</translation>
    </message>
    <message>
        <source>%1 %2 has not file shared.</source>
        <translation type="obsolete">%1 %2 non ha file condivisi in rete.</translation>
    </message>
    <message>
        <source>%1 %2 has shared %3 files.</source>
        <translation>%1 %2 ha condiviso in rete %3 file.</translation>
    </message>
    <message>
        <source>%1 is added to file sharing with %2 files, %3 (elapsed time: %4)</source>
        <translation>%1 è stata condivisa in rete con %2 file, %3 (elaborazione: %4)</translation>
    </message>
    <message>
        <source>%1 is removed from file sharing with %2 files</source>
        <translation>%1 è stata rimossa dalla condivisione in rete con %2 file</translation>
    </message>
    <message>
        <source>%1 is added to file sharing (%2)</source>
        <translation>%1 è stato rimosso dalla condivisione in rete (%2)</translation>
    </message>
    <message>
        <source>%1 is removed from file sharing</source>
        <translation>%1 è stato rimosso dalla condivisione in rete</translation>
    </message>
    <message>
        <source>%1 Members removed from the group: %2.</source>
        <translation>%1 Membri rimossi dal gruppo: %2.</translation>
    </message>
    <message>
        <source>%1 Members added to the group: %2.</source>
        <translation>%1 Membri aggiunti al gruppo: %2.</translation>
    </message>
    <message>
        <source>%1 cannot be informed that you have left the group.</source>
        <translation type="obsolete">A %1 non può essere notificato che hai lasciato il gruppo.</translation>
    </message>
    <message>
        <source>%1 You have left the group.</source>
        <translation>%1 Hai lasciato il gruppo.</translation>
    </message>
    <message>
        <source>%1 %2 has left the group.</source>
        <translation>%1 %2 ha lasciato il gruppo.</translation>
    </message>
    <message>
        <source>%1 %2 can not be invited to the group.</source>
        <translation type="obsolete">%1 %2 non può essere aggiunto al gruppo.</translation>
    </message>
    <message>
        <source>%1 %2 cannot be informed that you have left the group.</source>
        <translation>%1 A %2 non può essere notificato che hai lasciato il gruppo.</translation>
    </message>
    <message>
        <source>%1 %2 is a folder. You can share it.</source>
        <translation>%1 %2 è una cartella. Puoi condividerla se vuoi.</translation>
    </message>
    <message>
        <source>%1 Unable to send %2: %3 is not connected.</source>
        <translation>%1 Impossibile inviare %2: %3 non è in linea.</translation>
    </message>
    <message>
        <source>%1 You send %2 to %3.</source>
        <translation>%1 Stai inviando %2 a %3.</translation>
    </message>
    <message>
        <source>%1 An error occurred when %2 tries to add you to the group chat: %3.</source>
        <translation>%1 Si è verificato un errore quando %2 ha provato ad aggiungerti alla conversazione di gruppo: %3.</translation>
    </message>
    <message>
        <source>%1 %2 adds you to the group chat: %3.</source>
        <translation>%1 %2 ti ha aggiunto alla conversazione di gruppo: %3.</translation>
    </message>
    <message>
        <source>%1 (%2) is connected to %3 network.</source>
        <translation>%1 (%2) entra nella tua rete %3.</translation>
    </message>
    <message>
        <source>%1 Unable to send %2. File transfer is disabled.</source>
        <translation>%1 Impossibile inviare %2. Il trasferimento dei file è disabilitato.</translation>
    </message>
    <message>
        <source>You share this information</source>
        <translation>Tu condividi questa informazione</translation>
    </message>
    <message>
        <source>%1 shares this information</source>
        <translation>%1 condivide questa informazione</translation>
    </message>
    <message>
        <source>Happy birthday to Marco!</source>
        <translation type="obsolete">Buon compleanno Marco!</translation>
    </message>
    <message>
        <source>Happy birthday to Marco Mastroddi: %1 years old today! Cheers!!!</source>
        <translation>Buon compleanno Marco Mastroddi: %1 anni oggi! Auguri!!!</translation>
    </message>
    <message>
        <source>%1 %2 has found a filter on UDP port %3. Please check your firewall settings.</source>
        <translation>%1 %2 ha trovato un filtro sulla porta UDP %3. Controlla le impostazioni del tuo firewall.</translation>
    </message>
    <message>
        <source>%1 You cannot reach %2 Network.</source>
        <translation type="obsolete">%1 Non puoi raggiungere la rete %2.</translation>
    </message>
    <message>
        <source>is connected from external network (the new subnet is added to your broadcast address list).</source>
        <translation>proviene da una rete esterna (la sua sottorete è stata aggiunta alla tua lista).</translation>
    </message>
    <message>
        <source>%1 %2 will search users in these IP addresses: %3</source>
        <translation type="obsolete">%1 %2 cercherà gli utenti connessi a questi indirizzi IP: %3</translation>
    </message>
    <message>
        <source>%1 Contacting %2 host addresses previously saved...</source>
        <translation type="obsolete">%1 Connessione a %2 IP precedentemente salvati...</translation>
    </message>
    <message>
        <source>%1 Unable to download %2 from %3: folder %4 cannot be created.</source>
        <translation>%1 Impossibile scaricare %2 da %3: la cartella %4 non può essere creata.</translation>
    </message>
    <message>
        <source>%1 Unable to download %2 from %3: user is offline.</source>
        <translation>%1 Impossibile scaricare %2 da %3: l&apos;utente non è in linea.</translation>
    </message>
    <message>
        <source>%1 Unable to send %2 to %3: user is offline.</source>
        <translation>%1 Impossibile inviare %2 a %3: l&apos;utente non è in linea.</translation>
    </message>
    <message>
        <source>%1 Checking %2 more addresses...</source>
        <translation>%1 Controllo ancora %2 indirizzi...</translation>
    </message>
    <message>
        <source>%1 Contacting %2 ...</source>
        <translation type="obsolete">%1 Sto contattando %2 ...</translation>
    </message>
    <message>
        <source>The message will be delivered to %1.</source>
        <translation>Il messaggio sarà recapitato a %1.</translation>
    </message>
    <message>
        <source>%1 %2 can not join to the group.</source>
        <translation>%1 %2 non può far parte del gruppo.</translation>
    </message>
    <message>
        <source>Offline messages sent to %2.</source>
        <translation>I messaggi non inviati sono stati spediti ora a %2.</translation>
    </message>
    <message>
        <source>folder</source>
        <translation>cartella</translation>
    </message>
    <message>
        <source>%1 You have refused to download folder %2 from %3.</source>
        <translation>%1 Hai rifiutato di scaricare la cartella %2 da %3.</translation>
    </message>
    <message>
        <source>%1 You are about to send %2 to %3. Checking folder...</source>
        <translation>%1 Stai inviando %2 a %3. Controllo della cartella in corso...</translation>
    </message>
    <message>
        <source>%1 Unable to send folder %2</source>
        <translation>%1 Impossibile inviare la cartella %2</translation>
    </message>
    <message>
        <source>the folder is empty.</source>
        <translation>la cartella è vuota.</translation>
    </message>
    <message>
        <source>file transfer is not working.</source>
        <translation>il trasferimento dei file non è attivo.</translation>
    </message>
    <message>
        <source>%1 is not connected.</source>
        <translation>%1 non è in linea.</translation>
    </message>
    <message>
        <source>internal error.</source>
        <translation>errore interno.</translation>
    </message>
    <message>
        <source>%1 You send folder %2 to %3.</source>
        <translation>%1 Stai inviando la cartella %2 a %3.</translation>
    </message>
    <message>
        <source>%1 %2 has refused to download folder %3.</source>
        <translation>%1 %2 ha rifiutato di scaricare la cartella %3.</translation>
    </message>
    <message>
        <source>unknown folder</source>
        <translation>cartella sconosciuta</translation>
    </message>
    <message>
        <source>%1 %2 is sending to you the folder: %3.</source>
        <translation>%1 %2 ti sta inviando la cartella: %3.</translation>
    </message>
    <message>
        <source>is removed from favorites</source>
        <translation>non è più tra i preferiti</translation>
    </message>
    <message>
        <source>is added to favorites</source>
        <translation>è ora tra i preferiti</translation>
    </message>
    <message>
        <source>%1 You have created group from chat: %2.</source>
        <translation>%1 Hai creato il gruppo dalla conversazione: %2.</translation>
    </message>
    <message>
        <source>%1 You have deleted group: %2.</source>
        <translation>%1 Hai cancellato il gruppo: %2.</translation>
    </message>
    <message>
        <source>All paths are removed from file sharing</source>
        <translation>Tutte le condivisioni sono state eliminate</translation>
    </message>
    <message>
        <source>Help me to know how many people are really using BeeBEEP.</source>
        <translation>Aiutami a sapere quante persone usano realmente BeeBEEP.</translation>
    </message>
    <message>
        <source>Please add a like on Facebook.</source>
        <translation>Metti mi piace su Facebook.</translation>
    </message>
    <message>
        <source>%1 (%2) is disconnected from %3 network.</source>
        <translation>%1 (%2) esce dalla tua rete %3.</translation>
    </message>
    <message>
        <source>%1 %2 has not shared files.</source>
        <translation>%1 %2 non ha condiviso nessun file.</translation>
    </message>
    <message>
        <source>%1 Zero Configuration Networking (multicast DNS) started: %3</source>
        <translation type="obsolete">%1 Zero Configuration Networking (muticast DNS) inizializzato: %3</translation>
    </message>
    <message>
        <source>%1 Zero Configuration Networking (multicast DNS) closed.</source>
        <translation type="obsolete">%1 Zero Configuration Networking (multicast DNS) chiuso.</translation>
    </message>
    <message>
        <source>%1 Zero Configuration started with service name: %2</source>
        <translation>%1 Il servizio Zero Configuration è stato inizializzato con il nome: %2</translation>
    </message>
    <message>
        <source>%1 Zero Configuration service closed.</source>
        <translation>%1 Il servizio Zero Configuration è stato chiuso.</translation>
    </message>
    <message>
        <source>%1 Zero Configuration is browsing network for service: %2</source>
        <translation>%1 Zero Configuration sta cercando nella tua rete il servizio: %2</translation>
    </message>
    <message>
        <source>%1 Zero Configuration cannot browse network for service: %2</source>
        <translation>%1 Zero Configuration non può cercare nella rete il servizio: %2</translation>
    </message>
    <message>
        <source>Donwload</source>
        <translation type="obsolete">Scarica</translation>
    </message>
    <message>
        <source>Upload</source>
        <translation>Carica</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <source>%1 You have selected to join only in these workgroups: %2</source>
        <translation>%1 Hai selezionato di partecipare solo a questi gruppi di lavoro: %2</translation>
    </message>
    <message>
        <source>%1 User %2 cannot save settings in path: %3</source>
        <translation>%1 L&apos;utente %2 non può salvare le preferenze nell&apos;indirizzo: %3</translation>
    </message>
    <message>
        <source>%1 User %2 cannot save chat messages in path: %3</source>
        <translation>%1 L&apos;utente %2 non può salvare i messaggi all&apos;indirizzo: %3</translation>
    </message>
    <message>
        <source>Unable to send the message: this chat is disabled.</source>
        <translation>Impossibile inviare il messaggio: la conversazione è disabilitata.</translation>
    </message>
    <message>
        <source>Happy New Year!</source>
        <translation>Buon Anno!</translation>
    </message>
    <message>
        <source>%1 Network interface %2 is gone down.</source>
        <translation>%1 L&apos;interfaccia di rete %2 si è disattivata.</translation>
    </message>
    <message>
        <source>%1 The following networks appears as filtered: %2</source>
        <translation type="obsolete">%1 Le seguenti reti sembrano filtrate: %2</translation>
    </message>
    <message>
        <source>%1 The following networks appear as filtered: %2</source>
        <translation type="obsolete">%1 Le seguenti reti sembrano filtrate: %2</translation>
    </message>
    <message>
        <source>View the log messages for more informations</source>
        <translation>Guarda i messaggi di log per ulteriori informazioni</translation>
    </message>
    <message>
        <source>New version is available</source>
        <translation>Una nuova versione è disponibile</translation>
    </message>
    <message>
        <source>Click here to download</source>
        <translation>Premi qui per scaricarla</translation>
    </message>
    <message>
        <source>%1 saved chats are added to history</source>
        <translation>%1 conversazioni salvate sono state caricate nella storia</translation>
    </message>
</context>
<context>
    <name>FacebookService</name>
    <message>
        <source>Facebook</source>
        <translation type="obsolete">Facebook</translation>
    </message>
    <message>
        <source>You can connect to the Facebook chat.</source>
        <translation type="obsolete">Puoi connetterti alla chat di Facebook.</translation>
    </message>
</context>
<context>
    <name>File</name>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Immagini</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Documenti</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>Altri</translation>
    </message>
    <message>
        <source>Executable</source>
        <translation>Eseguibili</translation>
    </message>
    <message>
        <source>MacOSX</source>
        <translation>MacOSX</translation>
    </message>
</context>
<context>
    <name>FileTransferPeer</name>
    <message>
        <source>Unable to open file</source>
        <translation>Impossibile aprire il file</translation>
    </message>
    <message>
        <source>Unable to write in the file</source>
        <translation>Impossibile scrivere sul file</translation>
    </message>
    <message>
        <source>Transfer cancelled</source>
        <translation>Trasferimento cancellato</translation>
    </message>
    <message>
        <source>Transfer completed in %1</source>
        <translation>Trasferimento completato in %1</translation>
    </message>
    <message>
        <source>Transfer completed</source>
        <translation type="obsolete">Trasferimento completato</translation>
    </message>
    <message>
        <source>Connection timeout</source>
        <translation>Tempo esaurito per la connessione</translation>
    </message>
    <message>
        <source>%1 bytes sent not confirmed (%2 bytes confirmed)</source>
        <translation>%1 bytes inviato non confermati( %2 bytes confermati invece)</translation>
    </message>
    <message>
        <source>Unable to upload data</source>
        <translation>Impossibile inviare i dati</translation>
    </message>
    <message>
        <source>%1 bytes downloaded but the file size is only %2 bytes</source>
        <translation>%1 bytes scaricati ma la dimensione del file è di soli %2 bytes</translation>
    </message>
    <message>
        <source>%1 bytes uploaded but the file size is only %2 bytes</source>
        <translation>%1 bytes caricati ma la dimensione del file è di soli %2 bytes</translation>
    </message>
    <message>
        <source>invalid file size</source>
        <translation type="obsolete">dimensione del file non valida</translation>
    </message>
    <message>
        <source>unable to send file header</source>
        <translation>impossibile inizializzare l&apos;invio del file</translation>
    </message>
    <message>
        <source>invalid file header</source>
        <translation>inizializzazione non valida del file</translation>
    </message>
    <message>
        <source>Transfer timeout</source>
        <translation>Tempo esaurito per il trasferimento</translation>
    </message>
</context>
<context>
    <name>GTalkService</name>
    <message>
        <source>GTalk</source>
        <translation type="obsolete">GTalk</translation>
    </message>
    <message>
        <source>You can connect to the Google chat.</source>
        <translation type="obsolete">Puoi connetterti alla chat di Google.</translation>
    </message>
</context>
<context>
    <name>GuiAddUser</name>
    <message>
        <source>Add user</source>
        <translation>Aggiungi utente</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <source>Please insert a valid IP address.</source>
        <translation>Inserisci un indirizzo IP valido.</translation>
    </message>
    <message>
        <source>Please insert a valid port or use the default one %1.</source>
        <translation>Inserisci una porta valida o usa quella predefinita %1.</translation>
    </message>
    <message>
        <source>These IP address and port are already inserted in list.</source>
        <translation>Questa combinazione di indirizzo IP e porta è già stata inserita in lista.</translation>
    </message>
    <message>
        <source>Address</source>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <source>Remove user path</source>
        <translation>Rimuovi l&apos;indirizzo utente</translation>
    </message>
    <message>
        <source>Please select an user path in the list.</source>
        <translation>Seleziona un indirizzo utente nella lista.</translation>
    </message>
    <message>
        <source>Clear all</source>
        <translation>Cancella tutti</translation>
    </message>
    <message>
        <source>auto added</source>
        <translation>aggiunto automaticamente</translation>
    </message>
    <message>
        <source>your IP is %1 in LAN %2</source>
        <translation>il tuo IP è %1 nella LAN %2</translation>
    </message>
</context>
<context>
    <name>GuiAddUserDialog</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Add an IP address and port of the user you want to connect</source>
        <translation>Aggiungo un indirizzo IP e la porta dell&apos;utente a cui ti vuoi connettere</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation>Indirizzo IP</translation>
    </message>
    <message>
        <source>Port</source>
        <translation>Porta</translation>
    </message>
    <message>
        <source>Comment</source>
        <translation>Commento</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
    <message>
        <source>Click here to add user path</source>
        <translation>Premi qui per aggiungere un indirizzo utente</translation>
    </message>
    <message>
        <source>Split subnet in Ipv4 addresses</source>
        <translation type="obsolete">Usa i singoli indirizzi IPV4 invece di una sottorete</translation>
    </message>
    <message>
        <source>Auto add from LAN</source>
        <translation>Aggiungi dalla LAN</translation>
    </message>
    <message>
        <source>Split in IPv4 addresses</source>
        <translation>Usa i singoli indirizzi IPv4 invece di una sottorete</translation>
    </message>
</context>
<context>
    <name>GuiAskPassword</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Please enter the chat password (spaces are removed)</source>
        <translation type="obsolete">Per favore inserisci la password di rete (gli spazi saranno rimossi)</translation>
    </message>
    <message>
        <source>Remember password (not recommended)</source>
        <translation>Ricorda la password (non consigliato)</translation>
    </message>
    <message>
        <source>Use default chat password</source>
        <translation type="obsolete">Usa la password di rete automatica</translation>
    </message>
    <message>
        <source>Show this dialog at connection startup</source>
        <translation>Mostra questa finestra ad ogni connessione</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Chat Password - %1</source>
        <translation>Password di rete - %1</translation>
    </message>
    <message>
        <source>Password is empty. Please enter a valid one (spaces are removed).</source>
        <translation>La password è vuota. Per favore inseriscine una valida (gli spazi saranno rimossi).</translation>
    </message>
    <message>
        <source>Please enter the chat password (spaces are removed) *</source>
        <translation type="obsolete">Per favore inserisci la password di rete (gli spazi saranno rimossi) *</translation>
    </message>
    <message>
        <source>* Password must be the same for all client you want to reach</source>
        <translation type="obsolete">* La password deve essere la stessa per tutti i client connessi</translation>
    </message>
    <message>
        <source>* Password must be the same for all user you want to connect</source>
        <translation>* La password deve essere la stessa per tutti gli utenti connessi</translation>
    </message>
    <message>
        <source>Use standard session (encrypted but authentication is not required)</source>
        <translation>Usa la sessione standard (criptata ma nessuna autenticazione richiesta)</translation>
    </message>
    <message>
        <source>Use authentication password (spaces are removed) *</source>
        <translation>Usa la password di autenticazione (gli spazi saranno rimossi) *</translation>
    </message>
</context>
<context>
    <name>GuiChat</name>
    <message>
        <source>Copy to clipboard</source>
        <translation>Copia in memoria</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Seleziona tutto</translation>
    </message>
    <message>
        <source>(Last message %1)</source>
        <translation type="obsolete">(Ultimo messaggio %1)</translation>
    </message>
    <message>
        <source>To</source>
        <translation type="obsolete">A</translation>
    </message>
    <message>
        <source>Unable to open %1</source>
        <translation type="obsolete">Impossibile aprire %1</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>All users</source>
        <translation type="obsolete">Tutti</translation>
    </message>
    <message>
        <source>Nobody</source>
        <translation>Nessuno</translation>
    </message>
    <message>
        <source>%1 Secure Mode</source>
        <translation type="obsolete">Modalità Sicura %1</translation>
    </message>
    <message>
        <source>All Lan Users</source>
        <translation>Utenti Locali</translation>
    </message>
    <message>
        <source>%1 Local Secure Mode</source>
        <translation type="obsolete">Connessione Locale Sicura di %1</translation>
    </message>
    <message>
        <source>XMPP Secure Mode</source>
        <translation type="obsolete">Modalità Sicura XMPP</translation>
    </message>
    <message>
        <source>offline</source>
        <translation>non in linea</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation type="obsolete">Faccine</translation>
    </message>
    <message>
        <source>Add your preferred emoticon to the message</source>
        <translation type="obsolete">Aggiungi la tua faccina preferita al messaggio</translation>
    </message>
    <message>
        <source>Change font style</source>
        <translation>Cambia il carattere</translation>
    </message>
    <message>
        <source>Select your favourite chat font style</source>
        <translation>Seleziona il carattere in cui vuoi che vengano visualizzati i messaggi</translation>
    </message>
    <message>
        <source>Change font color</source>
        <translation>Cambia il colore del carattere</translation>
    </message>
    <message>
        <source>Select your favourite font color for the chat messages</source>
        <translation>Seleziona il colore del carattere per i tuoi messaggi</translation>
    </message>
    <message>
        <source>Send file</source>
        <translation>Invia un file</translation>
    </message>
    <message>
        <source>Send a file to a user or a group</source>
        <translation>Invia un file ad un utente o ad un gruppo</translation>
    </message>
    <message>
        <source>Save chat</source>
        <translation>Salva la conversazione</translation>
    </message>
    <message>
        <source>Save the messages of the current chat to a file</source>
        <translation>Salva i messaggi della conversazione corrente in un file</translation>
    </message>
    <message>
        <source>Create group chat</source>
        <translation type="obsolete">Crea una conversazione di gruppo</translation>
    </message>
    <message>
        <source>Create a group chat with two or more users</source>
        <translation type="obsolete">Crea una conversazione con due o più persone</translation>
    </message>
    <message>
        <source>Edit group chat</source>
        <translation type="obsolete">Modifica il gruppo</translation>
    </message>
    <message>
        <source>Change the name of the group or add and remove users</source>
        <translation>Cambia il nome del gruppo o aggiungi o rimuovi i membri</translation>
    </message>
    <message>
        <source>Clear messages</source>
        <translation>Cancella i messaggi</translation>
    </message>
    <message>
        <source>Clear all the messages of the chat</source>
        <translation>Cancella tutti i messaggi della conversazione</translation>
    </message>
    <message>
        <source>Please select a file to save the messages of the chat.</source>
        <translation>Seleziona un file dove salvare la conversazione in corso.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Attenzione</translation>
    </message>
    <message>
        <source>%1: unable to save the messages.
Please check the file or the directories write permissions.</source>
        <translation type="obsolete">%1: impossibile salvare i messaggi.
Controlla i permessi di scrittura nella cartella selezionata.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Informazione</translation>
    </message>
    <message>
        <source>%1: save completed.</source>
        <translation>%1: salvataggio completato.</translation>
    </message>
    <message>
        <source>Leave the group</source>
        <translation>Lascia il gruppo</translation>
    </message>
    <message>
        <source>(You have left)</source>
        <translation type="obsolete">(Hai lasciato)</translation>
    </message>
    <message>
        <source>You</source>
        <translation>Te</translation>
    </message>
    <message>
        <source>and</source>
        <translation>e</translation>
    </message>
    <message>
        <source>Last message %1</source>
        <translation>Ultimo messaggio alle %1</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation type="obsolete">Crea gruppo</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation type="obsolete">Crea un gruppo con due o più membri</translation>
    </message>
    <message>
        <source>Do you really want to send %1 %2 to the members of this chat?</source>
        <translation>Sei sicuro di voler inviare %1 %2 ai membri di questa conversazione?</translation>
    </message>
    <message>
        <source>file</source>
        <translation></translation>
    </message>
    <message>
        <source>files</source>
        <translation>file</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Qt library for this OS doesn&apos;t support Drag and Drop for files. You have to select again the file to send.</source>
        <translation>Le librerie Qt per questo OS non supportano il Drag and Drop per i file. Dovrai selezionare nuovamente il file da inviare.</translation>
    </message>
    <message>
        <source>Filter message</source>
        <translation>Filtro messaggi</translation>
    </message>
    <message>
        <source>Select the message types which will be showed in chat</source>
        <translation>Seleziona la tipologia di messaggi che vuoi che sia mostrata nella conversazione</translation>
    </message>
    <message>
        <source>Chat settings</source>
        <translation>Preferenze di conversazione</translation>
    </message>
    <message>
        <source>Click to show the settings menu of the chat</source>
        <translation>Premi qui per mostrare il menu delle preferenze delle conversazioni</translation>
    </message>
    <message>
        <source>Show only messages in default chat</source>
        <translation>Mostra solo i messaggi nella conversazione con tutti</translation>
    </message>
    <message>
        <source>Members</source>
        <translation>Membri</translation>
    </message>
    <message>
        <source>Show the members of the chat</source>
        <translation>Mostra i membri della conversazione</translation>
    </message>
    <message>
        <source>last %1 messages</source>
        <translation>ultimi %1 messaggi</translation>
    </message>
    <message>
        <source>Show profile</source>
        <translation>Mostra profilo</translation>
    </message>
    <message>
        <source>Show members</source>
        <translation>Mostra membri</translation>
    </message>
    <message>
        <source>Change background color</source>
        <translation>Cambia il colore di sfondo</translation>
    </message>
    <message>
        <source>Select your favourite background color for the chat window</source>
        <translation>Seleziona il tuo colore di sfonto preferito per la conversazione</translation>
    </message>
    <message>
        <source>Create group from chat</source>
        <translation>Crea il gruppo dalla conversazione</translation>
    </message>
    <message>
        <source>Create a group from this chat</source>
        <translation>Crea un gruppo da questa conversazione</translation>
    </message>
    <message>
        <source>Create chat</source>
        <translation type="obsolete">Crea una conversazione</translation>
    </message>
    <message>
        <source>Create a chat with two or more users</source>
        <translation type="obsolete">Crea una conversazione con due o più utenti</translation>
    </message>
    <message>
        <source>Edit group</source>
        <translation>Modifica gruppo</translation>
    </message>
    <message>
        <source>Unable to save temporary file: %1</source>
        <translation>Impossibile salvare il file temporaneo: %1</translation>
    </message>
    <message>
        <source>Use key Return to send message</source>
        <translation>Usa il tasto Invio per mandare i messaggi</translation>
    </message>
    <message>
        <source>Spell checking</source>
        <translation>Controllo ortografico</translation>
    </message>
    <message>
        <source>Use Return key to send message</source>
        <translation>Usa il tasto Invio per spedire il messaggio</translation>
    </message>
    <message>
        <source>Spell checking is enabled</source>
        <translation>Il controllo ortografico è abilitato</translation>
    </message>
    <message>
        <source>Spell checking is disabled</source>
        <translation>Il controllo ortografico è disabilitato</translation>
    </message>
    <message>
        <source>Use key Return to make a carriage return</source>
        <translation>Usa il tasto Invio per andare a capo</translation>
    </message>
    <message>
        <source>There is not a valid dictionary</source>
        <translation>Non è stato selezionato un dizionario valido</translation>
    </message>
    <message>
        <source>Word completer is enabled</source>
        <translation>Il completamento della parola è abilitato</translation>
    </message>
    <message>
        <source>Word completer is disabled</source>
        <translation>Il completamento della parola è disabilitato</translation>
    </message>
    <message>
        <source>Word completer</source>
        <translation>Completamento della parola</translation>
    </message>
    <message>
        <source>unread messages</source>
        <translation>messaggi non letti</translation>
    </message>
    <message>
        <source>Print...</source>
        <translation>Stampa...</translation>
    </message>
    <message>
        <source>Find text in chat</source>
        <translation>Trova testo nella conversazione</translation>
    </message>
    <message>
        <source>%1 not found in chat</source>
        <translation type="obsolete">%1 non trovato nella conversazione</translation>
    </message>
    <message>
        <source>%1 not found in chat.</source>
        <translation>%1 non trovato nella conversazione.</translation>
    </message>
    <message>
        <source>Send folder</source>
        <translation>Invia una cartella</translation>
    </message>
    <message>
        <source>Open selected text as url</source>
        <translation>Apri il testo selezionato come un link</translation>
    </message>
</context>
<context>
    <name>GuiChatList</name>
    <message>
        <source>Show</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <source>Clear all chat messages</source>
        <translation>Cancella tutti i messaggi della conversazione</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
</context>
<context>
    <name>GuiChatWidget</name>
    <message>
        <source>Send</source>
        <translation type="obsolete">Invia</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation>Parla</translation>
    </message>
    <message>
        <source>Write to: &lt;b&gt;ALL&lt;/b&gt;</source>
        <translation></translation>
    </message>
    <message>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <source>Click to send message or just hit enter</source>
        <translation>Premi qui o premi invio per spedire il messaggio</translation>
    </message>
    <message>
        <source>Detach chat</source>
        <translation>Stacca la finestra</translation>
    </message>
    <message>
        <source>Save geometry and state</source>
        <translation type="obsolete">Salva le dimensioni e lo stato della finestra</translation>
    </message>
    <message>
        <source>Save window&apos;s geometry </source>
        <translation type="obsolete">Salva le dimensioni della finestra</translation>
    </message>
    <message>
        <source>Save window&apos;s geometry</source>
        <translation>Salva le dimensioni della finestra</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroup</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Group name</source>
        <translation>Nome del gruppo</translation>
    </message>
    <message>
        <source>Please add member in the group:</source>
        <translation>Aggiungi membri al gruppo:</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Utenti</translation>
    </message>
    <message>
        <source>Create Group - %1</source>
        <translation>Crea Gruppo - %1</translation>
    </message>
    <message>
        <source>Edit Group - %1</source>
        <translation>Modifica Gruppo - %1</translation>
    </message>
    <message>
        <source>Please select two or more member for the group.</source>
        <translation>Seleziona due o più membri per il gruppo.</translation>
    </message>
    <message>
        <source>Please insert a group name.</source>
        <translation>Inserisci il nome del gruppo.</translation>
    </message>
    <message>
        <source>%1 already exists as group name or chat name.
Please select a different name.</source>
        <translation>%1 esiste già come nome del gruppo o di conversazione.
Inserisci un nome differente.</translation>
    </message>
    <message>
        <source>Create Chat - %1</source>
        <translation>Crea una conversazione - %1</translation>
    </message>
    <message>
        <source>Edit Chat - %1</source>
        <translation>Modifica una conversazione - %1</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroupChat</name>
    <message>
        <source>Ok</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annulla</translation>
    </message>
    <message>
        <source>Please add or remove member in the group chat:</source>
        <translation type="obsolete">Aggiungi o rimuovi membri di questa chat di gruppo:</translation>
    </message>
    <message>
        <source>Users</source>
        <translation type="obsolete">Utenti</translation>
    </message>
    <message>
        <source>Edit Group - %1</source>
        <translation type="obsolete">Modifica Gruppo - %1</translation>
    </message>
    <message>
        <source>Create Group - %1</source>
        <translation type="obsolete">Crea Gruppo - %1</translation>
    </message>
    <message>
        <source>Please select two or more member of the group chat.</source>
        <translation type="obsolete">Seleziona due o più membri per formare un gruppo.</translation>
    </message>
    <message>
        <source>Group name</source>
        <translation type="obsolete">Nome del gruppo</translation>
    </message>
    <message>
        <source>Please add member in the group chat:</source>
        <translation type="obsolete">Aggiungi dei membri alla chat di gruppo:</translation>
    </message>
    <message>
        <source>Please select different group name.</source>
        <translation type="obsolete">Seleziona un differente nome del gruppo.</translation>
    </message>
</context>
<context>
    <name>GuiEditVCard</name>
    <message>
        <source>%1 - Select your profile photo</source>
        <translation>%1 - Seleziona la foto del tuo profilo</translation>
    </message>
    <message>
        <source>Images (*.png *.xpm *.jpg *.jpeg)</source>
        <translation type="obsolete">Immagini (*.png *.xpm *.jpg *.jpeg)</translation>
    </message>
    <message>
        <source>Unable to load image %1.</source>
        <translation>Impossibile caricare l&apos;immagine %1.</translation>
    </message>
    <message>
        <source>Please insert your nickname.</source>
        <translation>Il campo del tuo soprannome non può essere vuoto.</translation>
    </message>
    <message>
        <source>Profile - %1</source>
        <translation type="obsolete">Profilo - %1</translation>
    </message>
    <message>
        <source>Edit your profile</source>
        <translation>Modifica il tuo profilo</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Immagini</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>GuiEmoticons</name>
    <message>
        <source>Smiley</source>
        <translation>Faccine</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation>Oggetti</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation>Natura</translation>
    </message>
    <message>
        <source>Places</source>
        <translation>Posti</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation>Simboli</translation>
    </message>
    <message>
        <source>Recent</source>
        <translation>Recenti</translation>
    </message>
</context>
<context>
    <name>GuiFileInfoList</name>
    <message>
        <source>Shared folders and files</source>
        <translation>Cartelle e file condivisi</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Dimensione</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
</context>
<context>
    <name>GuiFloatingChat</name>
    <message>
        <source>Show the bar of chat</source>
        <translation>Mostra la barra delle conversazioni</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation>Faccine</translation>
    </message>
    <message>
        <source>Show the emoticon panel</source>
        <translation>Mostra il pannello delle faccine</translation>
    </message>
    <message>
        <source>Add your preferred emoticon to the message</source>
        <translation>Aggiungi la tua faccina preferita al messaggio</translation>
    </message>
    <message>
        <source>Window&apos;s geometry and state saved</source>
        <translation>Le dimensioni e lo stato della finestra sono stati salvati</translation>
    </message>
</context>
<context>
    <name>GuiGroupList</name>
    <message>
        <source>Create group</source>
        <translation>Crea gruppo</translation>
    </message>
    <message>
        <source>Edit group</source>
        <translation>Modifica gruppo</translation>
    </message>
    <message>
        <source>Open chat</source>
        <translation>Apri la conversazione</translation>
    </message>
    <message>
        <source>Waiting for two or more connected user</source>
        <translation>Aspetta per due o più utenti connessi</translation>
    </message>
    <message>
        <source>Delete group</source>
        <translation>Cancella gruppo</translation>
    </message>
    <message>
        <source>Enable notifications</source>
        <translation>Abilita notifiche</translation>
    </message>
    <message>
        <source>Disable notifications</source>
        <translation>Disabilita notifiche</translation>
    </message>
</context>
<context>
    <name>GuiHome</name>
    <message>
        <source>Activity</source>
        <translation type="obsolete">Attività</translation>
    </message>
    <message>
        <source>Select a user you want to chat with or</source>
        <translation>Seleziona un utente con cui vuoi parlare oppure</translation>
    </message>
    <message>
        <source>%1 Activities</source>
        <translation type="obsolete">Attività di %1</translation>
    </message>
    <message>
        <source>Home</source>
        <translation>Home</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation>Copia in memoria</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Seleziona tutto</translation>
    </message>
    <message>
        <source>Print...</source>
        <translation>Stampa...</translation>
    </message>
    <message>
        <source>Show the datestamp</source>
        <translation>Mostra la data</translation>
    </message>
    <message>
        <source>Show the timestamp</source>
        <translation>Mostra l&apos;orario</translation>
    </message>
</context>
<context>
    <name>GuiHomeWidget</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Show this page at startup</source>
        <translation type="obsolete">Mostra questa pagina alla partenza</translation>
    </message>
    <message>
        <source>chat with all users</source>
        <translation>parla con tutti</translation>
    </message>
</context>
<context>
    <name>GuiLanguage</name>
    <message>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <source>Select a language folder</source>
        <translation>Seleziona una cartella per le traduzioni</translation>
    </message>
    <message>
        <source>Language &apos;%1&apos;&apos; not found.</source>
        <translation>La traduzione &apos;%1&apos; non è stata trovata.</translation>
    </message>
    <message>
        <source>Select language</source>
        <translation>Seleziona una lingua</translation>
    </message>
    <message>
        <source>For the latest language translations please visit the %1</source>
        <translation>Per avere le ultime traduzioni disponibili andare sul %1</translation>
    </message>
    <message>
        <source>website</source>
        <translation type="obsolete">sito web</translation>
    </message>
    <message>
        <source>official website</source>
        <translation>sito web ufficiale</translation>
    </message>
</context>
<context>
    <name>GuiLanguageDialog</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Lingua</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Percorso</translation>
    </message>
    <message>
        <source>Select language folder</source>
        <translation>Seleziona una cartella per le traduzioni</translation>
    </message>
    <message>
        <source>Restore to default language</source>
        <translation>Ripristina la lingua predefinita</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>Ripristina</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Seleziona</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
</context>
<context>
    <name>GuiLife</name>
    <message>
        <source>Restart</source>
        <translation type="obsolete">Riavvia</translation>
    </message>
    <message>
        <source>Press space bar to start</source>
        <translation type="obsolete">Premi spazio per iniziare</translation>
    </message>
    <message>
        <source>Paused (press space bar to continue)</source>
        <translation type="obsolete">Pausa (premi spazio per continuare)</translation>
    </message>
    <message>
        <source>Do you really want to restart?</source>
        <translation type="obsolete">Vuoi riavviare il gioco?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Si</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">No</translation>
    </message>
    <message>
        <source>Bees Alive: %1 - Bees Died: %2 - Spaces: %3 - Hive Generation: %4</source>
        <translation type="obsolete">Api Vive: %1 - Api Morte: %2 - Spazi: %3 - Generazione: %4</translation>
    </message>
    <message>
        <source>??? Evolution Completed ??? ... (or press space bar to continue)</source>
        <translation type="obsolete">??? Evoluzione Completata ??? ... (o premi spazio per continuare)</translation>
    </message>
    <message>
        <source>Alive: %1 - Died: %2 - Spaces: %3 - Step: %4</source>
        <translation type="obsolete">Vive: %1 - Morte: %2 - Spazi: %3 - Passi: %4</translation>
    </message>
    <message>
        <source>Do you really want to randomize and restart?</source>
        <translation type="obsolete">Vuoi creare una situazione iniziale a caso e ripartire?</translation>
    </message>
    <message>
        <source>Do you really want to clear all?</source>
        <translation type="obsolete">Vuoi cancellare tutto?</translation>
    </message>
</context>
<context>
    <name>GuiLifeWidget</name>
    <message>
        <source>Press space bar to start</source>
        <translation type="obsolete">Premi spazio per iniziare</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="obsolete">Riavvia</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Cancella</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="obsolete">Veloce</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="obsolete">Lento</translation>
    </message>
</context>
<context>
    <name>GuiLog</name>
    <message>
        <source>System Log</source>
        <translation>Log di sistema</translation>
    </message>
    <message>
        <source>Save log as</source>
        <translation>Salva log con nome</translation>
    </message>
    <message>
        <source>Save the log in a file</source>
        <translation>Salva il log in un file</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <source>keyword</source>
        <translation>parola chiave</translation>
    </message>
    <message>
        <source>Find</source>
        <translation>Trova</translation>
    </message>
    <message>
        <source>Find keywords in the log</source>
        <translation>Trova la parola chiave nel log</translation>
    </message>
    <message>
        <source>Please select a file to save the log.</source>
        <translation>Seleziona un file dove salvare il log.</translation>
    </message>
    <message>
        <source>Unable to save log in the file: %1</source>
        <translation>Impossibile salvare il log nel file: %1</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>%1: save log completed.</source>
        <translation>%1: salvataggio del log completato.</translation>
    </message>
    <message>
        <source>Open file</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <source>Open folder</source>
        <translation>Apri cartella</translation>
    </message>
    <message>
        <source>%1 not found</source>
        <translation>%1 non trovato</translation>
    </message>
    <message>
        <source>Case sensitive</source>
        <translation>Maiuscole/Minuscole</translation>
    </message>
    <message>
        <source>Whole word</source>
        <translation>Parola intera</translation>
    </message>
    <message>
        <source>Log to file</source>
        <translation>Usa un file di log</translation>
    </message>
    <message>
        <source>Block scrolling</source>
        <translation>Blocca scorrimento</translation>
    </message>
</context>
<context>
    <name>GuiLogWidget</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GuiMain</name>
    <message>
        <source>Show the ToolBar</source>
        <translation type="obsolete">Mostra la barra degli strumenti</translation>
    </message>
    <message>
        <source>offline</source>
        <translation>disconnesso</translation>
    </message>
    <message>
        <source>Do you really want to quit %1?</source>
        <translation>Vuoi realmente chiudere %1?</translation>
    </message>
    <message>
        <source>No new message available</source>
        <translation>Nessun nuovo messaggio disponibile</translation>
    </message>
    <message>
        <source>Please insert your nickname</source>
        <translation type="obsolete">Per favore inserisci il tuo nickname</translation>
    </message>
    <message>
        <source>Please insert the chat password (or just press Enter)</source>
        <translation type="obsolete">Inserisci la password per il collegamento (o premi solamente Invio)</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation type="obsolete">&amp;Disconnetti</translation>
    </message>
    <message>
        <source>Disconnect from %1 network</source>
        <translation>Disconnettiti dalla rete %1</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation type="obsolete">&amp;Connetti</translation>
    </message>
    <message>
        <source>Connect to %1 network</source>
        <translation>Connettiti alla rete %1</translation>
    </message>
    <message>
        <source>Secure Network Chat version %1</source>
        <translation type="obsolete">Secure Network Chat versione %1</translation>
    </message>
    <message>
        <source>&lt;br /&gt;developed by Marco &quot;Khelben&quot; Mastroddi&lt;br /&gt;e-mail: marco.mastroddi@gmail.com&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="obsolete">&lt;br /&gt;sviluppata da Marco Mastroddi&lt;br /&gt;e-mail: marco.mastroddi@gmail.com&lt;br /&gt;&lt;br /&gt;</translation>
    </message>
    <message>
        <source>&lt;br /&gt;developed by Marco Mastroddi&lt;br /&gt;e-mail: marco.mastroddi@gmail.com&lt;br /&gt;&lt;br /&gt;</source>
        <translation type="obsolete">&lt;br /&gt;sviluppata da Marco Mastroddi&lt;br /&gt;e-mail: marco.mastroddi@gmail.com&lt;br /&gt;&lt;br /&gt;</translation>
    </message>
    <message>
        <source>&amp;lt; Free is that mind guided by the fantasy &amp;gt;</source>
        <translation type="obsolete">&amp;lt; Libera è quella mente guidata dalla fantasia &amp;gt;</translation>
    </message>
    <message>
        <source>Search &amp;users...</source>
        <translation type="obsolete">Cerca &amp;utenti...</translation>
    </message>
    <message>
        <source>Search the users outside the %1 local network</source>
        <translation type="obsolete">Cerca nuovi utenti fuori dalla rete locale di %1</translation>
    </message>
    <message>
        <source>&amp;Save chat...</source>
        <translation type="obsolete">&amp;Salva la conversazione...</translation>
    </message>
    <message>
        <source>Save the messages of the current chat to a file</source>
        <translation type="obsolete">Salva i messaggi della conversazione corrente in un file</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="obsolete">&amp;Esci</translation>
    </message>
    <message>
        <source>Close the chat and quit %1</source>
        <translation>Esci da %1</translation>
    </message>
    <message>
        <source>Change your nickname...</source>
        <translation type="obsolete">Cambia il tuo nickname...</translation>
    </message>
    <message>
        <source>Select your favourite chat nickname</source>
        <translation type="obsolete">Seleziona il tuo nickname favorito per le conversazioni</translation>
    </message>
    <message>
        <source>Secure Network Chat</source>
        <translation type="obsolete">Secure Network Chat</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <source>developed by Marco Mastroddi</source>
        <translation type="obsolete">sviluppato da Marco Mastroddi</translation>
    </message>
    <message>
        <source>Profile...</source>
        <translation type="obsolete">Profilo...</translation>
    </message>
    <message>
        <source>Change your profile data</source>
        <translation type="obsolete">Cambia i dati del tuo profilo</translation>
    </message>
    <message>
        <source>Chat font style...</source>
        <translation type="obsolete">Cambia il carattere...</translation>
    </message>
    <message>
        <source>Select your favourite chat font style</source>
        <translation type="obsolete">Seleziona il carattere in cui vuoi che vengano visualizzati i messaggi</translation>
    </message>
    <message>
        <source>My message font color...</source>
        <translation type="obsolete">Cambia il colore del carattere...</translation>
    </message>
    <message>
        <source>Select your favourite font color for the chat messages</source>
        <translation type="obsolete">Seleziona il colore del carattere per i tuoi messaggi</translation>
    </message>
    <message>
        <source>Send a file...</source>
        <translation type="obsolete">Invia un file...</translation>
    </message>
    <message>
        <source>Send a file to a user</source>
        <translation type="obsolete">Invia un file ad un utente</translation>
    </message>
    <message>
        <source>Show the MenuBar</source>
        <translation type="obsolete">Mostra la barra generale</translation>
    </message>
    <message>
        <source>Show the main menu bar with the %1 options</source>
        <translation type="obsolete">Mostra la barra generale con le opzioni di %1</translation>
    </message>
    <message>
        <source>Show the main tool bar with settings and emoticons</source>
        <translation type="obsolete">Mostra la barra degli strumenti con le opzioni e le faccine</translation>
    </message>
    <message>
        <source>&amp;About %1...</source>
        <translation type="obsolete">&amp;Informazioni su %1...</translation>
    </message>
    <message>
        <source>Show the informations about %1</source>
        <translation>Mostra le informazioni sull&apos;applicazione %1</translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation type="obsolete">&amp;Generale</translation>
    </message>
    <message>
        <source>Select download folder...</source>
        <translation type="obsolete">Seleziona una cartella per i downloads...</translation>
    </message>
    <message>
        <source>Download folder...</source>
        <translation>Cartella file scaricati...</translation>
    </message>
    <message>
        <source>Select the download folder</source>
        <translation>Seleziona una cartella dove salvare i file scaricati</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="obsolete">&amp;Opzioni</translation>
    </message>
    <message>
        <source>Show only the nicknames</source>
        <translation type="obsolete">Mostra solo i soprannomi degli utenti</translation>
    </message>
    <message>
        <source>If enabled only the nickname of the connected users are shown</source>
        <translation type="obsolete">Se abilitata solamente i soprannomi degli utenti connessi saranno mostrati</translation>
    </message>
    <message>
        <source>Enable the compact mode in chat window</source>
        <translation>Abilita la versione compatta dei messaggi</translation>
    </message>
    <message>
        <source>If enabled the sender&apos;s nickname and his message are in the same line</source>
        <translation>Se abilitata il soprannome ed il messaggio relativo staranno sulla stessa linea</translation>
    </message>
    <message>
        <source>Add a blank line between the messages</source>
        <translation>Aggiungi una linea vuota tra i messaggi</translation>
    </message>
    <message>
        <source>If enabled the messages in the chat window are separated by a blank line</source>
        <translation>Se abilitata i messaggi saranno separati da una linea vuota</translation>
    </message>
    <message>
        <source>Show the messages&apos; timestamp</source>
        <translation type="obsolete">Mostra l&apos;orario nei messaggi</translation>
    </message>
    <message>
        <source>If enabled the message shows its timestamp in the chat window</source>
        <translation>Se abilitata i messaggi saranno preceduti dall&apos;orario di invio</translation>
    </message>
    <message>
        <source>Show the user&apos;s colors</source>
        <translation type="obsolete">Mostra i colori degli utenti</translation>
    </message>
    <message>
        <source>If enabled the user&apos;s nickname in chat and in list is colored</source>
        <translation>Se abilitata il soprannome degli utenti in chat e nella lista sarà colorato</translation>
    </message>
    <message>
        <source>Beep on new message arrived</source>
        <translation type="obsolete">Abilita suoni di notifica</translation>
    </message>
    <message>
        <source>If enabled when a new message is arrived a sound is emitted</source>
        <translation>Se abilitata un suono verrà emesso quando un nuovo messaggio arriverà</translation>
    </message>
    <message>
        <source>Generate automatic filename</source>
        <translation>Genera automaticamente il nome dei file</translation>
    </message>
    <message>
        <source>If the file to be downloaded already exists a new filename is automatically generated</source>
        <translation>Se il file esiste già automaticamente ne verrà creato uno con un nuovo nome</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation>Faccine</translation>
    </message>
    <message>
        <source>Add your preferred emoticon to the message</source>
        <translation>Aggiungi la tua faccina preferita al messaggio</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
    <message>
        <source>Select your status</source>
        <translation>Seleziona il tuo stato</translation>
    </message>
    <message>
        <source>Your status will be %1</source>
        <translation>Il tuo stato sarà %1</translation>
    </message>
    <message>
        <source>Add a status description...</source>
        <translation type="obsolete">Aggiungi una descrizione al tuo stato...</translation>
    </message>
    <message>
        <source>&amp;?</source>
        <translation type="obsolete">&amp;?</translation>
    </message>
    <message>
        <source>Tips of the day</source>
        <translation type="obsolete">Suggerimenti</translation>
    </message>
    <message>
        <source>Show me the tip of the day</source>
        <translation>Mostrami il suggerimento del giorno</translation>
    </message>
    <message>
        <source>About &amp;Qt...</source>
        <translation type="obsolete">Informazioni su &amp;Qt...</translation>
    </message>
    <message>
        <source>Show the informations about Qt library</source>
        <translation>Mostra le informazioni sulla libreria Qt</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <source>Users</source>
        <translation>Utenti</translation>
    </message>
    <message>
        <source>Show online users and active chats</source>
        <translation type="obsolete">Mostra gli utenti connessi</translation>
    </message>
    <message>
        <source>Show the list of the connected users and the active chats</source>
        <translation type="obsolete">Mostra la liste degli utenti connessi e le relative conversazioni attive</translation>
    </message>
    <message>
        <source>File Transfers</source>
        <translation>Trasferimento file</translation>
    </message>
    <message>
        <source>Show the file transfers</source>
        <translation type="obsolete">Mostra i trasferimenti dei file</translation>
    </message>
    <message>
        <source>Show the list of the file transfers</source>
        <translation>Mostra la lista dei trasferimenti dei file</translation>
    </message>
    <message>
        <source>User not found.</source>
        <translation>Utente non trovato.</translation>
    </message>
    <message>
        <source>Please select a file to save the messages of the chat.</source>
        <translation type="obsolete">Seleziona un file dove salvare la conversazione in corso.</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="obsolete">Attenzione</translation>
    </message>
    <message>
        <source>%1: unable to save the messages.
Please check the file or the directories write permissions.</source>
        <translation type="obsolete">%1: impossibile salvare i messaggi.
Controlla i permessi di scrittura nella cartella selezionata.</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="obsolete">Informazione</translation>
    </message>
    <message>
        <source>%1: save completed.</source>
        <translation type="obsolete">%1: salvataggio completato.</translation>
    </message>
    <message>
        <source>Please insert the Host Address or Broadcast Address to contact
(ex. 10.184.15.186 or 10.184.15.255)</source>
        <translation type="obsolete">Inserisci l&apos;indirizzo IP o la sottorete in cui cercare nuovi utenti
(es. 10.184.15.186 oppure 10.184.15.255)</translation>
    </message>
    <message>
        <source>You have selected an invalid host address.</source>
        <translation type="obsolete">Hai inserito un indirizzo IP o di sottorete non valido.</translation>
    </message>
    <message>
        <source>%1 is writing...</source>
        <translation>%1 sta scrivendo...</translation>
    </message>
    <message>
        <source>Please insert the new status description</source>
        <translation>Inserisci una descrizione per il tuo stato</translation>
    </message>
    <message>
        <source>There is no user connected.</source>
        <translation>Non ci sono utenti collegati.</translation>
    </message>
    <message>
        <source>Please select the user to whom you would like to send a file.</source>
        <translation>Seleziona l&apos;utente a cui vuoi inviare il file.</translation>
    </message>
    <message>
        <source>User %1 not found.</source>
        <translation type="obsolete">L&apos;utente %1 non è stato trovato.</translation>
    </message>
    <message>
        <source>%1 - Send a file to %2</source>
        <translation type="obsolete">%1 - Invia un file a %2</translation>
    </message>
    <message>
        <source>Do you want to download from %1
%2 (%3)?</source>
        <translation type="obsolete">Vuoi scaricare da %1
il file %2 (%3)?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>%1 already exists. Please select a new filename.</source>
        <translation>%1 esiste già. Seleziona un nuovo nome per il file.</translation>
    </message>
    <message>
        <source>%1 - Select the download folder</source>
        <translation>%1 - Seleziona la cartella dove salvare i file scaricati</translation>
    </message>
    <message>
        <source>%1 - Profile</source>
        <translation type="obsolete">%1 - Profilo</translation>
    </message>
    <message>
        <source>Use HTML tags</source>
        <translation>Usa tag HTML</translation>
    </message>
    <message>
        <source>If enabled HTML tags are not removed from the message</source>
        <translation>Se abilitata i tag HTML non saranno rimossi dai messaggi</translation>
    </message>
    <message>
        <source>Use clickable links</source>
        <translation>Mostra i link cliccabili</translation>
    </message>
    <message>
        <source>If enabled the links in the message are recognized and made clickable</source>
        <translation>Se abilitata i link nei messaggi saranno riconosciuti e resi cliccabili</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>&lt;p&gt;Plugin &lt;b&gt;%1&lt;/b&gt; developed by &lt;b&gt;%2&lt;/b&gt;.&lt;br /&gt;&lt;i&gt;%3&lt;/i&gt;&lt;/p&gt;&lt;br /&gt;</source>
        <translation type="obsolete">&lt;p&gt;&lt;b&gt;%1&lt;/b&gt; è un plugin sviluppato da &lt;b&gt;%2&lt;/b&gt;.&lt;br /&gt;&lt;i&gt;%3&lt;/i&gt;&lt;/p&gt;&lt;br /&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;&lt;b&gt;%1&lt;/b&gt; is a plugin developed by &lt;b&gt;%2&lt;/b&gt;.&lt;br /&gt;&lt;i&gt;%3&lt;/i&gt;&lt;/p&gt;&lt;br /&gt;</source>
        <translation type="obsolete">&lt;p&gt;&lt;b&gt;%1&lt;/b&gt; è un plugin sviluppato da &lt;b&gt;%2&lt;/b&gt;.&lt;br /&gt;&lt;i&gt;%3&lt;/i&gt;&lt;/p&gt;&lt;br /&gt;</translation>
    </message>
    <message>
        <source>Plugin Manager...</source>
        <translation>Gestione dei Plugin...</translation>
    </message>
    <message>
        <source>Open the plugin manager dialog and manage the installed plugins</source>
        <translation>Apri la finestra per gestire i plugin installati</translation>
    </message>
    <message>
        <source>is a plugin developed by</source>
        <translation>è un plugin sviluppato da</translation>
    </message>
    <message>
        <source>Network account...</source>
        <translation type="obsolete">Account di rete...</translation>
    </message>
    <message>
        <source>Show the network account dialog</source>
        <translation type="obsolete">Mostra la finestra per configurare il tuo account di rete</translation>
    </message>
    <message>
        <source>Network settings...</source>
        <translation type="obsolete">Opzioni di rete...</translation>
    </message>
    <message>
        <source>Show the network settings dialog</source>
        <translation type="obsolete">Mostra la finestra per impostare le opzioni di rete</translation>
    </message>
    <message>
        <source>Network account</source>
        <translation type="obsolete">Account di rete</translation>
    </message>
    <message>
        <source>Show the %1 login</source>
        <translation type="obsolete">Mostra la finestra di connessione a %1</translation>
    </message>
    <message>
        <source>Show only the online users</source>
        <translation>Mostra solo gli utenti connessi</translation>
    </message>
    <message>
        <source>If enabled only the online users are shown in the list</source>
        <translation>Se abilitata saranno visualizzati nella lista solo gli utenti connessi</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation>Conversazioni</translation>
    </message>
    <message>
        <source>Show the chat list</source>
        <translation type="obsolete">Mostra la lista delle conversazioni</translation>
    </message>
    <message>
        <source>Show the list of the chats</source>
        <translation>Mostra la lista delle conversazioni</translation>
    </message>
    <message>
        <source>Please insert the Host Address or Broadcast Address to contact
(ex. 10.184.15.186 or 10.184.15.255)
</source>
        <translation type="obsolete">Inserisci l&apos;indirizzo IP o la sottorete da contattare
(es. 10.184.15.186 o 10.184.15.255)</translation>
    </message>
    <message>
        <source>or insert a valid jabber id (ex: user@gmail.com)</source>
        <translation type="obsolete">o inserisci un identificativo valido (es: mario.rossi@gmail.com)</translation>
    </message>
    <message>
        <source>%1 (%2) wants to add you to the contact list. Do you accept?</source>
        <translation type="obsolete">%1 (%2) vuole aggiungerti alla sua lista di contatti. Accetti?</translation>
    </message>
    <message>
        <source>Do you really want to remove %1 from the contact list?</source>
        <translation type="obsolete">Vuoi realmente eliminare %1 dalla tua lista dei contatti?</translation>
    </message>
    <message>
        <source>Unable to remove %1 from the contact list.</source>
        <translation type="obsolete">Impossibile eliminare %1 dalla lista dei contatti.</translation>
    </message>
    <message>
        <source>Do you want to disconnect from %1 server?</source>
        <translation type="obsolete">Vuoi disconnetterti da %1?</translation>
    </message>
    <message>
        <source>You are %1%2</source>
        <translation>Il tuo stato è %1%2</translation>
    </message>
    <message>
        <source>You are connected to %1. Do you want to disconnect?</source>
        <translation type="obsolete">Vuoi disconnetterti da %1?</translation>
    </message>
    <message>
        <source>Unable to connect to %1. Plugin is not present or is not enabled.</source>
        <translation type="obsolete">Impossibile connettersi a %1. Il plugin non è presente o non è abilitato.</translation>
    </message>
    <message>
        <source>Show %1&apos;s license...</source>
        <translation>Licenza di %1...</translation>
    </message>
    <message>
        <source>Show the informations about %1&apos;s license</source>
        <translation>Mostra le informazioni sulla licenza di %1</translation>
    </message>
    <message>
        <source>Show emoticons</source>
        <translation type="obsolete">Mostra le faccine con delle immagini</translation>
    </message>
    <message>
        <source>If enabled the emoticons will be recognized and showed as images</source>
        <translation type="obsolete">Se abilitata le faccine saranno riconosciute e mostrate come immagini</translation>
    </message>
    <message>
        <source>Add or search &amp;user...</source>
        <translation type="obsolete">Aggiungi o cerca &amp;utenti...</translation>
    </message>
    <message>
        <source>Add or search an user in the %1 network</source>
        <translation type="obsolete">Aggiungi o cerca utenti nella rete di %1</translation>
    </message>
    <message>
        <source>Do you want to disconnect from %1 network?</source>
        <translation>Vuoi disconnetterti dalla rete di %1?</translation>
    </message>
    <message>
        <source>Minimize to tray icon</source>
        <translation type="obsolete">Minimizza ad icona di sistema</translation>
    </message>
    <message>
        <source>If enabled when the minimize button is clicked the window minimized to the system tray icon</source>
        <translation type="obsolete">Se abilitata quando il pulsante minimizza viene premuto la finestra sarà minimizzata tra le icone di sistema</translation>
    </message>
    <message>
        <source>%1 System Tray</source>
        <translation type="obsolete">Icona di Sistema di %1</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="obsolete">Apri</translation>
    </message>
    <message>
        <source>%1 will keep running in the background mode. To terminate the program, choose Quit in the context menu of the system tray icon.</source>
        <translation type="obsolete">%1 continuerà ad essere attivo anche se nascosto. Per chiudere il programma scegliere Esci dal menu dell&apos;icona di sistema.</translation>
    </message>
    <message>
        <source>Enable file sharing</source>
        <translation type="obsolete">Abilita la condivisione dei file</translation>
    </message>
    <message>
        <source>If enabled you can share files with the other users</source>
        <translation type="obsolete">Se abilitata puoi condividere i tuoi file con gli altri utenti</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="obsolete">&amp;Visualizza</translation>
    </message>
    <message>
        <source>Show the chat</source>
        <translation>Mostra la finestra delle conversazioni</translation>
    </message>
    <message>
        <source>Show the chat view</source>
        <translation>Mostra la finestra delle conversazioni</translation>
    </message>
    <message>
        <source>Show my shared files</source>
        <translation>Mostra i miei file condivisi</translation>
    </message>
    <message>
        <source>Show the list of the files which I have shared</source>
        <translation>Mostra la lista dei file che hai condiviso con gli altri utenti</translation>
    </message>
    <message>
        <source>Show the network shared files</source>
        <translation>Mostra i file condivisi presenti sulla rete</translation>
    </message>
    <message>
        <source>Show the list of the network shared files</source>
        <translation>Mostra la lista dei file condivisi dagli altri utenti sulla rete</translation>
    </message>
    <message>
        <source>Do you want to download %1 (%2) from the user %3?</source>
        <translation type="obsolete">Vuoi scaricare il file %1 (%2) dall&apos;utente %3?</translation>
    </message>
    <message>
        <source>Do you really want to open the file %1?</source>
        <translation>Vuoi aprire il file %1?</translation>
    </message>
    <message>
        <source>Unable to open %1</source>
        <translation>Impossibile aprire %1</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Show messages grouped by user</source>
        <translation>Mostra i messaggi raggruppati per utente</translation>
    </message>
    <message>
        <source>If enabled the messages will be showed grouped by user</source>
        <translation type="obsolete">Se abilitata i messaggi saranno raggruppati per utente</translation>
    </message>
    <message>
        <source>Stay on top</source>
        <translation type="obsolete">Mostra sempre in primo piano</translation>
    </message>
    <message>
        <source>If enabled %1 stays on top of the other windows</source>
        <translation>Se abilitata %1 starà sempre in primo piano</translation>
    </message>
    <message>
        <source>Close to tray icon</source>
        <translation type="obsolete">Chiudi ad icona di sistema</translation>
    </message>
    <message>
        <source>If enabled when the close button is clicked the window minimized to the system tray icon</source>
        <translation type="obsolete">Se abilitata quando il pulsante chiudi viene premuto la finestra sarà minimizzata tra le icone di sistema</translation>
    </message>
    <message>
        <source>%1 will keep running in the background mode</source>
        <translation type="obsolete">%1 continuerà ad essere attivo in modalità nascosta</translation>
    </message>
    <message>
        <source>Select beep file...</source>
        <translation>Seleziona un suono per il beep...</translation>
    </message>
    <message>
        <source>Select the file to play on new message arrived</source>
        <translation>Seleziona un file da suonare quando arriva un nuovo messaggio</translation>
    </message>
    <message>
        <source>Play beep</source>
        <translation>Prova il beep</translation>
    </message>
    <message>
        <source>Test the file to play on new message arrived</source>
        <translation>Prova il flle da suonare all&apos;arrivo di un nuovo messaggio</translation>
    </message>
    <message>
        <source>Sound files (*.wav)</source>
        <translation>Suoni (*.wav)</translation>
    </message>
    <message>
        <source>Sound is not enabled on a new message. Do you want to enable it?</source>
        <translation>Il suono non è abilitato all&apos;arrivo di un nuovo messaggio. Vuoi attivarlo?</translation>
    </message>
    <message>
        <source>Sound file %1 not found.</source>
        <translation type="obsolete">Il file sonoro %1 non è stato trovato.</translation>
    </message>
    <message>
        <source>Create/edit group chat...</source>
        <translation type="obsolete">Crea/modifica gruppo...</translation>
    </message>
    <message>
        <source>Create or edit a group chat</source>
        <translation type="obsolete">Crea o modifica una chat di gruppo</translation>
    </message>
    <message>
        <source>Create group chat...</source>
        <translation type="obsolete">Crea una chat di gruppo...</translation>
    </message>
    <message>
        <source>Create a group chat with two or more users</source>
        <translation type="obsolete">Crea una chat di gruppo con due o più persone</translation>
    </message>
    <message>
        <source>Edit group chat...</source>
        <translation type="obsolete">Modifica la chat di gruppo...</translation>
    </message>
    <message>
        <source>Change the name of the group or add users</source>
        <translation type="obsolete">Cambia il nome del gruppo e aggiungi altri utenti</translation>
    </message>
    <message>
        <source>Impossibile to add users in this chat. Please select a group one.</source>
        <translation type="obsolete">Impossibile aggiungee utenti a questa conversazione. Seleziona prima una chat di gruppo.</translation>
    </message>
    <message>
        <source>Raise on new message arrived</source>
        <translation type="obsolete">Porta in primo piano all&apos;arrivo di un nuovo messaggio</translation>
    </message>
    <message>
        <source>If enabled when a new message is arrived %1 is shown on top of all other windows</source>
        <translation>Se abilitata %1 sarà mostrato in primo piano all&apos;arrivo di ogni nuovo messaggio</translation>
    </message>
    <message>
        <source>Load %1 on Windows startup</source>
        <translation>Carica %1 alla partenza di Windows</translation>
    </message>
    <message>
        <source>If enabled you can automatically load %1 at system startup</source>
        <translation>Se abilitata %1 sarà lanciato automaticamente alla partenza di Windows</translation>
    </message>
    <message>
        <source>Now %1 will start on windows boot.</source>
        <translation>%1 sarà avviato alla partenza di Windows.</translation>
    </message>
    <message>
        <source>%1 will not start on windows boot.</source>
        <translation>%1 non sarà più avviato automaticamente alla partenza di Windows.</translation>
    </message>
    <message>
        <source>Prompts for network password on startup</source>
        <translation>Richiedi la password ad ogni connessione</translation>
    </message>
    <message>
        <source>If enabled the password dialog will be showed on connection startup</source>
        <translation type="obsolete">Se abilitata verrà mostrata la finestra per inserire la password ad ogni connessione</translation>
    </message>
    <message>
        <source>Please save the network password in the next dialog.</source>
        <translation>Inserisci e memorizza la password di rete nella prossima finestra.</translation>
    </message>
    <message>
        <source>Show the %1 log</source>
        <translation>Mostra il log di %1</translation>
    </message>
    <message>
        <source>Show the application log to see if an error occurred</source>
        <translation>Mostra i log dell&apos;applicazione per vedere se ci sono stati errori</translation>
    </message>
    <message>
        <source>If enabled the emoticons will be recognized and shown as images</source>
        <translation type="obsolete">Se abilitata le faccine saranno riconosciute e visualizzate come immagini</translation>
    </message>
    <message>
        <source>If enabled the messages will be shown grouped by user</source>
        <translation>Se abilitata i messaggi verranno visualizzati raggruppati per utente</translation>
    </message>
    <message>
        <source>If enabled the password dialog will be shown on connection startup</source>
        <translation>Se abilitata verrà mostrato all&apos;avvio la finestra con la richiesta della password</translation>
    </message>
    <message>
        <source>Save messages</source>
        <translation>Salva i messaggi</translation>
    </message>
    <message>
        <source>If enabled the messages are saved when the program is closed</source>
        <translation>Se abilitata i messaggi saranno salvati al termine del programma</translation>
    </message>
    <message>
        <source>&amp;Chat</source>
        <translation type="obsolete">&amp;Chat</translation>
    </message>
    <message>
        <source>History</source>
        <translation>Storia</translation>
    </message>
    <message>
        <source>Show the saved chat list</source>
        <translation type="obsolete">Mostra la lista delle conversazioni salvate</translation>
    </message>
    <message>
        <source>Show the list of the saved chats</source>
        <translation>Mostra la lista delle conversazioni salvate</translation>
    </message>
    <message>
        <source>Enable tray icon notification</source>
        <translation>Abilita le nofiche all&apos;icona di sistema </translation>
    </message>
    <message>
        <source>If enabled tray icon shows some notification about status and message</source>
        <translation>Se abilitata l&apos;icona di sistema mostrerà notifiche sullo stato e sui messaggi</translation>
    </message>
    <message>
        <source>Check for new version...</source>
        <translation>Controlla gli aggiornamenti...</translation>
    </message>
    <message>
        <source>Open %1 website and check if a new version exists</source>
        <translation>Apri il sito di %1 e controlla se esistono degli aggiornamenti</translation>
    </message>
    <message>
        <source>Play %1</source>
        <translation>Gioca a %1</translation>
    </message>
    <message>
        <source>is a game developed by</source>
        <translation>è un gioco sviluppato da</translation>
    </message>
    <message>
        <source>About %1</source>
        <translation>Informazioni su %1</translation>
    </message>
    <message>
        <source>Set away status when idle</source>
        <translation type="obsolete">Cambia lo stato in assente automaticamente</translation>
    </message>
    <message>
        <source>If enabled %1 set your status to away after an idle of %2 minutes</source>
        <translation type="obsolete">Se abilitata %1 metterà automaticamente il tuo stato ad assente quando sarai assente da almeno %2 minuti</translation>
    </message>
    <message>
        <source>Make a screenshot</source>
        <translation>Fai una foto allo schermo</translation>
    </message>
    <message>
        <source>Show the utility to capture a screenshot</source>
        <translation>Mostra l&apos;interfaccia per fare una foto allo schermo</translation>
    </message>
    <message>
        <source>Open %1 official website...</source>
        <translation>Apri il sito ufficiale di %1...</translation>
    </message>
    <message>
        <source>Explore %1 official website</source>
        <translation>Esplosa il sito ufficiale di %1</translation>
    </message>
    <message>
        <source>Show the main tool bar</source>
        <translation>Mostra la barra degli strumenti</translation>
    </message>
    <message>
        <source>Show the bar of plugins</source>
        <translation type="obsolete">Mostra la barra dei plugin</translation>
    </message>
    <message>
        <source>Show the tool bar with plugin shortcuts</source>
        <translation type="obsolete">Mostra la barra con i collegamenti ai plugin</translation>
    </message>
    <message>
        <source>Download plugins...</source>
        <translation>Scarica i plugin...</translation>
    </message>
    <message>
        <source>Open %1 website and download your preferred plugin</source>
        <translation>April sito ufficiale di %1 e scarica i tuoi plugin preferiti</translation>
    </message>
    <message>
        <source>Please select a chat you would like to link the saved text.</source>
        <translation>Seleziona una conversazione a cui vuoi associare il testo salvato.</translation>
    </message>
    <message>
        <source>The chat &apos;%1&apos; selected has already a saved text.&lt;br /&gt;What do you want to do with the selected saved text?</source>
        <translation>La conversazione &apos;%1&apos; selezionata ha già una storia salvata. &lt;br /&gt;Cosa vuoi fare con la storia selezionata?</translation>
    </message>
    <message>
        <source>Overwrite</source>
        <translation>Sovrascrivi</translation>
    </message>
    <message>
        <source>Add in the head</source>
        <translation>Aggiungi in cima</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Secure Lan Messenger</source>
        <translation></translation>
    </message>
    <message>
        <source>Show the bar of file sharing</source>
        <translation type="obsolete">Mostra del barra della condivisione dei file</translation>
    </message>
    <message>
        <source>New message from %1</source>
        <translation>Nuovo messaggio da %1</translation>
    </message>
    <message>
        <source>New message arrived</source>
        <translation>Nuovo messaggio arrivato</translation>
    </message>
    <message>
        <source>File is not available for download. User is offline.</source>
        <translation type="obsolete">Il file non è disponibile per il download. L&apos;utente non è più connesso.</translation>
    </message>
    <message>
        <source>Show the bar of local file sharing</source>
        <translation>Mostra la barra dei tuoi file condivisi</translation>
    </message>
    <message>
        <source>Show the bar of network file sharing</source>
        <translation>Mostra la barra di tutti i file condivisi in rete</translation>
    </message>
    <message>
        <source>Show the main tool bar with settings</source>
        <translation>Mostra la barra principale con le opzioni</translation>
    </message>
    <message>
        <source>Show the chat tool bar</source>
        <translation type="obsolete">Mostra la barra della conversazione</translation>
    </message>
    <message>
        <source>Show the bar of screenshot plugin</source>
        <translation>Mostra la barra del cattura schermo</translation>
    </message>
    <message>
        <source>Yes, and don&apos;t ask anymore</source>
        <translation>Si e non chiedermelo più</translation>
    </message>
    <message>
        <source>Chat with %1 is empty.</source>
        <translation>La conversazione con %1 è vuota.</translation>
    </message>
    <message>
        <source>Do you really want to clear messages with %1?</source>
        <translation>Vuoi cancellare i messaggi con %1?</translation>
    </message>
    <message>
        <source>Yes and delete history</source>
        <translation>Si e cancella la storia</translation>
    </message>
    <message>
        <source>Please donate for %1 :-)</source>
        <translation type="obsolete">Per favore dona qualcosa per %1 :-)</translation>
    </message>
    <message>
        <source>I&apos;m so grateful and pleased about that</source>
        <translation>Grazieeeee! Lo apprezzo molto</translation>
    </message>
    <message>
        <source>Show online users</source>
        <translation type="obsolete">Mostra utenti connessi</translation>
    </message>
    <message>
        <source>Show the list of the connected users</source>
        <translation>Moastra la lista degli utenti connessi</translation>
    </message>
    <message>
        <source>Groups</source>
        <translation>Gruppi</translation>
    </message>
    <message>
        <source>Show your groups</source>
        <translation type="obsolete">Mostra i tuoi gruppi</translation>
    </message>
    <message>
        <source>Show the list of your groups</source>
        <translation>Mostra la lista dei tuoi gruppi</translation>
    </message>
    <message>
        <source>Unable to add users in this chat. Please select a group one.</source>
        <translation>Impossibile aggiungere utenti a questa conversazione. Selezionane una di gruppo.</translation>
    </message>
    <message>
        <source>%1 is a your group. You can not leave the chat.</source>
        <translation>%1 è un gruppo creato da te, non puoi lasciare la conversazione.</translation>
    </message>
    <message>
        <source>Delete this group</source>
        <translation>Cancella questo gruppo</translation>
    </message>
    <message>
        <source>You cannot leave this chat.</source>
        <translation>Non puoi lasciare questa conversazione.</translation>
    </message>
    <message>
        <source>Do you really want to delete group &apos;%1&apos;?</source>
        <translation>Vuoi veramente cancellare il gruppo &apos;%1&apos;?</translation>
    </message>
    <message>
        <source>developed by</source>
        <translation>sviluppato da</translation>
    </message>
    <message>
        <source>How many minutes of idle %1 can wait before changing status to away?</source>
        <translation>Quanti minuti di inattività deve aspettare %1 prima di cambiare lo stato in assente?</translation>
    </message>
    <message>
        <source>%1 - Select a file</source>
        <translation>%1 - Seleziona un file</translation>
    </message>
    <message>
        <source>Unable to delete this chat.</source>
        <translation>Impossibile cancellare questa conversazione.</translation>
    </message>
    <message>
        <source>%1 has shared %2 files</source>
        <translation>%1 ha condiviso %2 file</translation>
    </message>
    <message>
        <source>Settings can not be saved. Do you want to continue?</source>
        <translation type="obsolete">Le preferenze non possono essere salvate. Vuoi continuare?</translation>
    </message>
    <message>
        <source>Settings can not be saved in path:</source>
        <translation type="obsolete">Le preferenze non possono essere salvate nel percorso seguente:</translation>
    </message>
    <message>
        <source>Do you want to close anyway?</source>
        <translation>Vuoi chiudere ugualmente il programma?</translation>
    </message>
    <message>
        <source>Settings can not be saved.</source>
        <translation type="obsolete">Le preferenze non possono essere salvate.</translation>
    </message>
    <message>
        <source>is not writable</source>
        <translation type="obsolete">non è un percorso scrivibile</translation>
    </message>
    <message>
        <source>&lt;b&gt;Settings can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Le preferenze non possono essere salvate&lt;/b&gt;. Il percorso:</translation>
    </message>
    <message>
        <source>&lt;b&gt;is not writable&lt;/b&gt; by user:</source>
        <translation>&lt;b&gt;non è scrivibile&lt;/b&gt; dall&apos;utente:</translation>
    </message>
    <message>
        <source>Tip of the day</source>
        <translation>Suggerimenti</translation>
    </message>
    <message>
        <source>Fact of the day</source>
        <translation>Perle di saggezza</translation>
    </message>
    <message>
        <source>Show me the fact of the day</source>
        <translation>Mostrami la perla di saggezza quotidiana</translation>
    </message>
    <message>
        <source>Select language...</source>
        <translation>Seleziona una lingua...</translation>
    </message>
    <message>
        <source>Select your preferred language</source>
        <translation>Seleziona la traduzione che vuoi che il programma usi</translation>
    </message>
    <message>
        <source>Enable file transfer</source>
        <translation>Abilita il trasferimento dei file</translation>
    </message>
    <message>
        <source>If enabled you can transfer files with the other users</source>
        <translation>Se abilitata puoi inviare e ricevere file dagli altri utenti connessi</translation>
    </message>
    <message>
        <source>Help online...</source>
        <translation>Aiuto online...</translation>
    </message>
    <message>
        <source>Open %1 website to have online support</source>
        <translation>Apri il sito ufficiale di %1 per avere supporto in linea</translation>
    </message>
    <message>
        <source>File transfer is disabled. You cannot download %1.</source>
        <translation>Il trasferimento dei file è disabilitato. Non puoi scaricare %1.</translation>
    </message>
    <message>
        <source>Default language is restored.</source>
        <translation>La lingua predefinita è stata ripristinata.</translation>
    </message>
    <message>
        <source>New language &apos;%1&apos; is selected.</source>
        <translation>La nuova traduzione &apos;%1&apos; è stata selezionata.</translation>
    </message>
    <message>
        <source>You must restart %1 to apply these changes.</source>
        <translation>Devi chiudere e riaprire %1 per applicare i cambiamenti.</translation>
    </message>
    <message>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br /&gt;it under the terms of the GNU General Public License as published&lt;br /&gt;by the Free Software Foundation, either version 3 of the License&lt;br /&gt;or (at your option) any later version.&lt;br /&gt;&lt;br /&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br /&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br /&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br /&gt;See the GNU General Public License for more details.</source>
        <translation></translation>
    </message>
    <message>
        <source>Sound file not found</source>
        <translation>Il file sonoro non è stato trovato</translation>
    </message>
    <message>
        <source>The default BEEP will be used.</source>
        <translation type="obsolete">Il BEEP di sistema sarà usato.</translation>
    </message>
    <message>
        <source>The default BEEP will be used</source>
        <translation>Verrà usato il BEEP di sistema</translation>
    </message>
    <message>
        <source>Sound module is not working. The default BEEP will be used.</source>
        <translation>Il modulo sonoro non funziona. Verrà usato il BEEP di sistema.</translation>
    </message>
    <message>
        <source>Try to refresh the list of shared files.</source>
        <translation type="obsolete">Prova a ricaricare la lista dei file condivisi.</translation>
    </message>
    <message>
        <source>File is not available for download.</source>
        <translation>Il file non è disponibile per essere scaricato.</translation>
    </message>
    <message>
        <source>%1 is not connected.</source>
        <translation>%1 non è in linea.</translation>
    </message>
    <message>
        <source>Please reload the list of shared files.</source>
        <translation>Ricarica la lista dei file condivisi.</translation>
    </message>
    <message>
        <source>Reload file list</source>
        <translation>Ricarica la lista</translation>
    </message>
    <message>
        <source>Do you want to download %1 (%2) from %3?</source>
        <translation>Vuoi scaricare %1 (%2) da %3?</translation>
    </message>
    <message>
        <source>for</source>
        <translation>per</translation>
    </message>
    <message>
        <source>Show a picture of the users in list</source>
        <translation type="obsolete">Mostra l&apos;immagine degli utenti in lista</translation>
    </message>
    <message>
        <source>If enabled you can see a picture of the users in the list (if they have)</source>
        <translation>Se abilitata puoi vedere un&apos;immagine degli utenti in lista (se l&apos;hanno pubblicata)</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation>Disconnetti</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation>Connetti</translation>
    </message>
    <message>
        <source>Configure network...</source>
        <translation type="obsolete">Configura la rete...</translation>
    </message>
    <message>
        <source>Configure %1 network to search a user who is not in your local subnet</source>
        <translation>Configura la impostazioni di %1 per cercare gli utenti che non sono nella tua sottorete</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>About %1...</source>
        <translation>Informazioni su %1...</translation>
    </message>
    <message>
        <source>Main</source>
        <translation>Generale</translation>
    </message>
    <message>
        <source>Broadcast to network</source>
        <translation>Cerca nella rete</translation>
    </message>
    <message>
        <source>Broadcast a message in your network to find available users</source>
        <translation>Manda un messaggio nella tua rete per individuare altri utenti connessi</translation>
    </message>
    <message>
        <source>Show colored nickname</source>
        <translation>Mostra i nickname colorati</translation>
    </message>
    <message>
        <source>Show the user&apos;s picture</source>
        <translation>Mostra la foto dell&apos;utente</translation>
    </message>
    <message>
        <source>Set Away status automatically</source>
        <translation type="obsolete">Cambia lo stato in Assente automaticamente</translation>
    </message>
    <message>
        <source>System</source>
        <translation type="obsolete">Sistema</translation>
    </message>
    <message>
        <source>Enable BEEP alert on new message</source>
        <translation>Abilita il BEEP sonoro all&apos;arrivo di un messaggio</translation>
    </message>
    <message>
        <source>Raise on top on new message</source>
        <translation>Porta in primo piano all&apos;arrivo di un messaggio</translation>
    </message>
    <message>
        <source>Always stay on top</source>
        <translation>Mostra sempre in primo piano</translation>
    </message>
    <message>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <source>View</source>
        <translation>Visualizza</translation>
    </message>
    <message>
        <source>?</source>
        <translation></translation>
    </message>
    <message>
        <source>Set status to away automatically</source>
        <translation>Cambia il tuo stato in assente automaticamente</translation>
    </message>
    <message>
        <source>If enabled %1 change your status to away after an idle of %2 minutes</source>
        <translation>%1 cambierà il tuo stato in assente automaticamente quando non userai il computer per %2 minuti</translation>
    </message>
    <message>
        <source>Edit your profile...</source>
        <translation>Modifica il tuo profilo...</translation>
    </message>
    <message>
        <source>Change your profile information like your picture or your email or phone number</source>
        <translation>Completa le informazioni del tuo profilo con la tua email il tuo numero di telefono o la tua immagine</translation>
    </message>
    <message>
        <source>Show send message icon</source>
        <translation type="obsolete">Mostra l&apos;icona per inviare messaggi</translation>
    </message>
    <message>
        <source>If enabled the icon of send message is shown in chat window</source>
        <translation type="obsolete">Se abilitata l&apos;icona per inviare i messaggi sarà visibile nella finestra delle conversazioni</translation>
    </message>
    <message>
        <source>Search for users...</source>
        <translation>Cerca utenti...</translation>
    </message>
    <message>
        <source>Show the chat toolbar</source>
        <translation type="obsolete">Mostra la barra della conversazione</translation>
    </message>
    <message>
        <source>Show the chat toolbar with emoticons, font and colors</source>
        <translation type="obsolete">Mostra la barra della conversazione con le faccine, i font ed i colori</translation>
    </message>
    <message>
        <source>Show the user panel</source>
        <translation>Mostra il pannello degli utenti</translation>
    </message>
    <message>
        <source>Show the group panel</source>
        <translation>Mostra il pannello dei gruppi</translation>
    </message>
    <message>
        <source>Show the chat panel</source>
        <translation>Mostra il pannello delle conversazioni</translation>
    </message>
    <message>
        <source>Show the history panel</source>
        <translation>Mostra il pannello della storia</translation>
    </message>
    <message>
        <source>Show the file transfer panel</source>
        <translation>Mostra il pannello del trasferimento dei file</translation>
    </message>
    <message>
        <source>Show the bar of chat</source>
        <translation>Mostra la barra delle conversazioni</translation>
    </message>
    <message>
        <source>Show the toolbar</source>
        <translation type="obsolete">Mostra la barra degli strumenti</translation>
    </message>
    <message>
        <source>Add users manually...</source>
        <translation>Aggiungi utenti manualmente...</translation>
    </message>
    <message>
        <source>Add the IP address and the port of the users you want to connect</source>
        <translation>Aggiungi l&apos;indirizzo IP e la porta degli utenti a cui vuoi connetterti</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Preferenze</translation>
    </message>
    <message>
        <source>Load on system tray at startup</source>
        <translation>Mostra solo icona di sitema alla partenza</translation>
    </message>
    <message>
        <source>If enabled %1 will be start hided in system tray</source>
        <translation>Se abilitata %1 partirà nascosto nell&apos;icona di sistema</translation>
    </message>
    <message>
        <source>Qt Library...</source>
        <translation>Librerie Qt...</translation>
    </message>
    <message>
        <source>Show</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Chiudi</translation>
    </message>
    <message>
        <source>Used Ports</source>
        <translation type="obsolete">Porte usate</translation>
    </message>
    <message>
        <source>disabled</source>
        <translation>disabilitato</translation>
    </message>
    <message>
        <source>Show %1 home</source>
        <translation>Mostra la pagina iniziale di %1</translation>
    </message>
    <message>
        <source>Show the homepage with %1 activity</source>
        <translation>Mostra la pagina iniziale con le attività di %1</translation>
    </message>
    <message>
        <source>You cannot download all these files at once. Do you want to download the first %1 files of the list?</source>
        <translation>Non puoi scaricare tutti questi file in una volta sola. Vuoi scaricare i primi %1 della lista?</translation>
    </message>
    <message>
        <source>Downloading %1 files is a hard duty. Maybe you have to wait a lot of minutes. Do yo want to continue?</source>
        <translation>Scaricare %1 file è un lavoro dispendioso. Potrebbero volerci molti minuti. Vuoi continuare?</translation>
    </message>
    <message>
        <source>%1 files are scheduled for download</source>
        <translation>%1 file sono stati messi in coda per essere scaricati</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>Opzioni</translation>
    </message>
    <message>
        <source>Show the user&apos;s vCard on right click</source>
        <translation>Mostra il profilo degli utenti con il pulsante destro</translation>
    </message>
    <message>
        <source>If enabled you can see the user&apos;s vCard when right click on it</source>
        <translation>Se abilitata potrai vedere il profilo degli utenti premento il tasto destro del mouse</translation>
    </message>
    <message>
        <source>Reset window geometry at startup</source>
        <translation>Reimposta le dimensioni della finestra all&apos;avvio</translation>
    </message>
    <message>
        <source>If enabled the window geometry will be reset to default value at next startup</source>
        <translation type="obsolete">Se abilitata le dimensioni della finestra saranno ripristinate a quelle iniziali nel prossimo avvio</translation>
    </message>
    <message>
        <source>Show only last %1 messages</source>
        <translation>Mostra solo gli ultimi %1 messaggi</translation>
    </message>
    <message>
        <source>If enabled only the last %1 messages will be shown in chat</source>
        <translation>Se alibilitata nella conversazione saranno mostrati solo gli ultimi %1 messaggi</translation>
    </message>
    <message>
        <source>Please select the maximum number of messages to be showed</source>
        <translation>Seleziona il massimo numero di messaggi da mostrare</translation>
    </message>
    <message>
        <source>Smiley</source>
        <translation type="obsolete">Smiley</translation>
    </message>
    <message>
        <source>Show the emoticon panel</source>
        <translation>Mostra il pannello delle faccine</translation>
    </message>
    <message>
        <source>Group chat will be deleted when all members goes offline.</source>
        <translation>La conversazione di gruppo sarà cancellata non appena tutti i membri saranno disconnessi.</translation>
    </message>
    <message>
        <source>If you want a persistent chat please consider to make a Group instead.</source>
        <translation>Se vuoi una conversazione persistente considera invece di creare un gruppo.</translation>
    </message>
    <message>
        <source>Do you wish to continue or create group?</source>
        <translation>Vuoi continuare o creare un gruppo?</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <source>Create Group</source>
        <translation>Create Gruppo</translation>
    </message>
    <message>
        <source>Close button minimize to tray icon</source>
        <translation>Il pulsante chiudi minimizzerà tutto ad icona di sistema</translation>
    </message>
    <message>
        <source>If enabled when the close button is clicked the window will be minimized to the system tray icon</source>
        <translation>Se abilitata quando il pulsante chiudi sarà premuto la finestra verrà minimizzata ad icona di sistema</translation>
    </message>
    <message>
        <source>Escape key minimize to tray icon</source>
        <translation>Il tasto ESC minimizzerà tutto ad icona di sistema</translation>
    </message>
    <message>
        <source>If enabled when the escape button is clicked the window will be minimized to the system tray icon</source>
        <translation>Se abilitata quando premerai il tasto ESC la finestra sarà minimizzata ad icona di sistema</translation>
    </message>
    <message>
        <source>Prompt before downloading file</source>
        <translation>Chiedi sempre prima di scaricare un file</translation>
    </message>
    <message>
        <source>If enabled you have to confirm the action before downloading a file</source>
        <translation>Se abilitata dovrai sempre confermare di voler scaricare un file</translation>
    </message>
    <message>
        <source>Show the timestamp</source>
        <translation>Mostra l&apos;orario</translation>
    </message>
    <message>
        <source>Parse Unicode and ASCII emoticons</source>
        <translation>Interpreta le faccine Ascii e Unicode</translation>
    </message>
    <message>
        <source>If enabled the ASCII emoticons will be recognized and shown as images</source>
        <translation>Se abilitata le faccine in ASCII e Unicode saranno riconosciute e mostrate come immagini</translation>
    </message>
    <message>
        <source>Use native emoticons</source>
        <translation>Usa le faccine native</translation>
    </message>
    <message>
        <source>If enabled the emoticons will be parsed by your system font</source>
        <translation>Se abilitata le faccine saranno mostrate usando il carattere di sistema</translation>
    </message>
    <message>
        <source>Do you want to download folder %1 (%2 files) from %3?</source>
        <translation>Vuoi scaricare la cartella %1 (%2 file) da %3?</translation>
    </message>
    <message>
        <source>If enabled the window geometry will be reset to default value at the next startup</source>
        <translation>Se abilitata le dimensioni della finestra saranno ripristinate a quelle iniziali nel prossimo avvio</translation>
    </message>
    <message>
        <source>Save the users on exit</source>
        <translation>Salva gli utenti all&apos;uscita</translation>
    </message>
    <message>
        <source>If enabled the user list will be save on exit</source>
        <translation>Se abilitata la lista degli utenti verrà salvata all&apos;uscita</translation>
    </message>
    <message>
        <source>%1 is online</source>
        <translation>%1 è in linea</translation>
    </message>
    <message>
        <source>%1 is offline</source>
        <translation>%1 non è più in linea</translation>
    </message>
    <message>
        <source>Show preview of the images</source>
        <translation>Mostra l&apos;anteprima delle immagini</translation>
    </message>
    <message>
        <source>If enabled the preview of the downloaded images will be showed in the chat window</source>
        <translation>Se abilitata l&apos;anteprima delle immagini scaricate sarà mostrata nella conversazione</translation>
    </message>
    <message>
        <source>or more</source>
        <translation>o più di uno</translation>
    </message>
    <message>
        <source>Change size of the user&apos;s picture</source>
        <translation>Cambia la dimensione della foto dell&apos;utente</translation>
    </message>
    <message>
        <source>Click to change the picture size of the users in the list</source>
        <translation>Clicca qui per cambiare la dimensione delle foto degli utenti in lista</translation>
    </message>
    <message>
        <source>When do you want %1 to play beep?</source>
        <translation type="obsolete">Quando vuoi che %1 ti avverta con un suono?</translation>
    </message>
    <message>
        <source>If it not visible</source>
        <translation type="obsolete">Se non visibile</translation>
    </message>
    <message>
        <source>Always</source>
        <translation>Sempre</translation>
    </message>
    <message>
        <source>Do you really want to delete chat with %1?</source>
        <translation>Vuoi cancellare la conversazione con %1?</translation>
    </message>
    <message>
        <source>Please select the new size of the user picture</source>
        <translation>Seleziona la nuova dimensione della foto degli utenti</translation>
    </message>
    <message>
        <source>Show activities home page at startup</source>
        <translation>Mostra la pagina delle attività all&apos;avvio</translation>
    </message>
    <message>
        <source>If enabled the activities home page instead of chat page will be showed at startup</source>
        <translation>Se abilitata la pagina delle attività sarà mostrata all&apos;avvio invece di quella di conversazione</translation>
    </message>
    <message>
        <source>Unable to add this key in the registry: permission denied.</source>
        <translation>Impossibile aggiungere questa chiave nel registro: permesso negato.</translation>
    </message>
    <message>
        <source>All the members of this chat are not online. The changes may not be permanent. Do you wish to continue?</source>
        <translation>Non tutti i membri di questa conversazione sono connessi. I cambiamenti potrebbero non essere permanenti. Vuoi continuare?</translation>
    </message>
    <message>
        <source>Show the bar of log</source>
        <translation>Mostra la barra del log</translation>
    </message>
    <message>
        <source>Show minimized at startup</source>
        <translation>Mostra minimizzato alla partenza</translation>
    </message>
    <message>
        <source>If enabled %1 is showed minimized at startup</source>
        <translation>Se abilitata %1 sarà mostrato minimizzato alla partenza</translation>
    </message>
    <message>
        <source>Prompt on quit (only when connected)</source>
        <translation>Chiedi prima di uscire (solo quando connesso)</translation>
    </message>
    <message>
        <source>If enabled you will be asked if you really want to close %1</source>
        <translation>Se abilitata ti verrà chiesto se vuoi realmente chiudere %1</translation>
    </message>
    <message>
        <source>Like %1 on Facebook</source>
        <translation>Dai un mi piace a %1 su Facebook</translation>
    </message>
    <message>
        <source>Help me to know how many people use BeeBEEP</source>
        <translation type="obsolete">Aiutami a conoscere quante persone usano BeeBEEP</translation>
    </message>
    <message>
        <source>Donate for %1</source>
        <translation>Dona per %1</translation>
    </message>
    <message>
        <source>Open your data folder</source>
        <translation>Apri la tua cartella dei dati</translation>
    </message>
    <message>
        <source>Click to open your data folder</source>
        <translation>Clicca qui per aprire la tua cartella con le preferenze ed i messaggi salvati</translation>
    </message>
    <message>
        <source>Always open a new floating chat window</source>
        <translation>Apri sempre la conversazione in una nuova finestra</translation>
    </message>
    <message>
        <source>If enabled when you always open chat in a new floating window</source>
        <translation>Se abilitata quando aprirai una conversazione questa sarà mostrata in una nuova finestra</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>Rete</translation>
    </message>
    <message>
        <source>inactive</source>
        <translation>non attivo</translation>
    </message>
    <message>
        <source>active</source>
        <translation>attivo</translation>
    </message>
    <message>
        <source>Show the bar of games</source>
        <translation>Mostra la barra dei giochi</translation>
    </message>
    <message>
        <source>&lt;b&gt;Chat messages can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;I messaggi non possono essere salvati&lt;/b&gt;. Indirizzo:</translation>
    </message>
    <message>
        <source>Recently used</source>
        <translation>Usati di recente</translation>
    </message>
    <message>
        <source>Change your status description...</source>
        <translation>Cambia la descrizione del tuo stato...</translation>
    </message>
    <message>
        <source>Clear all status descriptions</source>
        <translation>Cancella tutte le descrizioni di stato</translation>
    </message>
    <message>
        <source>at lunch</source>
        <translation>a pranzo</translation>
    </message>
    <message>
        <source>in a meeting</source>
        <translation>in riunione</translation>
    </message>
    <message>
        <source>Do you really want to clear all saved status descriptions?</source>
        <translation>Vuoi realmente cancellare tutte le descrizioni di stato salvate?</translation>
    </message>
    <message>
        <source>Show status color in background</source>
        <translation>Mostra il colore di stato sullo sfondo</translation>
    </message>
    <message>
        <source>If enabled the user in list has colored backrgound as status</source>
        <translation>Se abilitata l&apos;utente in lista avrà lo sfondo colorato in base al suo stato</translation>
    </message>
    <message>
        <source>Show only message notifications</source>
        <translation>Mostra solo notifiche dei messaggi</translation>
    </message>
    <message>
        <source>If enabled tray icon shows only message notifications</source>
        <translation>Se abilitata l&apos;icona di sistema mostrerà solo le notifiche dei messaggi</translation>
    </message>
    <message>
        <source>Shortcuts...</source>
        <translation>Scorciatoie...</translation>
    </message>
    <message>
        <source>Enable and edit your custom shortcuts</source>
        <translation>Abilita e modifica le scorciatoie personalizzabili</translation>
    </message>
    <message>
        <source>Open your resource folder</source>
        <translation>Apri la tua cartella delle risorse</translation>
    </message>
    <message>
        <source>Click to open your resource folder</source>
        <translation>Clicca qui per aprire la cartella delle risorse installate</translation>
    </message>
    <message>
        <source>Use key Return to send message</source>
        <translation type="obsolete">Usa il tasto Invio per mandare i messaggi</translation>
    </message>
    <message>
        <source>Dictionary...</source>
        <translation>Dizionario...</translation>
    </message>
    <message>
        <source>Select your preferred dictionary for spell checking</source>
        <translation>Selezione il tuo dizionario preferito per il controllo ortografico</translation>
    </message>
    <message>
        <source>Select your dictionary path</source>
        <translation>Seleziona il tuo dizionario</translation>
    </message>
    <message>
        <source>Dictionary selected: %1</source>
        <translation>Il dizionario selezionato è: %1</translation>
    </message>
    <message>
        <source>Unable to set dictionary: %1</source>
        <translation>Impossibile caricare il dizionario: %1</translation>
    </message>
    <message>
        <source>Show new message</source>
        <translation>Mostra nuovi messaggi</translation>
    </message>
    <message>
        <source>Create chat</source>
        <translation>Crea una conversazione</translation>
    </message>
    <message>
        <source>Create a chat with two or more users</source>
        <translation>Crea una conversazione con due o più utenti</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation>Crea gruppo</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation>Crea un gruppo con due o più membri</translation>
    </message>
    <message>
        <source>Use your name instead of &apos;You&apos;</source>
        <translation>Usa il tuo nome invece di &apos;Tu&apos;</translation>
    </message>
    <message>
        <source>Show the datestamp</source>
        <translation>Mostra la data</translation>
    </message>
    <message>
        <source>If enabled the message shows its datestamp in the chat window</source>
        <translation>Se abilitata il messaggio mostrerà la sua data nella finestra di conversazione</translation>
    </message>
    <message>
        <source>Check for new version at startup</source>
        <translation>Controlla la presenza di una nuova versione alla partenza</translation>
    </message>
    <message>
        <source>Add +1 user to anonymous usage statistics</source>
        <translation>Aggiungi +1 utente alle statistiche anonime</translation>
    </message>
    <message>
        <source>Help me to know how many users have BeeBEEP</source>
        <translation>Aiutami a sapere quante persone usano realmente BeeBEEP</translation>
    </message>
    <message>
        <source>Prompts for nickname on startup</source>
        <translation>Richiedi il nickname ad ogni connessione</translation>
    </message>
    <message>
        <source>Show chat message preview</source>
        <translation>Mostra l&apos;anteprima dei messaggi</translation>
    </message>
    <message>
        <source>Save main window geometry</source>
        <translation>Salva le dimensioni della finestra principale</translation>
    </message>
    <message>
        <source>Window geometry and state saved</source>
        <translation>Le dimensioni e lo stato della finestra sono stati salvati</translation>
    </message>
    <message>
        <source>If a file already exist</source>
        <translation type="obsolete">Se un file esiste </translation>
    </message>
    <message>
        <source>If the file to be downloaded already exists it is automatically overwritten</source>
        <translation>Se il file scaricato esiste già verrà automaticamente sovrascritto</translation>
    </message>
    <message>
        <source>Ask me</source>
        <translation>Chiedimi</translation>
    </message>
    <message>
        <source>When the chat is not visible</source>
        <translation>Quando la finestra non è visibile</translation>
    </message>
    <message>
        <source>Never</source>
        <translation>Mai</translation>
    </message>
    <message>
        <source>File transfer is not enabled.</source>
        <translation>Il trasferimento dei file non è attivo.</translation>
    </message>
    <message>
        <source>You are not connected.</source>
        <translation>Non sei connesso.</translation>
    </message>
    <message>
        <source>If a file already exists</source>
        <translation>Se un file esiste già </translation>
    </message>
</context>
<context>
    <name>GuiMessageEdit</name>
    <message>
        <source>Undo</source>
        <translation>Indietro di un&apos;azione</translation>
    </message>
    <message>
        <source>Redo</source>
        <translation>Avanti di un&apos;azione</translation>
    </message>
    <message>
        <source>Cut</source>
        <translation>Taglia</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>Copia</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Seleziona tutto</translation>
    </message>
</context>
<context>
    <name>GuiNetwork</name>
    <message>
        <source>Proxy Configuration</source>
        <translation type="obsolete">Configurazione del proxy</translation>
    </message>
    <message>
        <source>Proxy</source>
        <translation type="obsolete">Proxy</translation>
    </message>
    <message>
        <source>None</source>
        <translation type="obsolete">Nessuno</translation>
    </message>
    <message>
        <source>Automatic</source>
        <translation type="obsolete">Automatico</translation>
    </message>
    <message>
        <source>Manually</source>
        <translation type="obsolete">Manuale</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Indirizzo</translation>
    </message>
    <message>
        <source>Port</source>
        <translation type="obsolete">Porta</translation>
    </message>
    <message>
        <source>Use authentication</source>
        <translation type="obsolete">Usa autenticazione</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Utente</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Password</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annulla</translation>
    </message>
    <message>
        <source>Network Manager - %1</source>
        <translation type="obsolete">Opzioni di rete - %1</translation>
    </message>
    <message>
        <source>Please insert the network proxy hostname or address.</source>
        <translation type="obsolete">Inserisci l&apos;indirizzo o il nome del server proxy.</translation>
    </message>
    <message>
        <source>Please insert the user for the proxy authorization.</source>
        <translation type="obsolete">Inserisci l&apos;utente per l&apos;autenticazione sul server proxy.</translation>
    </message>
</context>
<context>
    <name>GuiNetworkLogin</name>
    <message>
        <source>User</source>
        <translation type="obsolete">Utente</translation>
    </message>
    <message>
        <source>(ex: user@gmail.com)</source>
        <translation type="obsolete">(es: utente@gmail.com)</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="obsolete">Password</translation>
    </message>
    <message>
        <source>Remember user</source>
        <translation type="obsolete">Ricorda il nome utente</translation>
    </message>
    <message>
        <source>Remember password</source>
        <translation type="obsolete">Ricorda la password</translation>
    </message>
    <message>
        <source>Automatic connection</source>
        <translation type="obsolete">Connessione automatica all&apos;avvio</translation>
    </message>
    <message>
        <source>Login</source>
        <translation type="obsolete">Accedi</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Annulla</translation>
    </message>
    <message>
        <source>Login - %1</source>
        <translation type="obsolete">Accesso alla rete - %1</translation>
    </message>
    <message>
        <source>Please insert the username (JabberId)</source>
        <translation type="obsolete">Inserisci il nome utente (JabberId)</translation>
    </message>
    <message>
        <source>Please insert the password</source>
        <translation type="obsolete">Inserisci la password</translation>
    </message>
    <message>
        <source>Account</source>
        <translation type="obsolete">Account</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Salva</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Chiudi</translation>
    </message>
    <message>
        <source>Disconnect</source>
        <translation type="obsolete">Disconnetti</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="obsolete">Connetti</translation>
    </message>
    <message>
        <source>User JID</source>
        <translation type="obsolete">JID Utente</translation>
    </message>
    <message>
        <source>JID</source>
        <translation type="obsolete">JID</translation>
    </message>
</context>
<context>
    <name>GuiPluginManager</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Enable All</source>
        <translation>Abilita Tutti</translation>
    </message>
    <message>
        <source>Disable All</source>
        <translation>Disabilita Tutti</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <source>Disable %1</source>
        <translation>Disabilita %1</translation>
    </message>
    <message>
        <source>Enable %1</source>
        <translation>Abilita %1</translation>
    </message>
    <message>
        <source>%1 is enabled</source>
        <translation>Il plugin %1 è abilitato</translation>
    </message>
    <message>
        <source>%1 is disabled</source>
        <translation>Il plugin %1 è disabilitato</translation>
    </message>
    <message>
        <source>Please select a plugin in the list.</source>
        <translation>Seleziona un plugin dalla lista.</translation>
    </message>
    <message>
        <source>%1 - Plugin Manager</source>
        <translation type="obsolete">%1 - Gestione dei Plugin</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation>Plugin</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Versione</translation>
    </message>
    <message>
        <source>Author</source>
        <translation>Autore</translation>
    </message>
    <message>
        <source>Plugin Manager - %1</source>
        <translation>Gestione dei Plugin - %1</translation>
    </message>
    <message>
        <source>Text Markers</source>
        <translation>Formattazione del Testo</translation>
    </message>
    <message>
        <source>Network Services</source>
        <translation type="obsolete">Servizi di Rete</translation>
    </message>
    <message>
        <source>Games</source>
        <translation>Giochi</translation>
    </message>
    <message>
        <source>%1 - Select the plugin folder</source>
        <translation>%1 - Seleziona la cartella dei plugin</translation>
    </message>
    <message>
        <source>Folder %1 not found.</source>
        <translation>La cartella %1 non è stata trovata.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChat</name>
    <message>
        <source>Saved chat</source>
        <translation>Conversazione salvata</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Vuota</translation>
    </message>
    <message>
        <source>Find text in chat</source>
        <translation>Trova testo nella conversazione</translation>
    </message>
    <message>
        <source>Copy to clipboard</source>
        <translation>Copia in memoria</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>Seleziona tutto</translation>
    </message>
    <message>
        <source>Open selected text as url</source>
        <translation>Apri il testo selezionato come un link</translation>
    </message>
    <message>
        <source>Print...</source>
        <translation>Stampa...</translation>
    </message>
    <message>
        <source>%1 not found in chat.</source>
        <translation>%1 non trovato nella conversazione.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatList</name>
    <message>
        <source>Show</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <source>Do you really want to delete this saved chat?</source>
        <translation>Vuoi veramente cancellare questa conversazione?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Link to chat</source>
        <translation>Associa alla conversazione</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatWidget</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GuiScreenShot</name>
    <message>
        <source>No screenshot available</source>
        <translation>Nessuna immagine disponibile</translation>
    </message>
    <message>
        <source>/beesshot-%1.</source>
        <translation></translation>
    </message>
    <message>
        <source>Save As</source>
        <translation>Salva con nome</translation>
    </message>
    <message>
        <source>%1 Files (*.%2)</source>
        <translation></translation>
    </message>
    <message>
        <source>/beesshottmp-%1.</source>
        <translation></translation>
    </message>
    <message>
        <source>Unable to save temporary file: %1</source>
        <translation>Impossibile salvare il file temporaneo: %1</translation>
    </message>
    <message>
        <source>Delay</source>
        <translation>Ritardo</translation>
    </message>
    <message>
        <source>Delay screenshot for selected seconds</source>
        <translation>Ritarda la cattura dello schermo dei secondi selezionati</translation>
    </message>
    <message>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <source>Hide this window</source>
        <translation>Nascondi questa finestra</translation>
    </message>
    <message>
        <source>Hide this window before capture screenshot</source>
        <translation>Nascondi questa finestra prima di fare la foto dello schermo</translation>
    </message>
    <message>
        <source>Capture</source>
        <translation>Cattura</translation>
    </message>
    <message>
        <source>Capture a screenshot of your desktop</source>
        <translation>Cattura una foto dello schermo</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Invia</translation>
    </message>
    <message>
        <source>Send the captured screenshot to an user</source>
        <translation>Invia la foto del tuo schermo ad un utente</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Save the captured screenshot as file</source>
        <translation>Salva la foto del tuo schermo come un file</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <source>Delete the captured screenshot</source>
        <translation>Cancella la foto catturata dal tuo schermo</translation>
    </message>
    <message>
        <source>Enable high dpi</source>
        <translation>Abilita alta definizione</translation>
    </message>
    <message>
        <source>Enable high dpi support to manage, for example, Apple Retina display</source>
        <translation>Abilita l&apos;alta definizione per gestire, per esempio, gli schermi Retina di Apple</translation>
    </message>
    <message>
        <source>Make a Screenshot</source>
        <translation>Fai una foto dello schermo</translation>
    </message>
</context>
<context>
    <name>GuiScreenShotWidget</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>No Screenshot Available</source>
        <translation>Nessuna immagine disponibile</translation>
    </message>
    <message>
        <source>Delay screenshot</source>
        <translation type="obsolete">Ritardo dello scatto</translation>
    </message>
    <message>
        <source>Delay</source>
        <translation type="obsolete">Ritardo</translation>
    </message>
    <message>
        <source> s</source>
        <translation type="obsolete">s</translation>
    </message>
    <message>
        <source>Hide this window before capture screenshot</source>
        <translation type="obsolete">Nascondi questa finestra prima di fare la foto dello schermo</translation>
    </message>
    <message>
        <source>Hide this window</source>
        <translation type="obsolete">Nascondi questa finestra</translation>
    </message>
    <message>
        <source>Capture screenshot</source>
        <translation type="obsolete">Fai una foto allo schermo</translation>
    </message>
    <message>
        <source>Send</source>
        <translation type="obsolete">Invia</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="obsolete">Salva</translation>
    </message>
    <message>
        <source>Delete screenshot</source>
        <translation type="obsolete">Cancella l&apos;immagine</translation>
    </message>
</context>
<context>
    <name>GuiSearchUser</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Please enter the IP addresses (separed by comma) where the lan users are available.</source>
        <translation type="obsolete">Inserire gli indirizzi IP, o una sottorete, (separati da una virgola) dove vuoi cercare utenti disponibili.</translation>
    </message>
    <message>
        <source>Save addresses</source>
        <translation type="obsolete">Memorizza gli indirizzi IP</translation>
    </message>
    <message>
        <source>Add or search User</source>
        <translation type="obsolete">Aggiungi o cerca un utente</translation>
    </message>
    <message>
        <source>Please insert the ID of the user (ex: test@gmail.com)</source>
        <translation type="obsolete">Inserire l&apos;identificativo dell&apos;utente che vuoi aggiungere (es: test@gmail.com)</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Search Users</source>
        <translation type="obsolete">Cerca Utenti</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>Attenzione</translation>
    </message>
    <message>
        <source>You have inserted an invalid host address:
%1 is removed from the list.</source>
        <translation>Hai inserito un indirizzo IP non valido:
%1 verrà rimosso dalla lista.</translation>
    </message>
    <message>
        <source>Local subnet address (read only)</source>
        <translation type="obsolete">Sottorete locale (sola lettura)</translation>
    </message>
    <message>
        <source>Addresses in beehosts.ini (read only)</source>
        <translation type="obsolete">Indirizzi IP in beehosts.ini (sola lettura)</translation>
    </message>
    <message>
        <source>Enter the IP addresses or subnet of your local area network separed by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation type="obsolete">Inserisci gli indirizzi IP o altre sottoreti della tua area separati dalla virgola (per esempio: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <source>Split subnet to IPV4 addresses</source>
        <translation>Usa i singoli indirizzi IPV4 invece di una sottorete</translation>
    </message>
    <message>
        <source>Unknown address</source>
        <translation>Indirizzo IP sconosciuto</translation>
    </message>
    <message>
        <source>File is empty</source>
        <translation>Il file è vuoto</translation>
    </message>
    <message>
        <source>UDP Port (in beebeep.rc)</source>
        <translation type="obsolete">Porta UDP (in beebeep.rc)</translation>
    </message>
    <message>
        <source>Configure network</source>
        <translation type="obsolete">Configura rete</translation>
    </message>
    <message>
        <source>Local subnet address *</source>
        <translation>Sottorete locale *</translation>
    </message>
    <message>
        <source>Addresses in beehosts.ini *</source>
        <translation>Indirizzi IP in beehosts.ini *</translation>
    </message>
    <message>
        <source>UDP Port in beebeep.rc *</source>
        <translation>Porta UDP in beebeep.rc *</translation>
    </message>
    <message>
        <source>* (read only section)</source>
        <translation>* (sezione non modificabile da qui)</translation>
    </message>
    <message>
        <source>(the same for all clients)</source>
        <translation>(la stessa per tutti i client)</translation>
    </message>
    <message>
        <source>(search users here by default)</source>
        <translation>(cerca utenti automaticamente in questa)</translation>
    </message>
    <message>
        <source>Search for users</source>
        <translation>Cerca utenti</translation>
    </message>
    <message>
        <source>Automatically add external subnet</source>
        <translation>Aggiungi automaticamente le sottorete esterne</translation>
    </message>
    <message>
        <source>Use Zero Configuration Networking</source>
        <translation type="obsolete">Usa lo Zero Configuration Networking</translation>
    </message>
    <message>
        <source>Enable Zero Configuration Networking</source>
        <translation>Abilita Zero Configuration Networking</translation>
    </message>
    <message>
        <source>Workgroups (enter your network groups separated by comma)</source>
        <translation>Gruppi di lavoro (inserisci i tuoi gruppi di rete separati da una virgola)</translation>
    </message>
    <message>
        <source>Accept connections only from your workgroups</source>
        <translation>Accetta solo connessioni dai membri dei tuoi gruppi di lavoro</translation>
    </message>
    <message>
        <source>Enter the IP addresses or subnet of your local area network separated by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation>Inserisci gli indirizzi IP o altre sottoreti della tua area separati dalla virgola (per esempio: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <source>Multicast group in beebeep.rc *</source>
        <translation>Gruppo multicast in beebeep.rc *</translation>
    </message>
    <message>
        <source>Enable broadcast interval of</source>
        <translation type="obsolete">Abilita la ricerca automatica in un intervallo di</translation>
    </message>
    <message>
        <source>s (0=disabled, 10=default)</source>
        <translation type="obsolete">s (0=disabilitata, 10=predefinita)</translation>
    </message>
    <message>
        <source>Enable broadcast interval</source>
        <translation>Abilita la ricerca automatica ogni </translation>
    </message>
    <message>
        <source>seconds (0=disabled, 10=default)</source>
        <translation>secondi (0=disabilitata, 10=predefinita)</translation>
    </message>
    <message>
        <source>Verbose</source>
        <translation>In modo approfondito</translation>
    </message>
    <message>
        <source>Max users to contact in a tick</source>
        <translation>Numero di utenti da contattare in un tick</translation>
    </message>
</context>
<context>
    <name>GuiShareLocal</name>
    <message>
        <source>Share</source>
        <translation type="obsolete">Condivisione</translation>
    </message>
    <message>
        <source>Filename</source>
        <translation type="obsolete">Nome del file</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Dimensione</translation>
    </message>
    <message>
        <source>Path</source>
        <translation>Percorso</translation>
    </message>
    <message>
        <source>Select a file to share</source>
        <translation>Seleziona un file da condividere</translation>
    </message>
    <message>
        <source>Select a folder to share</source>
        <translation>Seleziona una cartella da condividere</translation>
    </message>
    <message>
        <source>Please select a share path</source>
        <translation type="obsolete">Seleziona un percorso da mettere in condivisione</translation>
    </message>
    <message>
        <source>Please select a shared path.</source>
        <translation>Seleziona un elemento cha hai condiviso.</translation>
    </message>
    <message>
        <source>%1 is already shared.</source>
        <translation>%1 è già un elemento condiviso.</translation>
    </message>
    <message>
        <source>Shared files</source>
        <translation>File condivisi</translation>
    </message>
    <message>
        <source>File sharing is disabled. Open the option menu to enable it.</source>
        <translation type="obsolete">La condivisione dei file è disabilitata. Apri il menu delle opzioni per abilitarla.</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <source>Share a file</source>
        <translation>Condividi un file</translation>
    </message>
    <message>
        <source>Add a file to your local share</source>
        <translation>Aggiungi un file alla tua condivisione in rete</translation>
    </message>
    <message>
        <source>Share a folder</source>
        <translation>Condividi una cartella</translation>
    </message>
    <message>
        <source>Add a folder to your local share</source>
        <translation>Aggiungi una cartella alla tua condivisione in rete</translation>
    </message>
    <message>
        <source>Load shared files</source>
        <translation type="obsolete">Carica i file condivisi</translation>
    </message>
    <message>
        <source>Remove shared files from the selected path</source>
        <translation type="obsolete">Rimuovi i file condivi dal percorso selezionato</translation>
    </message>
    <message>
        <source>Remove shared path</source>
        <translation>Rimuovi il percorso condiviso</translation>
    </message>
    <message>
        <source>Remove shared path from the list</source>
        <translation>Rimuovi il percorso selezionato dalla condivisione in rete</translation>
    </message>
    <message>
        <source>File sharing is disabled</source>
        <translation type="obsolete">La condivisione dei file è disabilitata</translation>
    </message>
    <message>
        <source>Double click to open %1</source>
        <translation type="obsolete">Doppio click per aprire %1</translation>
    </message>
    <message>
        <source>File transfer is disabled</source>
        <translation>Il trasferimento dei file è disabilitato</translation>
    </message>
    <message>
        <source>File transfer is disabled. Open the option menu to enable it.</source>
        <translation>Il trasferimento dei file è disabilitato. Apri il menu delle opzioni per abilitarlo.</translation>
    </message>
    <message>
        <source>Share your folders or files</source>
        <translation>Condividi le tue cartelle o file</translation>
    </message>
    <message>
        <source>Click to open %1</source>
        <translation>Click per aprire %1</translation>
    </message>
    <message>
        <source>Update shared folders and files</source>
        <translation>Aggiorna le cartelle ed i file condivisi</translation>
    </message>
    <message>
        <source>Update shares</source>
        <translation>Aggiorna condivisioni</translation>
    </message>
    <message>
        <source>Clear all shares</source>
        <translation>Elimina tutte le condivisioni</translation>
    </message>
    <message>
        <source>Clear all shared paths from the list</source>
        <translation>Elimina tutte le condivisioni dalla lista</translation>
    </message>
    <message>
        <source>Do you really want to remove all shared paths?</source>
        <translation>Vuoi veramente eliminare tutte le condivisioni?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>%1 shared files</source>
        <translation>%1 file condivisi</translation>
    </message>
</context>
<context>
    <name>GuiShareLocalWidget</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GuiShareNetwork</name>
    <message>
        <source>File</source>
        <translation type="obsolete">Nome del file</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Dimensione</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <source>All Files</source>
        <translation>Tutti</translation>
    </message>
    <message>
        <source>All Users</source>
        <translation>Tutti</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Stato</translation>
    </message>
    <message>
        <source>Scan network</source>
        <translation>Cerca nella rete</translation>
    </message>
    <message>
        <source>Search shared files in your network</source>
        <translation>Cerca i file condivi all&apos;interno della tua rete</translation>
    </message>
    <message>
        <source>Reload list</source>
        <translation>Ricarica la lista</translation>
    </message>
    <message>
        <source>Clear and reload list</source>
        <translation>Svuota e ricarica la lista</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>Filtro</translation>
    </message>
    <message>
        <source>Apply Filter</source>
        <translation type="obsolete">Applica il filtro</translation>
    </message>
    <message>
        <source>Filter the files in list using some keywords</source>
        <translation type="obsolete">Filtra i file condivisi usando alcune parole chiavi</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation>Tipo di file</translation>
    </message>
    <message>
        <source>%1 is searching shared files in your network</source>
        <translation>%1 sta cercando file condivisi nella tua rete</translation>
    </message>
    <message>
        <source>%1 has shared %2 files (%3)</source>
        <translation>%1 ha condiviso %2 file (%3)</translation>
    </message>
    <message>
        <source>%1 files are shown in list (%2 are available in your network)</source>
        <translation>%1 file visibili in lista (%2 disponibili nella tua rete)</translation>
    </message>
    <message>
        <source>%1 files shared in your network</source>
        <translation>%1 file condivisi nella tua rete</translation>
    </message>
    <message>
        <source>Double click to download %1</source>
        <translation>Doppio click per scaricare %1</translation>
    </message>
    <message>
        <source>Double click to open %1</source>
        <translation>Doppio click per aprire %1</translation>
    </message>
    <message>
        <source>Transfer completed</source>
        <translation>Trasferimento completato</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation type="obsolete">Cartella</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <source>Download single or multiple files simultaneously</source>
        <translation>Scarica uno o più file simultaneamente</translation>
    </message>
    <message>
        <source>Download single file</source>
        <translation>Scarica un file</translation>
    </message>
    <message>
        <source>Download %1 selected files</source>
        <translation>Scarica %1 file selezionati</translation>
    </message>
    <message>
        <source>Clear selection</source>
        <translation>Deseleziona tutto</translation>
    </message>
    <message>
        <source>Please select one or more files to download.</source>
        <translation>Seleziona uno o più file da scaricare.</translation>
    </message>
    <message>
        <source>Folder and Files shared in your network</source>
        <translation>Cartelle e file condivisi nella tua rete</translation>
    </message>
    <message>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <source>Expand all items</source>
        <translation>Mostra tutti gli elementi</translation>
    </message>
    <message>
        <source>Collapse all items</source>
        <translation>Raggruppa tutti gli elementi</translation>
    </message>
    <message>
        <source>Search files</source>
        <translation type="obsolete">Cerca file</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>GuiShareNetworkWidget</name>
    <message>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Filter</source>
        <translation type="obsolete">Filtro</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="obsolete">Cerca</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation type="obsolete">Tipo di file</translation>
    </message>
    <message>
        <source>User</source>
        <translation type="obsolete">Utente</translation>
    </message>
</context>
<context>
    <name>GuiShortcut</name>
    <message>
        <source>Shortcuts</source>
        <translation>Scorciatoie</translation>
    </message>
    <message>
        <source>Key</source>
        <translation>Tasti</translation>
    </message>
    <message>
        <source>Action</source>
        <translation>Azione</translation>
    </message>
    <message>
        <source>Insert the new shorcut for %1</source>
        <translation type="obsolete">Inserisci la nuova combinazione per %1</translation>
    </message>
    <message>
        <source>Insert shorcut for the action: %1</source>
        <translation>Inserisci la combinazione di tasti per l&apos;azione: %1</translation>
    </message>
</context>
<context>
    <name>GuiShortcutDialog</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Use custom shortcuts (restart needed after changing)</source>
        <translation type="obsolete">Usa scorciatoie personalizzate (riavvio richiesto dopo un cambiamento)</translation>
    </message>
    <message>
        <source>Restore to default language</source>
        <translation type="obsolete">Ripristina la lingua predefinita</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>Ripristina</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Restore default shortcuts</source>
        <translation>Ripristina le scorcatoie predefinite</translation>
    </message>
    <message>
        <source>Use custom shortcuts</source>
        <translation>Usa le scorciatoie personalizzate</translation>
    </message>
</context>
<context>
    <name>GuiSystemTray</name>
    <message>
        <source>1 new message</source>
        <translation type="obsolete">1 nuovo messaggio</translation>
    </message>
    <message>
        <source>%1 new messages</source>
        <translation type="obsolete">%1 nuovi messaggi</translation>
    </message>
</context>
<context>
    <name>GuiTetris</name>
    <message>
        <source>Next Piece</source>
        <translation type="obsolete">Prossimo Pezzo</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Punteggio</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation type="obsolete">Linee</translation>
    </message>
    <message>
        <source>Level</source>
        <translation type="obsolete">Livello</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Nuova Partita</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Pausa</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Continua</translation>
    </message>
</context>
<context>
    <name>GuiTransferFile</name>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <source>User</source>
        <translation>Utente</translation>
    </message>
    <message>
        <source>Status</source>
        <translation>Stato</translation>
    </message>
    <message>
        <source>Completed</source>
        <translation>Completato</translation>
    </message>
    <message>
        <source>Cancel Transfer</source>
        <translation>Trasferimento Interrotto</translation>
    </message>
    <message>
        <source>Not Completed</source>
        <translation>Non Completato</translation>
    </message>
    <message>
        <source>Transfer completed</source>
        <translation>Trasferimento completato</translation>
    </message>
    <message>
        <source>Downloading</source>
        <translation>Scaricati</translation>
    </message>
    <message>
        <source>Uploading</source>
        <translation>Inviati</translation>
    </message>
    <message>
        <source>Do you really want to cancel the transfer of %1?</source>
        <translation>Vuoi realmente interrompere il trasferimento di %1?</translation>
    </message>
    <message>
        <source>Remove all transfers</source>
        <translation>Cancella tutti i trasferimenti</translation>
    </message>
</context>
<context>
    <name>GuiUserList</name>
    <message>
        <source>* All *</source>
        <translation type="obsolete">* Tutti *</translation>
    </message>
    <message>
        <source>Open chat with all users</source>
        <translation type="obsolete">Parla con tutti gli utenti</translation>
    </message>
    <message>
        <source>Open chat with %1</source>
        <translation type="obsolete">Parla con %1</translation>
    </message>
    <message>
        <source>%1 is %2</source>
        <translation type="obsolete">%1 è %2</translation>
    </message>
    <message>
        <source>Chat with all connected users</source>
        <translation type="obsolete">Parla con tutti gli utenti connessi</translation>
    </message>
    <message>
        <source>Chat with %1</source>
        <translation type="obsolete">Parla con %1</translation>
    </message>
    <message>
        <source>Chat with all users</source>
        <translation type="obsolete">Parla con tutti gli utenti</translation>
    </message>
    <message>
        <source>Open the private chat</source>
        <translation type="obsolete">Apri il canale privato</translation>
    </message>
    <message>
        <source>Send a file...</source>
        <translation type="obsolete">Invia un file...</translation>
    </message>
    <message>
        <source>Change the user color...</source>
        <translation type="obsolete">Cambia il colore dell&apos;utente...</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>GuiVCard</name>
    <message>
        <source>Birthday: %1</source>
        <translation>Data di nascita: %1</translation>
    </message>
    <message>
        <source>Chat with all</source>
        <translation>Parla con tutti</translation>
    </message>
    <message>
        <source>Open chat</source>
        <translation>Apri la conversazione</translation>
    </message>
    <message>
        <source>use old version</source>
        <translation type="obsolete">usa una vecchia versione</translation>
    </message>
    <message>
        <source>use old %1</source>
        <translation type="obsolete">una la vecchia %1</translation>
    </message>
    <message>
        <source>use new %1</source>
        <translation type="obsolete">usa la nuova %1</translation>
    </message>
    <message>
        <source>old unknown</source>
        <translation type="obsolete">vecchia</translation>
    </message>
    <message>
        <source>old %1</source>
        <translation>vecchia %1</translation>
    </message>
    <message>
        <source>new %1</source>
        <translation>nuova %1</translation>
    </message>
    <message>
        <source>unknown</source>
        <translation>sconosciuto</translation>
    </message>
    <message>
        <source>Remove from favorites</source>
        <translation>Rimuovi dai preferiti</translation>
    </message>
    <message>
        <source>Add to favorites</source>
        <translation>Aggiungi ai preferiti</translation>
    </message>
</context>
<context>
    <name>GuiVCardDialog</name>
    <message>
        <source>Nickname</source>
        <translation>Soprannome</translation>
    </message>
    <message>
        <source>First name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Last name</source>
        <translation>Cognome</translation>
    </message>
    <message>
        <source>Add or change photo</source>
        <translation>Aggiungi o cambia la tua foto</translation>
    </message>
    <message>
        <source>Change your nickname color</source>
        <translation>Cambia il colore del tuo soprannome</translation>
    </message>
    <message>
        <source>Gender</source>
        <translation type="obsolete">Sesso</translation>
    </message>
    <message>
        <source>Male</source>
        <translation type="obsolete">Maschile</translation>
    </message>
    <message>
        <source>Female</source>
        <translation type="obsolete">Femminile</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Data di nascita</translation>
    </message>
    <message>
        <source>Change photo</source>
        <translation type="obsolete">Cambia la foto del profilo</translation>
    </message>
    <message>
        <source>Change</source>
        <translation type="obsolete">Cambia</translation>
    </message>
    <message>
        <source>Remove photo</source>
        <translation>Rimuovi la foto del profilo</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation type="obsolete">Rimuovi</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <source>User ID</source>
        <translation>ID Utente</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefono</translation>
    </message>
    <message>
        <source>Other informations</source>
        <translation>Altre informazioni</translation>
    </message>
</context>
<context>
    <name>GuiVCardWidget</name>
    <message>
        <source>Chat</source>
        <translation type="obsolete">Parla</translation>
    </message>
    <message>
        <source>Send File</source>
        <translation type="obsolete">Invia file</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="obsolete">Colore</translation>
    </message>
    <message>
        <source>Widget</source>
        <translation></translation>
    </message>
    <message>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <source>User Path</source>
        <translation></translation>
    </message>
    <message>
        <source>User Name</source>
        <translation></translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Data di nascita</translation>
    </message>
    <message>
        <source>Email</source>
        <translation></translation>
    </message>
    <message>
        <source>Open chat</source>
        <translation>Apri la conversazione</translation>
    </message>
    <message>
        <source>Send a file</source>
        <translation>Invia un file</translation>
    </message>
    <message>
        <source>Change the nickname color</source>
        <translation>Cambia il colore del nickname</translation>
    </message>
    <message>
        <source>Remove from the contact list</source>
        <translation type="obsolete">Elimina dalla lista dei contatti</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>Telefono</translation>
    </message>
    <message>
        <source>Remove user</source>
        <translation>Rimuovi utente</translation>
    </message>
</context>
<context>
    <name>GuiWizard</name>
    <message>
        <source>Welcome to &lt;b&gt;%1 Network&lt;/b&gt;.</source>
        <translation>Benvenuti nella &lt;b&gt;Rete di %1&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Your nickname can not be empty.</source>
        <translation>Il campo del soprannome non può essere vuoto.</translation>
    </message>
    <message>
        <source>Your system account is</source>
        <translation>Il tuo account di sistema è</translation>
    </message>
</context>
<context>
    <name>GuiWizardDialog</name>
    <message>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <source>Welcome</source>
        <translation></translation>
    </message>
    <message>
        <source>Your nickname is:</source>
        <translation type="obsolete">Il tuo soprannome è:</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>JabberService</name>
    <message>
        <source>Jabber</source>
        <translation type="obsolete">Jabber</translation>
    </message>
    <message>
        <source>You can connect to the Jabber.org server.</source>
        <translation type="obsolete">Puoi connetterti al server di Jabber.org.</translation>
    </message>
</context>
<context>
    <name>LifeGame</name>
    <message>
        <source>&lt;b&gt;The Game of Life&lt;/b&gt;, also known simply as Life, is a cellular automaton devised by the British mathematician &lt;b&gt;John Horton Conway&lt;/b&gt; in 1970. The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead. Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:&lt;ul&gt;&lt;li&gt;Any live cell with less than two live neighbours dies, as if caused by under-population.&lt;/li&gt;&lt;li&gt;Any live cell with two or three live neighbours lives on to the next generation.&lt;/li&gt;&lt;li&gt;Any live cell with more than three live neighbours dies, as if by overcrowding.&lt;/li&gt;&lt;li&gt;Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.&lt;/li&gt;&lt;/ul&gt;The rules continue to be applied repeatedly to create further generations.&lt;br /&gt;For more info please visit &lt;a href=http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life&gt;Conway&apos;s Game of Life&lt;/a&gt;.</source>
        <translation type="obsolete">Il &lt;b&gt;Gioco della vita&lt;/b&gt; (Game of Life in inglese) è un automa cellulare sviluppato dal matematico inglese &lt;b&gt;John Conway&lt;/b&gt; sul finire degli anni sessanta. Si tratta in realtà di un gioco senza giocatori, intendendo che la sua evoluzione è determinata dal suo stato iniziale, senza necessità di alcun input da parte di giocatori umani. Si svolge su una griglia di caselle quadrate (celle) che si estende all&apos;infinito in tutte le direzioni; questa griglia è detta mondo. Ogni cella ha 8 vicini, che sono le celle ad essa adiacenti, includendo quelle in senso diagonale. Ogni cella può trovarsi in due stati: viva o morta (o accesa e spenta, on e off). Lo stato della griglia evolve in intervalli di tempo discreti. Gli stati di tutte le celle in un dato istante sono usati per calcolare lo stato delle celle all&apos;istante successivo. Tutte le celle del mondo vengono quindi aggiornate simultaneamente nel passaggio da un istante a quello successivo: passa così una generazione.&lt;br /&gt;
Le transizioni di stato dipendono unicamente dal numero di vicini vivi:&lt;ul&gt;
&lt;li&gt;Una cella morta con esattamente 3 vicini vivi nasce, diventando viva.&lt;/li&gt;
&lt;li&gt;Una cella viva con 2 o 3 vicini vivi sopravvive; altrimenti muore (per isolamento o sovraffollamento)&lt;/li&gt;
&lt;/ul&gt;
Per ulteriori informazioni basterà visitare il sito de &lt;a href=http://it.wikipedia.org/wiki/Gioco_della_vita&gt;Il Gioco della Vita&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>NumberTextMarker</name>
    <message>
        <source>Number Text Marker</source>
        <translation>Cifratura Stringhe</translation>
    </message>
    <message>
        <source>If you want to encode your message with numbers write a #text to encode# .</source>
        <translation>Se vuoi cifrare il tuo messaggio con dei numeri scrivi #testo da cifrare# .</translation>
    </message>
</context>
<context>
    <name>QDnsLookup</name>
    <message>
        <source>Operation cancelled</source>
        <translation type="obsolete">Operazione annullata</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>* All *</source>
        <translation type="obsolete">* Tutti *</translation>
    </message>
    <message>
        <source>Open chat with all users</source>
        <translation type="obsolete">Parla con tutti gli utenti</translation>
    </message>
    <message>
        <source>Open chat with %1</source>
        <translation>Parla con %1</translation>
    </message>
    <message>
        <source>%1 is %2</source>
        <translation>Lo stato di %1 è %2</translation>
    </message>
    <message>
        <source>Me</source>
        <translation type="obsolete">Io</translation>
    </message>
    <message>
        <source>Open chat with all local users</source>
        <translation>Apri la conversazione con tutti gli utenti locali</translation>
    </message>
    <message>
        <source>Nobody</source>
        <translation type="obsolete">Nessuno</translation>
    </message>
    <message>
        <source>You</source>
        <translation>Tu</translation>
    </message>
    <message>
        <source>All Lan Users</source>
        <translation>Utenti Locali</translation>
    </message>
    <message>
        <source>Saved in</source>
        <translation type="obsolete">Salvata in data</translation>
    </message>
    <message>
        <source>Started in</source>
        <translation type="obsolete">Iniziata in data</translation>
    </message>
    <message>
        <source>Double click to send message to group: %1</source>
        <translation type="obsolete">Doppio click per inviare un messaggio al gruppo: %1</translation>
    </message>
    <message>
        <source>Double click to view chat history: %1</source>
        <translation type="obsolete">Doppio click per vedere i messaggi salvati: %1</translation>
    </message>
    <message>
        <source>Double click to send a private message.</source>
        <translation type="obsolete">Doppio click per inviare un messaggio privato.</translation>
    </message>
    <message>
        <source>Double click to open chat with all local users</source>
        <translation type="obsolete">Doppio click per aprire una conversazione con tutti gli utenti connessi</translation>
    </message>
    <message>
        <source>Double click to send a private message</source>
        <translation type="obsolete">Doppio click per inviare un messaggio privato</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Dimensione</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Stato</translation>
    </message>
    <message>
        <source>Click to send message to group: %1</source>
        <translation>Click per inviare un messaggio al gruppo: %1</translation>
    </message>
    <message>
        <source>Click to view chat history: %1</source>
        <translation>Click per vedere i messaggi salvati: %1</translation>
    </message>
    <message>
        <source>Click to open chat with all local users</source>
        <translation>Click per aprire una conversazione con tutti gli utenti connessi </translation>
    </message>
    <message>
        <source>Click to send a private message</source>
        <translation>Click per inviare un messaggio privato</translation>
    </message>
    <message>
        <source>Empty</source>
        <translation>Vuota</translation>
    </message>
    <message>
        <source>Send file</source>
        <translation>Invia un file</translation>
    </message>
    <message>
        <source>Show file transfers</source>
        <translation>Mostra il trasferimento dei file</translation>
    </message>
    <message>
        <source>Set focus in message box</source>
        <translation>Attiva la finestra dei messaggi</translation>
    </message>
    <message>
        <source>Minimize all chats</source>
        <translation>Minimizza tutte le conversazioni</translation>
    </message>
    <message>
        <source>Show the next unread message</source>
        <translation>Mostra il prossimo messaggio non letto</translation>
    </message>
    <message>
        <source>Send chat message</source>
        <translation>Invia messaggio</translation>
    </message>
    <message>
        <source>Print</source>
        <translation>Stampa</translation>
    </message>
    <message>
        <source>Broadcast</source>
        <translation>Cerca utenti</translation>
    </message>
    <message>
        <source>Find text in chat</source>
        <translation>Trova testo nella conversazione</translation>
    </message>
    <message>
        <source>Find next text in chat</source>
        <translation>Trova testo successivo nella conversazione</translation>
    </message>
    <message>
        <source>Send folder</source>
        <translation>Invia una cartella</translation>
    </message>
    <message>
        <source>Show emoticons</source>
        <translation type="obsolete">Mostra le faccine con delle immagini</translation>
    </message>
    <message>
        <source>Show emoticons panel</source>
        <translation>Mostra il pannello delle faccine</translation>
    </message>
    <message>
        <source>Show all chats</source>
        <translation>Mostra tutte le finestre</translation>
    </message>
</context>
<context>
    <name>RainbowTextMarker</name>
    <message>
        <source>RainbowTextMarker</source>
        <translation type="obsolete">TestoArcobaleno</translation>
    </message>
    <message>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~.</source>
        <translation type="obsolete">Se vuoi un scrivi ~testo arcobaleno~.</translation>
    </message>
    <message>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~ .</source>
        <translation>Se vuoi avere un &lt;FONT COLOR=&quot;#FF0000&quot;&gt;t&lt;/FONT&gt;&lt;FONT COLOR=&quot;#FF6000&quot;&gt;e&lt;/FONT&gt;&lt;FONT COLOR=&quot;#FFC000&quot;&gt;s&lt;/FONT&gt;&lt;FONT COLOR=&quot;#FFff00&quot;&gt;t&lt;/FONT&gt;&lt;FONT COLOR=&quot;#9Fff00&quot;&gt;o&lt;/FONT&gt;&lt;FONT COLOR=&quot;#3Fff00&quot;&gt; &lt;/FONT&gt;&lt;FONT COLOR=&quot;#00ff00&quot;&gt;a&lt;/FONT&gt;&lt;FONT COLOR=&quot;#00ff60&quot;&gt;r&lt;/FONT&gt;&lt;FONT COLOR=&quot;#00ffC0&quot;&gt;c&lt;/FONT&gt;&lt;FONT COLOR=&quot;#00ffff&quot;&gt;o&lt;/FONT&gt;&lt;FONT COLOR=&quot;#00C0ff&quot;&gt;b&lt;/FONT&gt;&lt;FONT COLOR=&quot;#0060ff&quot;&gt;a&lt;/FONT&gt;&lt;FONT COLOR=&quot;#0000ff&quot;&gt;l&lt;/FONT&gt;&lt;FONT COLOR=&quot;#3F00ff&quot;&gt;e&lt;/FONT&gt;&lt;FONT COLOR=&quot;#9F00ff&quot;&gt;n&lt;/FONT&gt;&lt;FONT COLOR=&quot;#FF00ff&quot;&gt;o&lt;/FONT&gt; usa il simbolo ~ e scrivi ~testo arcobaleno~ (con spazi prima e dopo i simboli).</translation>
    </message>
    <message>
        <source>Rainbow Text Marker</source>
        <translation>Testo Arcobaleno</translation>
    </message>
</context>
<context>
    <name>RegularBoldTextMarker</name>
    <message>
        <source>Regular Bold Text Marker</source>
        <translation></translation>
    </message>
    <message>
        <source>If you want to format your message with words in regular and bold font write a [text to format] .</source>
        <translation>Se vuoi formattare il tuo messaggio con parole normali e in grassetto scrivi [testo da formattare] .</translation>
    </message>
</context>
<context>
    <name>TetrisBoard</name>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Pausa</translation>
    </message>
    <message>
        <source>Game Over</source>
        <translation type="obsolete">Game Over</translation>
    </message>
</context>
<context>
    <name>TetrisGame</name>
    <message>
        <source>Tetris</source>
        <translation type="obsolete">Tetris</translation>
    </message>
    <message>
        <source>Use &lt;b&gt;Left&lt;/b&gt; and &lt;b&gt;Right Arrow&lt;/b&gt; to move the pieces on the board. &lt;b&gt;Up&lt;/b&gt; and &lt;b&gt;Down Arrow&lt;/b&gt; to rotate left and right. &lt;b&gt;Space&lt;/b&gt; to drop down the piece. &lt;b&gt;D&lt;/b&gt; to drop the piece only one line. &lt;b&gt;P&lt;/b&gt; to pause the game.</source>
        <translation type="obsolete">Usa le frecce &lt;b&gt;Sinistra&lt;/b&gt; e &lt;b&gt;Destra&lt;/b&gt; per muovere i pezzi nello schermo. Le frecce &lt;b&gt;Su&lt;/b&gt; e &lt;b&gt;Giù&lt;/b&gt; per ruotare il pezzo a sinistra e destra rispettivamente. &lt;b&gt;Barra spaziatrice&lt;/b&gt; per far cadere il pezzo. Premi il tasto &lt;b&gt;D&lt;/b&gt; per scendere di una linea e &lt;b&gt;P&lt;/b&gt; per mettere in pausa il gioco.</translation>
    </message>
</context>
<context>
    <name>Tips</name>
    <message>
        <source>You can switch between chats with CTRL+TAB if there are new messages availables.</source>
        <translation>Puoi passare da una conversazione ad un&apos;altra usando CTRL+TAB se sono presenti nuovi messaggi.</translation>
    </message>
    <message>
        <source>If you want a &lt;b&gt;bold text&lt;/b&gt; write a *bold text*.</source>
        <translation>Se vuoi usare il &lt;b&gt;testo in grassetto&lt;/b&gt; usa gli asterischi e scrivi *testo in grassetto*.</translation>
    </message>
    <message>
        <source>If you want an &lt;i&gt;italic text&lt;/i&gt; write a /italic text/.</source>
        <translation>Se vuoi usare il &lt;i&gt;testo in corsivo&lt;/i&gt; puoi usare le / e scrivere /testo in corsivo/.</translation>
    </message>
    <message>
        <source>If you want an &lt;u&gt;underlined text&lt;/u&gt; write a _underlined text_.</source>
        <translation>Se vuoi usare il &lt;u&gt;testo sottilineato&lt;/u&gt; puoi usare il carattere _ e scrivere _testo sottolineato_.</translation>
    </message>
    <message>
        <source>&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</source>
        <translation>&lt;i&gt;Libera è quella mente guidata dalla fantasia.&lt;/i&gt; (Marco Mastroddi)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</source>
        <translation>&lt;i&gt;Siate affamati, siate folli.&lt;/i&gt; (Steve Jobs)</translation>
    </message>
    <message>
        <source>&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</source>
        <translation>&lt;i&gt;Ci sarà sempre un altro bug.&lt;/i&gt; (Legge di Lubarsky)</translation>
    </message>
    <message>
        <source>&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</source>
        <translation>&lt;i&gt;Se qualcosa può andare storto, ci andrà.&lt;/i&gt; (Legge di Murphy)</translation>
    </message>
    <message>
        <source>&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</source>
        <translation>&lt;i&gt;Se un programma è utile probabilmente dovrà essere cambiato.&lt;/i&gt; (Legge della Programmazione)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</source>
        <translation>&lt;i&gt;Le menti intelligentii risolvono i problemi; quelle geniali li prevengono.&lt;/i&gt; (Albert Einstein)</translation>
    </message>
    <message>
        <source>&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</source>
        <translation>&lt;i&gt;Quello che non mi uccide, mi fortifica.&lt;/i&gt; (Friedrich Nietzsche)</translation>
    </message>
    <message>
        <source>&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</source>
        <translation>&lt;i&gt;Non sono così giovane da conoscere ogni cosa.&lt;/i&gt; (Oscar Wilde)</translation>
    </message>
    <message>
        <source>&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</source>
        <translation>&lt;i&gt;Un&apos;assenza di dubbi conduce ad una mancanza di creatività.&lt;/i&gt; (Evert Jan Ouweneel)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</source>
        <translation>&lt;i&gt;La paura conduce al lato oscuro.&lt;/i&gt; (Joda)</translation>
    </message>
    <message>
        <source>&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</source>
        <translation>&lt;i&gt;Sogno i miei dipinti e poi dipingo i miei sogni.&lt;/i&gt; (Vincent Van Gogh)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</source>
        <translation>&lt;i&gt;Qualsiasi cosa tu possa immaginare è reale.&lt;/i&gt; (Pablo Picasso)</translation>
    </message>
    <message>
        <source>&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</source>
        <translation>&lt;i&gt;Tutte le verità sono facili da comprendere una volta scoperte; il punto è scoprirle.&lt;/i&gt; (Galileo Galilei)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</source>
        <translation>&lt;i&gt;La verità emerge quando le opinioni sono libere.&lt;/i&gt; (Thomas Paine)</translation>
    </message>
    <message>
        <source>&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</source>
        <translation>&lt;i&gt;Ho visto cose che voi umani non potreste immaginare...&lt;/i&gt; (Batty)</translation>
    </message>
    <message>
        <source>&lt;i&gt;A man`s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation type="obsolete">&lt;i&gt;Il carattere di un uomo è il suo destino.&lt;/i&gt; (Eraclito)</translation>
    </message>
    <message>
        <source>&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</source>
        <translation>&lt;i&gt;Un linguaggio diverso è una diversa visione della vita.&lt;/i&gt; (Federico Fellini)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</source>
        <translation>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</source>
        <translation>&lt;i&gt;Ogni giorno in Africa una gazzella si sveglia e sa che dovrà correre più veloce del leone o verrà uccisa. Ogni mattina un leone si sveglia e sa che dovrà correre più veloce della gazzella o morirà di fame. Non importa che tu sia leone o gazzella. Quando il sole sorge, inizia a correre.&lt;/i&gt; (Abe Gubegna)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</source>
        <translation>&lt;i&gt;Okay, Houston, qui abbiamo avuto un problema.&lt;/i&gt; (John L. Swigert)</translation>
    </message>
    <message>
        <source>&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</source>
        <translation>&lt;i&gt;Seconda stella a destra e poi dritti fino al mattino.&lt;/i&gt; (Peter Pan)</translation>
    </message>
    <message>
        <source>If you want an &lt;i&gt;underlined text&lt;/i&gt; write a _underlined text_.</source>
        <translation type="obsolete">Se vuoi usare il &lt;u&gt;testo sottilineato&lt;/u&gt; puoi usare il carattere _ e scrivere _testo sottolineato_.</translation>
    </message>
    <message>
        <source>You can search previous sent message in the history using the CTRL+Up e CTRL+Down keys.</source>
        <translation>Puoi navigare tra i messaggi che hai inviato usando le combinazioni CTRL+FrecciaSu e CTRL+FrecciaGiù.</translation>
    </message>
    <message>
        <source>You can drop files to active chat and send them to members.</source>
        <translation>Puoi trascinare un file nella conversazione attiva per inviarlo a tutti i membri.</translation>
    </message>
    <message>
        <source>You can select multiple files from network shares and download them simultaneously with a right click.</source>
        <translation>Puoi selezionare uno o più file dalla condivisione di rete e scaricarli tutti insieme usando il pulsante destro.</translation>
    </message>
    <message>
        <source>You can disable message notification from a group right clicking on its name on the list.</source>
        <translation>Puoi disabilitare le notifiche di un gruppo premendo il tasto destro sul nome del gruppo nella lista.</translation>
    </message>
    <message>
        <source>&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</source>
        <translation></translation>
    </message>
    <message>
        <source>&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>TwitterService</name>
    <message>
        <source>You can connect to the Twitter server.</source>
        <translation type="obsolete">Puoi connetterti al server di Twitter.</translation>
    </message>
</context>
<context>
    <name>Ui</name>
    <message>
        <source>%1 - Plugin Manager</source>
        <translation type="obsolete">%1 - Gestione dei Plugin</translation>
    </message>
    <message>
        <source>Plugin</source>
        <translation type="obsolete">Plugin</translation>
    </message>
    <message>
        <source>Version</source>
        <translation type="obsolete">Versione</translation>
    </message>
    <message>
        <source>Author</source>
        <translation type="obsolete">Autore</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <source>offline</source>
        <translation>non in linea</translation>
    </message>
    <message>
        <source>available</source>
        <translation>disponibile</translation>
    </message>
    <message>
        <source>busy</source>
        <translation>occupato</translation>
    </message>
    <message>
        <source>away</source>
        <translation>assente</translation>
    </message>
    <message>
        <source>status error</source>
        <translation>errore di stato</translation>
    </message>
</context>
<context>
    <name>WindowsLiveService</name>
    <message>
        <source>You can connect to the Windows Live Messenger.</source>
        <translation type="obsolete">Puoi connetterti a Windows Live Messenger.</translation>
    </message>
</context>
<context>
    <name>XmppManager</name>
    <message>
        <source>unknown</source>
        <translation type="obsolete">sconosciuto</translation>
    </message>
    <message>
        <source>connection error (%1)</source>
        <translation type="obsolete">errore di connessione (%1)</translation>
    </message>
    <message>
        <source>Error occurred (#%1)</source>
        <translation type="obsolete">C&apos;è stato un errore (#%1)</translation>
    </message>
    <message>
        <source>already connected to the server</source>
        <translation type="obsolete">già connesso al server</translation>
    </message>
    <message>
        <source>connection in progress to the server. Please wait</source>
        <translation type="obsolete">la connessione al server è in corso. Attendere prego</translation>
    </message>
    <message>
        <source>Username is empty. Unable to connect to the server</source>
        <translation type="obsolete">Il nome utente non è stato inserito. Impossibile effettuare la connessione</translation>
    </message>
    <message>
        <source>Password is empty. Unable to connect to the server</source>
        <translation type="obsolete">La password non è stata inserita. Impossibile effettuare la connessione</translation>
    </message>
    <message>
        <source>connection in progress..</source>
        <translation type="obsolete">connessione in corso..</translation>
    </message>
    <message>
        <source>invalid user or password</source>
        <translation type="obsolete">utente o password non validi</translation>
    </message>
    <message>
        <source>%1: error occurred (%2)</source>
        <translation type="obsolete">%1: si è verificato un errore (%2)</translation>
    </message>
    <message>
        <source>adding %1 to the contact list</source>
        <translation type="obsolete">%1 verrà aggiunto alla lista dei contatti</translation>
    </message>
    <message>
        <source>%1&apos;s request is rejected</source>
        <translation type="obsolete">la richiesta di %1 è stata rifiutata</translation>
    </message>
    <message>
        <source>removing %1 from the contact list</source>
        <translation type="obsolete">%1 verrà eliminato dalla lista dei contatti</translation>
    </message>
    <message>
        <source>%1 wants to subscribe to your contact list</source>
        <translation type="obsolete">%1 vorrebbe far parte della tua lista dei contatti</translation>
    </message>
    <message>
        <source>%1 has accepted your subscription</source>
        <translation type="obsolete">%1 ha accettato la tua richiesta</translation>
    </message>
    <message>
        <source>%1 has reject your subscription</source>
        <translation type="obsolete">%1 ha rifiutato la tua richiesta</translation>
    </message>
    <message>
        <source>unable to send the message. You are not connected to the server</source>
        <translation type="obsolete">impossibile inviare il messaggio. Il server non è connesso</translation>
    </message>
    <message>
        <source>is not connected. Unable to send the message</source>
        <translation type="obsolete">non è connesso. Impossibile inviare il messaggio</translation>
    </message>
    <message>
        <source>is not connected. Unable to add %1 to the contact list</source>
        <translation type="obsolete">non è connesso. Impossibile aggiungere %1 alla lista dei contatti</translation>
    </message>
    <message>
        <source>is not connected. Unable to remove %1 from the contact list</source>
        <translation type="obsolete">non è connesso. Impossibile rimuovere %1 dalla lista dei contatti</translation>
    </message>
    <message>
        <source>connected to the server with user %1</source>
        <translation type="obsolete">connessione riuscita con l&apos;utenza %1</translation>
    </message>
    <message>
        <source>disconnected from the server</source>
        <translation type="obsolete">disconnessione dal server effettuata</translation>
    </message>
    <message>
        <source>error occurred (%1)</source>
        <translation type="obsolete">si è verificato un errore (%1)</translation>
    </message>
</context>
</TS>
