<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="cs_CZ">
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="292"/>
        <source>Header</source>
        <translation>Hlavička</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="293"/>
        <source>System</source>
        <translation>Systém</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="294"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="295"/>
        <source>Connection</source>
        <translation>Spojení</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="296"/>
        <source>User Status</source>
        <translation>Status uživatele</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="297"/>
        <source>User Information</source>
        <translation>Informace o uživateli</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="298"/>
        <source>File Transfer</source>
        <translation>Přenos souborů</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="299"/>
        <source>History</source>
        <translation>Historie</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="300"/>
        <source>Other</source>
        <translation>Ostatní</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../src/core/Core.cpp" line="127"/>
        <source>%1 Unable to connect to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Nelze se připojit k %2 síti. Zkontrolujte prosím  nastavení firewallu.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="152"/>
        <source>%1 Unable to broadcast to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Nelze prohledat %2 síť. Zkontrolujte prosím  nastavení firewallu.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="165"/>
        <source>%1 You are connected to %2 Network.</source>
        <translation>%1 Jste připojeni do %2 sítě.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="277"/>
        <source>%1 You are disconnected from %2 Network.</source>
        <translation>%1 jste odpojeni ze %2 sítě.</translation>
    </message>
    <message>
        <source>%1 %2 will search users in these IP addresses: %3</source>
        <translation type="obsolete">%1 %2 bude hledat uživatele na těchto IP adresách: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="230"/>
        <source>%1 Zero Configuration started with service name: %2</source>
        <translation>%1 Zero Configuration začala s názvem služby: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="81"/>
        <source>%1 User %2 cannot save settings in path: %3</source>
        <translation>%1 Uživatel %2 nemůže uložit nastavení do: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="97"/>
        <source>%1 User %2 cannot save chat messages in path: %3</source>
        <translation>%1 Uživatel %2 nemůže uložit zprávy do: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="176"/>
        <source>%1 You have selected to join only in these workgroups: %2</source>
        <translation>%1 Zvolili jste připojení pouze do těchto pracovních skupin: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="245"/>
        <source>%1 Zero Configuration service closed.</source>
        <translation>%1 Zero Configuration byla ukončena.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="300"/>
        <source>%1 Zero Configuration is browsing network for service: %2</source>
        <translation>%1 Zero Configuration prohlíží síť a hledá službu: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="307"/>
        <source>%1 Zero Configuration cannot browse network for service: %2</source>
        <translation>%1 Zero Configuration nemůže procházet síť a hledat službu: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="325"/>
        <source>%1 Broadcasting to the %2 Network...</source>
        <translation>%1 Prohledávání %2 sítě...</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="331"/>
        <source>%1 You are not connected to %2 Network.</source>
        <translation>%1 Nejste připojen do %2 sítě.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="337"/>
        <source>%1 %2 has found a filter on UDP port %3. Please check your firewall settings.</source>
        <translation>%1 %2 nalezl filtr na UDP portu %3. Zkontrolujte prosím nastavení firewallu.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="341"/>
        <source>View the log messages for more informations</source>
        <translation>Zobrazit logy pro více informací</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="511"/>
        <source>New version is available</source>
        <translation>Je dostupná nová verze</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="512"/>
        <source>Click here to download</source>
        <translation>Zde kliknout pro stažení</translation>
    </message>
    <message>
        <source>%1 You cannot reach %2 Network.</source>
        <translation type="obsolete">%1 síť %2 je nedostupná.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="389"/>
        <source>is connected from external network (the new subnet is added to your broadcast address list).</source>
        <translation>je připojen z vnější sítě (do seznamu adres je přidána nová podsíť).</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="401"/>
        <source>%1 Checking %2 more addresses...</source>
        <translation>%1 Kontrola %2 více  adres...</translation>
    </message>
    <message>
        <source>%1 Contacting %2 ...</source>
        <translation type="obsolete">%1 kontaktuje %2 ...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="46"/>
        <source>%1 Chat with all local users.</source>
        <translation>%1 Chat se všemi uživateli.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="83"/>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to you!</source>
        <translation>Všechno nejlepší!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to %1!</source>
        <translation>Všechno nejlepší %1!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="64"/>
        <source>Happy birthday to Marco Mastroddi: %1 years old today! Cheers!!!</source>
        <translation>Všechno nejlepší k narozeninám vám přeje tvůrce programu Marco Mastroddi: dnes je vám %1 let! Na zdraví!!!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="76"/>
        <source>Happy New Year!</source>
        <translation>Šťastný Nový rok!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="99"/>
        <location filename="../src/core/CoreChat.cpp" line="164"/>
        <location filename="../src/core/CoreChat.cpp" line="244"/>
        <source>%1 Chat with %2.</source>
        <translation>%1 Chat s %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="147"/>
        <source>%1 You have created the group %2.</source>
        <translation>%1 Vytvořili jste skupinu %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="153"/>
        <source>%1 Welcome to the group %2.</source>
        <translation>%1 Vítejte ve skupině %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="198"/>
        <source>%1 The group has a new name: %2.</source>
        <translation>%1 Skupina má nové jméno: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="230"/>
        <source>%1 Members removed from the group: %2.</source>
        <translation>%1 Uživatelé byli odstraněni ze skupiny: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="237"/>
        <source>%1 Members added to the group: %2.</source>
        <translation>%1 Uživatelů přidáno do skupiny: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="262"/>
        <source>Unable to send the message: you are not connected.</source>
        <translation>Nelze odeslat zprávu: nejste připojeni.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="268"/>
        <source>Unable to send the message: this chat is disabled.</source>
        <translation>Nelze poslat zprávu: tento chat je zakázán.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="302"/>
        <source>Unable to send the message to %1.</source>
        <translation>Nelze odeslat zprávu %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="330"/>
        <source>The message will be delivered to %1.</source>
        <translation>Zpráva bude doručena uživateli %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="335"/>
        <source>Nobody has received the message.</source>
        <translation>Zprávu nikdo neobdržel.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="394"/>
        <source>%1 %2 can not join to the group.</source>
        <translation>%1 %2 se nemůže připojit do skupiny.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="457"/>
        <source>%1 saved chats are added to history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="573"/>
        <source>Offline messages sent to %2.</source>
        <translation>Offline zprávy odeslané %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="410"/>
        <source>%1 %2 cannot be informed that you have left the group.</source>
        <translation>%1 %2 nemůže být informován, že jste opustil skupinu.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="53"/>
        <source>Help me to know how many people are really using BeeBEEP.</source>
        <translation>Pomozte nám zjistit, kolik uživatelů reálně používá BeeBEEP.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="55"/>
        <source>Please add a like on Facebook.</source>
        <translation>Přidejte prosím lajk na Facebook.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="502"/>
        <source>%1 You have left the group.</source>
        <translation>%1 Opustil jste skupinu.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="504"/>
        <source>%1 %2 has left the group.</source>
        <translation>%1 %2 opustil skupinu.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="218"/>
        <source>%1 (%2) is disconnected from %3 network.</source>
        <translation>%1 (%2) je odpojen z %3 sítě.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="338"/>
        <source>%1 (%2) is connected to %3 network.</source>
        <translation>Uživatel %1 (%2) je připojen do %3 sítě.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="435"/>
        <source>%1 Network interface %2 is gone down.</source>
        <translation>%1Síťové rozhraní %2 spadlo.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="48"/>
        <source>%1 Unable to start file transfer server: bind address/port failed.</source>
        <translation>%1 Nelze spustit server přenosu souborů: bind adresa/port selhal.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="94"/>
        <source>%1 Unable to download %2 from %3: user is offline.</source>
        <translation>%1 Nelze stáhnout %2 od %3: uživatel je odpojen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="109"/>
        <source>%1 Unable to download %2 from %3: folder %4 cannot be created.</source>
        <translation>%1 Nelze stáhnout %2 od %3: složku %4 nelze vytvořit.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="121"/>
        <source>%1 Downloading %2 from %3.</source>
        <translation>%1 Stahování %2 od %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>from</source>
        <translation>od</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>to</source>
        <translation>uživateli</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="184"/>
        <source>Open</source>
        <translation>Otevřít</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="238"/>
        <source>%1 Unable to send %2 to %3: user is offline.</source>
        <translation>%1 Nelze odeslat %2 uživateli  %3: uživatel je odpojen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="215"/>
        <source>%1 Unable to send %2. File transfer is disabled.</source>
        <translation>%1 Nelze odeslat %2. Přenos souborů je zakázán.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Download</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Upload</source>
        <translation>Odesláno</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="187"/>
        <source>folder</source>
        <translation>složku</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="247"/>
        <source>%1 %2: file not found.</source>
        <translation>%1 %2: soubor nenalezen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="257"/>
        <source>%1 %2 is a folder. You can share it.</source>
        <translation>%1 %2 je složka. Můžete ji sdílet.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="278"/>
        <source>%1 Unable to send %2: %3 is not connected.</source>
        <translation>%1 Nelze odeslat %2: %3 není připojen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="272"/>
        <source>%1 You send %2 to %3.</source>
        <translation>%1 Odeslal jste  %2 uživateli  %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="303"/>
        <source>%1 You have refused to download %2 from %3.</source>
        <translation>%1 jste odmítl stáhnout %2 od %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="331"/>
        <source>%1 You have refused to download folder %2 from %3.</source>
        <translation>%1 jste odmítl stáhnout složku %2 od %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="394"/>
        <source>Adding to file sharing</source>
        <translation>Přidat ke sdílení</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="449"/>
        <source>%1 is added to file sharing (%2)</source>
        <translation>%1 je přidán ke sdílení (%2)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="451"/>
        <source>%1 is added to file sharing with %2 files, %3 (elapsed time: %4)</source>
        <translation>%1 je přidán do sílení souborů s %2 souborem, %3 (uplynulý čas: %4)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="487"/>
        <source>All paths are removed from file sharing</source>
        <translation>Všechny cesty jsou odstraněny ze sdílení</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="509"/>
        <source>%1 is removed from file sharing</source>
        <translation>%1 se odstraní ze sdílení</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="511"/>
        <source>%1 is removed from file sharing with %2 files</source>
        <translation>%1 je odebrán ze sdílení s %2 soubory</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="559"/>
        <source>%1 You are about to send %2 to %3. Checking folder...</source>
        <translation>%1 Chystáte se odeslat %2 k uživateli  %3. Kontroluji složku...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="594"/>
        <source>%1 Unable to send folder %2</source>
        <translation>%1 Nelze odeslat složku %2</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="599"/>
        <source>the folder is empty.</source>
        <translation>Složka je prázdná.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="606"/>
        <source>file transfer is not working.</source>
        <translation>přenos souborů nefunguje.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="614"/>
        <source>%1 is not connected.</source>
        <translation>%1 není připojen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="628"/>
        <source>internal error.</source>
        <translation>interní chyba.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="633"/>
        <source>%1 You send folder %2 to %3.</source>
        <translation>%1 poslal jste složku %2 k %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="148"/>
        <source>%1 %2 has refused to download %3.</source>
        <translation>%1 %2 odmítl stáhnout %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="175"/>
        <source>%1 %2 is sending to you the file: %3.</source>
        <translation>%1 %2 vám posílá soubor: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="245"/>
        <source>%1 An error occurred when %2 tries to add you to the group chat: %3.</source>
        <translation>%1 došlo k chybě, když se vás %2 snažil přidat do skupiny: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="251"/>
        <source>%1 %2 adds you to the group chat: %3.</source>
        <translation>%1 %2 si vás přidal do skupiny: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="285"/>
        <source>%1 %2 has not shared files.</source>
        <translation>%1 %2 nesdílí soubory.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="290"/>
        <source>%1 %2 has shared %3 files.</source>
        <translation>%1 %2 sdílí %3 souborů.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="319"/>
        <source>%1 %2 has refused to download folder %3.</source>
        <translation>%1 %2 odmítl stáhnout složku %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="326"/>
        <source>unknown folder</source>
        <translation>neznámá složka</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="334"/>
        <source>%1 %2 is sending to you the folder: %3.</source>
        <translation>%1 %2 vám posílá složku: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="86"/>
        <source>You are</source>
        <translation>Jste</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="88"/>
        <source>%1 is</source>
        <translation>%1 je</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="101"/>
        <source>You have changed your nickname from %1 to %2.</source>
        <translation>Změnil jste svou přezdívku z %1 na %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="103"/>
        <source>%1 has changed the nickname in %2.</source>
        <translation>%1 změnil přezdívku na %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="117"/>
        <source>The %1&apos;s profile has been received.</source>
        <translation>Profil %1 byl aktualizován.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>You share this information</source>
        <translation>Sdílet tyto informace</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>%1 shares this information</source>
        <translation>%1 sdílí tyto informace</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="235"/>
        <source>%1 You have created group from chat: %2.</source>
        <translation>%1 vytvořil jste skupinu z chatu: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="290"/>
        <source>%1 You have deleted group: %2.</source>
        <translation>%1 smazal jste skupinu: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="377"/>
        <source>is removed from favorites</source>
        <translation>odebrán z oblíbených</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="383"/>
        <source>is added to favorites</source>
        <translation>přidán do oblíbených</translation>
    </message>
</context>
<context>
    <name>File</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="274"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="275"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="276"/>
        <source>Image</source>
        <translation>Obrázek</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="277"/>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="278"/>
        <source>Other</source>
        <translation>Ostatní</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="279"/>
        <source>Executable</source>
        <translation>Spustitelný</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="280"/>
        <source>MacOSX</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>FileTransferPeer</name>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="77"/>
        <source>invalid file header</source>
        <translation>neplatná hlavička souboru</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="105"/>
        <source>Unable to open file</source>
        <translation>Nelze otevřít soubor</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="112"/>
        <source>Unable to write in the file</source>
        <translation>Nelze zapisovat do souboru</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="119"/>
        <source>%1 bytes downloaded but the file size is only %2 bytes</source>
        <translation>%1 bajtů staženo ale velikost souboru je pouze %2 bajtů</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="52"/>
        <source>Transfer cancelled</source>
        <translation>Přenos ukončen</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="113"/>
        <source>Transfer completed in %1</source>
        <translation>Přenos dokončen za  %1</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="171"/>
        <source>Connection timeout</source>
        <translation>Čas spojení vypršel</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="179"/>
        <source>Transfer timeout</source>
        <translation>Časový limit souboru</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="101"/>
        <source>unable to send file header</source>
        <translation>nelze poslat hlavičku souboru</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="116"/>
        <source>%1 bytes uploaded but the file size is only %2 bytes</source>
        <translation>%1 bajtů odesláno ale velikost souboru je pouze %2 bajtů</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="123"/>
        <source>%1 bytes sent not confirmed (%2 bytes confirmed)</source>
        <translation>odeslání %1 bajtůnebylo potvrzeno (%2 bajtů potvrzeno)</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="154"/>
        <source>Unable to upload data</source>
        <translation>Nelze odeslat data</translation>
    </message>
</context>
<context>
    <name>GuiAddUser</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="36"/>
        <source>Add user</source>
        <translation>Přidat uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="39"/>
        <source>your IP is %1 in LAN %2</source>
        <translation>Vaše IP je %1 v LAN %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Comment</source>
        <translation>Komentář</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Address</source>
        <translation>Adresa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="104"/>
        <source>Please insert a valid IP address.</source>
        <translation>Vložte platnou IP adresu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="114"/>
        <source>Please insert a valid port or use the default one %1.</source>
        <translation>Vložte platný port nebo použijte základní %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="148"/>
        <source>These IP address and port are already inserted in list.</source>
        <translation>Tato IP adresa a port již v seznamu existují.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="182"/>
        <source>Remove user path</source>
        <translation>Odebrat uživatelskou cestu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="184"/>
        <source>Clear all</source>
        <translation>Odstranit vše</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="209"/>
        <source>Please select an user path in the list.</source>
        <translation>Vybrat uživatelskou cestu ze seznamu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="234"/>
        <source>auto added</source>
        <translation>Přidat automaticky</translation>
    </message>
</context>
<context>
    <name>GuiAddUserDialog</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="73"/>
        <source>Add an IP address and port of the user you want to connect</source>
        <translation>Přidat IP adresu a port uživatele, kterého chcete připojit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="89"/>
        <source>IP Address</source>
        <translation>Ip adresa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="121"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="169"/>
        <source>Split in IPv4 addresses</source>
        <translation>Rozdělit na IPv4 adresy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="179"/>
        <source>Add</source>
        <translation>Přidat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="176"/>
        <source>Click here to add user path</source>
        <translation>Zde kliknout pro přidání uživatelské cesty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="156"/>
        <source>Comment</source>
        <translation>Komentář</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="25"/>
        <source>Auto add from LAN</source>
        <translation>Přidat automaticky z LAN</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="45"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="52"/>
        <source>Cancel</source>
        <translation>Ukončit</translation>
    </message>
</context>
<context>
    <name>GuiAskPassword</name>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="23"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="30"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="62"/>
        <source>Use standard session (encrypted but authentication is not required)</source>
        <translation>Použít standardní relaci (šifrováno, ale není vyžadováno ověření)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="69"/>
        <source>Use authentication password (spaces are removed) *</source>
        <translation>Použít ověření hesla (mezery budou odstraněny)) *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="124"/>
        <source>* Password must be the same for all user you want to connect</source>
        <translation>*Heslo musí být stejné pro všechny uživatele, které chcete připojit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="153"/>
        <source>Remember password (not recommended)</source>
        <translation>Pamatovat si heslo (není doporučeno)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="160"/>
        <source>Show this dialog at connection startup</source>
        <translation>Zobrazit tento dialog při spuštění</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="33"/>
        <source>Chat Password - %1</source>
        <translation>Heslo chatu - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="103"/>
        <source>Password is empty. Please enter a valid one (spaces are removed).</source>
        <translation>Heslo je prázdné. Zadejte prosím platné heslo (mezery budou odstraněny).</translation>
    </message>
</context>
<context>
    <name>GuiChat</name>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="139"/>
        <source>Change font style</source>
        <translation>Změnit styl písma</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="140"/>
        <source>Select your favourite chat font style</source>
        <translation>Vybrat styl písma pro zprávy chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="141"/>
        <source>Change font color</source>
        <translation>Změnit barvu písma</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="142"/>
        <source>Select your favourite font color for the chat messages</source>
        <translation>Vybrat barvu písma pro zprávy chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="143"/>
        <source>Change background color</source>
        <translation>Změnit barvu pozadí</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="144"/>
        <source>Select your favourite background color for the chat window</source>
        <translation>Vybrat barvu pozadí pro chatovací okno</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="145"/>
        <source>Filter message</source>
        <translation>Filtr zpráv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="146"/>
        <source>Select the message types which will be showed in chat</source>
        <translation>Vybrat typy zpráv, které se budou zobrazovat v chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="147"/>
        <source>Chat settings</source>
        <translation>Nastavení chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="148"/>
        <source>Click to show the settings menu of the chat</source>
        <translation>Nastavení chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="149"/>
        <source>Spell checking</source>
        <translation>Kontrola pravopisu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="151"/>
        <source>Word completer</source>
        <translation>Doplňovač slov</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="153"/>
        <source>Use Return key to send message</source>
        <translation>Použít klávesu Zpět k odeslání zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="158"/>
        <source>Members</source>
        <translation>Uživatelé</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="159"/>
        <source>Show the members of the chat</source>
        <translation>Zobrazit uživatele chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="165"/>
        <location filename="../src/desktop/GuiChat.cpp" line="1076"/>
        <source>Find text in chat</source>
        <translation>Najít text v chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="166"/>
        <source>Send file</source>
        <translation>Poslat soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="167"/>
        <source>Send a file to a user or a group</source>
        <translation>Poslat soubor uživateli nebo skupině</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="168"/>
        <source>Send folder</source>
        <translation>Poslat složku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="169"/>
        <source>Save chat</source>
        <translation>Uložit chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="170"/>
        <source>Save the messages of the current chat to a file</source>
        <translation>Uložit zprávy do souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="171"/>
        <source>Print...</source>
        <translation>Tisk...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="180"/>
        <source>Change the name of the group or add and remove users</source>
        <translation>Změnit jméno skupiny nebo přidat a odebrat uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="229"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="415"/>
        <source>unread messages</source>
        <translation>nepřečtené zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="964"/>
        <source>Spell checking is enabled</source>
        <translation>Kontrola pravopisu je povolena</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="971"/>
        <location filename="../src/desktop/GuiChat.cpp" line="995"/>
        <source>There is not a valid dictionary</source>
        <translation>Není dostupný příslušný slovník</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="988"/>
        <source>Word completer is enabled</source>
        <translation>Doplňovač slov je povolen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="990"/>
        <source>Word completer is disabled</source>
        <translation>Doplňovač slov je zakázán</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="1114"/>
        <source>%1 not found in chat.</source>
        <translation>%1 nebyl nalezen v chatu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="966"/>
        <source>Spell checking is disabled</source>
        <translation>Kontrola pravopisu je zakázána</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="173"/>
        <source>Clear messages</source>
        <translation>Odstranit zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="174"/>
        <source>Clear all the messages of the chat</source>
        <translation>Odstranit všechny zprávy chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="177"/>
        <source>Create group from chat</source>
        <translation>Vytvořit skupinu z chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="178"/>
        <source>Create a group from this chat</source>
        <translation>Vytvořit skupinu z tohoto chatu</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation type="obsolete">Vytvořit skupinu</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation type="obsolete">Vytvořit skupinu ze dvou nebo více uživatelů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="181"/>
        <location filename="../src/desktop/GuiChat.cpp" line="182"/>
        <source>Leave the group</source>
        <translation>Opustit skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="223"/>
        <source>Copy to clipboard</source>
        <translation>Kopírovat do schránky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="225"/>
        <source>Select All</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="257"/>
        <source>Show only messages in default chat</source>
        <translation>Ve výchozím chatu zobrazit pouze zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="297"/>
        <source>Last message %1</source>
        <translation>Poslední zpráva%1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="395"/>
        <source>All Lan Users</source>
        <translation>Všichni uživatelé v síti</translation>
    </message>
    <message>
        <source>(You have left)</source>
        <translation type="obsolete">(Opustili jste)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="411"/>
        <source>You</source>
        <translation>Já</translation>
    </message>
    <message>
        <source>Create chat</source>
        <translation type="obsolete">Vytvořit chat</translation>
    </message>
    <message>
        <source>Create a chat with two or more users</source>
        <translation type="obsolete">Vytvořit chat s více uživateli</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="179"/>
        <source>Edit group</source>
        <translation>Editovat skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="437"/>
        <source>offline</source>
        <translation>Nedostupný</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="445"/>
        <source>Show profile</source>
        <translation>Zobrazit profil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="455"/>
        <source>Show members</source>
        <translation>Zobrazit uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>Nobody</source>
        <translation>Nikdo</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>and</source>
        <translation>a</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="537"/>
        <source>last %1 messages</source>
        <translation>poslední %1 zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="706"/>
        <source>Please select a file to save the messages of the chat.</source>
        <translation>Zadejte prosím název souboru pro uložení zpráv z chatu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>%1: save completed.</source>
        <translation>%1: uložení kompletní.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="771"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Nelze uložit dočasný soubor: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="819"/>
        <source>Do you really want to send %1 %2 to the members of this chat?</source>
        <translation>Chcete opravdu poslat %1 %2 uživatelům toho chatu?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>file</source>
        <translation>soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>files</source>
        <translation>soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="834"/>
        <source>Qt library for this OS doesn&apos;t support Drag and Drop for files. You have to select again the file to send.</source>
        <translation>Qt knihovna pro tento operační systém nepodporuje Drag and Drop vkládání pro soubory. Vyberte znovu soubory k odeslání.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="948"/>
        <source>Use key Return to send message</source>
        <translation>Použít klávesu Zpět k odeslání zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="950"/>
        <source>Use key Return to make a carriage return</source>
        <translation>Použít klávesu Zpět k návratu na začátek textu</translation>
    </message>
</context>
<context>
    <name>GuiChatList</name>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="49"/>
        <source>Show</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="52"/>
        <source>Clear</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="53"/>
        <source>Clear all chat messages</source>
        <translation>Odstranit všechny zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="55"/>
        <source>Delete</source>
        <translation>Odstranit</translation>
    </message>
</context>
<context>
    <name>GuiChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="20"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="238"/>
        <source>Write to: &lt;b&gt;ALL&lt;/b&gt;</source>
        <translation>Napsat: &lt;b&gt;všem&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="260"/>
        <source>...</source>
        <translation>....</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="288"/>
        <source>Save window&apos;s geometry</source>
        <translation>Uložit velikost okna</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="326"/>
        <source>Detach chat</source>
        <translation>Odpojit chat</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;.Helvetica Neue DeskInterface&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;.Helvetica Neue DeskInterface&apos;; font-size:13pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="136"/>
        <source>Click to send message or just hit enter</source>
        <translation>Kliknout pro odeslání zprávy nebo zmáčknout enter</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroup</name>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="26"/>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="42"/>
        <source>TextLabel</source>
        <translation>Název</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="74"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="81"/>
        <source>Cancel</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="37"/>
        <source>Group name</source>
        <translation>Název skupiny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="38"/>
        <source>Please add member in the group:</source>
        <translation>Přidat uživatele do skupiny:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="41"/>
        <source>Users</source>
        <translation>Uživatelé</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="65"/>
        <source>Create Group - %1</source>
        <translation>Vytvořit skupinu - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="67"/>
        <source>Create Chat - %1</source>
        <translation>Vytvořit chat - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="73"/>
        <source>Edit Group - %1</source>
        <translation>Editovat profil %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="75"/>
        <source>Edit Chat - %1</source>
        <translation>Editovat chat %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="113"/>
        <source>Please select two or more member for the group.</source>
        <translation>Označte prosím dva nebo více uživatelů, které chcete přidat do skupiny.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="121"/>
        <source>Please insert a group name.</source>
        <translation>Vložte jméno skupiny.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="130"/>
        <source>%1 already exists as group name or chat name.
Please select a different name.</source>
        <translation>%1 již existuje jako jméno skupiny nebo jméno uživatele chatu.
Vyberte prosím jiné jméno.</translation>
    </message>
</context>
<context>
    <name>GuiEditVCard</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="35"/>
        <source>Edit your profile</source>
        <translation>Editovat profil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="103"/>
        <source>%1 - Select your profile photo</source>
        <translation>%1 -Vybrat profilovou fotku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="104"/>
        <source>Images</source>
        <translation>Obrázek</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <source>Unable to load image %1.</source>
        <translation>Nelze nahrát obrázek %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="147"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="156"/>
        <source>Please insert your nickname.</source>
        <translation>Vložte svoji přezdívku.</translation>
    </message>
</context>
<context>
    <name>GuiEmoticons</name>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="49"/>
        <source>Recent</source>
        <translation>Nedávné</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="53"/>
        <source>Smiley</source>
        <translation>Smajlík</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="57"/>
        <source>Objects</source>
        <translation>Objekty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="61"/>
        <source>Nature</source>
        <translation>Příroda</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="65"/>
        <source>Places</source>
        <translation>Místa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="69"/>
        <source>Symbols</source>
        <translation>Symboly</translation>
    </message>
</context>
<context>
    <name>GuiFileInfoList</name>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Shared folders and files</source>
        <translation>Sdílené složky a soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
</context>
<context>
    <name>GuiFloatingChat</name>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="43"/>
        <source>Show the bar of chat</source>
        <translation>Zobrazit lištu chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="51"/>
        <source>Emoticons</source>
        <translation>Smajlíci</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="63"/>
        <source>Show the emoticon panel</source>
        <translation>Zobraz panel smajlíků</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="64"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Přidat vašeho oblíbeného smajlíka do zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="244"/>
        <source>Window&apos;s geometry and state saved</source>
        <translation>Stav a velikost okna uloženy</translation>
    </message>
</context>
<context>
    <name>GuiGroupList</name>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="48"/>
        <source>Create group</source>
        <translation>Vytvořit skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="51"/>
        <source>Edit group</source>
        <translation>Editovat skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="54"/>
        <source>Open chat</source>
        <translation>Otevřít chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="57"/>
        <source>Enable notifications</source>
        <translation>Povolit oznámení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="60"/>
        <source>Disable notifications</source>
        <translation>Zakázat oznámení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="63"/>
        <source>Delete group</source>
        <translation>Odstranit skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="143"/>
        <source>Waiting for two or more connected user</source>
        <translation>Vyčkat na dva nebo více uživatelů</translation>
    </message>
</context>
<context>
    <name>GuiHome</name>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="39"/>
        <source>Home</source>
        <translation>Domů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="49"/>
        <source>Select a user you want to chat with or</source>
        <translation>Vybrat uživatele k chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="94"/>
        <source>Copy to clipboard</source>
        <translation>Kopírovat do schránky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="96"/>
        <source>Select All</source>
        <translation>Vybrat vše</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="98"/>
        <source>Print...</source>
        <translation>Tisk...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="105"/>
        <source>Show the datestamp</source>
        <translation>Zobrazit datum zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="109"/>
        <source>Show the timestamp</source>
        <translation>Zobrazit čas zprávy</translation>
    </message>
</context>
<context>
    <name>GuiHomeWidget</name>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="107"/>
        <source>chat with all users</source>
        <translation>Chat se všemi uživateli</translation>
    </message>
</context>
<context>
    <name>GuiLanguage</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="35"/>
        <source>Select language</source>
        <translation>Vybrat jazyk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="50"/>
        <source>For the latest language translations please visit the %1</source>
        <translation>Pro nejnovější jazykový soubor navštivte prosím %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="51"/>
        <source>official website</source>
        <translation>oficiální webová stránka</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="94"/>
        <source>Select a language folder</source>
        <translation>Vybrat složku s jazykem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="121"/>
        <source>Language &apos;%1&apos;&apos; not found.</source>
        <translation>Jazyk &apos;%1&apos;&apos; nebyl nalezen.</translation>
    </message>
</context>
<context>
    <name>GuiLanguageDialog</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="45"/>
        <source>Language</source>
        <translation>Jazyk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="96"/>
        <source>Path</source>
        <translation>Cesta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="127"/>
        <source>Select language folder</source>
        <translation>Vybrat složku s jazykem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="169"/>
        <source>Restore to default language</source>
        <translation>Obnovit výchozí jazyk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="172"/>
        <source>Restore</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="195"/>
        <source>Select</source>
        <translation>Vybrat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="205"/>
        <source>Cancel</source>
        <translation>Ukončit</translation>
    </message>
</context>
<context>
    <name>GuiLog</name>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="36"/>
        <source>System Log</source>
        <translation>Systémový log</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="55"/>
        <source>Save log as</source>
        <translation>Uložit log jako</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="56"/>
        <source>Save the log in a file</source>
        <translation>Uložit log do souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="60"/>
        <source>Log to file</source>
        <translation>Log do souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="72"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="78"/>
        <source>keyword</source>
        <translation>Klíčové slovo</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="84"/>
        <source>Find</source>
        <translation>Najít</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="85"/>
        <source>Find keywords in the log</source>
        <translation>Najít klíčové slovo v logu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="97"/>
        <source>Case sensitive</source>
        <translation>Rozlišovat velikost písmen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="102"/>
        <source>Whole word</source>
        <translation>Celá slova</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="109"/>
        <source>Block scrolling</source>
        <translation>Skákat po odstavcích</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="116"/>
        <source>Please select a file to save the log.</source>
        <translation>Vyberte prosím soubor pro uložení do logu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <source>Unable to save log in the file: %1</source>
        <translation>Nelze uložit log do souboru: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="151"/>
        <source>%1: save log completed.</source>
        <translation>%1: uložení logu ukončeno.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open file</source>
        <translation>Otevřít soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open folder</source>
        <translation>Otevřít složku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="193"/>
        <source>%1 not found</source>
        <translation>%1 nenalezen</translation>
    </message>
</context>
<context>
    <name>GuiLogWidget</name>
    <message>
        <location filename="../src/desktop/GuiLog.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
</context>
<context>
    <name>GuiMain</name>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="86"/>
        <source>Show the main tool bar</source>
        <translation>Zobrazit hlavní panel nástrojů</translation>
    </message>
    <message>
        <source>Show the bar of plugins</source>
        <translation type="obsolete">Zobrazit panel doplňků</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="283"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3291"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3292"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3293"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3294"/>
        <source>offline</source>
        <translation>Odpojen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="326"/>
        <source>Do you really want to quit %1?</source>
        <translation>Chcete opravdu odejít %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="349"/>
        <source>&lt;b&gt;Settings can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Nastavení nemůže být uloženo&lt;/b&gt;. Cesta:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="351"/>
        <location filename="../src/desktop/GuiMain.cpp" line="368"/>
        <source>&lt;b&gt;is not writable&lt;/b&gt; by user:</source>
        <translation>&lt;b&gt;nelze zapisovat&lt;/b&gt; uživatelem:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="353"/>
        <location filename="../src/desktop/GuiMain.cpp" line="370"/>
        <source>Do you want to close anyway?</source>
        <translation>Chcete přesto ukončit?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="414"/>
        <source>No new message available</source>
        <translation>Nejsou dostupné žádné nové zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="498"/>
        <source>Disconnect from %1 network</source>
        <translation>Odpojit ze %1 sítě</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="504"/>
        <source>Connect to %1 network</source>
        <translation>Připojit k %1 síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="587"/>
        <source>Secure Lan Messenger</source>
        <translation>Secure Lan Messenger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="588"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="590"/>
        <source>for</source>
        <translation>pro</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="592"/>
        <source>developed by</source>
        <translation>vyvinuto</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="601"/>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br /&gt;it under the terms of the GNU General Public License as published&lt;br /&gt;by the Free Software Foundation, either version 3 of the License&lt;br /&gt;or (at your option) any later version.&lt;br /&gt;&lt;br /&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br /&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br /&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br /&gt;See the GNU General Public License for more details.</source>
        <translation>BeeBEEP je svobodný software: můžete jej šířit nebo modifikovat podle podmínek GNU General Public License.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="617"/>
        <source>Search for users...</source>
        <translation>Najít uživatele...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="623"/>
        <source>Close the chat and quit %1</source>
        <translation>Zavřít chat a odejít %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="631"/>
        <source>Show the main tool bar with settings</source>
        <translation>Zobrazit hlavní panel nástrojů s nastavením</translation>
    </message>
    <message>
        <source>Show the tool bar with plugin shortcuts</source>
        <translation type="obsolete">Zobrazit panel nástrojů se zkratkami doplňků</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="635"/>
        <source>Show the informations about %1</source>
        <translation>Zobrazit informace o %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="667"/>
        <source>Select language...</source>
        <translation>Vybrat jazyk...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="668"/>
        <source>Select your preferred language</source>
        <translation>Vybrat preferovaný jazyk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="669"/>
        <source>Download folder...</source>
        <translation>Složka pro ukládání souborů...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="670"/>
        <source>Select the download folder</source>
        <translation>Vybrat složku pro ukládání souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="677"/>
        <source>Select beep file...</source>
        <translation>Vybrat zvuk zprávy...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="678"/>
        <source>Select the file to play on new message arrived</source>
        <translation>Vybrat soubor, který bude přehrán při příchodu nové zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="679"/>
        <source>Play beep</source>
        <translation>Přehrát zvuk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="680"/>
        <source>Test the file to play on new message arrived</source>
        <translation>Test souboru, který bude přehrán při příchodu nové zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="783"/>
        <source>Settings</source>
        <translation>Nastavení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="809"/>
        <source>If enabled the window geometry will be reset to default value at the next startup</source>
        <translation>Pokud je volba povolena, velikost okna bude obnovena do výchozího stavu při dalším spuštění</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="933"/>
        <source>Show only the online users</source>
        <translation>Zobrazit pouze uživatele, kteří jsou online</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="934"/>
        <source>If enabled only the online users are shown in the list</source>
        <translation>Pokud je volba povolena, v seznamu budou zobrazeni pouze připojení uživatelé</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="940"/>
        <source>If enabled you can see a picture of the users in the list (if they have)</source>
        <translation>Pokud je volba povolena, budou v seznamu zobrazeny obrázky uživatelů (pokud jsou k dispozici)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="866"/>
        <source>Set status to away automatically</source>
        <translation>Nastavit stav Nepřítomný automaticky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="867"/>
        <source>If enabled %1 change your status to away after an idle of %2 minutes</source>
        <translation>Pokud je volba povolena, %1 změní váš stav na Nepřítomný za %2 minut</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="881"/>
        <source>If enabled when a new message is arrived a sound is emitted</source>
        <translation>Pokud je volba povolena, při přijetí zprávy zazní zvukový signál</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="902"/>
        <source>If enabled when a new message is arrived %1 is shown on top of all other windows</source>
        <translation>Pokud je volba povolena, nově přijatá zpráva  %1 bude zobrazena nad všemi ostatními okny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="844"/>
        <source>Generate automatic filename</source>
        <translation>Generovat jména souborů automaticky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="845"/>
        <source>If the file to be downloaded already exists a new filename is automatically generated</source>
        <translation>Pokud název stahovaného souboru již existuje, bude nový soubor automaticky přejmenován</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1094"/>
        <source>Enable tray icon notification</source>
        <translation>Povolit oznámení v systémové liště</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1095"/>
        <source>If enabled tray icon shows some notification about status and message</source>
        <translation>Pokud je volba povolena,ikona v systémové liště zobrazuje některá oznámení a zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="908"/>
        <source>If enabled %1 stays on top of the other windows</source>
        <translation>Pokud je volba povolena,  %1 zůstává vždy na vrchu nad ostatními okny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="829"/>
        <source>Enable file transfer</source>
        <translation>Povolit přenos souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="830"/>
        <source>If enabled you can transfer files with the other users</source>
        <translation>Pokud je volba povolena, je možné přenášet soubory s jinými uživateli</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="790"/>
        <source>Prompts for network password on startup</source>
        <translation>Vyzvat k zadání síťového hesla při spuštění</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="791"/>
        <source>If enabled the password dialog will be shown on connection startup</source>
        <translation>Pokud je volba povolena,po spuštění bude požadováno zadání hesla</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="820"/>
        <source>Load %1 on Windows startup</source>
        <translation>Spustit %1 při startu systému</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="821"/>
        <source>If enabled you can automatically load %1 at system startup</source>
        <translation>Pokud je volba povolena, %1 se automaticky spustí po startu systému</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="709"/>
        <source>Enable the compact mode in chat window</source>
        <translation>Povolit kompaktní okno chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="710"/>
        <source>If enabled the sender&apos;s nickname and his message are in the same line</source>
        <translation>Pokud je volba povolena, přezdívka odesilatele a jeho zpráva budou na stejném řádku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="715"/>
        <source>Add a blank line between the messages</source>
        <translation>Přidat mezeru mezi zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="716"/>
        <source>If enabled the messages in the chat window are separated by a blank line</source>
        <translation>Pokud je volba povolena, zprávy v okně chatu budou odděleny prázdným řádkem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="722"/>
        <source>If enabled the message shows its timestamp in the chat window</source>
        <translation>Pokud je volba povolena, zpráva se zobrazí s časovou značkou v okně chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="704"/>
        <source>If enabled the user&apos;s nickname in chat and in list is colored</source>
        <translation>Pokud je volba povolena, přezdívka uživatele v chatu a v seznamu bude barevná</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="770"/>
        <source>Use HTML tags</source>
        <translation>Použít HTML tagy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="771"/>
        <source>If enabled HTML tags are not removed from the message</source>
        <translation>Pokud je volba povolena, HTML tagy nebudou ze zprávy odstraněny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="776"/>
        <source>Use clickable links</source>
        <translation>Použít klikací odkazy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="777"/>
        <source>If enabled the links in the message are recognized and made clickable</source>
        <translation>Pokud je volba povolena, odkazy budou rozpoznány a budou moci být otevřeny poklikem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="751"/>
        <source>Show messages grouped by user</source>
        <translation>Seskupit zprávy od jednoho uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="752"/>
        <source>If enabled the messages will be shown grouped by user</source>
        <translation>Pokud bude volba povolena, zprávy budou seskupeny dle uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="695"/>
        <source>Save messages</source>
        <translation>Uložit zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="663"/>
        <source>Add users manually...</source>
        <translation>Ručně přidat uživatele...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="664"/>
        <source>Add the IP address and the port of the users you want to connect</source>
        <translation>Přidat IP adresu a port uživatele, kterého chcete připojit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="696"/>
        <source>If enabled the messages are saved when the program is closed</source>
        <translation>Pokud je volba povolena, zprávy budou před uzavřením programu uloženy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="965"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="966"/>
        <source>Select your status</source>
        <translation>Vybrat váš stav</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="972"/>
        <location filename="../src/desktop/GuiMain.cpp" line="988"/>
        <source>Your status will be %1</source>
        <translation>Váš stav bude %1</translation>
    </message>
    <message>
        <source>Add a status description...</source>
        <translation type="obsolete">Přidat nový stav...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1008"/>
        <source>Show the chat</source>
        <translation>Zobrazit chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1009"/>
        <source>Show the chat view</source>
        <translation>Zobrazit náhled chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1010"/>
        <source>Show my shared files</source>
        <translation>Zobrazit moje sdílené soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1011"/>
        <source>Show the list of the files which I have shared</source>
        <translation>Zobrazit seznam sdílených souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1012"/>
        <source>Show the network shared files</source>
        <translation>Zobrazit sdílené soubory v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1013"/>
        <source>Show the list of the network shared files</source>
        <translation>Zobrazit seznam souborů sdílených po síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1022"/>
        <source>Plugins</source>
        <translation>Doplňky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1026"/>
        <source>Tip of the day</source>
        <translation>Tip dne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1027"/>
        <source>Show me the tip of the day</source>
        <translation>Zobrazit tip dne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1028"/>
        <source>Fact of the day</source>
        <translation>Citát dne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1029"/>
        <source>Show me the fact of the day</source>
        <translation>Zobrazit citát dne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1032"/>
        <source>Show %1&apos;s license...</source>
        <translation>Zobrazit %1&apos;s licenci...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1033"/>
        <source>Show the informations about %1&apos;s license</source>
        <translation>Zobrazit informaci o %1&apos;s licenci</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1035"/>
        <source>Show the informations about Qt library</source>
        <translation>Zobrazit informaci o Qt knihovne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1037"/>
        <source>Open %1 official website...</source>
        <translation>Otevřít %1 oficiální webovou stránku...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1038"/>
        <source>Explore %1 official website</source>
        <translation>Procházet %1 oficiální webovou stránku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1039"/>
        <source>Check for new version...</source>
        <translation>Zkontrolovat novou verzi...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1040"/>
        <source>Open %1 website and check if a new version exists</source>
        <translation>Otevřít %1 webovou stránku a zkontrolovat, jestli je k dispozici nová verze</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1041"/>
        <source>Download plugins...</source>
        <translation>Stažení doplňků...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1042"/>
        <source>Open %1 website and download your preferred plugin</source>
        <translation>Otevřít %1 webovou stránku a stáhnout vybraný doplněk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1043"/>
        <source>Help online...</source>
        <translation>Pomoc online...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1044"/>
        <source>Open %1 website to have online support</source>
        <translation>Otevřít %1 webovou stránku kvůli online pomoci</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1055"/>
        <source>I&apos;m so grateful and pleased about that</source>
        <translation>Jsem tak vděčný a rád</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="181"/>
        <source>Ready</source>
        <translation>Připraven</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1163"/>
        <source>Users</source>
        <translation>Uživatelé</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="497"/>
        <source>Disconnect</source>
        <translation>Odpojit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="503"/>
        <source>Connect</source>
        <translation>Připojit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="618"/>
        <source>Configure %1 network to search a user who is not in your local subnet</source>
        <translation>Konfigurovat %1 síť pro vyhledání uživatele, který není v místní síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="621"/>
        <source>Quit</source>
        <translation>Ukončit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="626"/>
        <source>Edit your profile...</source>
        <translation>Editovat profil...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="627"/>
        <source>Change your profile information like your picture or your email or phone number</source>
        <translation>Změnit informace o tvém profilu - jako obrázek, email nebo telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="634"/>
        <source>About %1...</source>
        <translation>O %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="653"/>
        <source>Main</source>
        <translation>Hlavní nabídka</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="659"/>
        <source>Broadcast to network</source>
        <translation>Prohledat síť</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="660"/>
        <source>Broadcast a message in your network to find available users</source>
        <translation>Prohledat síť a najít dostupné uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="703"/>
        <source>Show colored nickname</source>
        <translation>Zobrazit přezdívku barevně</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="939"/>
        <source>Show the user&apos;s picture</source>
        <translation>Zobrazit u uživatelů obrázky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="693"/>
        <source>Chat</source>
        <translation>Chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="880"/>
        <source>Enable BEEP alert on new message</source>
        <translation>Povolit zvuk při příchodu zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="901"/>
        <source>Raise on top on new message</source>
        <translation>Vyskočit do popředí při přijetí nové zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="907"/>
        <source>Always stay on top</source>
        <translation>Vždy na vrchu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1076"/>
        <source>Load on system tray at startup</source>
        <translation>Spustit při startu do systémové lišty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1077"/>
        <source>If enabled %1 will be start hided in system tray</source>
        <translation>Pokud je volba povolena, %1 se po startu skryje do systémové lišty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="995"/>
        <source>View</source>
        <translation>Zobrazení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="721"/>
        <source>Show the timestamp</source>
        <translation>Zobrazit čas zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="739"/>
        <source>Parse Unicode and ASCII emoticons</source>
        <translation>Zobrazit Unicode a ASCII smajlíky v chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="740"/>
        <source>If enabled the ASCII emoticons will be recognized and shown as images</source>
        <translation>Pokud jsou vybráni ASCII smajlíci, budou rozpoznáni a zobrazeni jako obrázky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="745"/>
        <source>Use native emoticons</source>
        <translation>Použít výchozí smajlíky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="746"/>
        <source>If enabled the emoticons will be parsed by your system font</source>
        <translation>Pokud je volba povolena, smajlíci budou zobrazeny jako font</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1639"/>
        <source>Show only last %1 messages</source>
        <translation>Zobrazit pouze posledních %1 znaků zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1640"/>
        <source>If enabled only the last %1 messages will be shown in chat</source>
        <translation>Pokud je volba povolena,v chatu se zobrazí pouze poslední %1 zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="858"/>
        <source>Prompt before downloading file</source>
        <translation>Vyzvat před stažením souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="859"/>
        <source>If enabled you have to confirm the action before downloading a file</source>
        <translation>Pokud je volba povolena, je potřeba potvrdit výzvu před stažením souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="808"/>
        <source>Reset window geometry at startup</source>
        <translation>Obnovit velikost okna po startu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="923"/>
        <source>Options</source>
        <translation>Možnosti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="945"/>
        <source>Show the user&apos;s vCard on right click</source>
        <translation>Zobrazit vizitku uživatele pravým klikem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="946"/>
        <source>If enabled you can see the user&apos;s vCard when right click on it</source>
        <translation>Pokud je volba povolena, je možno pravým klikem zobrazit vizitku uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="925"/>
        <source>Save the users on exit</source>
        <translation>Uložit uživatele při ukončení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="687"/>
        <source>Open your data folder</source>
        <translation>Otevřít složku se soubory BeeBeep chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="688"/>
        <source>Click to open your data folder</source>
        <translation>Kliknutím otevřít datovou složku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="733"/>
        <source>Show preview of the images</source>
        <translation>Zobrazit náhled obrázku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="734"/>
        <source>If enabled the preview of the downloaded images will be showed in the chat window</source>
        <translation>Pokud je volba povolena, náhled stahovaných obrázků bude zobrazen v okně chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="796"/>
        <source>Show activities home page at startup</source>
        <translation>Zobrazit informace na domovské stránce při startu BeeBeep</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="797"/>
        <source>If enabled the activities home page instead of chat page will be showed at startup</source>
        <translation>Pokud je volba povolena, po spuštění se zobrazí domovská stránka s informacemi</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="802"/>
        <source>Show minimized at startup</source>
        <translation>Minimalizovat po startu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="803"/>
        <source>If enabled %1 is showed minimized at startup</source>
        <translation>Pokud je volba povolena, %1 se po spuštění minimalizuje</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="836"/>
        <source>If a file already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="915"/>
        <source>Prompt on quit (only when connected)</source>
        <translation>Potvrdit v případě ukončení (pokud je uživatel připojen)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="916"/>
        <source>If enabled you will be asked if you really want to close %1</source>
        <translation>Pokud bude volba povolena, bude uživatel dotázán, jestli opravdu chce ukončit program %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="926"/>
        <source>If enabled the user list will be save on exit</source>
        <translation>Pokud je volba povolena, seznam uživatelů bude uložen při ukončení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="959"/>
        <source>Change size of the user&apos;s picture</source>
        <translation>Změnit velikost uživatelských obrázků</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="960"/>
        <source>Click to change the picture size of the users in the list</source>
        <translation>Kliknout pro změnu velikosti obrázku uživatele v seznamu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1006"/>
        <source>Show %1 home</source>
        <translation>Zobrazit %1 domovskou stránku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1007"/>
        <source>Show the homepage with %1 activity</source>
        <translation>Zobrazit domácí stránku %1 chatu s výpisem aktivit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1025"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1034"/>
        <source>Qt Library...</source>
        <translation>Qt knihovna...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1046"/>
        <source>Like %1 on Facebook</source>
        <translation>Lajk %1 na Facebook</translation>
    </message>
    <message>
        <source>Help me to know how many people use BeeBEEP</source>
        <translation type="obsolete">Pomozte mi zjistit, kolik uživatelů používá BeeBEEP</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1054"/>
        <source>Donate for %1</source>
        <translation>Přispět na %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1059"/>
        <source>Show</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1082"/>
        <source>Close button minimize to tray icon</source>
        <translation>Zavřít do systémové lišty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1083"/>
        <source>If enabled when the close button is clicked the window will be minimized to the system tray icon</source>
        <translation>Pokud je volba povolena, klikem na uzavírací tlačítko bude okno minimalizováno do systémové lišty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1088"/>
        <source>Escape key minimize to tray icon</source>
        <translation>Klávesou Escape ukončit do systémové lišty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1089"/>
        <source>If enabled when the escape button is clicked the window will be minimized to the system tray icon</source>
        <translation>Pokud je volba povolena, stiskem klávesy Escape bude okno minimalizováno do systémové lišty</translation>
    </message>
    <message>
        <source>Used Ports</source>
        <translation type="obsolete">Použité porty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1172"/>
        <source>Show the list of the connected users</source>
        <translation>Zobrazit seznam připojených uživatelů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1175"/>
        <source>Groups</source>
        <translation>Skupiny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1184"/>
        <source>Show the list of your groups</source>
        <translation>Zobrazit seznam vašich skupin</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1187"/>
        <source>Chats</source>
        <translation>Chaty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1196"/>
        <source>Show the list of the chats</source>
        <translation>Zobrazit seznam vašich chatů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1199"/>
        <source>History</source>
        <translation>Historie</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1208"/>
        <source>Show the list of the saved chats</source>
        <translation>Zobrazit seznam uložených chatů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1211"/>
        <source>File Transfers</source>
        <translation>Přenos souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1220"/>
        <source>Show the list of the file transfers</source>
        <translation>Zobrazit seznam přenesených souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1223"/>
        <source>Emoticons</source>
        <translation>Smajlíci</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1235"/>
        <source>Show the emoticon panel</source>
        <translation>Zobraz panel smajlíků</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1236"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Přidat smajlíka do zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1281"/>
        <source>Show the bar of local file sharing</source>
        <translation>Zobrazit lištu místního sdílení souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1292"/>
        <source>Show the bar of network file sharing</source>
        <translation>Zobrazit lištu sdílených souborů v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1303"/>
        <source>Show the bar of log</source>
        <translation>Zobrazit lištu logů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1317"/>
        <source>Show the bar of screenshot plugin</source>
        <translation>Zobrazit lištu screenshot doplňku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1342"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2406"/>
        <source>Play %1</source>
        <translation>Hrát %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1346"/>
        <source>is a game developed by</source>
        <translation>je hra vyvinutá</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1349"/>
        <source>About %1</source>
        <translation>O %1</translation>
    </message>
    <message>
        <source>When do you want %1 to play beep?</source>
        <translation type="obsolete">Kdy chcete, aby %1 přehrál zvuk?</translation>
    </message>
    <message>
        <source>If it not visible</source>
        <translation type="obsolete">Pokud není viditelný</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="888"/>
        <source>Always</source>
        <translation>Vždy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1485"/>
        <source>Please save the network password in the next dialog.</source>
        <translation>Uložte prosím síťové heslo v následujícím dialogovém okně.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1507"/>
        <source>How many minutes of idle %1 can wait before changing status to away?</source>
        <translation>Kolik minut nečinnosti %1 má program čekat, než se přepne do stavu Nepřítomný?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1543"/>
        <source>Please select the maximum number of messages to be showed</source>
        <translation>Vyberte prosím maximální počet zobrazovaných zpráv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1722"/>
        <source>New message from %1</source>
        <translation>Nová zpráva od %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1725"/>
        <source>New message arrived</source>
        <translation>Obdržena nová zpráva</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1829"/>
        <source>%1 is writing...</source>
        <translation>%1 píše....</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1842"/>
        <source>Do you want to disconnect from %1 network?</source>
        <translation>Chcete se odpojit od %1 sítě?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1875"/>
        <source>You are %1%2</source>
        <translation>Jste %1%2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1886"/>
        <source>Please insert the new status description</source>
        <translation>Zadejte prosím nový popis stavu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>%1 - Select a file</source>
        <translation>%1 - Vybrat jeden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>or more</source>
        <translation>nebo více souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1971"/>
        <source>There is no user connected.</source>
        <translation>Žádný uživatel není připojen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1977"/>
        <source>Please select the user to whom you would like to send a file.</source>
        <translation>Vyberte prosím uživatele, kterému chcete odeslat soubor.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1986"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2993"/>
        <source>User not found.</source>
        <translation>Uživatel nebyl nalezen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2009"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2159"/>
        <source>File transfer is disabled. You cannot download %1.</source>
        <translation>Přenos souborů je zakázán.Nelze stáhnout %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <source>Yes, and don&apos;t ask anymore</source>
        <translation>Ano a příště se neptat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2063"/>
        <source>%1 already exists. Please select a new filename.</source>
        <translation>%1 existuje. Vyberte nový název souboru.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2102"/>
        <source>You cannot download all these files at once. Do you want to download the first %1 files of the list?</source>
        <translation>Nelze stáhnout všechny tyto soubory najednou.  Chcete stáhnout prvních %1 souborů ze seznamu?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2110"/>
        <source>Downloading %1 files is a hard duty. Maybe you have to wait a lot of minutes. Do yo want to continue?</source>
        <translation>Stahování  %1 souborů může trvat velmi dlouho. Opravdu chcete pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2128"/>
        <source>%1 files are scheduled for download</source>
        <translation>%1 soubor je zařazen ke stažení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2167"/>
        <source>Do you want to download folder %1 (%2 files) from %3?</source>
        <translation>Chcete stáhnout složku %1 (%2 souborů) od %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2208"/>
        <source>%1 - Select the download folder</source>
        <translation>%1 - vybrat složku pro uložení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2361"/>
        <source>Plugin Manager...</source>
        <translation>Správce doplňků...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2362"/>
        <source>Open the plugin manager dialog and manage the installed plugins</source>
        <translation>Otevřít správce doplňků a spravovat nainstalované doplňky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3589"/>
        <source>at lunch</source>
        <translation>na obědě</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3598"/>
        <source>in a meeting</source>
        <translation>na schůzi</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3634"/>
        <source>Do you really want to clear all saved status descriptions?</source>
        <translation>Opravdu chcete odstranit všechny uložené popisy stavu?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1014"/>
        <source>Show the %1 log</source>
        <translation>Zobrazit %1 log</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="366"/>
        <source>&lt;b&gt;Chat messages can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Zprávy nelze uložit&lt;/b&gt;. Cesta:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="638"/>
        <source>Create chat</source>
        <translation>Vytvořit chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="639"/>
        <source>Create a chat with two or more users</source>
        <translation>Vytvořit chat s více uživateli</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="642"/>
        <source>Create group</source>
        <translation>Vytvořit skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="643"/>
        <source>Create a group with two or more users</source>
        <translation>Vytvořit skupinu ze dvou nebo více uživatelů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="671"/>
        <source>Shortcuts...</source>
        <translation>Zkratky...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="672"/>
        <source>Enable and edit your custom shortcuts</source>
        <translation>Povolit a upravit vaše vlastní zkratky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="673"/>
        <source>Dictionary...</source>
        <translation>Slovník...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="674"/>
        <source>Select your preferred dictionary for spell checking</source>
        <translation>Vyberte váš preferovaný slovník pro kontrolu pravopisu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="684"/>
        <source>Open your resource folder</source>
        <translation>Otevřít vaše zdrojové složky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="685"/>
        <source>Click to open your resource folder</source>
        <translation>Klikem otevřít vaše zdrojové složky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="727"/>
        <source>Show the datestamp</source>
        <translation>Zobrazit datum zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="728"/>
        <source>If enabled the message shows its datestamp in the chat window</source>
        <translation>Pokud je volba povolena, zobrazit čas zprávy v okně chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="763"/>
        <source>Use your name instead of &apos;You&apos;</source>
        <translation>Použít své jméno místo  &apos;Já&apos;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="785"/>
        <source>Prompts for nickname on startup</source>
        <translation>Vyzvat k zadání přezdívky při startu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="814"/>
        <source>Check for new version at startup</source>
        <translation>Po startu zkontrolovat novou verzi</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="840"/>
        <source>If the file to be downloaded already exists it is automatically overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="849"/>
        <source>Ask me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="874"/>
        <source>Always open a new floating chat window</source>
        <translation>Vždy otevřít nové plovoucí okno chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="875"/>
        <source>If enabled when you always open chat in a new floating window</source>
        <translation>Pokud je volba povolena, otevře se chat vždy v novém plovoucím okně</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="884"/>
        <source>When the chat is not visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="892"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="951"/>
        <source>Show status color in background</source>
        <translation>Zobrazit barvu stavu na pozadí</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="952"/>
        <source>If enabled the user in list has colored backrgound as status</source>
        <translation>Pokud je volba povolena, uživatel v seznamu bude mít zobrazen svůj stav jako barvu na pozadí </translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="978"/>
        <source>Recently used</source>
        <translation>Nedávno použité</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="983"/>
        <source>Change your status description...</source>
        <translation>Změnit váš popis stavu...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="984"/>
        <source>Clear all status descriptions</source>
        <translation>Odstranit všechny popisy stavu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="996"/>
        <source>Save main window geometry</source>
        <translation>Uložit velikost okna</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1015"/>
        <source>Show the application log to see if an error occurred</source>
        <translation>Zobrazit log pro zjištění případné chyby</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1016"/>
        <source>Make a screenshot</source>
        <translation>Pořídit screenshot obrazovky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1017"/>
        <source>Show the utility to capture a screenshot</source>
        <translation>Zobrazit nástroj k pořízení screenshotu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1018"/>
        <source>Show new message</source>
        <translation>Zobrazit novou zprávu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1048"/>
        <source>Add +1 user to anonymous usage statistics</source>
        <translation>Přidat uživatele do anonymní uživatelské statistiky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1049"/>
        <source>Help me to know how many users have BeeBEEP</source>
        <translation>Pomoci mě zjistit, kolik uživatelů používá BeeBEEP</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1066"/>
        <source>Network</source>
        <translation>Síť</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1100"/>
        <source>Show only message notifications</source>
        <translation>Zobrazit pouze upozornění na zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1101"/>
        <source>If enabled tray icon shows only message notifications</source>
        <translation>Pokud je volba povolena,upozornění na zprávy bude zobrazovat pouze ikona v systémové liště</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1106"/>
        <source>Show chat message preview</source>
        <translation>Zobrazit náhled zprávy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1954"/>
        <source>File transfer is not enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1960"/>
        <source>You are not connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2364"/>
        <source>is a plugin developed by</source>
        <translation>je doplněk vyvinutý</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2389"/>
        <source>Show the bar of games</source>
        <translation>Zobrazit lištu her</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2636"/>
        <source>Do you really want to open the file %1?</source>
        <translation>Opravdu chcete otevřít soubor %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Unable to open %1</source>
        <translation>Nelze otevřít %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2661"/>
        <source>Sound files (*.wav)</source>
        <translation>Zvukové soubory (*.wav)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <source>Sound is not enabled on a new message. Do you want to enable it?</source>
        <translation>U nové zprávy není povolen zvuk. Chcete ho povolit?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2685"/>
        <source>Sound module is not working. The default BEEP will be used.</source>
        <translation>Zvukový modul nefunguje. Bude použit výchozí pípnutí.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2690"/>
        <source>Sound file not found</source>
        <translation>Zvukový soubor nebyl nalezen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2691"/>
        <source>The default BEEP will be used</source>
        <translation>Bude použito výchozí pípnutí</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2737"/>
        <source>Group chat will be deleted when all members goes offline.</source>
        <translation>Skupinový chat bude zrušen, jakmile všichni uživatelé tento chat opustí.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2738"/>
        <source>If you want a persistent chat please consider to make a Group instead.</source>
        <translation>Pokud chcete chatovat s těmito uživateli trvale, zvažte prosím vytvoření skupiny.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2739"/>
        <source>Do you wish to continue or create group?</source>
        <translation>Chcete pokračovat nebo vytvořit skupinu?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Continue</source>
        <translation>Pokračovat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Create Group</source>
        <translation>Vytvořit skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2766"/>
        <source>Unable to add users in this chat. Please select a group one.</source>
        <translation>Nelze přidat uživatele v tomto chatu. Vyberte prosím skupinu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2813"/>
        <source>Now %1 will start on windows boot.</source>
        <translation>Nyní se %1 spustí při startu Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2815"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2822"/>
        <source>Unable to add this key in the registry: permission denied.</source>
        <translation>Nelze přidat tento klíč do registru: přístup odepřen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2820"/>
        <source>%1 will not start on windows boot.</source>
        <translation>%1 se nespustí při startu Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2877"/>
        <source>Please select a chat you would like to link the saved text.</source>
        <translation>Vyberte prosím chat, do kterého chcete přiřadit uložený text.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2886"/>
        <source>The chat &apos;%1&apos; selected has already a saved text.&lt;br /&gt;What do you want to do with the selected saved text?</source>
        <translation>Chat &apos;%1&apos; má již uložený text.&lt;br /&gt;Co chcete udělat s vybraným uloženým textem?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="839"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Overwrite</source>
        <translation>Přepsat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Add in the head</source>
        <translation>Přidat do hlavičky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3037"/>
        <source>All the members of this chat are not online. The changes may not be permanent. Do you wish to continue?</source>
        <translation>Všichni členové tohoto chatu nejsou online. Tyto změny nemusí být trvalé. Přejete si pokračovat?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3175"/>
        <source>Do you really want to delete chat with %1?</source>
        <translation>Opravdu chcete vymazat chat s %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3296"/>
        <source>inactive</source>
        <translation>neaktivní</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3316"/>
        <source>disabled</source>
        <translation>zakázán</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3323"/>
        <source>active</source>
        <translation>aktivní</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3396"/>
        <source>%1 is online</source>
        <translation>Uživatel %1 je připojen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3398"/>
        <source>%1 is offline</source>
        <translation>Uživatel %1 je odpojen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3410"/>
        <source>Please select the new size of the user picture</source>
        <translation>Vyberte prosím novou velikost obrázku uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3811"/>
        <source>Select your dictionary path</source>
        <translation>Vybrat cestu ke slovníku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3819"/>
        <source>Dictionary selected: %1</source>
        <translation>Vybrán slovník: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3821"/>
        <source>Unable to set dictionary: %1</source>
        <translation>Nelze nastavit slovník: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3982"/>
        <source>Window geometry and state saved</source>
        <translation>Velikost a stav okna uloženy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1171"/>
        <source>Show the user panel</source>
        <translation>Zobrazit okno uživatelů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1183"/>
        <source>Show the group panel</source>
        <translation>Zobrazit okno skupin</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1195"/>
        <source>Show the chat panel</source>
        <translation>Zobrazit okno chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1207"/>
        <source>Show the history panel</source>
        <translation>Zobrazit okno historie</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1219"/>
        <source>Show the file transfer panel</source>
        <translation>Zobrazit okno přenosu souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1268"/>
        <source>Show the bar of chat</source>
        <translation>Zobrazit lištu chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2030"/>
        <source>Do you want to download %1 (%2) from %3?</source>
        <translation>Chcete stáhnout %1 (%2) od %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2143"/>
        <source>File is not available for download.</source>
        <translation>Soubor není k dispozici.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2145"/>
        <source>%1 is not connected.</source>
        <translation>%1 není připojen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2146"/>
        <source>Please reload the list of shared files.</source>
        <translation>Obnovte prosím seznam sdílených souborů.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <source>Reload file list</source>
        <translation>Obnovit seznam souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3122"/>
        <source>Chat with %1 is empty.</source>
        <translation>Chat s %1 je prázdný.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3126"/>
        <source>Do you really want to clear messages with %1?</source>
        <translation>Opravdu chcete odstranit zprávy od %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3129"/>
        <source>Yes and delete history</source>
        <translation>Ano a odstranit historii</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3059"/>
        <source>%1 is a your group. You can not leave the chat.</source>
        <translation>%1 je vaše skupina. Nemůžete opustit chat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Delete this group</source>
        <translation>Odstranit tuto skupinu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3084"/>
        <source>You cannot leave this chat.</source>
        <translation>Nelze opustit tento chat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3096"/>
        <source>Do you really want to delete group &apos;%1&apos;?</source>
        <translation>Opravdu chcete vymazat skupinu &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3197"/>
        <source>Unable to delete this chat.</source>
        <translation>Nelze odstranit tento chat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3219"/>
        <source>%1 has shared %2 files</source>
        <translation>Uživatel %1 nasdílel %2 souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3241"/>
        <source>Default language is restored.</source>
        <translation>Výchozí jazyk byl obnoven.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3243"/>
        <source>New language &apos;%1&apos; is selected.</source>
        <translation>Nový jazyk &apos;%1&apos; byl vybrán.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3246"/>
        <source>You must restart %1 to apply these changes.</source>
        <translation>Pro použití těchto změn je nutno restartovat %1.</translation>
    </message>
</context>
<context>
    <name>GuiMessageEdit</name>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="322"/>
        <source>Undo</source>
        <translation>Zpět</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="324"/>
        <source>Redo</source>
        <translation>Vpřed</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="327"/>
        <source>Cut</source>
        <translation>Vyjmout</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="328"/>
        <source>Copy</source>
        <translation>Kopírovat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="329"/>
        <source>Paste</source>
        <translation>Vložit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="332"/>
        <source>Select All</source>
        <translation>Vybrat vše</translation>
    </message>
</context>
<context>
    <name>GuiPluginManager</name>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="100"/>
        <source>Enable All</source>
        <translation>Povolit vše</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="110"/>
        <source>Disable All</source>
        <translation>Zakázat vše</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="133"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="36"/>
        <source>Plugin Manager - %1</source>
        <translation>Správce doplňků - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Plugin</source>
        <translation>Doplněk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Version</source>
        <translation>Verze</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Author</source>
        <translation>Autor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Disable %1</source>
        <translation>Zakázat %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Enable %1</source>
        <translation>Povolit %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is enabled</source>
        <translation>%1 povolen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is disabled</source>
        <translation>%1 zakázán</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="129"/>
        <source>Please select a plugin in the list.</source>
        <translation>Vyberte prosím doplněk ze seznamu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="152"/>
        <source>Text Markers</source>
        <translation>Textové značky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="171"/>
        <source>Games</source>
        <translation>Hry</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="192"/>
        <source>%1 - Select the plugin folder</source>
        <translation>%1 - Vybrat složku doplňků</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="208"/>
        <source>Folder %1 not found.</source>
        <translation>Složka %1 nebyla nalezena.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChat</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="56"/>
        <source>Saved chat</source>
        <translation>Uložený chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="58"/>
        <source>Empty</source>
        <translation>Prázdný</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="72"/>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="143"/>
        <source>Find text in chat</source>
        <translation type="unfinished">Najít text v chatu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="77"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Kopírovat do schránky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="79"/>
        <source>Select All</source>
        <translation type="unfinished">Vybrat vše</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="83"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="86"/>
        <source>Print...</source>
        <translation type="unfinished">Tisk...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="181"/>
        <source>%1 not found in chat.</source>
        <translation type="unfinished">%1 nebyl nalezen v chatu.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatList</name>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="47"/>
        <source>Show</source>
        <translation>Zobrazit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="50"/>
        <source>Link to chat</source>
        <translation>Odkaz na chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="52"/>
        <source>Delete</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Do you really want to delete this saved chat?</source>
        <translation>Opravdu chcete odstranit uložené chaty?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
</context>
<context>
    <name>GuiScreenShot</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="35"/>
        <source>Make a Screenshot</source>
        <translation>Pořídit screenshot</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="44"/>
        <source>Delay</source>
        <translation>Odložit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="45"/>
        <source>Delay screenshot for selected seconds</source>
        <translation>Odložit screenshot o vybrané vteřiny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="53"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="61"/>
        <source>Hide this window</source>
        <translation>Skrýt toto okno</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="62"/>
        <source>Hide this window before capture screenshot</source>
        <translation>Skrýt toto okno před vytvořením scrennshotu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="73"/>
        <source>Enable high dpi</source>
        <translation>Povolit vysoké DPI</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="74"/>
        <source>Enable high dpi support to manage, for example, Apple Retina display</source>
        <translation>Povolit podporu vysokého DPI, např.pro Apple Retina displeje</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="80"/>
        <source>Capture</source>
        <translation>Vytvořit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="81"/>
        <source>Capture a screenshot of your desktop</source>
        <translation>Pořídit screenshot vaší plochy</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="82"/>
        <source>Send</source>
        <translation>Poslat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="83"/>
        <source>Send the captured screenshot to an user</source>
        <translation>Odeslat pořízený screenshot uživateli</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="84"/>
        <source>Save</source>
        <translation>Uložit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="85"/>
        <source>Save the captured screenshot as file</source>
        <translation>Uložit vytvořený screenshot do souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="86"/>
        <source>Delete</source>
        <translation>Odstranit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="87"/>
        <source>Delete the captured screenshot</source>
        <translation>Odstranitt vytvořený screenshot</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="107"/>
        <source>No screenshot available</source>
        <translation>Screenshot nebyl nalezen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="179"/>
        <source>/beesshot-%1.</source>
        <translation>/screenshot_BeeBEEP-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="182"/>
        <source>Save As</source>
        <translation>Uložit jako</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="184"/>
        <source>%1 Files (*.%2)</source>
        <translation>%1 soubory (*.%2)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="199"/>
        <source>/beesshottmp-%1.</source>
        <translation>/Scrennshot_BeeBEEP_tmp-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="206"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Nelze uložit dočasný soubor: %1</translation>
    </message>
</context>
<context>
    <name>GuiScreenShotWidget</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="84"/>
        <source>No Screenshot Available</source>
        <translation>Screenshot není dostupný</translation>
    </message>
</context>
<context>
    <name>GuiSearchUser</name>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="77"/>
        <source>Local subnet address *</source>
        <translation>Adresa místní podsítě *</translation>
    </message>
    <message>
        <source>Enter the IP addresses or subnet of your local area network separed by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation type="obsolete">Zadat IP adresy nebo podsítě vaší místní sítě oddělené čárkou (příklad: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="204"/>
        <source>Addresses in beehosts.ini *</source>
        <translation>Adresy v beehosts.ini *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="35"/>
        <source>UDP Port in beebeep.rc *</source>
        <translation>UDP Port v beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="64"/>
        <source>(the same for all clients)</source>
        <translation>(stejné pro všechny klienty)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="106"/>
        <source>(search users here by default)</source>
        <translation>(ve výchozím nastavení zde hledat uživatele)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="119"/>
        <source>Multicast group in beebeep.rc *</source>
        <translation>Multicast skupina v beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="155"/>
        <source>Enable broadcast interval</source>
        <translation>Povolit interval prohledávání</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="184"/>
        <source>seconds (0=disabled, 10=default)</source>
        <translation>s (0=zakázáno, 10=výchozí)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="351"/>
        <source>Workgroups (enter your network groups separated by comma)</source>
        <translation>Pracovní skupiny (zadejte vaše pracovní skupiny oddělené čárkou)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="239"/>
        <source>Accept connections only from your workgroups</source>
        <translation>Přijmout připojení pouze od svých pracovních skupin</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="258"/>
        <source>Enter the IP addresses or subnet of your local area network separated by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation>Zadat IP adresy nebo podsítě vaší místní sítě oddělené čárkou (příklad: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="284"/>
        <source>Split subnet to IPV4 addresses</source>
        <translation>Rozdělit podsítě podle IPV4 adres</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="297"/>
        <source>Verbose</source>
        <translation>Upovídaný</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="328"/>
        <source>Max users to contact in a tick</source>
        <translation>Maximální počet uživatelů k dispozici v tomto okamžiku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="337"/>
        <source>Automatically add external subnet</source>
        <translation>Automaticky přidat externí podsíť</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="344"/>
        <source>Enable Zero Configuration Networking</source>
        <translation>Povolit Zero Configuration sítě</translation>
    </message>
    <message>
        <source>Enable broadcast interval of</source>
        <translation type="obsolete">Povolit interval prohledávání</translation>
    </message>
    <message>
        <source>s (0=disabled, 10=default)</source>
        <translation type="obsolete">s (0=zakázáno, 10=výchozí)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="367"/>
        <source>* (read only section)</source>
        <translation>* (část pouze pro čtení)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="374"/>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="381"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="36"/>
        <source>Search for users</source>
        <translation>Najít uživatele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="54"/>
        <source>Unknown address</source>
        <translation>Neznámá adresa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="62"/>
        <source>File is empty</source>
        <translation>Soubor je prázdný</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="116"/>
        <source>Warning</source>
        <translation>Varování</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>You have inserted an invalid host address:
%1 is removed from the list.</source>
        <translation>Zadali jste neplatnou adresu:
 %1 je odebrán ze seznamu.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocal</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="37"/>
        <source>Share your folders or files</source>
        <translation>Sdílet složky nebo soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Size</source>
        <translation>Velikost</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Path</source>
        <translation>Cesta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="77"/>
        <source>Share a file</source>
        <translation>Sdílet soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="78"/>
        <source>Add a file to your local share</source>
        <translation>Přidat soubor do místního sdílení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="80"/>
        <source>Share a folder</source>
        <translation>Sdílet složku</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="81"/>
        <source>Add a folder to your local share</source>
        <translation>Přidat složku do místního sdílení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="83"/>
        <source>Update shares</source>
        <translation>Obnovit sdílené</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="84"/>
        <source>Update shared folders and files</source>
        <translation>Obnovit sdílené složky a soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="86"/>
        <source>Remove shared path</source>
        <translation>Odstranit sdílené cesty</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="87"/>
        <source>Remove shared path from the list</source>
        <translation>Odstranit sdílené cesty ze seznamu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="89"/>
        <source>Clear all shares</source>
        <translation>Odstranit všechna sdílení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="90"/>
        <source>Clear all shared paths from the list</source>
        <translation>Odstranit všechna sdílení ze seznamu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="99"/>
        <source>Shared files</source>
        <translation>Sdílené soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="101"/>
        <source>File transfer is disabled</source>
        <translation>Přenos souboru je zakázán</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="119"/>
        <source>Select a file to share</source>
        <translation>Vybrat soubor ke sdílení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="132"/>
        <source>Select a folder to share</source>
        <translation>Vybrat složku ke sdílení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="150"/>
        <source>Please select a shared path.</source>
        <translation>Vyberte prosím sdílenou cestu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="167"/>
        <source>Do you really want to remove all shared paths?</source>
        <translation>Opravdu chcete odstranit všechny sdílené cesty?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>Yes</source>
        <translation>Ano</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>No</source>
        <translation>Ne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="214"/>
        <source>Click to open %1</source>
        <translation>Klikem otevřít %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="229"/>
        <source>%1 is already shared.</source>
        <translation>%1 je již sdílen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="270"/>
        <source>File transfer is disabled. Open the option menu to enable it.</source>
        <translation>Přenos souboru je zakázán. Otevřít menu možností pro povolení přenosu.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="334"/>
        <source>%1 shared files</source>
        <translation>%1 sdílené soubory</translation>
    </message>
</context>
<context>
    <name>GuiShareLocalWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="87"/>
        <source>2</source>
        <translation>2</translation>
    </message>
</context>
<context>
    <name>GuiShareNetwork</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="98"/>
        <source>User</source>
        <translation>Uživatel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="52"/>
        <source>Scan network</source>
        <translation>Prohledat síť</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="39"/>
        <source>Folder and Files shared in your network</source>
        <translation>Složky a soubory sdílené v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="53"/>
        <source>Search shared files in your network</source>
        <translation>Vyhledat sdílené soubory v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="56"/>
        <source>Reload list</source>
        <translation>Obnovit list</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="57"/>
        <source>Clear and reload list</source>
        <translation>Odstranit a obnovit seznam</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="74"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="83"/>
        <source>File Type</source>
        <translation>Typ souboru</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="89"/>
        <source>All Files</source>
        <translation>Všechny soubory</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="61"/>
        <source>Download</source>
        <translation>Stáhnout</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="62"/>
        <source>Download single or multiple files simultaneously</source>
        <translation>Stáhnout jeden nebo více souborů současně</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="68"/>
        <source>Filter</source>
        <translation>Filtr</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="116"/>
        <source>All Users</source>
        <translation>Všichni uživatelé</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="141"/>
        <source>%1 is searching shared files in your network</source>
        <translation>%1 hledá sdílené soubory v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="195"/>
        <source>Double click to download %1</source>
        <translation>Dvojklikem stáhnout %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="234"/>
        <source>%1 has shared %2 files (%3)</source>
        <translation>%1 sdílí %2 souborů (%3)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="315"/>
        <source>Double click to open %1</source>
        <translation>Dvojklikem otevřít %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="317"/>
        <source>Transfer completed</source>
        <translation>Přenos kompletní</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="331"/>
        <source>%1 files are shown in list (%2 are available in your network)</source>
        <translation>%1 soubory jsou zobrazeny v seznamu (%2 jsou přístupné v síti)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="333"/>
        <source>%1 files shared in your network</source>
        <translation>%1 souborů sdílených v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download single file</source>
        <translation>Stáhnout jeden soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download %1 selected files</source>
        <translation>Stáhnout %1 vybraných souborů</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="377"/>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="380"/>
        <source>Clear selection</source>
        <translation>Odstranit vybrané</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="384"/>
        <source>Expand all items</source>
        <translation>Rozbalit všechny položky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="385"/>
        <source>Collapse all items</source>
        <translation>Sbalit všechny položky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="396"/>
        <source>Please select one or more files to download.</source>
        <translation>Vyberte prosím jeden nebo více souborů ke stažení.</translation>
    </message>
</context>
<context>
    <name>GuiShareNetworkWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.ui" line="14"/>
        <source>Form</source>
        <translation>Forma</translation>
    </message>
</context>
<context>
    <name>GuiShortcut</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="34"/>
        <source>Shortcuts</source>
        <translation>Zkratky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Key</source>
        <translation>Klávesová zkratka</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Action</source>
        <translation>Akce</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="114"/>
        <source>Insert shorcut for the action: %1</source>
        <translation>Vložit klávesovou zkratku pro akci: %1</translation>
    </message>
</context>
<context>
    <name>GuiShortcutDialog</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Restore to default language</source>
        <translation type="obsolete">Obnovit výchozí jazyk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="23"/>
        <source>Use custom shortcuts</source>
        <translation>Použít vlastní klávesové zkratky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="50"/>
        <source>Restore default shortcuts</source>
        <translation>Obnovit výchozí zkratky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="53"/>
        <source>Restore</source>
        <translation>Obnovit</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="76"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="89"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>GuiTransferFile</name>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>File</source>
        <translation>Soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>User</source>
        <translation>Uživatel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>Status</source>
        <translation>Stav</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="116"/>
        <source>Completed</source>
        <translation>Kompletní</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="122"/>
        <source>Cancel Transfer</source>
        <translation>Zrušit přenos</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="128"/>
        <source>Not Completed</source>
        <translation>Nekompletní</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="161"/>
        <source>Transfer completed</source>
        <translation>Přenos kompletní</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Downloading</source>
        <translation>Stahování</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Uploading</source>
        <translation>Nahrávání</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="218"/>
        <source>Do you really want to cancel the transfer of %1?</source>
        <translation>Opravdu chcete zrušit přenos %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="251"/>
        <source>Remove all transfers</source>
        <translation>Odstranit všechny přenosy</translation>
    </message>
</context>
<context>
    <name>GuiUserList</name>
    <message>
        <location filename="../src/desktop/GuiUserList.cpp" line="55"/>
        <source>Search</source>
        <translation>Hledat</translation>
    </message>
</context>
<context>
    <name>GuiVCard</name>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="59"/>
        <source>Birthday: %1</source>
        <translation>Narozeniny: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="85"/>
        <source>unknown</source>
        <translation>Neznámý</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="87"/>
        <source>old %1</source>
        <translation>starý %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="89"/>
        <source>new %1</source>
        <translation>nový %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="105"/>
        <source>Remove from favorites</source>
        <translation>Odebrat z oblíbených</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="110"/>
        <source>Add to favorites</source>
        <translation>Přidat do oblíbených</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="115"/>
        <source>Chat with all</source>
        <translation>Chatovat se všemi</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="121"/>
        <source>Open chat</source>
        <translation>Otevřít chat</translation>
    </message>
</context>
<context>
    <name>GuiVCardDialog</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="22"/>
        <source>User ID</source>
        <translation>Uživatelské ID</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="150"/>
        <source>Nickname</source>
        <translation>Přezdívka</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="207"/>
        <source>First name</source>
        <translation>Jméno</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="224"/>
        <source>Last name</source>
        <translation>Příjmení</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="241"/>
        <source>Birthday</source>
        <translation>Narozeniny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="274"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="291"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="356"/>
        <source>Add or change photo</source>
        <translation>Přidat nebo změnit foto</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="385"/>
        <source>Remove photo</source>
        <translation>Odstranit foto</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="417"/>
        <source>Change your nickname color</source>
        <translation>Změnit barvu přezdívky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="456"/>
        <source>Other informations</source>
        <translation>Další informace</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="499"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="509"/>
        <source>Cancel</source>
        <translation>Zrušit</translation>
    </message>
</context>
<context>
    <name>GuiVCardWidget</name>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="81"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="291"/>
        <source>User Path</source>
        <translation>Uživatelská cesta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="307"/>
        <source>User Name</source>
        <translation>Uživatelské jméno</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="323"/>
        <source>Birthday</source>
        <translation>Narozeniny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="339"/>
        <source>Email</source>
        <translation>Email</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="355"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="417"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="452"/>
        <source>Open chat</source>
        <translation>Otevřít chat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="490"/>
        <source>Send a file</source>
        <translation>Poslat soubor</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="525"/>
        <source>Change the nickname color</source>
        <translation>Změnit barvu přezdívky</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="583"/>
        <source>Remove user</source>
        <translation>Odstranit uživatele</translation>
    </message>
</context>
<context>
    <name>GuiWizard</name>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="36"/>
        <source>Welcome to &lt;b&gt;%1 Network&lt;/b&gt;.</source>
        <translation>Vítejte v &lt;b&gt;%1 síti&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="42"/>
        <source>Your system account is</source>
        <translation>Váš systémový účet je</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="59"/>
        <source>Your nickname can not be empty.</source>
        <translation>Vaše přezdívka nemůže být prázdná.</translation>
    </message>
</context>
<context>
    <name>GuiWizardDialog</name>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="56"/>
        <source>Welcome</source>
        <translation>Vítejte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>NumberTextMarker</name>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="33"/>
        <source>Number Text Marker</source>
        <translation>Number Text Marker</translation>
    </message>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="48"/>
        <source>If you want to encode your message with numbers write a #text to encode# .</source>
        <translation>Chcete-li zakódovat svou zprávu s čísly, napište #text k zakódování# .</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="63"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3119"/>
        <location filename="../src/desktop/GuiUserItem.cpp" line="84"/>
        <source>All Lan Users</source>
        <translation>Všichni uživatelé v síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="64"/>
        <source>Open chat with all local users</source>
        <translation>Otevřít chat se všemi lokálními uživateli</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="84"/>
        <source>Open chat with %1</source>
        <translation>Otevřít chat s %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatMessage.cpp" line="61"/>
        <source>You</source>
        <translation>Já</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="148"/>
        <source>Click to open chat with all local users</source>
        <translation>Kliknout pro otveření chatu se všemi uživateli v lokální síti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="152"/>
        <source>%1 is %2</source>
        <translation>%1 je %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="167"/>
        <source>Click to send a private message</source>
        <translation>Kliknout pro odeslaní soukromé zprávy</translation>
    </message>
    <message>
        <source>Saved in</source>
        <translation type="obsolete">Uložit do </translation>
    </message>
    <message>
        <source>Started in</source>
        <translation type="obsolete">Spustit v</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupItem.cpp" line="67"/>
        <source>Click to send message to group: %1</source>
        <translation>Kliknnout pro odeslání zprávy skupině: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="129"/>
        <source>Click to view chat history: %1</source>
        <translation>Kliknout pro zobrazení historie chatu: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="100"/>
        <source>Empty</source>
        <translation type="unfinished">Prázdný</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="102"/>
        <source>Send file</source>
        <translation type="unfinished">Poslat soubor</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="104"/>
        <source>Show file transfers</source>
        <translation type="unfinished">Zobrazit přenosy souborů</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="106"/>
        <source>Set focus in message box</source>
        <translation type="unfinished">Nastavit zaměření v okně zprávy</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="108"/>
        <source>Minimize all chats</source>
        <translation type="unfinished">Minimalizovat všechny chaty</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="110"/>
        <source>Show the next unread message</source>
        <translation type="unfinished">Zobrazit další nepřečtenou zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="112"/>
        <source>Send chat message</source>
        <translation type="unfinished">Poslat zprávu</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="114"/>
        <source>Print</source>
        <translation type="unfinished">Tisk</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="116"/>
        <source>Broadcast</source>
        <translation type="unfinished">Prohledat</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="118"/>
        <source>Find text in chat</source>
        <translation type="unfinished">Najít text v chatu</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="120"/>
        <source>Find next text in chat</source>
        <translation type="unfinished">Najít další text v chatu</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="122"/>
        <source>Send folder</source>
        <translation type="unfinished">Poslat složku</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="124"/>
        <source>Show emoticons panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="126"/>
        <source>Show all chats</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RainbowTextMarker</name>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="37"/>
        <source>Rainbow Text Marker</source>
        <translation>Rainbow Text Marker</translation>
    </message>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="52"/>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~ .</source>
        <translation>Pokud chcete &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; napiište ~rainbow text~ .</translation>
    </message>
</context>
<context>
    <name>RegularBoldTextMarker</name>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="32"/>
        <source>Regular Bold Text Marker</source>
        <translation>Regular Bold Text Marker</translation>
    </message>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="47"/>
        <source>If you want to format your message with words in regular and bold font write a [text to format] .</source>
        <translation>Chcete-li svou zprávu formátovat se slovy ve formátu obvyklý (Regular) a tučný (Bold), napište [text k formátování] .</translation>
    </message>
</context>
<context>
    <name>Tips</name>
    <message>
        <location filename="../src/Tips.h" line="31"/>
        <source>You can switch between chats with CTRL+TAB if there are new messages availables.</source>
        <translation type="unfinished">Pokud jsou k dispozici nové zprávy, můžete použitím CTRL+TAB přepínat mezi chaty.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="32"/>
        <source>If you want a &lt;b&gt;bold text&lt;/b&gt; write a *bold text*.</source>
        <translation type="unfinished">Pokud chcete &lt;b&gt;tučný text&lt;/b&gt;, napište *tučný text*.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="33"/>
        <source>If you want an &lt;i&gt;italic text&lt;/i&gt; write a /italic text/.</source>
        <translation type="unfinished">Pokud chcete &lt;i&gt;text kurzívou&lt;/i&gt;, napište /text kurzívou/.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="34"/>
        <source>If you want an &lt;u&gt;underlined text&lt;/u&gt; write a _underlined text_.</source>
        <translation type="unfinished">Pokud chcete &lt;u&gt;podtržený text&lt;/u&gt;,napište _podtržený text_.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="35"/>
        <source>You can search previous sent message in the history using the CTRL+Up e CTRL+Down keys.</source>
        <translation type="unfinished">Můžete vyhledávat předchozí poslanou zprávu v historii použitím kláves CTRL+Up nebo CTRL+Down.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="37"/>
        <source>You can drop files to active chat and send them to members.</source>
        <translation type="unfinished">Můžete vkládat soubory do aktivního chatu a posílat je uživatelům.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="38"/>
        <source>You can select multiple files from network shares and download them simultaneously with a right click.</source>
        <translation type="unfinished">Použitím pravého kliku myši můžete vybrat více souborů sdílených na síti a stahovat je současně.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="39"/>
        <source>You can disable message notification from a group right clicking on its name on the list.</source>
        <translation type="unfinished">Můžete zakázat oznámení o zprávě ze skupiny pravým klikem na jeho jméno na seznamu.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="47"/>
        <source>&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</source>
        <translation type="unfinished">&lt;i&gt;Svoboda je, když je mysl řízena fantazií.&lt;/i&gt; (Marco Mastroddi)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="48"/>
        <source>&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</source>
        <translation type="unfinished">&lt;i&gt;Zůstaňte hladoví, zůstaňte pošetílí.&lt;/i&gt; (Steve Jobs)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="49"/>
        <source>&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</source>
        <translation type="unfinished">&lt;i&gt;Vždy tam zůstane ještě jedna chyba.&lt;/i&gt; (Lubarského zákon)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="50"/>
        <source>&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</source>
        <translation type="unfinished">&lt;i&gt;Pokud se něco může pokazit, tak se to taky pokazí.&lt;/i&gt; (Murphyho zákon)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="51"/>
        <source>&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</source>
        <translation type="unfinished">&lt;i&gt;Pokud je program užitečný, bude muset být změněn.&lt;/i&gt; (Zákon počítačového programování)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="53"/>
        <source>&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</source>
        <translation type="unfinished">&lt;i&gt;Intelektuálové řeší problémy, géniové jim předcházejí.&lt;/i&gt; (Albert Einstein)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="54"/>
        <source>&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</source>
        <translation type="unfinished">&lt;i&gt;Co mě nezabije, to mě posílí.&lt;/i&gt; (Friedrich Nietzsche)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="55"/>
        <source>&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</source>
        <translation type="unfinished">&lt;i&gt;Nejsem dost mladý na to, abych věděl vše.&lt;/i&gt; (Oscar Wilde)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="56"/>
        <source>&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</source>
        <translation type="unfinished">&lt;i&gt;Nedostatek pochybností vede k nedostatku kreativity.&lt;/i&gt; (Evert Jan Ouweneel)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="57"/>
        <source>&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</source>
        <translation type="unfinished">&lt;i&gt;Strach je cesta k temné straně.&lt;/i&gt; (Joda)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="59"/>
        <source>&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</source>
        <translation type="unfinished">&lt;i&gt;Sním můj obraz a pak maluji svůj sen.&lt;/i&gt; (Vincent Van Gogh)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="60"/>
        <source>&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</source>
        <translation type="unfinished">&lt;i&gt;Vše,co si dokážete představit, je skutečnél.&lt;/i&gt; (Pablo Picasso)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="61"/>
        <source>&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</source>
        <translation type="unfinished">&lt;i&gt;Všechny pravdy je jednoduché pochopit, když jsou objeveny - jde o to, aby byly objeveny.&lt;/i&gt; (Galileo Galilei)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="62"/>
        <source>&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</source>
        <translation type="unfinished">&lt;i&gt;Pravda vítězí tam, kde jsou svobodné názory.&lt;/i&gt; (Thomas Paine)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="63"/>
        <source>&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</source>
        <translation type="unfinished">&lt;i&gt;Viděl jsem věci, kterým byste vy, lidé, nevěřili...&lt;/i&gt; (Batty)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="65"/>
        <source>&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation type="unfinished">&lt;i&gt;Charakterem muže je jeho osud.&lt;/i&gt; (Eraclitus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="66"/>
        <source>&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</source>
        <translation type="unfinished">&lt;i&gt;Jiný jazyk je jiný pohled na život.&lt;/i&gt; (Federico Fellini)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="67"/>
        <source>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</source>
        <translation type="unfinished">&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="68"/>
        <source>&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</source>
        <translation type="unfinished">&lt;i&gt;Každý den se v Africe probudí gazela. Ví, že musí utíkat rychleji než nejrychlejší lev, jinak zemře. Každý den se probouzí také lev. Ví, že musí předběhnout nejpomalejší gazelu, jinak zahyne hlady. Je jedno, jestli jste gazela nebo lev. Jakmile vyjde slunce, raději se rozběhněte.&lt;/i&gt; (Abe Gubegna)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="73"/>
        <source>&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</source>
        <translation type="unfinished">&lt;i&gt;OK Houstone, máme tady problém.&lt;/i&gt; (John L. Swigert)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="75"/>
        <source>&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</source>
        <translation type="unfinished">&lt;i&gt;Druhá hvězda napravo a rovně až do rána.&lt;/i&gt; (Peter Pan)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="76"/>
        <source>&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</source>
        <translation type="unfinished">&lt;i&gt;Nutnost je poslední a nejsilnější zbraň.&lt;/i&gt; (Titus Livius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="77"/>
        <source>&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</source>
        <translation type="unfinished">&lt;i&gt;Staří lidé nejsou moudří - oni sou pouze opatrní.&lt;/i&gt; (Ernest Hemingway)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="78"/>
        <source>&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</source>
        <translation type="unfinished">&lt;i&gt;Cesta dlouhá tisíc mil začíná prvním krokem.&lt;/i&gt; (Confucius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="79"/>
        <source>&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</source>
        <translation type="unfinished">&lt;i&gt;Život bez odvahy zemřít je otroctví.&lt;/i&gt; (Lucius Annaeus Seneca)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="81"/>
        <source>&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</source>
        <translation type="unfinished">&lt;i&gt;Lze vypočítat pohyb nebeských těles, ale ne šílenství lidí.&lt;/i&gt; (Isaac Newton)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="82"/>
        <source>&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</source>
        <translation type="unfinished">&lt;i&gt;Zázrak je začátek moudrosti.&lt;/i&gt; (Socrates)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="83"/>
        <source>&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</source>
        <translation type="unfinished">&lt;i&gt;Žádný mladý člověk se nepřeje být mladší.&lt;/i&gt; (Jonathan Swift)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="84"/>
        <source>&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</source>
        <translation type="unfinished">&lt;i&gt;Člověk, který nikdy nedělá chyby, je člověk, který nikdy nedělá nic.&lt;/i&gt; (Theodore Roosevelt)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="85"/>
        <source>&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</source>
        <translation type="unfinished">&lt;i&gt;Postoj je malá věc, která dělá velký rozdíl.&lt;/i&gt; (Winston Churchill)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="87"/>
        <source>&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</source>
        <translation type="unfinished">&lt;i&gt;Vše, co jsme, je výsledkem toho, co jsme si mysleli.&lt;/i&gt; (Buddha)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="88"/>
        <source>&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</source>
        <translation type="unfinished">&lt;i&gt;Obtíže jsou věci, které ukazují člověka, jakým je.&lt;/i&gt; (Epictetus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="89"/>
        <source>&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</source>
        <translation type="unfinished">&lt;i&gt;Kdo bude strážit stráže?&lt;/i&gt; (Decimus Junius Juvenal)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="90"/>
        <source>&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</source>
        <translation type="unfinished">&lt;i&gt;Domov bez knih je jako tělo bez duše.&lt;/i&gt; (Marcus Tullius Cicero)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="91"/>
        <source>&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</source>
        <translatorcomment>&lt;i&gt;Nemůžeme zastavit přát si naše přání.&lt;/i&gt; (Arthur Schopenhauer)</translatorcomment>
        <translation type="unfinished">&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="93"/>
        <source>&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</source>
        <translation type="unfinished">&lt;i&gt;Trpělivost je také forma činnosti.&lt;/i&gt; (Auguste Rodin)</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="62"/>
        <source>offline</source>
        <translation>Nedostupný</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="63"/>
        <source>available</source>
        <translation>Dostupný</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="64"/>
        <source>busy</source>
        <translation>Zaneprázdněný</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="65"/>
        <source>away</source>
        <translation>Nepřítomný</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="66"/>
        <source>status error</source>
        <translation>Neznámý stav</translation>
    </message>
</context>
</TS>
