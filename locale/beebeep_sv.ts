<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sv">
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="292"/>
        <source>Header</source>
        <translation>Rubrik</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="293"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="294"/>
        <source>Chat</source>
        <translation>Chatt</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="295"/>
        <source>Connection</source>
        <translation>Anslutning</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="296"/>
        <source>User Status</source>
        <translation>Användarstatus</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="297"/>
        <source>User Information</source>
        <translation>Användarinfo</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="298"/>
        <source>File Transfer</source>
        <translation>Filöverföring</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="299"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="300"/>
        <source>Other</source>
        <translation>Annat</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../src/core/Core.cpp" line="127"/>
        <source>%1 Unable to connect to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Kan inte ansluta till nätverket, %2. Kontrollera dina brandväggsinställningar.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="152"/>
        <source>%1 Unable to broadcast to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Kan inte sända till nätverket, %2. Kontrollera dina brandväggsinställningar.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="165"/>
        <source>%1 You are connected to %2 Network.</source>
        <translation>%1 Du är ansluten till nätverket, %2.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="277"/>
        <source>%1 You are disconnected from %2 Network.</source>
        <translation>%1 Du är bortkopplad från nätverket, %2.</translation>
    </message>
    <message>
        <source>%1 %2 will search users in these IP addresses: %3</source>
        <translation type="obsolete">%1 %2 kommer att söka användare på dessa IP-adresser: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="230"/>
        <source>%1 Zero Configuration started with service name: %2</source>
        <translation>%1 Zero Configuration startad med tjänstnamnet: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="81"/>
        <source>%1 User %2 cannot save settings in path: %3</source>
        <translation>%1 Användare %2 kan inte spara inställningar på sökväg: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="97"/>
        <source>%1 User %2 cannot save chat messages in path: %3</source>
        <translation>%1 Användare %2 kan inte spara chattmeddelanden på sökväg: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="176"/>
        <source>%1 You have selected to join only in these workgroups: %2</source>
        <translation>%1 Du har valt att endast gå med i följande arbetsgrupper: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="245"/>
        <source>%1 Zero Configuration service closed.</source>
        <translation>%1 Zero Configuration-tjänsten stängd.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="300"/>
        <source>%1 Zero Configuration is browsing network for service: %2</source>
        <translation>%1 Zero Configuration genomsöker nätverket efter tjänsten: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="307"/>
        <source>%1 Zero Configuration cannot browse network for service: %2</source>
        <translation>%1 Zero Configuration kan inte söka i nätverket efter tjänsten: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="325"/>
        <source>%1 Broadcasting to the %2 Network...</source>
        <translation>%1 sänder i nätverket, %2...</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="331"/>
        <source>%1 You are not connected to %2 Network.</source>
        <translation>%1 Du är inte ansluten till nätverket, %2.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="337"/>
        <source>%1 %2 has found a filter on UDP port %3. Please check your firewall settings.</source>
        <translation>%1 %2 har hittat ett filter på UDP-port %3. Kontrollera dina brandväggsinställningar.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="341"/>
        <source>View the log messages for more informations</source>
        <translation>Se loggmeddelanden för mer information</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="511"/>
        <source>New version is available</source>
        <translation>En ny version finns tillgänglig</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="512"/>
        <source>Click here to download</source>
        <translation>Klicka här för att ladda ner</translation>
    </message>
    <message>
        <source>%1 The following networks appear as filtered: %2</source>
        <translation type="obsolete">%1 Följande nätverk visas som filtrerade: %2</translation>
    </message>
    <message>
        <source>%1 You cannot reach %2 Network.</source>
        <translation type="obsolete">%1 Du kan inte nå nätverket, %2.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="389"/>
        <source>is connected from external network (the new subnet is added to your broadcast address list).</source>
        <translation>är ansluten från externt nätverk (det nya undernätet läggs till i din adresslista).</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="401"/>
        <source>%1 Checking %2 more addresses...</source>
        <translation>%1 Kontrollerar %2 adresser till...</translation>
    </message>
    <message>
        <source>%1 Contacting %2 ...</source>
        <translation type="obsolete">%1 Kontaktar %2 ...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="46"/>
        <source>%1 Chat with all local users.</source>
        <translation>%1 Chatta med alla lokala användare.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="83"/>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to you!</source>
        <translation>Grattis på födelsedagen!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to %1!</source>
        <translation>Grattis på födelsedagen, till %1!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="64"/>
        <source>Happy birthday to Marco Mastroddi: %1 years old today! Cheers!!!</source>
        <translation>Grattis på födelsedagen, till Marco Mastroddi! %1 år gammal i dag! Skål!!!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="76"/>
        <source>Happy New Year!</source>
        <translation>Gott Nytt År!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="99"/>
        <location filename="../src/core/CoreChat.cpp" line="164"/>
        <location filename="../src/core/CoreChat.cpp" line="244"/>
        <source>%1 Chat with %2.</source>
        <translation>%1 Chatta med %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="147"/>
        <source>%1 You have created the group %2.</source>
        <translation>%1 Du har skapat gruppen &apos;%2&apos;.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="153"/>
        <source>%1 Welcome to the group %2.</source>
        <translation>%1 Välkommen till %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="198"/>
        <source>%1 The group has a new name: %2.</source>
        <translation>%1 Gruppen har fått ett nytt namn: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="230"/>
        <source>%1 Members removed from the group: %2.</source>
        <translation>%1 Medlemmar borttagna från gruppen: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="237"/>
        <source>%1 Members added to the group: %2.</source>
        <translation>%1 Medlemmar tillagda i gruppen: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="262"/>
        <source>Unable to send the message: you are not connected.</source>
        <translation>Kunde inte skicka meddelandet. Du är inte ansluten.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="268"/>
        <source>Unable to send the message: this chat is disabled.</source>
        <translation>Kunde inte skicka meddelandet. Denna chatt är inaktiverad.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="302"/>
        <source>Unable to send the message to %1.</source>
        <translation>Kunde inte skicka meddelande till %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="330"/>
        <source>The message will be delivered to %1.</source>
        <translation>Inget meddelande levereras till %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="335"/>
        <source>Nobody has received the message.</source>
        <translation>Ingen har tagit emot meddelandet.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="394"/>
        <source>%1 %2 can not join to the group.</source>
        <translation>%1 %2 kan inte ansluta till gruppen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="457"/>
        <source>%1 saved chats are added to history</source>
        <translation>%1 sparade chattar har lagts till i historiken</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="573"/>
        <source>Offline messages sent to %2.</source>
        <translation>Nedkopplat meddelande skickat till %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="410"/>
        <source>%1 %2 cannot be informed that you have left the group.</source>
        <translation>%1 %2 kan inte informeras om att du lämnat gruppen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="53"/>
        <source>Help me to know how many people are really using BeeBEEP.</source>
        <translation>Hjälp mig ta reda på hur många som verkligen använder BeeBEEP.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="55"/>
        <source>Please add a like on Facebook.</source>
        <translation>Gilla oss på Facebook.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="502"/>
        <source>%1 You have left the group.</source>
        <translation>%1 Du har lämnat gruppen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="504"/>
        <source>%1 %2 has left the group.</source>
        <translation>%1 %2 har lämnat gruppen.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="218"/>
        <source>%1 (%2) is disconnected from %3 network.</source>
        <translation>%1 (%2) är bortkopplad från nätverket, %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="338"/>
        <source>%1 (%2) is connected to %3 network.</source>
        <translation>%1 (%2) är ansluten till nätverket, %3.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="435"/>
        <source>%1 Network interface %2 is gone down.</source>
        <translation>%1 Nätverksgränssnittet %2 har gått ner.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="48"/>
        <source>%1 Unable to start file transfer server: bind address/port failed.</source>
        <translation>%1 Kunde inte starta filöverföringsservern. Bindningsadress/-port misslyckades.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="94"/>
        <source>%1 Unable to download %2 from %3: user is offline.</source>
        <translation>%1 Kunde inte ladda ner %2 från %3. Användaren är nerkopplad.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="109"/>
        <source>%1 Unable to download %2 from %3: folder %4 cannot be created.</source>
        <translation>%1 Kunde inte ladda ner &apos;%2&apos; från %3. Mappen &apos;%4&apos; kan inte skapas.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="121"/>
        <source>%1 Downloading %2 from %3.</source>
        <translation>%1 Laddar ner %2 från %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>from</source>
        <translation>från</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>to</source>
        <translation>till</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="184"/>
        <source>Open</source>
        <translation>Öppna</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="238"/>
        <source>%1 Unable to send %2 to %3: user is offline.</source>
        <translation>%1 Kunde inte skicka %2 till %3. Användaren är nerkopplad.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="215"/>
        <source>%1 Unable to send %2. File transfer is disabled.</source>
        <translation>%1 Kunde inte skicka %2. Filöverföring är inaktiverad.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Upload</source>
        <translation>Ladda upp</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="187"/>
        <source>folder</source>
        <translation>mapp</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="247"/>
        <source>%1 %2: file not found.</source>
        <translation>%1 %2: Filen kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="257"/>
        <source>%1 %2 is a folder. You can share it.</source>
        <translation>%1 %2 är en mapp. Du kan dela den.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="278"/>
        <source>%1 Unable to send %2: %3 is not connected.</source>
        <translation>%1 Kan inte skicka %2: %3 är inte ansluten.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="272"/>
        <source>%1 You send %2 to %3.</source>
        <translation>%1 Du skickar %2 till %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="303"/>
        <source>%1 You have refused to download %2 from %3.</source>
        <translation>%1 Du har nekat att ladda ner %2 från %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="331"/>
        <source>%1 You have refused to download folder %2 from %3.</source>
        <translation>%1 Du har nekat att ladda ner mappen &apos;%2&apos; från %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="394"/>
        <source>Adding to file sharing</source>
        <translation>Lägger till som fildelning</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="449"/>
        <source>%1 is added to file sharing (%2)</source>
        <translation>%1 har lagts till i fildelningen (%2)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="451"/>
        <source>%1 is added to file sharing with %2 files, %3 (elapsed time: %4)</source>
        <translation>%1 har lagts till i fildelningen med %2 filer, %3 (förfluten tid: %4)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="487"/>
        <source>All paths are removed from file sharing</source>
        <translation>Alla mappar har tagits bort från fildelning</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="509"/>
        <source>%1 is removed from file sharing</source>
        <translation>%1 har tagits bort från fildelning</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="511"/>
        <source>%1 is removed from file sharing with %2 files</source>
        <translation>%1 har tagits bort från fildelning, med %2 filer</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="559"/>
        <source>%1 You are about to send %2 to %3. Checking folder...</source>
        <translation>%1 Du är på väg att skicka %2 till %3. Kontrollerar mapp...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="594"/>
        <source>%1 Unable to send folder %2</source>
        <translation>%1 Kunde inte skicka mappen &apos;%2&apos;</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="599"/>
        <source>the folder is empty.</source>
        <translation>mappen är tom.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="606"/>
        <source>file transfer is not working.</source>
        <translation>filöverföring fungerar inte.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="614"/>
        <source>%1 is not connected.</source>
        <translation>%1 är inte ansluten.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="628"/>
        <source>internal error.</source>
        <translation>internt fel.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="633"/>
        <source>%1 You send folder %2 to %3.</source>
        <translation>%1 Du skickar mappen &apos;%2&apos; till %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="148"/>
        <source>%1 %2 has refused to download %3.</source>
        <translation>%1 %2 har nekat att ladda ner %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="175"/>
        <source>%1 %2 is sending to you the file: %3.</source>
        <translation>%1 %2 skickar dig filen: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="245"/>
        <source>%1 An error occurred when %2 tries to add you to the group chat: %3.</source>
        <translation>%1 Ett fel uppstår när %2 försöker lägga till dig i gruppchatten: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="251"/>
        <source>%1 %2 adds you to the group chat: %3.</source>
        <translation>%1 %2 lägger till dig i gruppchatten: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="285"/>
        <source>%1 %2 has not shared files.</source>
        <translation>%1 %2 har inte delat filer.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="290"/>
        <source>%1 %2 has shared %3 files.</source>
        <translation>%1 %2 har delat %3 filer.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="319"/>
        <source>%1 %2 has refused to download folder %3.</source>
        <translation>%1 %2 har nekat att ladda ner mappen &apos;%3&apos;.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="326"/>
        <source>unknown folder</source>
        <translation>okänd mapp</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="334"/>
        <source>%1 %2 is sending to you the folder: %3.</source>
        <translation>%1 %2 skickar dig mappen: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="86"/>
        <source>You are</source>
        <translation>Du är</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="88"/>
        <source>%1 is</source>
        <translation>%1 är</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="101"/>
        <source>You have changed your nickname from %1 to %2.</source>
        <translation>Du har ändrat ditt chattnamn från %1 till %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="103"/>
        <source>%1 has changed the nickname in %2.</source>
        <translation>%1 har bytt chattnamn i %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="117"/>
        <source>The %1&apos;s profile has been received.</source>
        <translation>%1s profil har tagits emot.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>You share this information</source>
        <translation>Du delar den här informationen</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>%1 shares this information</source>
        <translation>%1 delar den här informationen</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="235"/>
        <source>%1 You have created group from chat: %2.</source>
        <translation>%1 Du har skapat en grupp från chatt: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="290"/>
        <source>%1 You have deleted group: %2.</source>
        <translation>%1 Du har tagit bort gruppen: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="377"/>
        <source>is removed from favorites</source>
        <translation>är borttagen från favoriter</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="383"/>
        <source>is added to favorites</source>
        <translation>har lagts till i favoriter</translation>
    </message>
</context>
<context>
    <name>File</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="274"/>
        <source>Audio</source>
        <translation>Musik</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="275"/>
        <source>Video</source>
        <translation>Filmer</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="276"/>
        <source>Image</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="277"/>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="278"/>
        <source>Other</source>
        <translation>Annat</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="279"/>
        <source>Executable</source>
        <translation>Körbara</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="280"/>
        <source>MacOSX</source>
        <translation>MacOSX</translation>
    </message>
</context>
<context>
    <name>FileTransferPeer</name>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="77"/>
        <source>invalid file header</source>
        <translation>ogiltigt filhuvud</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="105"/>
        <source>Unable to open file</source>
        <translation>Kan inte öppna filen</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="112"/>
        <source>Unable to write in the file</source>
        <translation>Kan inte skriva till filen</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="119"/>
        <source>%1 bytes downloaded but the file size is only %2 bytes</source>
        <translation>%1 byte nerladdat, men filstorleken är endast %2 byte</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="52"/>
        <source>Transfer cancelled</source>
        <translation>Överföringen avbruten</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="113"/>
        <source>Transfer completed in %1</source>
        <translation>Överföringen slutförd på %1</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="171"/>
        <source>Connection timeout</source>
        <translation>Anslutningen överskred tidsgränsen</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="179"/>
        <source>Transfer timeout</source>
        <translation>Tidsgräns för överföring</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="101"/>
        <source>unable to send file header</source>
        <translation>kunde inte skicka filhuvud</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="116"/>
        <source>%1 bytes uploaded but the file size is only %2 bytes</source>
        <translation>%1 byte uppladdat, men filstorleken är endast %2 byte</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="123"/>
        <source>%1 bytes sent not confirmed (%2 bytes confirmed)</source>
        <translation>%1 bytes skickade obekräftat (%2 byte bekräftat)</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="154"/>
        <source>Unable to upload data</source>
        <translation>Kan inte ladda upp data</translation>
    </message>
</context>
<context>
    <name>GuiAddUser</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="36"/>
        <source>Add user</source>
        <translation>Lägg till användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="39"/>
        <source>your IP is %1 in LAN %2</source>
        <translation>Ditt IP är %1 i LAN %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Address</source>
        <translation>Adress</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="104"/>
        <source>Please insert a valid IP address.</source>
        <translation>Ange en giltig IP-adress.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="114"/>
        <source>Please insert a valid port or use the default one %1.</source>
        <translation>Ange ett giltigt portnummer eller använd standardport %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="148"/>
        <source>These IP address and port are already inserted in list.</source>
        <translation>Dessa IP-adresser och portnummer finns redan i listan.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="182"/>
        <source>Remove user path</source>
        <translation>Ta bort användarsökväg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="184"/>
        <source>Clear all</source>
        <translation>Rensa alla</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="209"/>
        <source>Please select an user path in the list.</source>
        <translation>Välj en användarsökväg från listan.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="234"/>
        <source>auto added</source>
        <translation>tillagt automatiskt</translation>
    </message>
</context>
<context>
    <name>GuiAddUserDialog</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="73"/>
        <source>Add an IP address and port of the user you want to connect</source>
        <translation>Lägg till IP-adress och port för den användare du vill ansluta till</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="89"/>
        <source>IP Address</source>
        <translation>IP-adress</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="121"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="169"/>
        <source>Split in IPv4 addresses</source>
        <translation>Dela upp i IPv4-adresser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="179"/>
        <source>Add</source>
        <translation>Lägg till</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="176"/>
        <source>Click here to add user path</source>
        <translation>Klicka här för att lägga till användarsökväg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="156"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="25"/>
        <source>Auto add from LAN</source>
        <translation>Lägg till automatiskt från LAN</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="45"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="52"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiAskPassword</name>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="23"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="30"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="62"/>
        <source>Use standard session (encrypted but authentication is not required)</source>
        <translation>Använd standardsession (krypterad men utan autentiseringskrav)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="69"/>
        <source>Use authentication password (spaces are removed) *</source>
        <translation>Använd autentiseringslösenord (mellanslag tas bort)*</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="124"/>
        <source>* Password must be the same for all user you want to connect</source>
        <translation>*Lösenordet måste vara detsamma för alla användare du vill ansluta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="153"/>
        <source>Remember password (not recommended)</source>
        <translation>Kom ihåg lösenordet (rekommenderas inte)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="160"/>
        <source>Show this dialog at connection startup</source>
        <translation>Visa denna dialog vid inledande anslutning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="33"/>
        <source>Chat Password - %1</source>
        <translation>Chattlösenord - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="103"/>
        <source>Password is empty. Please enter a valid one (spaces are removed).</source>
        <translation>Lösenordet är tomt! Ange ett giltigt lösenord (mellanslag tas bort).</translation>
    </message>
</context>
<context>
    <name>GuiChat</name>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="139"/>
        <source>Change font style</source>
        <translation>Byt teckensnitt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="140"/>
        <source>Select your favourite chat font style</source>
        <translation>Välj din egen chattstil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="141"/>
        <source>Change font color</source>
        <translation>Byt teckenfärg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="142"/>
        <source>Select your favourite font color for the chat messages</source>
        <translation>Välj teckenfärg på dina chattmeddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="143"/>
        <source>Change background color</source>
        <translation>Byt bakgrundsfärg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="144"/>
        <source>Select your favourite background color for the chat window</source>
        <translation>Välj din egen bakgrundsfärg i chattfönstret</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="145"/>
        <source>Filter message</source>
        <translation>Filtrera meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="146"/>
        <source>Select the message types which will be showed in chat</source>
        <translation>Välj vilka typer av meddelanden som skall visas i chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="147"/>
        <source>Chat settings</source>
        <translation>Chattinställningar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="148"/>
        <source>Click to show the settings menu of the chat</source>
        <translation>Klicka för att visa chattens inställningsmeny</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="149"/>
        <source>Spell checking</source>
        <translation>Stavningskontroll</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="151"/>
        <source>Word completer</source>
        <translation>Ordkomplettering</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="153"/>
        <source>Use Return key to send message</source>
        <translation>Använd returtangenten för att skicka meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="158"/>
        <source>Members</source>
        <translation>Medlemmar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="159"/>
        <source>Show the members of the chat</source>
        <translation>Visa chattens medlemmar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="165"/>
        <location filename="../src/desktop/GuiChat.cpp" line="1076"/>
        <source>Find text in chat</source>
        <translation>Sök text i chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="166"/>
        <source>Send file</source>
        <translation>Skicka fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="167"/>
        <source>Send a file to a user or a group</source>
        <translation>Skicka en fil till en användare eller grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="168"/>
        <source>Send folder</source>
        <translation>Skicka mapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="169"/>
        <source>Save chat</source>
        <translation>Spara chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="170"/>
        <source>Save the messages of the current chat to a file</source>
        <translation>Spara meddelanden från den aktuella chatten, i en fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="171"/>
        <source>Print...</source>
        <translation>Skriv ut...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="180"/>
        <source>Change the name of the group or add and remove users</source>
        <translation>Byt namn på gruppen eller lägg till och ta bort användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="229"/>
        <source>Open selected text as url</source>
        <translation>Öppna markerad text som URL</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="415"/>
        <source>unread messages</source>
        <translation>olästa meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="964"/>
        <source>Spell checking is enabled</source>
        <translation>Stavningskontroll är aktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="971"/>
        <location filename="../src/desktop/GuiChat.cpp" line="995"/>
        <source>There is not a valid dictionary</source>
        <translation>Det finns ingen giltig ordbok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="988"/>
        <source>Word completer is enabled</source>
        <translation>Ordkomplettering är aktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="990"/>
        <source>Word completer is disabled</source>
        <translation>Ordkomplettering är inaktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="1114"/>
        <source>%1 not found in chat.</source>
        <translation>%1 hittades inte i chatten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="966"/>
        <source>Spell checking is disabled</source>
        <translation>Stavningskontroll är inaktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="173"/>
        <source>Clear messages</source>
        <translation>Rensa meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="174"/>
        <source>Clear all the messages of the chat</source>
        <translation>Ta bort alla meddelanden i chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="177"/>
        <source>Create group from chat</source>
        <translation>Skapa en grupp av chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="178"/>
        <source>Create a group from this chat</source>
        <translation>Skapa en grupp av den här chatten</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation type="obsolete">Skapa grupp</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation type="obsolete">Skapa en grupp med 2 eller fler användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="181"/>
        <location filename="../src/desktop/GuiChat.cpp" line="182"/>
        <source>Leave the group</source>
        <translation>Lämna gruppen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="223"/>
        <source>Copy to clipboard</source>
        <translation>Kopiera till Urklipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="225"/>
        <source>Select All</source>
        <translation>Markera allt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="257"/>
        <source>Show only messages in default chat</source>
        <translation>Visa endast meddelanden i standardchatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="297"/>
        <source>Last message %1</source>
        <translation>Senaste meddelande %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="395"/>
        <source>All Lan Users</source>
        <translation>Alla LAN-användare</translation>
    </message>
    <message>
        <source>(You have left)</source>
        <translation type="obsolete">(Du har lämnat chatten)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="411"/>
        <source>You</source>
        <translation>Du</translation>
    </message>
    <message>
        <source>Create chat</source>
        <translation type="obsolete">Skapa chatt</translation>
    </message>
    <message>
        <source>Create a chat with two or more users</source>
        <translation type="obsolete">Skapa en chatt med två eller flera användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="179"/>
        <source>Edit group</source>
        <translation>Redigera grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="437"/>
        <source>offline</source>
        <translation>bortkopplad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="445"/>
        <source>Show profile</source>
        <translation>Visa profil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="455"/>
        <source>Show members</source>
        <translation>Visa medlemmar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>Nobody</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>and</source>
        <translation>och</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="537"/>
        <source>last %1 messages</source>
        <translation>senaste %1 meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="706"/>
        <source>Please select a file to save the messages of the chat.</source>
        <translation>Välj en fil att spara chatten i.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>%1: save completed.</source>
        <translation>%1: Sparat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="771"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Kan inte spara tempfilen: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="819"/>
        <source>Do you really want to send %1 %2 to the members of this chat?</source>
        <translation>Vill du verkligen skicka %1 %2 till medlemmarna i den här chatten?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="834"/>
        <source>Qt library for this OS doesn&apos;t support Drag and Drop for files. You have to select again the file to send.</source>
        <translation>Qt library för det här operativsystemet, stöder inte &apos;dra och släpp&apos; för filer. Du måste välja filen igen för att skicka.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="948"/>
        <source>Use key Return to send message</source>
        <translation>Använd returtangenten för att skicka meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="950"/>
        <source>Use key Return to make a carriage return</source>
        <translation>Använd returtangenten för ny rad</translation>
    </message>
</context>
<context>
    <name>GuiChatList</name>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="49"/>
        <source>Show</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="52"/>
        <source>Clear</source>
        <translation>Rensa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="53"/>
        <source>Clear all chat messages</source>
        <translation>Rensa alla chattmeddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="55"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
</context>
<context>
    <name>GuiChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="20"/>
        <source>Chat</source>
        <translation>Chatta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="238"/>
        <source>Write to: &lt;b&gt;ALL&lt;/b&gt;</source>
        <translation>Skriv till: &lt;b&gt;ALLA&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="260"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="288"/>
        <source>Save window&apos;s geometry</source>
        <translation>Spara fönstergeometri</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="326"/>
        <source>Detach chat</source>
        <translation>Koppla loss chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="136"/>
        <source>Click to send message or just hit enter</source>
        <translation>Klicka för att skicka meddelande eller tryck Retur</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroup</name>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="26"/>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="42"/>
        <source>TextLabel</source>
        <translation>Textetikett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="74"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="81"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="37"/>
        <source>Group name</source>
        <translation>Gruppnamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="38"/>
        <source>Please add member in the group:</source>
        <translation>Lägg till medlem i gruppen:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="41"/>
        <source>Users</source>
        <translation>Användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="65"/>
        <source>Create Group - %1</source>
        <translation>Skapa grupp - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="67"/>
        <source>Create Chat - %1</source>
        <translation>Skapa chatt - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="73"/>
        <source>Edit Group - %1</source>
        <translation>Redigera grupp - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="75"/>
        <source>Edit Chat - %1</source>
        <translation>Redigera chatt - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="113"/>
        <source>Please select two or more member for the group.</source>
        <translation>Välj två eller fler medlemmar till gruppen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="121"/>
        <source>Please insert a group name.</source>
        <translation>Ange ett gruppnamn.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="130"/>
        <source>%1 already exists as group name or chat name.
Please select a different name.</source>
        <translation>%1 finns redan som gruppnamn eller chattnamn.
Välj ett annat namn.</translation>
    </message>
</context>
<context>
    <name>GuiEditVCard</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="35"/>
        <source>Edit your profile</source>
        <translation>Redigera din profil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="103"/>
        <source>%1 - Select your profile photo</source>
        <translation>%1 Välj din profilbild</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="104"/>
        <source>Images</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <source>Unable to load image %1.</source>
        <translation>Kan inte läsa in bilden, %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="147"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="156"/>
        <source>Please insert your nickname.</source>
        <translation>Ange ditt chattnamn.</translation>
    </message>
</context>
<context>
    <name>GuiEmoticons</name>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="49"/>
        <source>Recent</source>
        <translation>Nyligen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="53"/>
        <source>Smiley</source>
        <translation>Smiley</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="57"/>
        <source>Objects</source>
        <translation>Objekt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="61"/>
        <source>Nature</source>
        <translation>Natur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="65"/>
        <source>Places</source>
        <translation>Platser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="69"/>
        <source>Symbols</source>
        <translation>Symboler</translation>
    </message>
</context>
<context>
    <name>GuiFileInfoList</name>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Shared folders and files</source>
        <translation>Delade mappar och filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Size</source>
        <translation>Storlek</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
</context>
<context>
    <name>GuiFloatingChat</name>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="43"/>
        <source>Show the bar of chat</source>
        <translation>Visa chattfält</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="51"/>
        <source>Emoticons</source>
        <translation>Känsloikoner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="63"/>
        <source>Show the emoticon panel</source>
        <translation>Visa panelen för känsloikoner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="64"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Lägg till önskad känsloikon till meddelandet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="244"/>
        <source>Window&apos;s geometry and state saved</source>
        <translation>Fönstergeometri och status sparad</translation>
    </message>
</context>
<context>
    <name>GuiGroupList</name>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="48"/>
        <source>Create group</source>
        <translation>Skapa grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="51"/>
        <source>Edit group</source>
        <translation>Redigera grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="54"/>
        <source>Open chat</source>
        <translation>Öppna chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="57"/>
        <source>Enable notifications</source>
        <translation>Aktivera avisering</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="60"/>
        <source>Disable notifications</source>
        <translation>Inaktivera avisering</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="63"/>
        <source>Delete group</source>
        <translation>Ta bort grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="143"/>
        <source>Waiting for two or more connected user</source>
        <translation>Väntar på två eller fler anslutna användare</translation>
    </message>
</context>
<context>
    <name>GuiHome</name>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="39"/>
        <source>Home</source>
        <translation>Hem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="49"/>
        <source>Select a user you want to chat with or</source>
        <translation>Välj en användare du vill chatta med eller</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="94"/>
        <source>Copy to clipboard</source>
        <translation>Kopiera till Urklipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="96"/>
        <source>Select All</source>
        <translation>Markera allt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="98"/>
        <source>Print...</source>
        <translation>Skriv ut...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="105"/>
        <source>Show the datestamp</source>
        <translation>Visa datumstämpel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="109"/>
        <source>Show the timestamp</source>
        <translation>Visa tidsstämpel</translation>
    </message>
</context>
<context>
    <name>GuiHomeWidget</name>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="107"/>
        <source>chat with all users</source>
        <translation>Chatta med alla användare</translation>
    </message>
</context>
<context>
    <name>GuiLanguage</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="35"/>
        <source>Select language</source>
        <translation>Välj språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="50"/>
        <source>For the latest language translations please visit the %1</source>
        <translation>Besök %1 för de senaste språkfilerna</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="51"/>
        <source>official website</source>
        <translation>officiell webbsida</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="94"/>
        <source>Select a language folder</source>
        <translation>Välj en språkmapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="121"/>
        <source>Language &apos;%1&apos;&apos; not found.</source>
        <translation>Språket &apos;%1&apos; kan inte hittas.</translation>
    </message>
</context>
<context>
    <name>GuiLanguageDialog</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="45"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="96"/>
        <source>Path</source>
        <translation>Sökväg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="127"/>
        <source>Select language folder</source>
        <translation>Välj språkmapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="169"/>
        <source>Restore to default language</source>
        <translation>Återställ till standardspråk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="172"/>
        <source>Restore</source>
        <translation>Återställ</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="195"/>
        <source>Select</source>
        <translation>Välj</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="205"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiLog</name>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="36"/>
        <source>System Log</source>
        <translation>Systemlogg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="55"/>
        <source>Save log as</source>
        <translation>Spara logg som</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="56"/>
        <source>Save the log in a file</source>
        <translation>Spara loggen i en fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="72"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="78"/>
        <source>keyword</source>
        <translation>nyckelord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="84"/>
        <source>Find</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="85"/>
        <source>Find keywords in the log</source>
        <translation>Sök nyckelord i loggen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="97"/>
        <source>Case sensitive</source>
        <translation>Skiftlägeskänslig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="102"/>
        <source>Whole word</source>
        <translation>Hela ord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="60"/>
        <source>Log to file</source>
        <translation>Logga till fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="109"/>
        <source>Block scrolling</source>
        <translation>Blockera skrollning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="116"/>
        <source>Please select a file to save the log.</source>
        <translation>Välj en fil för att spara loggen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <source>Unable to save log in the file: %1</source>
        <translation>Kunde inte spara loggen i filen: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="151"/>
        <source>%1: save log completed.</source>
        <translation>%1: Logg sparad.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open file</source>
        <translation>Öppna fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open folder</source>
        <translation>Öppna mapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="193"/>
        <source>%1 not found</source>
        <translation>%1 kunde inte hittas</translation>
    </message>
</context>
<context>
    <name>GuiLogWidget</name>
    <message>
        <location filename="../src/desktop/GuiLog.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
</context>
<context>
    <name>GuiMain</name>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="86"/>
        <source>Show the main tool bar</source>
        <translation>Visa primärt verktygsfält</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="283"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3291"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3292"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3293"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3294"/>
        <source>offline</source>
        <translation>bortkopplad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="326"/>
        <source>Do you really want to quit %1?</source>
        <translation>Vill du verkligen avsluta %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="349"/>
        <source>&lt;b&gt;Settings can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Inställningarna kan inte sparas&lt;/b&gt;. Sökväg:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="351"/>
        <location filename="../src/desktop/GuiMain.cpp" line="368"/>
        <source>&lt;b&gt;is not writable&lt;/b&gt; by user:</source>
        <translation>&lt;b&gt;är skrivskyddad&lt;/b&gt; för:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="353"/>
        <location filename="../src/desktop/GuiMain.cpp" line="370"/>
        <source>Do you want to close anyway?</source>
        <translation>Vill du avsluta i alla fall?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="414"/>
        <source>No new message available</source>
        <translation>Det finns inga nya meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="498"/>
        <source>Disconnect from %1 network</source>
        <translation>Koppla bort nätverket, %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="504"/>
        <source>Connect to %1 network</source>
        <translation>Anslut till nätverket, %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="587"/>
        <source>Secure Lan Messenger</source>
        <translation>Säker LAN-chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="588"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="590"/>
        <source>for</source>
        <translation>för</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="592"/>
        <source>developed by</source>
        <translation>utvecklad av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="601"/>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br /&gt;it under the terms of the GNU General Public License as published&lt;br /&gt;by the Free Software Foundation, either version 3 of the License&lt;br /&gt;or (at your option) any later version.&lt;br /&gt;&lt;br /&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br /&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br /&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br /&gt;See the GNU General Public License for more details.</source>
        <translation>BeeBEEP är fri mjukvara! Du kan återdistribuera och/eller modifiera&lt;br /&gt;den under villkoren för GNU General Public License, som publicerad&lt;br /&gt;av Free Software Foundation, antingen version 3 av licensen&lt;br /&gt;eller (om så önskas) någon senare version.&lt;br /&gt;&lt;br /&gt;BeeBEEP distribueras i hopp om att den skall vara användbar,&lt;br /&gt;men UTAN NÅGRA GARANTIER. Även utan underförstådd&lt;br /&gt;garanti om SÄLJBARHET eller LÄMPLIGHET FÖR ETT VISST SYFTE.&lt;br /&gt;Se GNU General Public License för mer information.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="617"/>
        <source>Search for users...</source>
        <translation>Sök efter användare...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="623"/>
        <source>Close the chat and quit %1</source>
        <translation>Stäng chatten och avsluta %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="631"/>
        <source>Show the main tool bar with settings</source>
        <translation>Visa primärt verktygsfält med inställningar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="635"/>
        <source>Show the informations about %1</source>
        <translation>Visa information om %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="667"/>
        <source>Select language...</source>
        <translation>Välj språk...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="668"/>
        <source>Select your preferred language</source>
        <translation>Välj önskat språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="669"/>
        <source>Download folder...</source>
        <translation>Nedladdningsmapp...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="670"/>
        <source>Select the download folder</source>
        <translation>Välj nedladdningsmapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="677"/>
        <source>Select beep file...</source>
        <translation>Välj ljudfil...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="678"/>
        <source>Select the file to play on new message arrived</source>
        <translation>Välj ljudfil att spela upp när nya meddelanden inkommer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="679"/>
        <source>Play beep</source>
        <translation>Spela upp ljud</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="680"/>
        <source>Test the file to play on new message arrived</source>
        <translation>Testa ljudfilen för avisering av nya meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="783"/>
        <source>Settings</source>
        <translation>Inställningar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="809"/>
        <source>If enabled the window geometry will be reset to default value at the next startup</source>
        <translation>Vid aktivering kommer fönstergeometrin att återställs till standardvärden vid nästa uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="836"/>
        <source>If a file already exists</source>
        <translation>Om en fil redan finns</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="874"/>
        <source>Always open a new floating chat window</source>
        <translation>Öppna alltid ett nytt flytande chattfönster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="875"/>
        <source>If enabled when you always open chat in a new floating window</source>
        <translation>Vid aktivering öppnas alltid ett nytt flytande chattfönster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="933"/>
        <source>Show only the online users</source>
        <translation>Visa endast anslutna användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="934"/>
        <source>If enabled only the online users are shown in the list</source>
        <translation>Vid aktivering visas endast anslutna användare i listan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="940"/>
        <source>If enabled you can see a picture of the users in the list (if they have)</source>
        <translation>Vid aktivering kan du se en användarbild i listan(om tillgänglig)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="866"/>
        <source>Set status to away automatically</source>
        <translation>Ange automatiskt status som borta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="867"/>
        <source>If enabled %1 change your status to away after an idle of %2 minutes</source>
        <translation>Vid aktivering ändrar %1 din status till borta, efter %2 minuters inaktivitet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="881"/>
        <source>If enabled when a new message is arrived a sound is emitted</source>
        <translation>Vid aktivering kommer ett ljud att höras när nytt meddelande inkommer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="902"/>
        <source>If enabled when a new message is arrived %1 is shown on top of all other windows</source>
        <translation>Vid aktivering visas %1 ovanpå alla andra fönster, när ett nytt meddelande inkommer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="844"/>
        <source>Generate automatic filename</source>
        <translation>Generera filnamn automatiskt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="845"/>
        <source>If the file to be downloaded already exists a new filename is automatically generated</source>
        <translation>Om filen som laddas ner redan finns, kommer ett nytt namn att genereras automatiskt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1094"/>
        <source>Enable tray icon notification</source>
        <translation>Aktivera avisering i meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1095"/>
        <source>If enabled tray icon shows some notification about status and message</source>
        <translation>Vid aktivering kommer status och meddelanden att aviseras i meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="908"/>
        <source>If enabled %1 stays on top of the other windows</source>
        <translation>Vid aktivering visas %1 alltid överst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="829"/>
        <source>Enable file transfer</source>
        <translation>Aktivera filöverföring</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="830"/>
        <source>If enabled you can transfer files with the other users</source>
        <translation>Vid aktivering kan du överföra filer till och från andra användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="790"/>
        <source>Prompts for network password on startup</source>
        <translation>Fråga efter nätverkslösenord vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="791"/>
        <source>If enabled the password dialog will be shown on connection startup</source>
        <translation>Vid aktivering visas en lösenordsdialog vid anslutningsstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="820"/>
        <source>Load %1 on Windows startup</source>
        <translation>Starta %1 vid Windows systemstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="821"/>
        <source>If enabled you can automatically load %1 at system startup</source>
        <translation>Vid aktivering startas %1 automatiskt vid systemstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="709"/>
        <source>Enable the compact mode in chat window</source>
        <translation>Aktivera kompaktläge i chattfönster</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="710"/>
        <source>If enabled the sender&apos;s nickname and his message are in the same line</source>
        <translation>Vid aktivering visas chattnamn och meddelande på samma rad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="715"/>
        <source>Add a blank line between the messages</source>
        <translation>Lägg till en tom rad mellan meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="716"/>
        <source>If enabled the messages in the chat window are separated by a blank line</source>
        <translation>Vid aktivering separeras meddelanden i chattfönstret med en tom rad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="722"/>
        <source>If enabled the message shows its timestamp in the chat window</source>
        <translation>Vid aktivering visas meddelandens tidsstämpel i chattfönstret</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="704"/>
        <source>If enabled the user&apos;s nickname in chat and in list is colored</source>
        <translation>Vid aktivering färgmarkeras chattnamn i lista och chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="770"/>
        <source>Use HTML tags</source>
        <translation>Använd HTML-taggar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="771"/>
        <source>If enabled HTML tags are not removed from the message</source>
        <translation>Vid aktivering tas inte HTML-taggar bort från meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="776"/>
        <source>Use clickable links</source>
        <translation>Använd klickbara länkar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="777"/>
        <source>If enabled the links in the message are recognized and made clickable</source>
        <translation>Vid aktivering känns länkar igen och görs klickbara</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="751"/>
        <source>Show messages grouped by user</source>
        <translation>Gruppera meddelanden efter användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="752"/>
        <source>If enabled the messages will be shown grouped by user</source>
        <translation>Vid aktivering visas meddelanden grupperade efter användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="695"/>
        <source>Save messages</source>
        <translation>Spara meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="663"/>
        <source>Add users manually...</source>
        <translation>Lägg till användare manuellt...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="664"/>
        <source>Add the IP address and the port of the users you want to connect</source>
        <translation>Ange IP-adress och portnummer till de användare du vill ansluta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="696"/>
        <source>If enabled the messages are saved when the program is closed</source>
        <translation>Vid aktivering sparas meddelanden när programmet avslutas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="965"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="966"/>
        <source>Select your status</source>
        <translation>Välj status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="972"/>
        <location filename="../src/desktop/GuiMain.cpp" line="988"/>
        <source>Your status will be %1</source>
        <translation>Din status visas som %1</translation>
    </message>
    <message>
        <source>Add a status description...</source>
        <translation type="obsolete">Lägg till en statusbeskrivning...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1008"/>
        <source>Show the chat</source>
        <translation>Visa chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1009"/>
        <source>Show the chat view</source>
        <translation>Visa chattvyn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1010"/>
        <source>Show my shared files</source>
        <translation>Visa mina delade filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1011"/>
        <source>Show the list of the files which I have shared</source>
        <translation>Visa en lista över de filer jag delar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1012"/>
        <source>Show the network shared files</source>
        <translation>Visa nätverksdelade filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1013"/>
        <source>Show the list of the network shared files</source>
        <translation>Visa en lista över nätverksdelade filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1022"/>
        <source>Plugins</source>
        <translation>Insticksmoduler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1026"/>
        <source>Tip of the day</source>
        <translation>Dagens tips</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1027"/>
        <source>Show me the tip of the day</source>
        <translation>Visa dagens tips</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1028"/>
        <source>Fact of the day</source>
        <translation>Dagens fakta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1029"/>
        <source>Show me the fact of the day</source>
        <translation>Visa dagens fakta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1032"/>
        <source>Show %1&apos;s license...</source>
        <translation>Visa %1s licens...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1033"/>
        <source>Show the informations about %1&apos;s license</source>
        <translation>Visa information om %1s licens</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1035"/>
        <source>Show the informations about Qt library</source>
        <translation>Visa information om Qt library</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1037"/>
        <source>Open %1 official website...</source>
        <translation>Öppna %1s officiella webbplats...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1038"/>
        <source>Explore %1 official website</source>
        <translation>Utforska %1s webbplats</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1039"/>
        <source>Check for new version...</source>
        <translation>Sök efter ny version...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1040"/>
        <source>Open %1 website and check if a new version exists</source>
        <translation>Öppna %1s webbplats och kontrollera om det finns någon ny version</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1041"/>
        <source>Download plugins...</source>
        <translation>Ladda ner insticksmoduler...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1042"/>
        <source>Open %1 website and download your preferred plugin</source>
        <translation>Öppna %1s webbplats och ladda ner önskade insticksmoduler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1043"/>
        <source>Help online...</source>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1044"/>
        <source>Open %1 website to have online support</source>
        <translation>Öppna %1s webbplats för hjälp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1055"/>
        <source>I&apos;m so grateful and pleased about that</source>
        <translation>Jag är så tacksam och nöjd med det</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="181"/>
        <source>Ready</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1163"/>
        <source>Users</source>
        <translation>Användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="497"/>
        <source>Disconnect</source>
        <translation>Koppla bort</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="503"/>
        <source>Connect</source>
        <translation>Anslut</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="618"/>
        <source>Configure %1 network to search a user who is not in your local subnet</source>
        <translation>Konfigurera %1s nätverk för att söka en användare som inte finns i ditt lokala undernät</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="621"/>
        <source>Quit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="626"/>
        <source>Edit your profile...</source>
        <translation>Redigera din profil...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="627"/>
        <source>Change your profile information like your picture or your email or phone number</source>
        <translation>Ändra din profilinformation, såsom användarbild, e-postadress eller telefonnummer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="634"/>
        <source>About %1...</source>
        <translation>Om %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="653"/>
        <source>Main</source>
        <translation>Primärt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="659"/>
        <source>Broadcast to network</source>
        <translation>Sänd till nätverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="660"/>
        <source>Broadcast a message in your network to find available users</source>
        <translation>Skicka ett meddelande i ditt lokala nätverk för att hitta anslutna användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="703"/>
        <source>Show colored nickname</source>
        <translation>Visa färgade chattnamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="939"/>
        <source>Show the user&apos;s picture</source>
        <translation>Visa användarbilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="693"/>
        <source>Chat</source>
        <translation>Chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="880"/>
        <source>Enable BEEP alert on new message</source>
        <translation>Aktivera ljudavisering vid nya meddelanden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="901"/>
        <source>Raise on top on new message</source>
        <translation>Visa överst vid nytt meddelande</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="907"/>
        <source>Always stay on top</source>
        <translation>Visa alltid överst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1076"/>
        <source>Load on system tray at startup</source>
        <translation>Minimera vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1077"/>
        <source>If enabled %1 will be start hided in system tray</source>
        <translation>Vid aktivering startas %1 minimerat i meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="995"/>
        <source>View</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="721"/>
        <source>Show the timestamp</source>
        <translation>Visa tidsstämplar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="739"/>
        <source>Parse Unicode and ASCII emoticons</source>
        <translation>Tolka unicode- och ASCII-känsloikoner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="740"/>
        <source>If enabled the ASCII emoticons will be recognized and shown as images</source>
        <translation>Vid aktivering kommer ASCII-känsloikoner att kännas igen och visas som bilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="745"/>
        <source>Use native emoticons</source>
        <translation>Använd inbyggda känsloikoner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="746"/>
        <source>If enabled the emoticons will be parsed by your system font</source>
        <translation>Vid aktivering kommer känsloikoner att tolkas av ditt systemteckensnitt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1639"/>
        <source>Show only last %1 messages</source>
        <translation>Visa endast %1 senaste meddelandena</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1640"/>
        <source>If enabled only the last %1 messages will be shown in chat</source>
        <translation>Vid aktivering visas endast de %1 senaste meddelandena i chatten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="858"/>
        <source>Prompt before downloading file</source>
        <translation>Fråga före filnedladdning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="859"/>
        <source>If enabled you have to confirm the action before downloading a file</source>
        <translation>Vid aktivering måste du bekräfta innan filer laddas ner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="808"/>
        <source>Reset window geometry at startup</source>
        <translation>Återställ fönstergeometri vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="923"/>
        <source>Options</source>
        <translation>Alternativ</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="945"/>
        <source>Show the user&apos;s vCard on right click</source>
        <translation>Visa användares vCard vid högerklick</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="946"/>
        <source>If enabled you can see the user&apos;s vCard when right click on it</source>
        <translation>Vid aktivering kan du se användares vCard när du högerklickar på dem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="925"/>
        <source>Save the users on exit</source>
        <translation>Spara användare vid avslut</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="366"/>
        <source>&lt;b&gt;Chat messages can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Chattmeddelanden kan inte sparas&lt;/b&gt;. Sökväg:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="638"/>
        <source>Create chat</source>
        <translation>Skapa chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="639"/>
        <source>Create a chat with two or more users</source>
        <translation>Skapa en chatt med två eller flera användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="642"/>
        <source>Create group</source>
        <translation>Skapa grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="643"/>
        <source>Create a group with two or more users</source>
        <translation>Skapa en grupp med 2 eller fler användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="671"/>
        <source>Shortcuts...</source>
        <translation>Genvägar...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="672"/>
        <source>Enable and edit your custom shortcuts</source>
        <translation>Aktivera och redigera dina anpassade genvägar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="673"/>
        <source>Dictionary...</source>
        <translation>Ordbok...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="674"/>
        <source>Select your preferred dictionary for spell checking</source>
        <translation>Välj önskad ordbok för stavningskontroll</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="684"/>
        <source>Open your resource folder</source>
        <translation>Öppna din resursmapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="685"/>
        <source>Click to open your resource folder</source>
        <translation>Klicka för att öppna din resursmapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="687"/>
        <source>Open your data folder</source>
        <translation>Öppna din datamapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="688"/>
        <source>Click to open your data folder</source>
        <translation>Klicka för att öppna din datamapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="727"/>
        <source>Show the datestamp</source>
        <translation>Visa datumstämpel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="728"/>
        <source>If enabled the message shows its datestamp in the chat window</source>
        <translation>Vid aktivering visar meddelande, datumstämpel i chattfönstret</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="733"/>
        <source>Show preview of the images</source>
        <translation>Förhandsgranska bilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="734"/>
        <source>If enabled the preview of the downloaded images will be showed in the chat window</source>
        <translation>Vid aktivering visas en förhandsgranskning av nedladdade bilder i chattfönstret</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="763"/>
        <source>Use your name instead of &apos;You&apos;</source>
        <translation>Använd ditt namn istället för &apos;Du&apos;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="785"/>
        <source>Prompts for nickname on startup</source>
        <translation>Frågar efter chattnamn vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="796"/>
        <source>Show activities home page at startup</source>
        <translation>Visa hemsidan vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="797"/>
        <source>If enabled the activities home page instead of chat page will be showed at startup</source>
        <translation>Vid aktivering visas hemsidan istället för chattsidan vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="802"/>
        <source>Show minimized at startup</source>
        <translation>Minimera vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="803"/>
        <source>If enabled %1 is showed minimized at startup</source>
        <translation>Vid aktivering minimeras %1 vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="814"/>
        <source>Check for new version at startup</source>
        <translation>Sök efter nya versioner vid uppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="840"/>
        <source>If the file to be downloaded already exists it is automatically overwritten</source>
        <translation>Om filen som skall laddas ner redan finns, skrivs den över automatiskt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="849"/>
        <source>Ask me</source>
        <translation>Fråga mig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="884"/>
        <source>When the chat is not visible</source>
        <translation>När chatten inte är synlig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="892"/>
        <source>Never</source>
        <translation>Aldrig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="915"/>
        <source>Prompt on quit (only when connected)</source>
        <translation>Bekräfta vid avslut (endast om ansluten)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="916"/>
        <source>If enabled you will be asked if you really want to close %1</source>
        <translation>Vid aktivering tillfrågas du om du verkligen vill avsluta %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="926"/>
        <source>If enabled the user list will be save on exit</source>
        <translation>Vid aktivering sparas användarlistan vid avslut</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="951"/>
        <source>Show status color in background</source>
        <translation>Visa statusfärg i bakgrunden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="952"/>
        <source>If enabled the user in list has colored backrgound as status</source>
        <translation>Vid aktivering får användare i listan, statusfärgad bakgrund</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="959"/>
        <source>Change size of the user&apos;s picture</source>
        <translation>Ändra storlek på användarbilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="960"/>
        <source>Click to change the picture size of the users in the list</source>
        <translation>Klicka för att ändra storlek på listans användarbilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="978"/>
        <source>Recently used</source>
        <translation>Senast använda</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="983"/>
        <source>Change your status description...</source>
        <translation>Ändra din statusbeskrivning...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="984"/>
        <source>Clear all status descriptions</source>
        <translation>Rensa alla statusbeskrivningar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="996"/>
        <source>Save main window geometry</source>
        <translation>Spara huvudfönstrets geometri</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1006"/>
        <source>Show %1 home</source>
        <translation>Visa %1s hem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1007"/>
        <source>Show the homepage with %1 activity</source>
        <translation>Visa hemsidan med %1-aktivitet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1018"/>
        <source>Show new message</source>
        <translation>Visa nytt meddelande</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1025"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1034"/>
        <source>Qt Library...</source>
        <translation>Qt Library...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1046"/>
        <source>Like %1 on Facebook</source>
        <translation>Gilla %1 på Facebook</translation>
    </message>
    <message>
        <source>Help me to know how many people use BeeBEEP</source>
        <translation type="obsolete">Hjälp mig att ta reda på hur många som använder BeeBEEP</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1054"/>
        <source>Donate for %1</source>
        <translation>Donera till %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1059"/>
        <source>Show</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1082"/>
        <source>Close button minimize to tray icon</source>
        <translation>Stängknappen minimerar till meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1083"/>
        <source>If enabled when the close button is clicked the window will be minimized to the system tray icon</source>
        <translation>Vid aktivering minimerar stängknappen,  programfönstret till  meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1088"/>
        <source>Escape key minimize to tray icon</source>
        <translation>Esc-tangenten minimerar till meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1089"/>
        <source>If enabled when the escape button is clicked the window will be minimized to the system tray icon</source>
        <translation>Vid aktivering minimerar Esc-knappen,  programfönstret till  meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1100"/>
        <source>Show only message notifications</source>
        <translation>Visa endast meddelandeaviseringar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1101"/>
        <source>If enabled tray icon shows only message notifications</source>
        <translation>Vid aktivering visas endast meddelandeavisering i meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1106"/>
        <source>Show chat message preview</source>
        <translation>Visa förhandsgranskning av chattmeddelande</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1172"/>
        <source>Show the list of the connected users</source>
        <translation>Visa listan över anslutna användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1175"/>
        <source>Groups</source>
        <translation>Grupper</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1184"/>
        <source>Show the list of your groups</source>
        <translation>Visa grupplistan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1187"/>
        <source>Chats</source>
        <translation>Chattar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1196"/>
        <source>Show the list of the chats</source>
        <translation>Visa chattlistan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1199"/>
        <source>History</source>
        <translation>Historik</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1208"/>
        <source>Show the list of the saved chats</source>
        <translation>Visa listan med sparade chattar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1211"/>
        <source>File Transfers</source>
        <translation>Filöverföringar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1220"/>
        <source>Show the list of the file transfers</source>
        <translation>Visa listan med filöverföringar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1223"/>
        <source>Emoticons</source>
        <translation>Känsloikoner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1235"/>
        <source>Show the emoticon panel</source>
        <translation>Visa panel med känsloikoner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1236"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Lägg till önskad känsloikon i meddelandet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1281"/>
        <source>Show the bar of local file sharing</source>
        <translation>Visa fältet för lokal fildelning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1292"/>
        <source>Show the bar of network file sharing</source>
        <translation>Visa fältet för nätverksfildelning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1303"/>
        <source>Show the bar of log</source>
        <translation>Visa loggfältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1317"/>
        <source>Show the bar of screenshot plugin</source>
        <translation>Visa fältet för skärmklippsinsticket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1342"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2406"/>
        <source>Play %1</source>
        <translation>Spela %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1346"/>
        <source>is a game developed by</source>
        <translation>är ett spel, utvecklat av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1349"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <source>When do you want %1 to play beep?</source>
        <translation type="obsolete">När vill du att %1 skall spela upp ljud?</translation>
    </message>
    <message>
        <source>If it not visible</source>
        <translation type="obsolete">Om inte synligt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="888"/>
        <source>Always</source>
        <translation>Alltid</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1485"/>
        <source>Please save the network password in the next dialog.</source>
        <translation>Spara nätverkslösenordet i nästa dialog.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1507"/>
        <source>How many minutes of idle %1 can wait before changing status to away?</source>
        <translation>Hur många minuter skall %1 vänta innan status ändras till borta?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1543"/>
        <source>Please select the maximum number of messages to be showed</source>
        <translation>Ange max antal meddelanden som visas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1722"/>
        <source>New message from %1</source>
        <translation>Nytt meddelande från %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1725"/>
        <source>New message arrived</source>
        <translation>Nytt meddelande mottaget</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1829"/>
        <source>%1 is writing...</source>
        <translation>%1 skriver...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1842"/>
        <source>Do you want to disconnect from %1 network?</source>
        <translation>Vill du koppla bort nätverket %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1875"/>
        <source>You are %1%2</source>
        <translation>Du är %1%2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1886"/>
        <source>Please insert the new status description</source>
        <translation>Ange ny statusbeskrivning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>%1 - Select a file</source>
        <translation>%1 - Välj en fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>or more</source>
        <translation>eller fler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1954"/>
        <source>File transfer is not enabled.</source>
        <translation>Filöverföring är inte aktiverat.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1960"/>
        <source>You are not connected.</source>
        <translation>Du är inte ansluten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1971"/>
        <source>There is no user connected.</source>
        <translation>Det finns inga anslutna användare.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1977"/>
        <source>Please select the user to whom you would like to send a file.</source>
        <translation>Välj den användare som du vill skicka en fil till.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1986"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2993"/>
        <source>User not found.</source>
        <translation>Användaren kan inte hittas.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2009"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2159"/>
        <source>File transfer is disabled. You cannot download %1.</source>
        <translation>Filöverföring är inaktiverat. Du kan inte ladda ner %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <source>Yes, and don&apos;t ask anymore</source>
        <translation>Ja, och fråga inte fler gånger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2063"/>
        <source>%1 already exists. Please select a new filename.</source>
        <translation>%1 finns redan. Välj ett annat filnamn.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2102"/>
        <source>You cannot download all these files at once. Do you want to download the first %1 files of the list?</source>
        <translation>Du kan inte ladda ner alla dessa filer samtidigt. Vill du ladda ner de %1 första filerna i listan?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2110"/>
        <source>Downloading %1 files is a hard duty. Maybe you have to wait a lot of minutes. Do yo want to continue?</source>
        <translation>Nedladdning av %1 filer är en tuff uppgift. Det kanske kommer att ta många minuter. Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2128"/>
        <source>%1 files are scheduled for download</source>
        <translation>%1 filer är köade för nedladdning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2167"/>
        <source>Do you want to download folder %1 (%2 files) from %3?</source>
        <translation>Vill du ladda ner mappen &apos;%1&apos; (%2 filer) från %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2208"/>
        <source>%1 - Select the download folder</source>
        <translation>%1 - Välj nedladdningsmapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2361"/>
        <source>Plugin Manager...</source>
        <translation>Instickshanterare...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2362"/>
        <source>Open the plugin manager dialog and manage the installed plugins</source>
        <translation>Öppna instickshanteraren och hantera installerade insticksmoduler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3589"/>
        <source>at lunch</source>
        <translation>på lunch</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3598"/>
        <source>in a meeting</source>
        <translation>i ett möte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3634"/>
        <source>Do you really want to clear all saved status descriptions?</source>
        <translation>Vill du verkligen ta bort alla sparade statusbeskrivningar?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3811"/>
        <source>Select your dictionary path</source>
        <translation>Välj sökväg till ordboken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3819"/>
        <source>Dictionary selected: %1</source>
        <translation>Vald ordbok: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3821"/>
        <source>Unable to set dictionary: %1</source>
        <translation>Kunde inte ange ordbok: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3982"/>
        <source>Window geometry and state saved</source>
        <translation>Fönstergeometri och status sparades</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1014"/>
        <source>Show the %1 log</source>
        <translation>Visa %1-loggen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1015"/>
        <source>Show the application log to see if an error occurred</source>
        <translation>Visa programloggen för att se om något är fel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1016"/>
        <source>Make a screenshot</source>
        <translation>Ta ett skärmklipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1017"/>
        <source>Show the utility to capture a screenshot</source>
        <translation>Visa skärmklippverktyget</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1048"/>
        <source>Add +1 user to anonymous usage statistics</source>
        <translation>Lägg till +1 användare till anonym användarstatistik</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1049"/>
        <source>Help me to know how many users have BeeBEEP</source>
        <translation>Hjälpa mig ta reda på hur många som använder BeeBEEP</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1066"/>
        <source>Network</source>
        <translation>Nätverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2364"/>
        <source>is a plugin developed by</source>
        <translation>är en insticksmodul, utvecklad av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2389"/>
        <source>Show the bar of games</source>
        <translation>Visa spelfältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2636"/>
        <source>Do you really want to open the file %1?</source>
        <translation>Vill du verkligen öppna filen &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Unable to open %1</source>
        <translation>Kunde inte öppna %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2661"/>
        <source>Sound files (*.wav)</source>
        <translation>Ljudfiler (* wav)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <source>Sound is not enabled on a new message. Do you want to enable it?</source>
        <translation>Ljudet är inte aktiverat för nya meddelanden. Vill du aktivera det?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2685"/>
        <source>Sound module is not working. The default BEEP will be used.</source>
        <translation>Ljudmodulen fungerar inte. Standardljudet kommer att användas.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2690"/>
        <source>Sound file not found</source>
        <translation>Ljudfilen kan inte hittas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2691"/>
        <source>The default BEEP will be used</source>
        <translation>Standardljudet kommer att användas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2737"/>
        <source>Group chat will be deleted when all members goes offline.</source>
        <translation>Gruppchatten kommer att tas bort när alla medlemmar kopplar ner.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2738"/>
        <source>If you want a persistent chat please consider to make a Group instead.</source>
        <translation>Överväg att skapa en grupp istället, om du vill ha en permanent chatt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2739"/>
        <source>Do you wish to continue or create group?</source>
        <translation>Vill du fortsätta eller skapa en grupp?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Continue</source>
        <translation>Fortsätt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Create Group</source>
        <translation>Skapa grupp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2766"/>
        <source>Unable to add users in this chat. Please select a group one.</source>
        <translation>Kan inte lägga till användare i den här chatten. Välj en gruppchatt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2813"/>
        <source>Now %1 will start on windows boot.</source>
        <translation>Nu kommer %1 att starta vid Windows systemstart.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2815"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2822"/>
        <source>Unable to add this key in the registry: permission denied.</source>
        <translation>Kan inte lägga till den här nyckeln i registret. Åtkomst nekad.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2820"/>
        <source>%1 will not start on windows boot.</source>
        <translation>%1 startar inte vid Windows systemstart.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2877"/>
        <source>Please select a chat you would like to link the saved text.</source>
        <translation>Välj den chatt du vill länka den sparade texten till.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2886"/>
        <source>The chat &apos;%1&apos; selected has already a saved text.&lt;br /&gt;What do you want to do with the selected saved text?</source>
        <translation>Chatten &apos;%1&apos; har redan en sparad text.&lt;br /&gt;Vad vill du göra med den sparade texten?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="839"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Overwrite</source>
        <translation>Skriv över</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Add in the head</source>
        <translation>Lägg till som rubrik</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3037"/>
        <source>All the members of this chat are not online. The changes may not be permanent. Do you wish to continue?</source>
        <translation>Alla medlemmar i den här chatten är inte anslutna. Ändringarna kanske inte blir permanenta. Vill du fortsätta?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3175"/>
        <source>Do you really want to delete chat with %1?</source>
        <translation>Vill du verkligen ta bort chatten med %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3296"/>
        <source>inactive</source>
        <translation>inaktiv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3316"/>
        <source>disabled</source>
        <translation>inaktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3323"/>
        <source>active</source>
        <translation>aktiv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3396"/>
        <source>%1 is online</source>
        <translation>%1 är ansluten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3398"/>
        <source>%1 is offline</source>
        <translation>%1 är nerkopplad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3410"/>
        <source>Please select the new size of the user picture</source>
        <translation>Välj ny storlek på användarbilden</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1171"/>
        <source>Show the user panel</source>
        <translation>Visa användarpanelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1183"/>
        <source>Show the group panel</source>
        <translation>Visa gruppanelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1195"/>
        <source>Show the chat panel</source>
        <translation>Visa chattpanelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1207"/>
        <source>Show the history panel</source>
        <translation>Visa historikpanelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1219"/>
        <source>Show the file transfer panel</source>
        <translation>Visa filöverföringspanelen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1268"/>
        <source>Show the bar of chat</source>
        <translation>Visa chattfältet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2030"/>
        <source>Do you want to download %1 (%2) from %3?</source>
        <translation>Vill du ladda ner %1 (%2) från %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2143"/>
        <source>File is not available for download.</source>
        <translation>Filen är inte tillgänglig för nedladdning.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2145"/>
        <source>%1 is not connected.</source>
        <translation>%1 är inte ansluten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2146"/>
        <source>Please reload the list of shared files.</source>
        <translation>Uppdatera listan över delade filer.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <source>Reload file list</source>
        <translation>Uppdatera fillistan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3122"/>
        <source>Chat with %1 is empty.</source>
        <translation>Chatten med %1 är tom.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3126"/>
        <source>Do you really want to clear messages with %1?</source>
        <translation>Vill du verkligen rensa konversationen med %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3129"/>
        <source>Yes and delete history</source>
        <translation>Ja, och ta bort historiken</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3059"/>
        <source>%1 is a your group. You can not leave the chat.</source>
        <translation>%1 är din grupp. Du kan inte lämna chatten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Delete this group</source>
        <translation>Ta bort den här gruppen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3084"/>
        <source>You cannot leave this chat.</source>
        <translation>Du kan inte lämna den här chatten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3096"/>
        <source>Do you really want to delete group &apos;%1&apos;?</source>
        <translation>Vill du verkligen ta bort gruppen &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3197"/>
        <source>Unable to delete this chat.</source>
        <translation>Kan inte ta bort den här chatten.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3219"/>
        <source>%1 has shared %2 files</source>
        <translation>%1 har delat %2 filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3241"/>
        <source>Default language is restored.</source>
        <translation>Standardspråket är återställt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3243"/>
        <source>New language &apos;%1&apos; is selected.</source>
        <translation>Nya språket &apos;%1&apos; är valt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3246"/>
        <source>You must restart %1 to apply these changes.</source>
        <translation>Du måste starta om %1 för att verkställa dessa ändringar.</translation>
    </message>
</context>
<context>
    <name>GuiMessageEdit</name>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="322"/>
        <source>Undo</source>
        <translation>Ångra</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="324"/>
        <source>Redo</source>
        <translation>Upprepa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="327"/>
        <source>Cut</source>
        <translation>Klipp ut</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="328"/>
        <source>Copy</source>
        <translation>Kopiera</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="329"/>
        <source>Paste</source>
        <translation>Klistra in</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="332"/>
        <source>Select All</source>
        <translation>Markera allt</translation>
    </message>
</context>
<context>
    <name>GuiPluginManager</name>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="100"/>
        <source>Enable All</source>
        <translation>Aktivera alla</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="110"/>
        <source>Disable All</source>
        <translation>Inaktivera alla</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="133"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="36"/>
        <source>Plugin Manager - %1</source>
        <translation>Instickshanterare - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Plugin</source>
        <translation>Insticksmodul</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Author</source>
        <translation>Utvecklare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Disable %1</source>
        <translation>Inaktivera %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Enable %1</source>
        <translation>Aktivera %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is enabled</source>
        <translation>%1 är aktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is disabled</source>
        <translation>%1 är inaktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="129"/>
        <source>Please select a plugin in the list.</source>
        <translation>Välj en modul i listan.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="152"/>
        <source>Text Markers</source>
        <translation>Textmarkörer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="171"/>
        <source>Games</source>
        <translation>Spel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="192"/>
        <source>%1 - Select the plugin folder</source>
        <translation>%1 - Välj mapp för insticksmoduler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="208"/>
        <source>Folder %1 not found.</source>
        <translation>Mappen &apos;%1&apos; kunde inte hittas.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChat</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="56"/>
        <source>Saved chat</source>
        <translation>Sparad chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="58"/>
        <source>Empty</source>
        <translation>Tom</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="72"/>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="143"/>
        <source>Find text in chat</source>
        <translation type="unfinished">Sök text i chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="77"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Kopiera till Urklipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="79"/>
        <source>Select All</source>
        <translation type="unfinished">Markera allt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="83"/>
        <source>Open selected text as url</source>
        <translation type="unfinished">Öppna markerad text som URL</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="86"/>
        <source>Print...</source>
        <translation type="unfinished">Skriv ut...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="181"/>
        <source>%1 not found in chat.</source>
        <translation type="unfinished">%1 hittades inte i chatten.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatList</name>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="47"/>
        <source>Show</source>
        <translation>Visa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="50"/>
        <source>Link to chat</source>
        <translation>Länka till chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="52"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Do you really want to delete this saved chat?</source>
        <translation>Vill du verkligen ta bort den här sparade chatten?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
</context>
<context>
    <name>GuiScreenShot</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="35"/>
        <source>Make a Screenshot</source>
        <translation>Skapa ett skärmklipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="44"/>
        <source>Delay</source>
        <translation>Fördröj</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="45"/>
        <source>Delay screenshot for selected seconds</source>
        <translation>Fördröj skärmklippet med angivet antal sekunder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="53"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="61"/>
        <source>Hide this window</source>
        <translation>Dölj det här fönstret</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="62"/>
        <source>Hide this window before capture screenshot</source>
        <translation>Dölj det här fönstret innan skärmklippet tas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="73"/>
        <source>Enable high dpi</source>
        <translation>Aktivera hög upplösning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="74"/>
        <source>Enable high dpi support to manage, for example, Apple Retina display</source>
        <translation>Aktivera stödet för hög upplösning, t.ex Apples Retina-skärm</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="80"/>
        <source>Capture</source>
        <translation>Klipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="81"/>
        <source>Capture a screenshot of your desktop</source>
        <translation>Ta ett skärmklipp av ditt skrivbord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="82"/>
        <source>Send</source>
        <translation>Skicka</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="83"/>
        <source>Send the captured screenshot to an user</source>
        <translation>Skicka skärmklippet till en användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="84"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="85"/>
        <source>Save the captured screenshot as file</source>
        <translation>Spara skärmklippet som fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="86"/>
        <source>Delete</source>
        <translation>Ta bort</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="87"/>
        <source>Delete the captured screenshot</source>
        <translation>Ta bort skärmklippet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="107"/>
        <source>No screenshot available</source>
        <translation>Det finns inga skärmklipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="179"/>
        <source>/beesshot-%1.</source>
        <translation>/beesshot-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="182"/>
        <source>Save As</source>
        <translation>Spara som</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="184"/>
        <source>%1 Files (*.%2)</source>
        <translation>%1 Filer (*.%2)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="199"/>
        <source>/beesshottmp-%1.</source>
        <translation>/beesshottmp-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="206"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Kan inte spara tempfilen: %1</translation>
    </message>
</context>
<context>
    <name>GuiScreenShotWidget</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="84"/>
        <source>No Screenshot Available</source>
        <translation>Inget skärmklipp tillgängligt</translation>
    </message>
</context>
<context>
    <name>GuiSearchUser</name>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="77"/>
        <source>Local subnet address *</source>
        <translation>Lokal undernätadress *</translation>
    </message>
    <message>
        <source>Enter the IP addresses or subnet of your local area network separed by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation type="obsolete">Ange IP-adresser eller undernät i ditt lokala nätverk, separerade med kommatecken (exempel: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="204"/>
        <source>Addresses in beehosts.ini *</source>
        <translation>Adresser i beehosts.ini *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="35"/>
        <source>UDP Port in beebeep.rc *</source>
        <translation>UDP-port i beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="64"/>
        <source>(the same for all clients)</source>
        <translation>(samma för alla klienter)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="106"/>
        <source>(search users here by default)</source>
        <translation>(sök användare här, som standard)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="119"/>
        <source>Multicast group in beebeep.rc *</source>
        <translation>Multicast-grupp i beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="155"/>
        <source>Enable broadcast interval</source>
        <translation>Aktivera sändningsintervall</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="184"/>
        <source>seconds (0=disabled, 10=default)</source>
        <translation>sekunder (0=inaktiverat, 10=standard)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="351"/>
        <source>Workgroups (enter your network groups separated by comma)</source>
        <translation>Arbetsgrupper (ange dina nätverksgrupper separerade med kommatecken)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="239"/>
        <source>Accept connections only from your workgroups</source>
        <translation>Acceptera anslutning endast från dina arbetsgrupper</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="258"/>
        <source>Enter the IP addresses or subnet of your local area network separated by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation>Ange IP-adresser eller undernät i ditt lokala nätverk, separerade med kommatecken (exempel: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="284"/>
        <source>Split subnet to IPV4 addresses</source>
        <translation>Dela upp undernät i IPv4-adresser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="297"/>
        <source>Verbose</source>
        <translation>Detaljerat</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="328"/>
        <source>Max users to contact in a tick</source>
        <translation>Max antal användare att kontakta på en gång</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="337"/>
        <source>Automatically add external subnet</source>
        <translation>Lägg till externt undernät automatiskt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="344"/>
        <source>Enable Zero Configuration Networking</source>
        <translation>Aktivera Zero Configuration Networking</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="367"/>
        <source>* (read only section)</source>
        <translation>* (skrivskyddad sektion)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="374"/>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="381"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="36"/>
        <source>Search for users</source>
        <translation>Sök efter användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="54"/>
        <source>Unknown address</source>
        <translation>Okänd adress</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="62"/>
        <source>File is empty</source>
        <translation>Filen är tom</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="116"/>
        <source>Warning</source>
        <translation>Varning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>You have inserted an invalid host address:
%1 is removed from the list.</source>
        <translation>Du har angett en ogiltig värdadress:
%1 är borttagen från listan.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocal</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="37"/>
        <source>Share your folders or files</source>
        <translation>Dela dina mappar eller filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Size</source>
        <translation>Storlek</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Path</source>
        <translation>Sökväg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="77"/>
        <source>Share a file</source>
        <translation>Dela en fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="78"/>
        <source>Add a file to your local share</source>
        <translation>Lägg til en fil i din lokala utdelning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="80"/>
        <source>Share a folder</source>
        <translation>Dela en mapp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="81"/>
        <source>Add a folder to your local share</source>
        <translation>Lägg till en mapp i din lokala utdelning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="83"/>
        <source>Update shares</source>
        <translation>Uppdatera utdelningar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="84"/>
        <source>Update shared folders and files</source>
        <translation>Uppdatera delade mappar och filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="86"/>
        <source>Remove shared path</source>
        <translation>Ta bort utdelning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="87"/>
        <source>Remove shared path from the list</source>
        <translation>Ta bort utdelning från listan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="89"/>
        <source>Clear all shares</source>
        <translation>Rensa alla utdelningar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="90"/>
        <source>Clear all shared paths from the list</source>
        <translation>Ta bort alla utdelningar från listan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="99"/>
        <source>Shared files</source>
        <translation>Delade filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="101"/>
        <source>File transfer is disabled</source>
        <translation>Filöverföring är inaktiverad</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="119"/>
        <source>Select a file to share</source>
        <translation>Välj en fil att dela</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="132"/>
        <source>Select a folder to share</source>
        <translation>Välj en mapp att dela</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="150"/>
        <source>Please select a shared path.</source>
        <translation>Välj en utdelning.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="167"/>
        <source>Do you really want to remove all shared paths?</source>
        <translation>Vill du verkligen ta bort alla utdelningar?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="214"/>
        <source>Click to open %1</source>
        <translation>Klicka för att öppna %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="229"/>
        <source>%1 is already shared.</source>
        <translation>%1 är redan delad.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="270"/>
        <source>File transfer is disabled. Open the option menu to enable it.</source>
        <translation>Filöverföring är inaktiverad. Öppna inställningsmenyn för att aktivera det.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="334"/>
        <source>%1 shared files</source>
        <translation>%1 delade filer</translation>
    </message>
</context>
<context>
    <name>GuiShareLocalWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="87"/>
        <source>2</source>
        <translation>2</translation>
    </message>
</context>
<context>
    <name>GuiShareNetwork</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="98"/>
        <source>User</source>
        <translation>Användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="52"/>
        <source>Scan network</source>
        <translation>Skanna nätverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="39"/>
        <source>Folder and Files shared in your network</source>
        <translation>Delade mappar och filer i ditt nätverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="53"/>
        <source>Search shared files in your network</source>
        <translation>Sök efter delade filer i ditt nätverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="56"/>
        <source>Reload list</source>
        <translation>Uppdatera listan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="57"/>
        <source>Clear and reload list</source>
        <translation>Rensa och uppdatera listan</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="74"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="83"/>
        <source>File Type</source>
        <translation>Filtyp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="89"/>
        <source>All Files</source>
        <translation>Alla filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="61"/>
        <source>Download</source>
        <translation>Ladda ner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="62"/>
        <source>Download single or multiple files simultaneously</source>
        <translation>Ladda ner enskild eller flera filer samtidigt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="68"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="116"/>
        <source>All Users</source>
        <translation>Alla användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="141"/>
        <source>%1 is searching shared files in your network</source>
        <translation>%1 söker efter delade filer i ditt nätverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="195"/>
        <source>Double click to download %1</source>
        <translation>Dubbelklicka för att ladda ner &apos;%1&apos;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="234"/>
        <source>%1 has shared %2 files (%3)</source>
        <translation>%1 har delat %2 filer (%3)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="315"/>
        <source>Double click to open %1</source>
        <translation>Dubbelklicka för att öppna %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="317"/>
        <source>Transfer completed</source>
        <translation>Överföring slutförd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="331"/>
        <source>%1 files are shown in list (%2 are available in your network)</source>
        <translation>%1 filer visas i listan (%2 är tillgängliga i ditt nätverk)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="333"/>
        <source>%1 files shared in your network</source>
        <translation>%1 filer delade i ditt nätverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download single file</source>
        <translation>Ladda ner enskild fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download %1 selected files</source>
        <translation>Ladda ner %1 valda filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="377"/>
        <source>MAX</source>
        <translation>MAX</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="380"/>
        <source>Clear selection</source>
        <translation>Avmarkera alla</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="384"/>
        <source>Expand all items</source>
        <translation>Expandera alla objekt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="385"/>
        <source>Collapse all items</source>
        <translation>Komprimera alla objekt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="396"/>
        <source>Please select one or more files to download.</source>
        <translation>Välj en eller flera filer för nedladdning.</translation>
    </message>
</context>
<context>
    <name>GuiShareNetworkWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.ui" line="14"/>
        <source>Form</source>
        <translation>Formulär</translation>
    </message>
</context>
<context>
    <name>GuiShortcut</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="34"/>
        <source>Shortcuts</source>
        <translation>Genvägar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Key</source>
        <translation>Tangent</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Action</source>
        <translation>Åtgärd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="114"/>
        <source>Insert shorcut for the action: %1</source>
        <translation>Infoga genväg för åtgärden: %1</translation>
    </message>
</context>
<context>
    <name>GuiShortcutDialog</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <source>Restore to default language</source>
        <translation type="obsolete">Återställ till standardspråk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="23"/>
        <source>Use custom shortcuts</source>
        <translation>Använd anpassade genvägar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="50"/>
        <source>Restore default shortcuts</source>
        <translation>Återställ standardgenvägar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="53"/>
        <source>Restore</source>
        <translation>Återställ</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="76"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="89"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiTransferFile</name>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>User</source>
        <translation>Användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="116"/>
        <source>Completed</source>
        <translation>Slutfört</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="122"/>
        <source>Cancel Transfer</source>
        <translation>Avbryt överföring</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="128"/>
        <source>Not Completed</source>
        <translation>Ej slutfört</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="161"/>
        <source>Transfer completed</source>
        <translation>Överföring slutförd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Downloading</source>
        <translation>Laddar ner</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Uploading</source>
        <translation>Laddar upp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="218"/>
        <source>Do you really want to cancel the transfer of %1?</source>
        <translation>Vill du verkligen avbryta överföringen av %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="251"/>
        <source>Remove all transfers</source>
        <translation>Ta bort alla överföringar</translation>
    </message>
</context>
<context>
    <name>GuiUserList</name>
    <message>
        <location filename="../src/desktop/GuiUserList.cpp" line="55"/>
        <source>Search</source>
        <translation>Sök</translation>
    </message>
</context>
<context>
    <name>GuiVCard</name>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="59"/>
        <source>Birthday: %1</source>
        <translation>Födelsedag: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="85"/>
        <source>unknown</source>
        <translation>okänd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="87"/>
        <source>old %1</source>
        <translation>gammal %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="89"/>
        <source>new %1</source>
        <translation>ny %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="105"/>
        <source>Remove from favorites</source>
        <translation>Ta bort från favoriter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="110"/>
        <source>Add to favorites</source>
        <translation>Lägg till i favoriter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="115"/>
        <source>Chat with all</source>
        <translation>Chatta med alla</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="121"/>
        <source>Open chat</source>
        <translation>Öppna chatt</translation>
    </message>
</context>
<context>
    <name>GuiVCardDialog</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="22"/>
        <source>User ID</source>
        <translation>Användar-ID</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="150"/>
        <source>Nickname</source>
        <translation>Chattnamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="207"/>
        <source>First name</source>
        <translation>Förnamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="224"/>
        <source>Last name</source>
        <translation>Efternamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="241"/>
        <source>Birthday</source>
        <translation>Födelsedag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="274"/>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="291"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="356"/>
        <source>Add or change photo</source>
        <translation>Lägg till eller ändra bild</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="385"/>
        <source>Remove photo</source>
        <translation>Ta bort bild</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="417"/>
        <source>Change your nickname color</source>
        <translation>Byt färg på ditt chattnamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="456"/>
        <source>Other informations</source>
        <translation>Annan information</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="499"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="509"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiVCardWidget</name>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="81"/>
        <source>Widget</source>
        <translation>Widget</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="291"/>
        <source>User Path</source>
        <translation>Användarsökväg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="307"/>
        <source>User Name</source>
        <translation>Användarnamn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="323"/>
        <source>Birthday</source>
        <translation>Födelsedag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="339"/>
        <source>Email</source>
        <translation>E-post</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="355"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="417"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="452"/>
        <source>Open chat</source>
        <translation>Öppna chatt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="490"/>
        <source>Send a file</source>
        <translation>Skicka en fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="525"/>
        <source>Change the nickname color</source>
        <translation>Byt chattnamnsfärg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="583"/>
        <source>Remove user</source>
        <translation>Ta bort användaren</translation>
    </message>
</context>
<context>
    <name>GuiWizard</name>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="36"/>
        <source>Welcome to &lt;b&gt;%1 Network&lt;/b&gt;.</source>
        <translation>Välkommen till &lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="42"/>
        <source>Your system account is</source>
        <translation>Ditt systemkonto är</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="59"/>
        <source>Your nickname can not be empty.</source>
        <translation>Ditt chattnamn kan inte lämnas tomt.</translation>
    </message>
</context>
<context>
    <name>GuiWizardDialog</name>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="56"/>
        <source>Welcome</source>
        <translation>Välkommen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="94"/>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>NumberTextMarker</name>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="33"/>
        <source>Number Text Marker</source>
        <translation>Siffertext</translation>
    </message>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="48"/>
        <source>If you want to encode your message with numbers write a #text to encode# .</source>
        <translation>Skriv #text# #som# #skall# #kodas# , om du vill sifferkoda ditt meddelande.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="63"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3119"/>
        <location filename="../src/desktop/GuiUserItem.cpp" line="84"/>
        <source>All Lan Users</source>
        <translation>Alla LAN-användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="64"/>
        <source>Open chat with all local users</source>
        <translation>Öppna chatt med alla lokala användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="84"/>
        <source>Open chat with %1</source>
        <translation>Öppna chatt med %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatMessage.cpp" line="61"/>
        <source>You</source>
        <translation>Du</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="148"/>
        <source>Click to open chat with all local users</source>
        <translation>Klicka för att öppna chatten med alla lokala användare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="152"/>
        <source>%1 is %2</source>
        <translation>%1 är %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="167"/>
        <source>Click to send a private message</source>
        <translation>Klicka för att skicka ett privat meddelande</translation>
    </message>
    <message>
        <source>Saved in</source>
        <translation type="obsolete">Sparat i</translation>
    </message>
    <message>
        <source>Started in</source>
        <translation type="obsolete">Startad i</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupItem.cpp" line="67"/>
        <source>Click to send message to group: %1</source>
        <translation>Klicka för att skicka meddelande till gruppen: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="129"/>
        <source>Click to view chat history: %1</source>
        <translation>Klicka för att visa chatthistorik: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="100"/>
        <source>Empty</source>
        <translation>Tom</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="102"/>
        <source>Send file</source>
        <translation>Skicka fil</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="104"/>
        <source>Show file transfers</source>
        <translation>Visa filöverföringar</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="106"/>
        <source>Set focus in message box</source>
        <translation>Sätt fokus i meddelandefältet</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="108"/>
        <source>Minimize all chats</source>
        <translation>Minimera alla chattar</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="110"/>
        <source>Show the next unread message</source>
        <translation>Visa nästa olästa meddelande</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="112"/>
        <source>Send chat message</source>
        <translation>Skicka chattmeddelande</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="114"/>
        <source>Print</source>
        <translation>Skriv ut</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="116"/>
        <source>Broadcast</source>
        <translation>Sänd</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="118"/>
        <source>Find text in chat</source>
        <translation>Sök text i chatt</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="120"/>
        <source>Find next text in chat</source>
        <translation>Sök nästa text i chatt</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="122"/>
        <source>Send folder</source>
        <translation>Skicka mapp</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="124"/>
        <source>Show emoticons panel</source>
        <translation>Visa smilie-panel</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="126"/>
        <source>Show all chats</source>
        <translation>Visa alla chattar</translation>
    </message>
</context>
<context>
    <name>RainbowTextMarker</name>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="37"/>
        <source>Rainbow Text Marker</source>
        <translation>Regnbågstext</translation>
    </message>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="52"/>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~ .</source>
        <translation>Skriv ~regnbågstext~, om du vill ha en &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;e&lt;/font&gt;&lt;font color=#FFff00&gt;g&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;å&lt;/font&gt;&lt;font color=#00ffff&gt;g&lt;/font&gt;&lt;font color=#0080ff&gt;s&lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt;.</translation>
    </message>
</context>
<context>
    <name>RegularBoldTextMarker</name>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="32"/>
        <source>Regular Bold Text Marker</source>
        <translation>Fet text</translation>
    </message>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="47"/>
        <source>If you want to format your message with words in regular and bold font write a [text to format] .</source>
        <translation>Skriv [fet] [text] om du vill formatera ditt meddelande med fet text.</translation>
    </message>
</context>
<context>
    <name>Tips</name>
    <message>
        <location filename="../src/Tips.h" line="31"/>
        <source>You can switch between chats with CTRL+TAB if there are new messages availables.</source>
        <translation>Du kan växla mellan chattar med CTRL+TAB, om det finns nya meddelanden tillgängliga.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="32"/>
        <source>If you want a &lt;b&gt;bold text&lt;/b&gt; write a *bold text*.</source>
        <translation>Vill du ha &lt;b&gt;fet text&lt;/b&gt;, skriver du *fet text*.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="33"/>
        <source>If you want an &lt;i&gt;italic text&lt;/i&gt; write a /italic text/.</source>
        <translation>Vill du ha &lt;i&gt;kursiv text&lt;/i&gt;, skriver du /kursiv text/.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="34"/>
        <source>If you want an &lt;u&gt;underlined text&lt;/u&gt; write a _underlined text_.</source>
        <translation>Vill du ha &lt;u&gt;understruken text&lt;/u&gt;, skriver du _understruken text_.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="35"/>
        <source>You can search previous sent message in the history using the CTRL+Up e CTRL+Down keys.</source>
        <translation>Du kan bläddra i tidigare skickade meddelanden med snabbtangenterna CTRL+PIL UPP och CTRL+PIL NER.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="37"/>
        <source>You can drop files to active chat and send them to members.</source>
        <translation>Du kan släppa filer i aktiv chatt för att skicka dem till medlemmarna.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="38"/>
        <source>You can select multiple files from network shares and download them simultaneously with a right click.</source>
        <translation>Du kan välja flera filer från nätverksdelningar och ladda ner dem samtidigt med ett högerklick.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="39"/>
        <source>You can disable message notification from a group right clicking on its name on the list.</source>
        <translation>Du kan inaktivera meddelandeavisering från en grupp, genom högerklick på gruppnamnet i listan.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="47"/>
        <source>&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</source>
        <translation>&lt;i&gt;Ett fritt sinne vägleds av fantasin.&lt;/i&gt; (Marco Mastroddi)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="48"/>
        <source>&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</source>
        <translation>&lt;i&gt;Var hungrig, var dum.&lt;/i&gt; (Steve Jobs)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="49"/>
        <source>&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</source>
        <translation>&lt;i&gt;Det finns alltid ett fel till.&lt;/i&gt; (Lubarsky&apos;s Law)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="50"/>
        <source>&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</source>
        <translation>&lt;i&gt;Om något kan gå fel, kommer det att göra så.&lt;/i&gt; (Murphy&apos;s lag)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="51"/>
        <source>&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</source>
        <translation>&lt;i&gt;Om ett program är användbart, måste det ändras.&lt;/i&gt; (Law of Computer Programming)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="53"/>
        <source>&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</source>
        <translation>&lt;i&gt;Intellektuella löser problem, genier förebygger dem.&lt;/i&gt; (Albert Einstein)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="54"/>
        <source>&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</source>
        <translation>&lt;i&gt;Vad som inte förgör mig, gör mig stark.&lt;/i&gt; (Friedrich Nietzsche)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="55"/>
        <source>&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</source>
        <translation>&lt;i&gt;Jag är inte ung nog för att veta allt.&lt;/i&gt; (Oscar Wilde)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="56"/>
        <source>&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</source>
        <translation>&lt;i&gt;Brist på tvivel leder till brist på kreativitet.&lt;/i&gt; (Evert Jan Ouweneel)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="57"/>
        <source>&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</source>
        <translation>&lt;i&gt;Fruktan är vägen till den mörka sidan.&lt;/i&gt; (Joda)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="59"/>
        <source>&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</source>
        <translation>&lt;i&gt;Jag drömmer min målning och sedan målar jag min dröm.&lt;/i&gt; (Vincent Van Gogh)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="60"/>
        <source>&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</source>
        <translation>&lt;i&gt;Allt du kan föreställa dig är verkligt.&lt;/i&gt; (Pablo Picasso)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="61"/>
        <source>&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</source>
        <translation>&lt;i&gt;All sanning är lätt att förstå, när den väl har upptäckts. Svårigheten är att upptäcka den.&lt;/i&gt; (Galileo Galilei)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="62"/>
        <source>&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</source>
        <translation>&lt;i&gt;Sanningen segrar där åsikter är fria.&lt;/i&gt; (Thomas Paine)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="63"/>
        <source>&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</source>
        <translation>&lt;i&gt;Jag har sett saker ni inte skulle tro...&lt;/i&gt; (Batty)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="65"/>
        <source>&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation>&lt;i&gt;En mans karaktär är hans öde.&lt;/i&gt; (Eraclitus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="66"/>
        <source>&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</source>
        <translation>&lt;i&gt;Ett annat språk är en annan syn på livet.&lt;/i&gt; (Federico Fellini)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="67"/>
        <source>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</source>
        <translation>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="68"/>
        <source>&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</source>
        <translation>&lt;i&gt;Varje dag vaknar en gasell i Afrika. Den vet att den måste springa snabbare än det snabbaste lejonet, eller så kommer den att dödas. Varje morgon vaknar ett lejon. Det vet att det måste springa fortare än den långsammaste gasellen, eller också kommer det att svälta ihjäl. Det spelar ingen roll om du är ett lejon eller en gasell. När solen går upp är det bäst du springer.&lt;/i&gt; (Abe Gubegna)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="73"/>
        <source>&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</source>
        <translation>&lt;i&gt;Okay, Houston, vi har haft ett problem här.&lt;/i&gt; (John L. Swigert)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="75"/>
        <source>&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</source>
        <translation>&lt;i&gt;Andra stjärnan till höger och rakt fram till morgonen.&lt;/i&gt; (Peter Pan)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="76"/>
        <source>&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</source>
        <translation>&lt;i&gt;Nöden är det senaste och starkaste vapnet.&lt;/i&gt; (Titus Livius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="77"/>
        <source>&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</source>
        <translation>&lt;i&gt;Gamla människor är inte visa, de är bara försiktiga.&lt;/i&gt; (Ernest Hemingway)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="78"/>
        <source>&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</source>
        <translation>&lt;i&gt;En resa på tusen mil börjar med ett steg.&lt;/i&gt; (Confucius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="79"/>
        <source>&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</source>
        <translation>&lt;i&gt;Ett liv utan dödsförakt är slaveri.&lt;/i&gt; (Lucius Annaeus Seneca)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="81"/>
        <source>&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</source>
        <translation>&lt;i&gt;Jag kan beräkna himlakroppars rörelse, men inte människors galenskap.&lt;/i&gt; (Isaac Newton)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="82"/>
        <source>&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</source>
        <translation>&lt;i&gt;Undran är vishetens begynnelse.&lt;/i&gt; (Socrates)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="83"/>
        <source>&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</source>
        <translation>&lt;i&gt;Ingen vis man har någonsin önskat att han var yngre&lt;/i&gt; (Jonathan Swift)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="84"/>
        <source>&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</source>
        <translation>&lt;i&gt;Den enda man som aldrig begår några misstag, är han som aldrig gör någonting.&lt;/i&gt; (Theodore Roosevelt)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="85"/>
        <source>&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</source>
        <translation>&lt;i&gt;Attityd är en liten sak som gör stor skillnad.&lt;/i&gt; (Winston Churchill)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="87"/>
        <source>&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</source>
        <translation>&lt;i&gt;Vi blir vad vi tänker.&lt;/i&gt; (Buddha)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="88"/>
        <source>&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</source>
        <translation>&lt;i&gt;Svårigheter är saker som visar en person vad de är.&lt;/i&gt; (Epictetus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="89"/>
        <source>&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</source>
        <translation>&lt;i&gt;Vem skall vakta väktarna?&lt;/i&gt; (Decimus Junius Juvenal)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="90"/>
        <source>&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</source>
        <translation>&lt;i&gt;Ett hem utan böcker är som en kropp utan själ.&lt;/i&gt; (Marcus Tullius Cicero)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="91"/>
        <source>&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</source>
        <translation>&lt;i&gt;Vi kan inte sluta önska oss.&lt;/i&gt; (Arthur Schopenhauer)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="93"/>
        <source>&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</source>
        <translation>&lt;i&gt;Tålamod är också en sorts åtgärd.&lt;/i&gt; (Auguste Rodin)</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="62"/>
        <source>offline</source>
        <translation>nerkopplad</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="63"/>
        <source>available</source>
        <translation>tillgänglig</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="64"/>
        <source>busy</source>
        <translation>upptagen</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="65"/>
        <source>away</source>
        <translation>borta</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="66"/>
        <source>status error</source>
        <translation>statusfel</translation>
    </message>
</context>
</TS>
