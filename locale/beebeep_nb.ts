<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="nb_NO">
<context>
    <name>ChatMessage</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="292"/>
        <source>Header</source>
        <translation>Overskrift</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="293"/>
        <source>System</source>
        <translation>System</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="294"/>
        <source>Chat</source>
        <translation>Samtale</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="295"/>
        <source>Connection</source>
        <translation>Forbindelse</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="296"/>
        <source>User Status</source>
        <translation>Bruker Status</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="297"/>
        <source>User Information</source>
        <translation>Brukerinformasjon</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="298"/>
        <source>File Transfer</source>
        <translation>Filoverføring</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="299"/>
        <source>History</source>
        <translation>Historikk</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="300"/>
        <source>Other</source>
        <translation>Annet</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../src/core/Core.cpp" line="127"/>
        <source>%1 Unable to connect to %2 Network. Please check your firewall settings.</source>
        <translation>%1 kan ikke koble til %2 nettverk. Kontroller dine brannmurinstillinger.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="152"/>
        <source>%1 Unable to broadcast to %2 Network. Please check your firewall settings.</source>
        <translation>%1 kan ikke kringkaste til %2 nettverk. Kontroller dine brannmurinstillinger.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="165"/>
        <source>%1 You are connected to %2 Network.</source>
        <translation>%1 Du er kobla til %2 nettverk.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="277"/>
        <source>%1 You are disconnected from %2 Network.</source>
        <translation>%1 Du er kobla fra %2 nettverk.</translation>
    </message>
    <message>
        <source>%1 %2 will search users in these IP addresses: %3</source>
        <translation type="obsolete">%1 %2 vil lete etter brukere på disse IP-adressene: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="230"/>
        <source>%1 Zero Configuration started with service name: %2</source>
        <translation>%1 Null-konfigurasjon starta med tjenestenavn: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="81"/>
        <source>%1 User %2 cannot save settings in path: %3</source>
        <translation>%1 Bruker %2 kan ikke lagre innstillinger i stien: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="97"/>
        <source>%1 User %2 cannot save chat messages in path: %3</source>
        <translation>%1 Bruker %2 kan ikke lagre samtalemeldinger i stien: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="176"/>
        <source>%1 You have selected to join only in these workgroups: %2</source>
        <translation>%1 Du har valgt å delta bare i disse arbeidsgruppene: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="245"/>
        <source>%1 Zero Configuration service closed.</source>
        <translation>%1 Nullkonfigurasjonstjeneste lukka.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="300"/>
        <source>%1 Zero Configuration is browsing network for service: %2</source>
        <translation>%1 Nullkonfigurasjon søker i nettverk etter tjeneste: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="307"/>
        <source>%1 Zero Configuration cannot browse network for service: %2</source>
        <translation>%1 Nullkonfigurasjon kan ikke søke i nettverket etter tjeneste: %2</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="325"/>
        <source>%1 Broadcasting to the %2 Network...</source>
        <translation>%1 Kringkaster til %2-nettverket...</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="331"/>
        <source>%1 You are not connected to %2 Network.</source>
        <translation>%1 Du er ikke tilkobla %2-nettverket.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="337"/>
        <source>%1 %2 has found a filter on UDP port %3. Please check your firewall settings.</source>
        <translation>%1 %2 fant et filter på UDP-port %3. Kontroller dine brannmursinnstillinger.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="341"/>
        <source>View the log messages for more informations</source>
        <translation>Vis loggmeldingene for mer informasjon</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="511"/>
        <source>New version is available</source>
        <translation>Ny versjon er tilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="512"/>
        <source>Click here to download</source>
        <translation>Klikk her for å laste ned</translation>
    </message>
    <message>
        <source>%1 You cannot reach %2 Network.</source>
        <translation type="obsolete">%1 Du kan ikke nå %2-nettverket.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="389"/>
        <source>is connected from external network (the new subnet is added to your broadcast address list).</source>
        <translation>er tilkobla fra eksternt nettverk (det nye subnettet er lagt til din kringkastingsadresseliste).</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="401"/>
        <source>%1 Checking %2 more addresses...</source>
        <translation>%1 Sjekker %2 flere adresser...</translation>
    </message>
    <message>
        <source>%1 Contacting %2 ...</source>
        <translation type="obsolete">%1 Kontakter %2 ...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="46"/>
        <source>%1 Chat with all local users.</source>
        <translation>%1 Samtal med alle lokale brukere.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="83"/>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to you!</source>
        <translation>Gratulerer med fødselsdagen!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to %1!</source>
        <translation>Fødseldagsgratulasjoner til %1!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="64"/>
        <source>Happy birthday to Marco Mastroddi: %1 years old today! Cheers!!!</source>
        <translation>Gratulasjoner til Marco Mastroddi: %1 år i dag! Tillykke!!!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="76"/>
        <source>Happy New Year!</source>
        <translation>Godt nyttår!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="99"/>
        <location filename="../src/core/CoreChat.cpp" line="164"/>
        <location filename="../src/core/CoreChat.cpp" line="244"/>
        <source>%1 Chat with %2.</source>
        <translation>%1 Samtale med %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="147"/>
        <source>%1 You have created the group %2.</source>
        <translation>%1 Du har oppretta gruppa %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="153"/>
        <source>%1 Welcome to the group %2.</source>
        <translation>%1 Velkommen til gruppa %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="198"/>
        <source>%1 The group has a new name: %2.</source>
        <translation>%1 Gruppa har nytt navn: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="230"/>
        <source>%1 Members removed from the group: %2.</source>
        <translation>%1 Medlemmer fjernet fra gruppa: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="237"/>
        <source>%1 Members added to the group: %2.</source>
        <translation>%1 Medlemmer lagt til gruppa: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="262"/>
        <source>Unable to send the message: you are not connected.</source>
        <translation>Kan ikke sende melding: du er ikke tilkobla.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="268"/>
        <source>Unable to send the message: this chat is disabled.</source>
        <translation>Kan ikke sende melding: denne samtalen er avvikla.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="302"/>
        <source>Unable to send the message to %1.</source>
        <translation>Kan ikke sende melding til %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="330"/>
        <source>The message will be delivered to %1.</source>
        <translation>Meldinga blir levert til %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="335"/>
        <source>Nobody has received the message.</source>
        <translation>Ingen har mottatt meldinga.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="394"/>
        <source>%1 %2 can not join to the group.</source>
        <translation>%1 %2 kan ikke knyttes til gruppa.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="457"/>
        <source>%1 saved chats are added to history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="573"/>
        <source>Offline messages sent to %2.</source>
        <translation>Frakobla meldinger sendt til %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="410"/>
        <source>%1 %2 cannot be informed that you have left the group.</source>
        <translation>%1 %2 kan ikke informeres om at du har forlatt gruppa.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="53"/>
        <source>Help me to know how many people are really using BeeBEEP.</source>
        <translation>Hjelp meg å kunne vite hvor mange som aktivt bruker BeeBEEP.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="55"/>
        <source>Please add a like on Facebook.</source>
        <translation>Vennligst legg til en &quot;liker&quot; på Facebook.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="502"/>
        <source>%1 You have left the group.</source>
        <translation>%1 Du har forlatt gruppa.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="504"/>
        <source>%1 %2 has left the group.</source>
        <translation>%1 %2 har forlatt gruppa.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="218"/>
        <source>%1 (%2) is disconnected from %3 network.</source>
        <translation>%1 (%2) er kobla fra %3-nettverket.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="338"/>
        <source>%1 (%2) is connected to %3 network.</source>
        <translation>%1 (%2) er kobla til %3-nettverket.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="435"/>
        <source>%1 Network interface %2 is gone down.</source>
        <translation>%1 nettverksgrensesnittet %2 er nede.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="48"/>
        <source>%1 Unable to start file transfer server: bind address/port failed.</source>
        <translation>%1 Kan ikke starte filoverføringstjener: bind adresse/port mislyktes.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="94"/>
        <source>%1 Unable to download %2 from %3: user is offline.</source>
        <translation>%1 Kan ikke laste ned %2 fra %3: bruker er frakobla.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="109"/>
        <source>%1 Unable to download %2 from %3: folder %4 cannot be created.</source>
        <translation>%1 Kan ikke laste ned %2 fra %3: mappe %4 kan ikke opprettes.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="121"/>
        <source>%1 Downloading %2 from %3.</source>
        <translation>%1 Laster ned %2 fra %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>from</source>
        <translation>fra</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>to</source>
        <translation>til</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="184"/>
        <source>Open</source>
        <translation>Åpne</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="238"/>
        <source>%1 Unable to send %2 to %3: user is offline.</source>
        <translation>%1 Kan ikke sende %2 til %3: bruker er frakobla.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="215"/>
        <source>%1 Unable to send %2. File transfer is disabled.</source>
        <translation>%1 Kan ikke sende %2. Filoverføring er deaktivert.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Upload</source>
        <translation>Last opp</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Download</source>
        <translation>Last ned</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="187"/>
        <source>folder</source>
        <translation>mappe</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="247"/>
        <source>%1 %2: file not found.</source>
        <translation>%1 %2: fil.ikke funnet.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="257"/>
        <source>%1 %2 is a folder. You can share it.</source>
        <translation>%1 %2 er en mappe. Du kan dele den.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="278"/>
        <source>%1 Unable to send %2: %3 is not connected.</source>
        <translation>%1 Kan ikke sende %2: %3 er ikke tilkobla.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="272"/>
        <source>%1 You send %2 to %3.</source>
        <translation>%1 Du sendte %2 til %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="303"/>
        <source>%1 You have refused to download %2 from %3.</source>
        <translation>%1 Du har avslått å laste ned %2 fra %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="331"/>
        <source>%1 You have refused to download folder %2 from %3.</source>
        <translation>%1 Du har avslått å laste ned mappe %2 fra %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="394"/>
        <source>Adding to file sharing</source>
        <translation>Legger til fildeling</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="449"/>
        <source>%1 is added to file sharing (%2)</source>
        <translation>%1 er lagt til fildeling (%2)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="451"/>
        <source>%1 is added to file sharing with %2 files, %3 (elapsed time: %4)</source>
        <translation>%1 er lagt til fildeling med %2 filer, %3 (medgått tid: %4)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="487"/>
        <source>All paths are removed from file sharing</source>
        <translation>Alle filstier er fjerna fra fildeling</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="509"/>
        <source>%1 is removed from file sharing</source>
        <translation>%1 er fjerna fra fildeling</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="511"/>
        <source>%1 is removed from file sharing with %2 files</source>
        <translation>%1 er fjerna fra fildeling med %2 filer</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="559"/>
        <source>%1 You are about to send %2 to %3. Checking folder...</source>
        <translation>%1 Du er i ferd med å sende %2 til %3. Sjekker mappe...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="594"/>
        <source>%1 Unable to send folder %2</source>
        <translation>%1 Kan ikke sende mappe %2</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="599"/>
        <source>the folder is empty.</source>
        <translation>mappa er tom.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="606"/>
        <source>file transfer is not working.</source>
        <translation>filoverføring virker ikke.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="614"/>
        <source>%1 is not connected.</source>
        <translation>%1 er ikke tilkobla.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="628"/>
        <source>internal error.</source>
        <translation>intern feil.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="633"/>
        <source>%1 You send folder %2 to %3.</source>
        <translation>%1 Du sendte mappe %2 til %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="148"/>
        <source>%1 %2 has refused to download %3.</source>
        <translation>%1 %2 har avslått å laste ned %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="175"/>
        <source>%1 %2 is sending to you the file: %3.</source>
        <translation>%1 %2 sender til deg fila: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="245"/>
        <source>%1 An error occurred when %2 tries to add you to the group chat: %3.</source>
        <translation>%1 En feil oppstod når %2 prøver å legge deg til gruppesamtalen: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="251"/>
        <source>%1 %2 adds you to the group chat: %3.</source>
        <translation>%1 %2 legger deg til gruppesamtalen: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="285"/>
        <source>%1 %2 has not shared files.</source>
        <translation>%1 %2 har ikke delt filer.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="290"/>
        <source>%1 %2 has shared %3 files.</source>
        <translation>%1 %2 har delt %3 filer.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="319"/>
        <source>%1 %2 has refused to download folder %3.</source>
        <translation>%1 %2 har avslått å laste ned mappe %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="326"/>
        <source>unknown folder</source>
        <translation>ukjent mappe</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="334"/>
        <source>%1 %2 is sending to you the folder: %3.</source>
        <translation>%1 %2 sender til deg mappa: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="86"/>
        <source>You are</source>
        <translation>Du er</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="88"/>
        <source>%1 is</source>
        <translation>%1 er</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="101"/>
        <source>You have changed your nickname from %1 to %2.</source>
        <translation>Du har endra kallenavn fra %1 til %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="103"/>
        <source>%1 has changed the nickname in %2.</source>
        <translation>%1 har endra kallenavn i %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="117"/>
        <source>The %1&apos;s profile has been received.</source>
        <translation>%1&apos;s profil er mottatt.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>You share this information</source>
        <translation>Du deler denne informasjonen</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>%1 shares this information</source>
        <translation>%1 deler denne informasjonen</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="235"/>
        <source>%1 You have created group from chat: %2.</source>
        <translation>%1 Du har oppretta gruppe fra samtale: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="290"/>
        <source>%1 You have deleted group: %2.</source>
        <translation>%1 Du har sletta gruppe: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="377"/>
        <source>is removed from favorites</source>
        <translation>er fjerna fra favoritter</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="383"/>
        <source>is added to favorites</source>
        <translation>er lagt til favoritter</translation>
    </message>
</context>
<context>
    <name>File</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="274"/>
        <source>Audio</source>
        <translation>Lyd</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="275"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="276"/>
        <source>Image</source>
        <translation>Bilde</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="277"/>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="278"/>
        <source>Other</source>
        <translation>Annet</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="279"/>
        <source>Executable</source>
        <translation>Program</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="280"/>
        <source>MacOSX</source>
        <translation>MacOSX</translation>
    </message>
</context>
<context>
    <name>FileTransferPeer</name>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="77"/>
        <source>invalid file header</source>
        <translation>ugyldig fil-header</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="105"/>
        <source>Unable to open file</source>
        <translation>Kan ikke åpne fil</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="112"/>
        <source>Unable to write in the file</source>
        <translation>Kan ikke skrive i fila</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="119"/>
        <source>%1 bytes downloaded but the file size is only %2 bytes</source>
        <translation>%1 bytes lasta ned, men filstørrelsen er bare %2 bytes</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="52"/>
        <source>Transfer cancelled</source>
        <translation>Overføring avbrutt</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="113"/>
        <source>Transfer completed in %1</source>
        <translation>Overføring fullført i %1</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="171"/>
        <source>Connection timeout</source>
        <translation>Forbindelse tidsutkobla</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="179"/>
        <source>Transfer timeout</source>
        <translation>Overføring tidsutkopling</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="101"/>
        <source>unable to send file header</source>
        <translation>kan ikke sende fil-header</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="116"/>
        <source>%1 bytes uploaded but the file size is only %2 bytes</source>
        <translation>%1 bytes lasta opp, men filstørrelsen er bare %2 bytes</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="123"/>
        <source>%1 bytes sent not confirmed (%2 bytes confirmed)</source>
        <translation>%1 bytes sendt ikke bekrefta (%2 bytes bekrefta)</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="154"/>
        <source>Unable to upload data</source>
        <translation>Kan ikke laste opp data</translation>
    </message>
</context>
<context>
    <name>GuiAddUser</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="36"/>
        <source>Add user</source>
        <translation>Legg til bruker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="39"/>
        <source>your IP is %1 in LAN %2</source>
        <translation>din IP er %1 i LAN %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="104"/>
        <source>Please insert a valid IP address.</source>
        <translation>Angi en gyldig IP-adresse.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="114"/>
        <source>Please insert a valid port or use the default one %1.</source>
        <translation>Angi en gyldig port eller bruk standard %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="148"/>
        <source>These IP address and port are already inserted in list.</source>
        <translation>Disse IP-adressene og portene er allerede lagt til liste.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="182"/>
        <source>Remove user path</source>
        <translation>Fjern brukersti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="184"/>
        <source>Clear all</source>
        <translation>Blank ut alt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="209"/>
        <source>Please select an user path in the list.</source>
        <translation>Velg en brukersti i lista.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="234"/>
        <source>auto added</source>
        <translation>lagt til automatisk</translation>
    </message>
</context>
<context>
    <name>GuiAddUserDialog</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="73"/>
        <source>Add an IP address and port of the user you want to connect</source>
        <translation>Legg til IP-adresse og port til brukeren du ønsker å tilknytte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="89"/>
        <source>IP Address</source>
        <translation>IP-adresse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="121"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="169"/>
        <source>Split in IPv4 addresses</source>
        <translation>Del opp i IPv4-adresser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="179"/>
        <source>Add</source>
        <translation>Legg til</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="176"/>
        <source>Click here to add user path</source>
        <translation>Klikk her for å legge til brukersti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="156"/>
        <source>Comment</source>
        <translation>Kommentar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="25"/>
        <source>Auto add from LAN</source>
        <translation>Legg til automatisk fra LAN</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="45"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="52"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiAskPassword</name>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="23"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="30"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="62"/>
        <source>Use standard session (encrypted but authentication is not required)</source>
        <translation>Bruk standardsesjon (kryptert, men autentisering er ikke påkrevd)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="69"/>
        <source>Use authentication password (spaces are removed) *</source>
        <translation>Bruk tilgangspassord (mellomrom fjernes) *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="124"/>
        <source>* Password must be the same for all user you want to connect</source>
        <translation>* Passord må være det samme for alle brukere du vil tilknytte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="153"/>
        <source>Remember password (not recommended)</source>
        <translation>Husk passord (anbefales ikke)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="160"/>
        <source>Show this dialog at connection startup</source>
        <translation>Vis denne dialogen ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="33"/>
        <source>Chat Password - %1</source>
        <translation>Samtalepassord - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="103"/>
        <source>Password is empty. Please enter a valid one (spaces are removed).</source>
        <translation>Passord er blankt. Angi et gyldig passord (mellomrom fjernes).</translation>
    </message>
</context>
<context>
    <name>GuiChat</name>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="139"/>
        <source>Change font style</source>
        <translation>Endre skriftstil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="140"/>
        <source>Select your favourite chat font style</source>
        <translation>Velg din foretrukne skriftsstil for samtaler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="141"/>
        <source>Change font color</source>
        <translation>Endre skriftfarge</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="142"/>
        <source>Select your favourite font color for the chat messages</source>
        <translation>Velg din foretrukne skriftsfarge for samtalemeldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="143"/>
        <source>Change background color</source>
        <translation>Endre bakgrunnsfarge</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="144"/>
        <source>Select your favourite background color for the chat window</source>
        <translation>Velg din foretrukne bakgrunnsfarge for samtalevinduet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="145"/>
        <source>Filter message</source>
        <translation>Filtrer melding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="146"/>
        <source>Select the message types which will be showed in chat</source>
        <translation>Velg meldingstypene som skal vises i samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="147"/>
        <source>Chat settings</source>
        <translation>Samtaleinnstillinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="148"/>
        <source>Click to show the settings menu of the chat</source>
        <translation>Klikk for å vise innstillingsmenyen for samtalen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="149"/>
        <source>Spell checking</source>
        <translation>Stavekontroll</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="151"/>
        <source>Word completer</source>
        <translation>Ord fullfører</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="153"/>
        <source>Use Return key to send message</source>
        <translation>Bruke returtast til å sende melding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="158"/>
        <source>Members</source>
        <translation>Medlemmer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="159"/>
        <source>Show the members of the chat</source>
        <translation>Vis deltakerne i samtalen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="165"/>
        <location filename="../src/desktop/GuiChat.cpp" line="1076"/>
        <source>Find text in chat</source>
        <translation>Finn tekst i samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="166"/>
        <source>Send file</source>
        <translation>Send fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="167"/>
        <source>Send a file to a user or a group</source>
        <translation>Send ei fil til en bruker eller ei gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="168"/>
        <source>Send folder</source>
        <translation>Send mappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="169"/>
        <source>Save chat</source>
        <translation>Lagre samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="170"/>
        <source>Save the messages of the current chat to a file</source>
        <translation>Lagre meldingene for denne samtalen til ei fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="171"/>
        <source>Print...</source>
        <translation>Skriv ut...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="180"/>
        <source>Change the name of the group or add and remove users</source>
        <translation>Endre gruppenavn eller leggtil og fjern brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="229"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="415"/>
        <source>unread messages</source>
        <translation>uleste meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="964"/>
        <source>Spell checking is enabled</source>
        <translation>Stavekontroll er slått på</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="971"/>
        <location filename="../src/desktop/GuiChat.cpp" line="995"/>
        <source>There is not a valid dictionary</source>
        <translation>Det er ingen gyldig ordbok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="988"/>
        <source>Word completer is enabled</source>
        <translation>Ord-fullføring er slått på</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="990"/>
        <source>Word completer is disabled</source>
        <translation>Ord-fullføring er slått av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="1114"/>
        <source>%1 not found in chat.</source>
        <translation>%1 ikke funnet i samtale.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="966"/>
        <source>Spell checking is disabled</source>
        <translation>Stavekontroll er slått av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="173"/>
        <source>Clear messages</source>
        <translation>Slett meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="174"/>
        <source>Clear all the messages of the chat</source>
        <translation>Slett alle meldingene i samtalen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="177"/>
        <source>Create group from chat</source>
        <translation>Opprett ei gruppe fra samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="178"/>
        <source>Create a group from this chat</source>
        <translation>Opprett ei gruppe fra denne samtalen</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation type="obsolete">Opprett gruppe</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation type="obsolete">Opprett ei gruppe med to eller flere brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="181"/>
        <location filename="../src/desktop/GuiChat.cpp" line="182"/>
        <source>Leave the group</source>
        <translation>Forlat gruppa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="223"/>
        <source>Copy to clipboard</source>
        <translation>Kopier til utklippstavle</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="225"/>
        <source>Select All</source>
        <translation>Velg alt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="257"/>
        <source>Show only messages in default chat</source>
        <translation>Vis bare meldinger i basis-samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="297"/>
        <source>Last message %1</source>
        <translation>Siste melding %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="395"/>
        <source>All Lan Users</source>
        <translation>Alle LAN-brukere</translation>
    </message>
    <message>
        <source>(You have left)</source>
        <translation type="obsolete">(Du har forlatt)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="411"/>
        <source>You</source>
        <translation>Du</translation>
    </message>
    <message>
        <source>Create chat</source>
        <translation type="obsolete">Opprett samtale</translation>
    </message>
    <message>
        <source>Create a chat with two or more users</source>
        <translation type="obsolete">Opprett samtale med to eller flere brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="179"/>
        <source>Edit group</source>
        <translation>Rediger gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="437"/>
        <source>offline</source>
        <translation>utilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="445"/>
        <source>Show profile</source>
        <translation>Vis profil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="455"/>
        <source>Show members</source>
        <translation>Vis medlemmer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>Nobody</source>
        <translation>Ingen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>and</source>
        <translation>og</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="537"/>
        <source>last %1 messages</source>
        <translation>siste %1 meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="706"/>
        <source>Please select a file to save the messages of the chat.</source>
        <translation>Velg ei fil å lagre meldingene i samtalen til.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>%1: save completed.</source>
        <translation>%1: lagring fullført.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="771"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Kan ikke lagre midlertidig fil: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="819"/>
        <source>Do you really want to send %1 %2 to the members of this chat?</source>
        <translation>Vil du sende %1 %2 til medlemmene av denne samtalen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>file</source>
        <translation>fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>files</source>
        <translation>filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="834"/>
        <source>Qt library for this OS doesn&apos;t support Drag and Drop for files. You have to select again the file to send.</source>
        <translation>Qt-biblioteket for dette OS&apos;et støtter ikke Dra-og-slipp av filer. Du må velge på nytt fila som skal sendes.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="948"/>
        <source>Use key Return to send message</source>
        <translation>Bruk returtast til å sende melding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="950"/>
        <source>Use key Return to make a carriage return</source>
        <translation>Bruk returtast til linjeskift</translation>
    </message>
</context>
<context>
    <name>GuiChatList</name>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="49"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="52"/>
        <source>Clear</source>
        <translation>Blank ut</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="53"/>
        <source>Clear all chat messages</source>
        <translation>Blank ut alle samtalemeldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="55"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
</context>
<context>
    <name>GuiChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="20"/>
        <source>Chat</source>
        <translation>Samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="238"/>
        <source>Write to: &lt;b&gt;ALL&lt;/b&gt;</source>
        <translation>Skriv til: &lt;b&gt;ALLE&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="260"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="288"/>
        <source>Save window&apos;s geometry</source>
        <translation>Lagre vindusgeometri</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="326"/>
        <source>Detach chat</source>
        <translation>Koble fra samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="136"/>
        <source>Click to send message or just hit enter</source>
        <translation>Klikk for å sende melding eller bare trykk Enter</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroup</name>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="26"/>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="42"/>
        <source>TextLabel</source>
        <translation>Tekstetikett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="74"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="81"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="37"/>
        <source>Group name</source>
        <translation>Gruppenavn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="38"/>
        <source>Please add member in the group:</source>
        <translation>Legg til medlemmer i gruppa:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="41"/>
        <source>Users</source>
        <translation>Brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="65"/>
        <source>Create Group - %1</source>
        <translation>Opprett gruppe - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="67"/>
        <source>Create Chat - %1</source>
        <translation>Opprett samtale - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="73"/>
        <source>Edit Group - %1</source>
        <translation>Rediger gruppe - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="75"/>
        <source>Edit Chat - %1</source>
        <translation>Rediger samtale - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="113"/>
        <source>Please select two or more member for the group.</source>
        <translation>Velg to eller flere medlemmer til gruppa.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="121"/>
        <source>Please insert a group name.</source>
        <translation>Skriv inn et gruppenavn.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="130"/>
        <source>%1 already exists as group name or chat name.
Please select a different name.</source>
        <translation>%1 er allerede brukt som gruppe eller samtalenavn.
Velg et annet navn.</translation>
    </message>
</context>
<context>
    <name>GuiEditVCard</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="35"/>
        <source>Edit your profile</source>
        <translation>Rediger profilen din</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="103"/>
        <source>%1 - Select your profile photo</source>
        <translation>%1 - Velg ditt profilbilde</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="104"/>
        <source>Images</source>
        <translation>Bilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <source>Unable to load image %1.</source>
        <translation>Kan ikke laste inn bilde %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="147"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="156"/>
        <source>Please insert your nickname.</source>
        <translation>Skriv inn kallenavnet ditt.</translation>
    </message>
</context>
<context>
    <name>GuiEmoticons</name>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="49"/>
        <source>Recent</source>
        <translation>Nylig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="53"/>
        <source>Smiley</source>
        <translation>Smilefjes</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="57"/>
        <source>Objects</source>
        <translation>Objekter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="61"/>
        <source>Nature</source>
        <translation>Natur</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="65"/>
        <source>Places</source>
        <translation>Steder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="69"/>
        <source>Symbols</source>
        <translation>Symboler</translation>
    </message>
</context>
<context>
    <name>GuiFileInfoList</name>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Shared folders and files</source>
        <translation>Delte mapper og filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
</context>
<context>
    <name>GuiFloatingChat</name>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="43"/>
        <source>Show the bar of chat</source>
        <translation>Vis menylinje for samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="51"/>
        <source>Emoticons</source>
        <translation>Uttrykksikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="63"/>
        <source>Show the emoticon panel</source>
        <translation>Vis panelet for uttrykksikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="64"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Legg til ditt foretrukne uttrykksikon for meldinga</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="244"/>
        <source>Window&apos;s geometry and state saved</source>
        <translation>Vindusgeomtri og status lagret</translation>
    </message>
</context>
<context>
    <name>GuiGroupList</name>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="48"/>
        <source>Create group</source>
        <translation>Opprett gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="51"/>
        <source>Edit group</source>
        <translation>Rediger gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="54"/>
        <source>Open chat</source>
        <translation>Åpne samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="57"/>
        <source>Enable notifications</source>
        <translation>Slå på varsler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="60"/>
        <source>Disable notifications</source>
        <translation>Slå av varsler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="63"/>
        <source>Delete group</source>
        <translation>Slett gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="143"/>
        <source>Waiting for two or more connected user</source>
        <translation>Venter på to eller flere tilkobla brukere</translation>
    </message>
</context>
<context>
    <name>GuiHome</name>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="39"/>
        <source>Home</source>
        <translation>Heim</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="49"/>
        <source>Select a user you want to chat with or</source>
        <translation>Velg en bruker du vil samtale med eller</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="94"/>
        <source>Copy to clipboard</source>
        <translation>Kopier til utklippstavle</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="96"/>
        <source>Select All</source>
        <translation>Velg alt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="98"/>
        <source>Print...</source>
        <translation>Skriv ut...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="105"/>
        <source>Show the datestamp</source>
        <translation>Vis datomerking</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="109"/>
        <source>Show the timestamp</source>
        <translation>Vis tidspunkt</translation>
    </message>
</context>
<context>
    <name>GuiHomeWidget</name>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="107"/>
        <source>chat with all users</source>
        <translation>samtal med alle brukere</translation>
    </message>
</context>
<context>
    <name>GuiLanguage</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="35"/>
        <source>Select language</source>
        <translation>Velg språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="50"/>
        <source>For the latest language translations please visit the %1</source>
        <translation>For de nyeste oversettelsene gå til %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="51"/>
        <source>official website</source>
        <translation>offsielt nettsted</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="94"/>
        <source>Select a language folder</source>
        <translation>Velg ei språkmappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="121"/>
        <source>Language &apos;%1&apos;&apos; not found.</source>
        <translation>Språk &apos;%1&apos; finnes ikke.</translation>
    </message>
</context>
<context>
    <name>GuiLanguageDialog</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="45"/>
        <source>Language</source>
        <translation>Språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="96"/>
        <source>Path</source>
        <translation>Sti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="127"/>
        <source>Select language folder</source>
        <translation>Velg ei språkmappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="169"/>
        <source>Restore to default language</source>
        <translation>Gjenopprett til standardspråk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="172"/>
        <source>Restore</source>
        <translation>Gjenopprett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="195"/>
        <source>Select</source>
        <translation>Velg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="205"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiLog</name>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="36"/>
        <source>System Log</source>
        <translation>Systemlogg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="55"/>
        <source>Save log as</source>
        <translation>Lagre logg som</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="56"/>
        <source>Save the log in a file</source>
        <translation>Lagre loggen i ei fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="72"/>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="78"/>
        <source>keyword</source>
        <translation>nøkkelord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="84"/>
        <source>Find</source>
        <translation>Finn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="85"/>
        <source>Find keywords in the log</source>
        <translation>Finn nøkkelrord i loggen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="97"/>
        <source>Case sensitive</source>
        <translation>Skill mellom store og små bokstaver</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="102"/>
        <source>Whole word</source>
        <translation>Hele ord</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="60"/>
        <source>Log to file</source>
        <translation>Logg til fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="109"/>
        <source>Block scrolling</source>
        <translation>Hindre scrolling</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="116"/>
        <source>Please select a file to save the log.</source>
        <translation>Velg ei fil å lagre logg til.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <source>Unable to save log in the file: %1</source>
        <translation>Kunne ikke lagre logg i fila: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="151"/>
        <source>%1: save log completed.</source>
        <translation>%1: logglagring fullført.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open file</source>
        <translation>Åpne fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open folder</source>
        <translation>Åpne mappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="193"/>
        <source>%1 not found</source>
        <translation>%1 ikke funnet</translation>
    </message>
</context>
<context>
    <name>GuiLogWidget</name>
    <message>
        <location filename="../src/desktop/GuiLog.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
</context>
<context>
    <name>GuiMain</name>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="86"/>
        <source>Show the main tool bar</source>
        <translation>Vis hovedverktøyslinja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="283"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3291"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3292"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3293"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3294"/>
        <source>offline</source>
        <translation>fraværende</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="326"/>
        <source>Do you really want to quit %1?</source>
        <translation>Vil du avslutte %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="349"/>
        <source>&lt;b&gt;Settings can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Innstillinger kan ikke lagres&lt;/b&gt;. Sti:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="351"/>
        <location filename="../src/desktop/GuiMain.cpp" line="368"/>
        <source>&lt;b&gt;is not writable&lt;/b&gt; by user:</source>
        <translation>&lt;b&gt;er ikke skrivbar&lt;/b&gt; for bruker:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="353"/>
        <location filename="../src/desktop/GuiMain.cpp" line="370"/>
        <source>Do you want to close anyway?</source>
        <translation>Vil du lukke uansett?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="414"/>
        <source>No new message available</source>
        <translation>Ingen nye meldinger tilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="498"/>
        <source>Disconnect from %1 network</source>
        <translation>Frakoble fra %1 nettverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="504"/>
        <source>Connect to %1 network</source>
        <translation>Koble til %1 nettverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="587"/>
        <source>Secure Lan Messenger</source>
        <translation>Sikker LAN meldingstransportør</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="588"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="590"/>
        <source>for</source>
        <translation>for</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="592"/>
        <source>developed by</source>
        <translation>utvikla av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="601"/>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br /&gt;it under the terms of the GNU General Public License as published&lt;br /&gt;by the Free Software Foundation, either version 3 of the License&lt;br /&gt;or (at your option) any later version.&lt;br /&gt;&lt;br /&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br /&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br /&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br /&gt;See the GNU General Public License for more details.</source>
        <translation>BeeBEEP er fri programvare: du kan videredistribuere den og/eller endre&lt;br /&gt;den i henhold til betingelsene til GNU General Public License publisert&lt;br /&gt;av the Free Software Foundation, enten lisensversjon 3&lt;br /&gt;eller (etter ditt valg) uansett senere versjon.&lt;br /&gt;&lt;br /&gt;BeeBEEP distributeres i håp om å vise seg nyttig,&lt;br /&gt;men UTEN NOEN GARANTI; ikke engang implisitt&lt;br /&gt;for EGENSKAPER eller EGNETHET TIL NOE SPESIELT FORMÅL.&lt;br /&gt;Se GNU General Public License for flere detaljer.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="617"/>
        <source>Search for users...</source>
        <translation>Søke etter brukere...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="623"/>
        <source>Close the chat and quit %1</source>
        <translation>Lukk samtalen og avlutt %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="631"/>
        <source>Show the main tool bar with settings</source>
        <translation>Vis hovedverktøyslinja med innstillinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="635"/>
        <source>Show the informations about %1</source>
        <translation>Vis informasjonen om %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="667"/>
        <source>Select language...</source>
        <translation>Velg språk...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="668"/>
        <source>Select your preferred language</source>
        <translation>Velg foretrukket språk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="669"/>
        <source>Download folder...</source>
        <translation>Last ned mappe...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="670"/>
        <source>Select the download folder</source>
        <translation>Velg nedlastingsmappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="677"/>
        <source>Select beep file...</source>
        <translation>Velg varsellyd-fil...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="678"/>
        <source>Select the file to play on new message arrived</source>
        <translation>Velg fil å avspille når ny melding mottas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="679"/>
        <source>Play beep</source>
        <translation>Spill varsellyd</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="680"/>
        <source>Test the file to play on new message arrived</source>
        <translation>Test fil å avspille når ny melding mottas</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="783"/>
        <source>Settings</source>
        <translation>Innstillinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="809"/>
        <source>If enabled the window geometry will be reset to default value at the next startup</source>
        <translation>Merk av for å tilbakestille vindusgeometri til standardverdi ved neste oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="836"/>
        <source>If a file already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="874"/>
        <source>Always open a new floating chat window</source>
        <translation>Alltid åpne et nytt separat samtalevindu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="875"/>
        <source>If enabled when you always open chat in a new floating window</source>
        <translation>Merk av når du alltid åpner samtale i et nytt separat vindu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="933"/>
        <source>Show only the online users</source>
        <translation>Vis kun tilkobla brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="934"/>
        <source>If enabled only the online users are shown in the list</source>
        <translation>Merk av for å kun vise tilkobla brukere i lista</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="940"/>
        <source>If enabled you can see a picture of the users in the list (if they have)</source>
        <translation>Merk av for å vise et bilde av brukerne i lista (hvis de har et)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="866"/>
        <source>Set status to away automatically</source>
        <translation>Sett status til borte automatisk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="867"/>
        <source>If enabled %1 change your status to away after an idle of %2 minutes</source>
        <translation>Merk av for at %1 skal endre statusen din til borte etter inaktivitet på %2 minutter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="881"/>
        <source>If enabled when a new message is arrived a sound is emitted</source>
        <translation>Merk av for å spille en lyd når en ny melding ankommer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="902"/>
        <source>If enabled when a new message is arrived %1 is shown on top of all other windows</source>
        <translation>Merk av for at %1 skal vises foran alle andre vinduer når en ny melding ankommer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="844"/>
        <source>Generate automatic filename</source>
        <translation>Generer filnavn automatisk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="845"/>
        <source>If the file to be downloaded already exists a new filename is automatically generated</source>
        <translation>Hvis fila som skal lastes ned allerede finnes, blir et nytt filnavn generert automatisk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1094"/>
        <source>Enable tray icon notification</source>
        <translation>Slå på ikonvarsel på oppgavelinje</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1095"/>
        <source>If enabled tray icon shows some notification about status and message</source>
        <translation>Merk av for at oppgavelinjeikon skal vise varslinger om status og meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="908"/>
        <source>If enabled %1 stays on top of the other windows</source>
        <translation>Merk av for at %1 skal legges foran andre vinduer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="829"/>
        <source>Enable file transfer</source>
        <translation>Slå på filoverføring</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="830"/>
        <source>If enabled you can transfer files with the other users</source>
        <translation>Merk av for å kunne overføre filer mot de andre brukerne</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="790"/>
        <source>Prompts for network password on startup</source>
        <translation>Ber om nettverkspassord ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="791"/>
        <source>If enabled the password dialog will be shown on connection startup</source>
        <translation>Merk av for å  vise passord-dialogen ved oppstart av forbindelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="820"/>
        <source>Load %1 on Windows startup</source>
        <translation>Last %1 ved oppstart av Windows</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="821"/>
        <source>If enabled you can automatically load %1 at system startup</source>
        <translation>Merk av for å starte %1 automatisk ved systemoppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="709"/>
        <source>Enable the compact mode in chat window</source>
        <translation>Slå på kompaktmodus i samtalevindu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="710"/>
        <source>If enabled the sender&apos;s nickname and his message are in the same line</source>
        <translation>Merk av for å vise avsenders kallenavn på samme linje som meldinga</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="715"/>
        <source>Add a blank line between the messages</source>
        <translation>Legg en blank linje mellom meldingene</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="716"/>
        <source>If enabled the messages in the chat window are separated by a blank line</source>
        <translation>Merk av for å adskille meldingene i samtalevinduene med ei blank linje</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="722"/>
        <source>If enabled the message shows its timestamp in the chat window</source>
        <translation>Merk av for å vise meldingstidspunkt i samtalevindu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="704"/>
        <source>If enabled the user&apos;s nickname in chat and in list is colored</source>
        <translation>Merk av for å fargelegge brukers kallenavn i samtale og liste</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="770"/>
        <source>Use HTML tags</source>
        <translation>Bruk HTML-tag&apos;er</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="771"/>
        <source>If enabled HTML tags are not removed from the message</source>
        <translation>Merk av for å beholde HTML-tag&apos;er i meldinga</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="776"/>
        <source>Use clickable links</source>
        <translation>Bruk klikkbare linker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="777"/>
        <source>If enabled the links in the message are recognized and made clickable</source>
        <translation>Merk av for å gjenkjenne linker i meldinger og gjøre dem klikkbare</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="751"/>
        <source>Show messages grouped by user</source>
        <translation>Vis meldinger gruppert etter bruker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="752"/>
        <source>If enabled the messages will be shown grouped by user</source>
        <translation>Merk av får å vise meldeinger gruppert etter bruker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="695"/>
        <source>Save messages</source>
        <translation>Lagre meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="663"/>
        <source>Add users manually...</source>
        <translation>Legg til bruker manuelt...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="664"/>
        <source>Add the IP address and the port of the users you want to connect</source>
        <translation>Legg til IP-adresse og port til brukeren du ønsker å tilkoble</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="696"/>
        <source>If enabled the messages are saved when the program is closed</source>
        <translation>Merk av for at meldinger skal lagres når programmet lukkes</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="965"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="966"/>
        <source>Select your status</source>
        <translation>Velg din status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="972"/>
        <location filename="../src/desktop/GuiMain.cpp" line="988"/>
        <source>Your status will be %1</source>
        <translation>Statusen din settes til %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1008"/>
        <source>Show the chat</source>
        <translation>Vis samtalen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1009"/>
        <source>Show the chat view</source>
        <translation>Vis samtaleoversikten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1010"/>
        <source>Show my shared files</source>
        <translation>Vis mine delte filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1011"/>
        <source>Show the list of the files which I have shared</source>
        <translation>Vis lista over filer jeg har delt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1012"/>
        <source>Show the network shared files</source>
        <translation>Vis filer delt over nettverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1013"/>
        <source>Show the list of the network shared files</source>
        <translation>Vis lista over filene delt over nettverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1022"/>
        <source>Plugins</source>
        <translation>Utvidelser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1026"/>
        <source>Tip of the day</source>
        <translation>Dagens tips</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1027"/>
        <source>Show me the tip of the day</source>
        <translation>Vis dagens tips</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1028"/>
        <source>Fact of the day</source>
        <translation>Dagens fakta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1029"/>
        <source>Show me the fact of the day</source>
        <translation>Vis dagens fakta</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1032"/>
        <source>Show %1&apos;s license...</source>
        <translation>Vis lisens for %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1033"/>
        <source>Show the informations about %1&apos;s license</source>
        <translation>Vis informasjon om lisensen til %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1035"/>
        <source>Show the informations about Qt library</source>
        <translation>Vis informasjon om Qt-biblioteket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1037"/>
        <source>Open %1 official website...</source>
        <translation>Åpne %1 offisielle nettsted...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1038"/>
        <source>Explore %1 official website</source>
        <translation>Utforsk %1 offisielle nettsted</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1039"/>
        <source>Check for new version...</source>
        <translation>Se etter ny versjon...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1040"/>
        <source>Open %1 website and check if a new version exists</source>
        <translation>Åpne nettsted for %1 og se etter ny versjon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1041"/>
        <source>Download plugins...</source>
        <translation>Last ned utvidelser...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1042"/>
        <source>Open %1 website and download your preferred plugin</source>
        <translation>Åpne nettsted for %1 og last ned din foretrukne utvidelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1043"/>
        <source>Help online...</source>
        <translation>Online-hjelp...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1044"/>
        <source>Open %1 website to have online support</source>
        <translation>Åpne nettsted for %1 for online brukerstøtte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1055"/>
        <source>I&apos;m so grateful and pleased about that</source>
        <translation>Jeg er så takknemlig og tilfreds med det</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="181"/>
        <source>Ready</source>
        <translation>Klar</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1163"/>
        <source>Users</source>
        <translation>Brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="497"/>
        <source>Disconnect</source>
        <translation>Koble fra</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="503"/>
        <source>Connect</source>
        <translation>Koble til</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="618"/>
        <source>Configure %1 network to search a user who is not in your local subnet</source>
        <translation>Konfigurer %1-nettverk til å søke en bruker som ikke er i ditt lokale subnet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="621"/>
        <source>Quit</source>
        <translation>Avslutt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="626"/>
        <source>Edit your profile...</source>
        <translation>Rediger profilen din...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="627"/>
        <source>Change your profile information like your picture or your email or phone number</source>
        <translation>Endre din profilinformasjon som bilde eller epost eller telefonnummer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="634"/>
        <source>About %1...</source>
        <translation>Om %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="653"/>
        <source>Main</source>
        <translation>Hoved</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="659"/>
        <source>Broadcast to network</source>
        <translation>Kringkast til nettverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="660"/>
        <source>Broadcast a message in your network to find available users</source>
        <translation>Kringkast en melding i nettverket for å finne tilgjengelige brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="703"/>
        <source>Show colored nickname</source>
        <translation>Vis fargelagt kallenavn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="939"/>
        <source>Show the user&apos;s picture</source>
        <translation>Vis brukerens bilde</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="693"/>
        <source>Chat</source>
        <translation>Samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="880"/>
        <source>Enable BEEP alert on new message</source>
        <translation>Slå på BEEP-varsel ved nye meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="901"/>
        <source>Raise on top on new message</source>
        <translation>Legg fremst ved ny melding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="907"/>
        <source>Always stay on top</source>
        <translation>Alltid fremst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1076"/>
        <source>Load on system tray at startup</source>
        <translation>Last til oppgavelinje ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1077"/>
        <source>If enabled %1 will be start hided in system tray</source>
        <translation>Merk av for at %1 startes skjult i oppgavelinje</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="995"/>
        <source>View</source>
        <translation>Visning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="721"/>
        <source>Show the timestamp</source>
        <translation>Vis tidspunkt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="739"/>
        <source>Parse Unicode and ASCII emoticons</source>
        <translation>Handter Unicode og ASCII uttrykksikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="740"/>
        <source>If enabled the ASCII emoticons will be recognized and shown as images</source>
        <translation>Merk av for at ASCII utrykksikon skal gjenkjennes og vises som bilder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="745"/>
        <source>Use native emoticons</source>
        <translation>Bruk originale uttrykkesikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="746"/>
        <source>If enabled the emoticons will be parsed by your system font</source>
        <translation>Merk av for å tolke uttrykksikon med systemets skrifttype</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1639"/>
        <source>Show only last %1 messages</source>
        <translation>Vis kun de %1 siste meldingene</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1640"/>
        <source>If enabled only the last %1 messages will be shown in chat</source>
        <translation>Merk av for å vise bare de %1 siste meldingene i samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="858"/>
        <source>Prompt before downloading file</source>
        <translation>Spør for nedlasting av fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="859"/>
        <source>If enabled you have to confirm the action before downloading a file</source>
        <translation>Merk av for å måtte bekrefte valg før nedlasting av fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="808"/>
        <source>Reset window geometry at startup</source>
        <translation>Nullstille vindusgeometri ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="923"/>
        <source>Options</source>
        <translation>Valg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="945"/>
        <source>Show the user&apos;s vCard on right click</source>
        <translation>Vis brukerens vCard ved høyreklikk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="946"/>
        <source>If enabled you can see the user&apos;s vCard when right click on it</source>
        <translation>Merk av for kunne se brukerens vCard når du høyreklikker på det</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="925"/>
        <source>Save the users on exit</source>
        <translation>Lagre brukerne ved avslutt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="366"/>
        <source>&lt;b&gt;Chat messages can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Samtalemeldinger kan ikke lagres&lt;/b&gt;. Sti:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="638"/>
        <source>Create chat</source>
        <translation>Opprett samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="639"/>
        <source>Create a chat with two or more users</source>
        <translation>Opprett samtale med to eller flere brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="642"/>
        <source>Create group</source>
        <translation>Opprett gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="643"/>
        <source>Create a group with two or more users</source>
        <translation>Opprett ei gruppe med to eller flere brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="671"/>
        <source>Shortcuts...</source>
        <translation>Snarveier...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="672"/>
        <source>Enable and edit your custom shortcuts</source>
        <translation>Slå på og rediger dine egne snarveier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="673"/>
        <source>Dictionary...</source>
        <translation>Ordbok...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="674"/>
        <source>Select your preferred dictionary for spell checking</source>
        <translation>Velg din foretrukne ordbok for stavekontroll</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="684"/>
        <source>Open your resource folder</source>
        <translation>Åpne ressursmappa di</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="685"/>
        <source>Click to open your resource folder</source>
        <translation>Klikk for å åpne ressursmappa di</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="687"/>
        <source>Open your data folder</source>
        <translation>Åpne datamappa di</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="688"/>
        <source>Click to open your data folder</source>
        <translation>Klikk for å åpne datamappa di</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="727"/>
        <source>Show the datestamp</source>
        <translation>Vis datomerking</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="728"/>
        <source>If enabled the message shows its datestamp in the chat window</source>
        <translation>Merk av for at melding skal vise datomerking i samtalevindu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="733"/>
        <source>Show preview of the images</source>
        <translation>Forhåndsvis bildene</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="734"/>
        <source>If enabled the preview of the downloaded images will be showed in the chat window</source>
        <translation>Merk av for å forhåndsvise de nedlasta bildene i samtalevinduet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="763"/>
        <source>Use your name instead of &apos;You&apos;</source>
        <translation>Bruk navnet ditt i stedet for &apos;Du&apos;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="785"/>
        <source>Prompts for nickname on startup</source>
        <translation>Spør etter kallenavn ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="796"/>
        <source>Show activities home page at startup</source>
        <translation>Vis hjemmeside for aktiviteter ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="797"/>
        <source>If enabled the activities home page instead of chat page will be showed at startup</source>
        <translation>Merk av for å vise hjemmesiden for aktiviteter i stedet for samtaleside ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="802"/>
        <source>Show minimized at startup</source>
        <translation>Vis minimert ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="803"/>
        <source>If enabled %1 is showed minimized at startup</source>
        <translation>Merk av for at %1 skal vises minimert ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="814"/>
        <source>Check for new version at startup</source>
        <translation>Se etter ny versjon ved oppstart</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="840"/>
        <source>If the file to be downloaded already exists it is automatically overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="849"/>
        <source>Ask me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="884"/>
        <source>When the chat is not visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="892"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="915"/>
        <source>Prompt on quit (only when connected)</source>
        <translation>Spør ved avslutning (kun når tilgjengelig)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="916"/>
        <source>If enabled you will be asked if you really want to close %1</source>
        <translation>Merk av for å blir spurt om du vil lukke %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="926"/>
        <source>If enabled the user list will be save on exit</source>
        <translation>Merk av for å lagre brukerliste ved avslutning</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="951"/>
        <source>Show status color in background</source>
        <translation>Vis statusfarge i bakgrunn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="952"/>
        <source>If enabled the user in list has colored backrgound as status</source>
        <translation>Merk av for gi brukeren i liste farget bakgrunn som status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="959"/>
        <source>Change size of the user&apos;s picture</source>
        <translation>Endre størrelse på brukerbildet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="960"/>
        <source>Click to change the picture size of the users in the list</source>
        <translation>Klikk for å endre bildestørrelse for brukerne i lista</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="978"/>
        <source>Recently used</source>
        <translation>Nylig brukt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="983"/>
        <source>Change your status description...</source>
        <translation>Endre statusbeskrivelsen din...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="984"/>
        <source>Clear all status descriptions</source>
        <translation>Blank ut alle statusbeskrivelser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="996"/>
        <source>Save main window geometry</source>
        <translation>Lagre geometri for hovedvindu</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1006"/>
        <source>Show %1 home</source>
        <translation>Vis %1 hjem</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1007"/>
        <source>Show the homepage with %1 activity</source>
        <translation>Vis hjemmeside med %1 aktivitet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1018"/>
        <source>Show new message</source>
        <translation>Vis nye meldinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1025"/>
        <source>?</source>
        <translation>?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1034"/>
        <source>Qt Library...</source>
        <translation>Qt-bibliotek...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1046"/>
        <source>Like %1 on Facebook</source>
        <translation>Lik %1 på Facebook</translation>
    </message>
    <message>
        <source>Help me to know how many people use BeeBEEP</source>
        <translation type="obsolete">Hjelp meg å vite hvor mange som bruker BeeBEEP</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1054"/>
        <source>Donate for %1</source>
        <translation>Doner til %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1059"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1082"/>
        <source>Close button minimize to tray icon</source>
        <translation>Lukke-knapp minimerer til oppgavelinje-ikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1083"/>
        <source>If enabled when the close button is clicked the window will be minimized to the system tray icon</source>
        <translation>Merk av for at klikk på lukke-knapp skal minimere vindu til oppgavelinje-ikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1088"/>
        <source>Escape key minimize to tray icon</source>
        <translation>Escape-tast minimerer til oppgavelinje-ikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1089"/>
        <source>If enabled when the escape button is clicked the window will be minimized to the system tray icon</source>
        <translation>Merk av for at klikk på escape-knapp skal minimere vindu til oppgavelinje-ikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1100"/>
        <source>Show only message notifications</source>
        <translation>Bare vis meldingsvarsler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1101"/>
        <source>If enabled tray icon shows only message notifications</source>
        <translation>Merk av for at oppgavelinjeikon kun skal vise meldingsvarsler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1106"/>
        <source>Show chat message preview</source>
        <translation>Forhåndsvis samtalemelding</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1172"/>
        <source>Show the list of the connected users</source>
        <translation>Vis lista over tilgjengelige brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1175"/>
        <source>Groups</source>
        <translation>Grupper</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1184"/>
        <source>Show the list of your groups</source>
        <translation>Vis lista over dine grupper</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1187"/>
        <source>Chats</source>
        <translation>Samtaler</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1196"/>
        <source>Show the list of the chats</source>
        <translation>Vis lista over samtalene</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1199"/>
        <source>History</source>
        <translation>Historikk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1208"/>
        <source>Show the list of the saved chats</source>
        <translation>Vis lista over de lagra samtalene</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1211"/>
        <source>File Transfers</source>
        <translation>Filoverføringer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1220"/>
        <source>Show the list of the file transfers</source>
        <translation>Vis lista over filoverføringene</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1223"/>
        <source>Emoticons</source>
        <translation>Utrykksikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1235"/>
        <source>Show the emoticon panel</source>
        <translation>Vis panelet med uttrykksikon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1236"/>
        <source>Add your preferred emoticon to the message</source>
        <translation>Legg til ditt foretruken uttrykksikon til meldinga</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1281"/>
        <source>Show the bar of local file sharing</source>
        <translation>Vis linja med lokal fildeling</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1292"/>
        <source>Show the bar of network file sharing</source>
        <translation>Vis linja med nettverksfildeling</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1303"/>
        <source>Show the bar of log</source>
        <translation>Vis linja med logg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1317"/>
        <source>Show the bar of screenshot plugin</source>
        <translation>Vis linja med skjermkopi-utvidelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1342"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2406"/>
        <source>Play %1</source>
        <translation>Spill %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1346"/>
        <source>is a game developed by</source>
        <translation>er et spill utvikla av </translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1349"/>
        <source>About %1</source>
        <translation>Om %1</translation>
    </message>
    <message>
        <source>When do you want %1 to play beep?</source>
        <translation type="obsolete">Når ønsker du at %1 skal spille varsellyd?</translation>
    </message>
    <message>
        <source>If it not visible</source>
        <translation type="obsolete">Når ikke synlig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="888"/>
        <source>Always</source>
        <translation>Alltid</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1485"/>
        <source>Please save the network password in the next dialog.</source>
        <translation>Lagre nettverkspassordet i neste dialog.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1507"/>
        <source>How many minutes of idle %1 can wait before changing status to away?</source>
        <translation>Hvor mange minutter med inaktivitet kan %1 vente før status endres til borte?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1543"/>
        <source>Please select the maximum number of messages to be showed</source>
        <translation>Velg maksimum antall meldinger som skal vises</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1722"/>
        <source>New message from %1</source>
        <translation>Ny melding fra %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1725"/>
        <source>New message arrived</source>
        <translation>Ny melding ankom</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1829"/>
        <source>%1 is writing...</source>
        <translation>%1 skriver...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1842"/>
        <source>Do you want to disconnect from %1 network?</source>
        <translation>Vil du koble fra %1-nettverk?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1875"/>
        <source>You are %1%2</source>
        <translation>Du er %1%2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1886"/>
        <source>Please insert the new status description</source>
        <translation>Skriv inn den nye statusbeskrivelsen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>%1 - Select a file</source>
        <translation>%1 - Velg ei fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>or more</source>
        <translation>eller flere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1954"/>
        <source>File transfer is not enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1960"/>
        <source>You are not connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1971"/>
        <source>There is no user connected.</source>
        <translation>Det er ingen brukere tilgjengelig.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1977"/>
        <source>Please select the user to whom you would like to send a file.</source>
        <translation>Velg brukeren du vil sende ei fil til.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1986"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2993"/>
        <source>User not found.</source>
        <translation>Fant ikke bruker.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2009"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2159"/>
        <source>File transfer is disabled. You cannot download %1.</source>
        <translation>Filoverføring er inaktiv. Du kan ikke laste ned %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <source>Yes, and don&apos;t ask anymore</source>
        <translation>Ja og ikke spør flere ganger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2063"/>
        <source>%1 already exists. Please select a new filename.</source>
        <translation>%1 eksisterer allerde. Angi et annet filnavn.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2102"/>
        <source>You cannot download all these files at once. Do you want to download the first %1 files of the list?</source>
        <translation>Du kan ikke laste ned alle disse filene på en gang. Vil du laste ned de første %1 filene i lista?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2110"/>
        <source>Downloading %1 files is a hard duty. Maybe you have to wait a lot of minutes. Do yo want to continue?</source>
        <translation>Å laste ned %1 filer er tidkrevende. Du må kanskje vente mange minutter. Vil du fortsette?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2128"/>
        <source>%1 files are scheduled for download</source>
        <translation>%1 filer er lagt til nedlasting</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2167"/>
        <source>Do you want to download folder %1 (%2 files) from %3?</source>
        <translation>Vil du laste ned mappe %1 (%2 filer) fra %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2208"/>
        <source>%1 - Select the download folder</source>
        <translation>%1 - Velg nedlastingsmappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2361"/>
        <source>Plugin Manager...</source>
        <translation>Utvidelsesstyrer...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2362"/>
        <source>Open the plugin manager dialog and manage the installed plugins</source>
        <translation>Åpne utvidelsesstyrer-dialogen og administrer intallerte utvidelser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3589"/>
        <source>at lunch</source>
        <translation>til lunsj</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3598"/>
        <source>in a meeting</source>
        <translation>i et møte</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3634"/>
        <source>Do you really want to clear all saved status descriptions?</source>
        <translation>Vil du blanke ut alle lagra statusbeskrivelser?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3811"/>
        <source>Select your dictionary path</source>
        <translation>Velg ordbok-mappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3819"/>
        <source>Dictionary selected: %1</source>
        <translation>Valgt ordbok: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3821"/>
        <source>Unable to set dictionary: %1</source>
        <translation>Kunne ikke velge ordbok: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3982"/>
        <source>Window geometry and state saved</source>
        <translation>Vindusgeometri og status lagret</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1014"/>
        <source>Show the %1 log</source>
        <translation>Vis %1-loggen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1015"/>
        <source>Show the application log to see if an error occurred</source>
        <translation>Vis programloggen for å se om det oppsto en feil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1016"/>
        <source>Make a screenshot</source>
        <translation>Gjør en skjermfangst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1017"/>
        <source>Show the utility to capture a screenshot</source>
        <translation>Vis verktøyet for å gjøre en skjermfangst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1048"/>
        <source>Add +1 user to anonymous usage statistics</source>
        <translation>Legg +1 bruker til anonym brukerstatistikk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1049"/>
        <source>Help me to know how many users have BeeBEEP</source>
        <translation>Hjelp meg å anslå antall brukere av BeeBEEP</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1066"/>
        <source>Network</source>
        <translation>Nettverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2364"/>
        <source>is a plugin developed by</source>
        <translation>er en utvidelse utvikla av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2389"/>
        <source>Show the bar of games</source>
        <translation>Vis linja med spill</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2636"/>
        <source>Do you really want to open the file %1?</source>
        <translation>Vil du åpne fila %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Unable to open %1</source>
        <translation>Kan ikke åpne %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2661"/>
        <source>Sound files (*.wav)</source>
        <translation>Lydfiler (*.wav)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <source>Sound is not enabled on a new message. Do you want to enable it?</source>
        <translation>Lyd er ikke slått på for ny melding. Vil du slå på lyd?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2685"/>
        <source>Sound module is not working. The default BEEP will be used.</source>
        <translation>Lydmodul virker ikke. Standardvarsel blir brukt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2690"/>
        <source>Sound file not found</source>
        <translation>Fant ikke lydfil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2691"/>
        <source>The default BEEP will be used</source>
        <translation>Standard varslingslyd brukes</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2737"/>
        <source>Group chat will be deleted when all members goes offline.</source>
        <translation>Gruppesamtale blir sletta når alle medlemmer går ut.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2738"/>
        <source>If you want a persistent chat please consider to make a Group instead.</source>
        <translation>Hvis du vil ha en vedvarende samtale, vurder å opprette en gruppe i stedet.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2739"/>
        <source>Do you wish to continue or create group?</source>
        <translation>Vil du fortsette eller opprette gruppe?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Continue</source>
        <translation>Fortsett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Create Group</source>
        <translation>Opprett gruppe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2766"/>
        <source>Unable to add users in this chat. Please select a group one.</source>
        <translation>Kan ikke legge til brukere i denne samtalen. Velg en gruppesamtale.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2813"/>
        <source>Now %1 will start on windows boot.</source>
        <translation>Nå vil %1 starte ved oppstart av Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2815"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2822"/>
        <source>Unable to add this key in the registry: permission denied.</source>
        <translation>Kan ikke legge denne nøkkelen i registeret: tilgang nekta.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2820"/>
        <source>%1 will not start on windows boot.</source>
        <translation>%1 blir ikke startet ved oppstart av Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2877"/>
        <source>Please select a chat you would like to link the saved text.</source>
        <translation>Velg en samtale du vil linke lagra tekst til.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2886"/>
        <source>The chat &apos;%1&apos; selected has already a saved text.&lt;br /&gt;What do you want to do with the selected saved text?</source>
        <translation>Valgt samtale &apos;%1&apos; har allerede en lagret tekst.&lt;br/&gt;Hva vil du gjøre med den valgte lagra teksten?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="839"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Overwrite</source>
        <translation>Overskrive</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Add in the head</source>
        <translation>Legg til øverst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3037"/>
        <source>All the members of this chat are not online. The changes may not be permanent. Do you wish to continue?</source>
        <translation>Alle medlemmene i denne samtalen er ikke tilgjengelig. Endringene kan bli flyktige. Vil du fortsette?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3175"/>
        <source>Do you really want to delete chat with %1?</source>
        <translation>Vil du slette samtale med %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3296"/>
        <source>inactive</source>
        <translation>inaktiv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3316"/>
        <source>disabled</source>
        <translation>inaktiv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3323"/>
        <source>active</source>
        <translation>aktiv</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3396"/>
        <source>%1 is online</source>
        <translation>%1 er tilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3398"/>
        <source>%1 is offline</source>
        <translation>%1 er utilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3410"/>
        <source>Please select the new size of the user picture</source>
        <translation>Velg den nye størrelsen på brukerbildet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1171"/>
        <source>Show the user panel</source>
        <translation>Vis brukerpanelet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1183"/>
        <source>Show the group panel</source>
        <translation>Vis gruppepanelet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1195"/>
        <source>Show the chat panel</source>
        <translation>Vis samtalepanelet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1207"/>
        <source>Show the history panel</source>
        <translation>Vis historikkpanelet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1219"/>
        <source>Show the file transfer panel</source>
        <translation>Vis filoverføringspanelet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1268"/>
        <source>Show the bar of chat</source>
        <translation>Vis samtalelinja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2030"/>
        <source>Do you want to download %1 (%2) from %3?</source>
        <translation>Vil du laste ned %1 (%2) fra %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2143"/>
        <source>File is not available for download.</source>
        <translation>Fila er ikke tilgjengeli for nedlasting.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2145"/>
        <source>%1 is not connected.</source>
        <translation>%1 er ikke tilkobla.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2146"/>
        <source>Please reload the list of shared files.</source>
        <translation>Last inn lista over delte filer på nytt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <source>Reload file list</source>
        <translation>Last inn filliste på nytt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3122"/>
        <source>Chat with %1 is empty.</source>
        <translation>Samtale med %1 er tom.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3126"/>
        <source>Do you really want to clear messages with %1?</source>
        <translation>Vil du blanke ut meldingene med %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3129"/>
        <source>Yes and delete history</source>
        <translation>Ja og slett historikk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3059"/>
        <source>%1 is a your group. You can not leave the chat.</source>
        <translation>%1 er din gruppe. Du kan ikke forlate samtalen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Delete this group</source>
        <translation>Slett denne gruppa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3084"/>
        <source>You cannot leave this chat.</source>
        <translation>Du kan ikke forlate denne samtalen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3096"/>
        <source>Do you really want to delete group &apos;%1&apos;?</source>
        <translation>Vil du slette gruppa &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3197"/>
        <source>Unable to delete this chat.</source>
        <translation>Kan ikke slette denne samtalen.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3219"/>
        <source>%1 has shared %2 files</source>
        <translation>%1 har delt %2 filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3241"/>
        <source>Default language is restored.</source>
        <translation>Standardspråk er gjenoppretta.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3243"/>
        <source>New language &apos;%1&apos; is selected.</source>
        <translation>Nytt språk &apos;%1&apos; er valgt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3246"/>
        <source>You must restart %1 to apply these changes.</source>
        <translation>Du må restarte %1 for å sette verk disse endringene.</translation>
    </message>
</context>
<context>
    <name>GuiMessageEdit</name>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="322"/>
        <source>Undo</source>
        <translation>Angre</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="324"/>
        <source>Redo</source>
        <translation>Gjør på nytt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="327"/>
        <source>Cut</source>
        <translation>Klipp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="328"/>
        <source>Copy</source>
        <translation>Kopier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="329"/>
        <source>Paste</source>
        <translation>Lim inn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="332"/>
        <source>Select All</source>
        <translation>Velg alt</translation>
    </message>
</context>
<context>
    <name>GuiPluginManager</name>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="100"/>
        <source>Enable All</source>
        <translation>Slå på alt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="110"/>
        <source>Disable All</source>
        <translation>Slå av alt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="133"/>
        <source>Close</source>
        <translation>Lukk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="36"/>
        <source>Plugin Manager - %1</source>
        <translation>Utvidelsesstyrer - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Plugin</source>
        <translation>Utvidelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Version</source>
        <translation>Versjon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Author</source>
        <translation>Forfatter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Disable %1</source>
        <translation>Slå av %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Enable %1</source>
        <translation>Slå på %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is enabled</source>
        <translation>%1 er slått på</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is disabled</source>
        <translation>%1 er slått av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="129"/>
        <source>Please select a plugin in the list.</source>
        <translation>Velg en utvidelse i lista.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="152"/>
        <source>Text Markers</source>
        <translation>Tekstmarkeringer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="171"/>
        <source>Games</source>
        <translation>Spill</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="192"/>
        <source>%1 - Select the plugin folder</source>
        <translation>%1 - Velg utvidelsesmappa</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="208"/>
        <source>Folder %1 not found.</source>
        <translation>Mappe %1 ikke funnet.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChat</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="56"/>
        <source>Saved chat</source>
        <translation>Lagra samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="58"/>
        <source>Empty</source>
        <translation>Tom</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="72"/>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="143"/>
        <source>Find text in chat</source>
        <translation type="unfinished">Finn tekst i samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="77"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Kopier til utklippstavle</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="79"/>
        <source>Select All</source>
        <translation type="unfinished">Velg alt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="83"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="86"/>
        <source>Print...</source>
        <translation type="unfinished">Skriv ut...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="181"/>
        <source>%1 not found in chat.</source>
        <translation type="unfinished">%1 ikke funnet i samtale.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatList</name>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="47"/>
        <source>Show</source>
        <translation>Vis</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="50"/>
        <source>Link to chat</source>
        <translation>Link til samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="52"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Do you really want to delete this saved chat?</source>
        <translation>Vil du slette denne lagra samtalen?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
</context>
<context>
    <name>GuiScreenShot</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="35"/>
        <source>Make a Screenshot</source>
        <translation>Gjør en skjermfangst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="44"/>
        <source>Delay</source>
        <translation>Forsinkelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="45"/>
        <source>Delay screenshot for selected seconds</source>
        <translation>Forsink skjermfangst med valgte sekunder</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="53"/>
        <source>s</source>
        <translation>s</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="61"/>
        <source>Hide this window</source>
        <translation>Skjul dette vinduet</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="62"/>
        <source>Hide this window before capture screenshot</source>
        <translation>Skjul dette vinduet før skjermfangst</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="73"/>
        <source>Enable high dpi</source>
        <translation>Slå på høy dpi</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="74"/>
        <source>Enable high dpi support to manage, for example, Apple Retina display</source>
        <translation>Slå på høy dpi-støtte for å handtere for eksempel Apple Retina skjerm</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="80"/>
        <source>Capture</source>
        <translation>Fang</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="81"/>
        <source>Capture a screenshot of your desktop</source>
        <translation>Ta en skjermfangst av hele skjermen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="82"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="83"/>
        <source>Send the captured screenshot to an user</source>
        <translation>Send skjermfangsten til en bruker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="84"/>
        <source>Save</source>
        <translation>Lagre</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="85"/>
        <source>Save the captured screenshot as file</source>
        <translation>Lagre skjermfangsten til fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="86"/>
        <source>Delete</source>
        <translation>Slett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="87"/>
        <source>Delete the captured screenshot</source>
        <translation>Slett skjermfangsten</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="107"/>
        <source>No screenshot available</source>
        <translation>Ingen skjermfangster tilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="179"/>
        <source>/beesshot-%1.</source>
        <translation>/beesfangst-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="182"/>
        <source>Save As</source>
        <translation>Lagre som</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="184"/>
        <source>%1 Files (*.%2)</source>
        <translation>%1 filer (*.%2)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="199"/>
        <source>/beesshottmp-%1.</source>
        <translation>/beesfangsttmp-%1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="206"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Kunne ikke lagre midlertidig fil: %1</translation>
    </message>
</context>
<context>
    <name>GuiScreenShotWidget</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="84"/>
        <source>No Screenshot Available</source>
        <translation>Skjermfangst ikke tilgjengelig</translation>
    </message>
</context>
<context>
    <name>GuiSearchUser</name>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="20"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="77"/>
        <source>Local subnet address *</source>
        <translation>Lokal subnett-adresse: *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="204"/>
        <source>Addresses in beehosts.ini *</source>
        <translation>Adresser i beehosts.ini *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="35"/>
        <source>UDP Port in beebeep.rc *</source>
        <translation>UDP-port i beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="64"/>
        <source>(the same for all clients)</source>
        <translation>(det samme for alle klienter)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="106"/>
        <source>(search users here by default)</source>
        <translation>(søk bruker her som standard)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="119"/>
        <source>Multicast group in beebeep.rc *</source>
        <translation>Multicast-gruppe i beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="155"/>
        <source>Enable broadcast interval</source>
        <translation>Aktiver kringkastingsintervall</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="184"/>
        <source>seconds (0=disabled, 10=default)</source>
        <translation>sekunder (0=avslått, 10=standard)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="351"/>
        <source>Workgroups (enter your network groups separated by comma)</source>
        <translation>Arbeidsgrupper (angi nettversgrupper adskilt med komma)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="239"/>
        <source>Accept connections only from your workgroups</source>
        <translation>Godta forbindelser kun fra dine arbeidsgrupper</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="258"/>
        <source>Enter the IP addresses or subnet of your local area network separated by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation>Angi IP-adresser eller subnett til ditt lokalnett adskilt med komma (eksempel: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="284"/>
        <source>Split subnet to IPV4 addresses</source>
        <translation>Del subnett til IPV4-adresser</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="297"/>
        <source>Verbose</source>
        <translation>Utfyllende</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="328"/>
        <source>Max users to contact in a tick</source>
        <translation>Maks antall bruker å kontakte samtidig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="337"/>
        <source>Automatically add external subnet</source>
        <translation>Legg til eksternt subnett automatisk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="344"/>
        <source>Enable Zero Configuration Networking</source>
        <translation>Slå på nullkonfigurasjonsnettverk</translation>
    </message>
    <message>
        <source>Enable broadcast interval of</source>
        <translation type="obsolete">Aktiver kringkastingsintervall på</translation>
    </message>
    <message>
        <source>s (0=disabled, 10=default)</source>
        <translation type="obsolete">s (0=avslått, 10=standard)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="367"/>
        <source>* (read only section)</source>
        <translation>* (kun les seksjon)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="374"/>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="381"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="36"/>
        <source>Search for users</source>
        <translation>Søke etter brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="54"/>
        <source>Unknown address</source>
        <translation>Ukjent adresse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="62"/>
        <source>File is empty</source>
        <translation>Fila er tom</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="116"/>
        <source>Warning</source>
        <translation>Advarsel</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>You have inserted an invalid host address:
%1 is removed from the list.</source>
        <translation>Du har satt inn en ugyldig maskinadresse:
%1 er fjerna fra lista.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocal</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="37"/>
        <source>Share your folders or files</source>
        <translation>Del dine mapper og filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Size</source>
        <translation>Størrelse</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Path</source>
        <translation>Sti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="77"/>
        <source>Share a file</source>
        <translation>Del ei fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="78"/>
        <source>Add a file to your local share</source>
        <translation>Legg til ei fil i din lokale deling</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="80"/>
        <source>Share a folder</source>
        <translation>Del ei mappe</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="81"/>
        <source>Add a folder to your local share</source>
        <translation>Legg til ei mappe i din lokale deling</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="83"/>
        <source>Update shares</source>
        <translation>Oppdater delinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="84"/>
        <source>Update shared folders and files</source>
        <translation>Oppdater delte mapper og filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="86"/>
        <source>Remove shared path</source>
        <translation>Fjern delt sti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="87"/>
        <source>Remove shared path from the list</source>
        <translation>Fjern delt sti fra lista</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="89"/>
        <source>Clear all shares</source>
        <translation>Tøm alle delinger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="90"/>
        <source>Clear all shared paths from the list</source>
        <translation>Tøm alle delte stier fra lista</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="99"/>
        <source>Shared files</source>
        <translation>Delte filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="101"/>
        <source>File transfer is disabled</source>
        <translation>Fileoverføring er slått av</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="119"/>
        <source>Select a file to share</source>
        <translation>Velg fil å dele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="132"/>
        <source>Select a folder to share</source>
        <translation>Velg mappe å dele</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="150"/>
        <source>Please select a shared path.</source>
        <translation>Velg en delt sti.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="167"/>
        <source>Do you really want to remove all shared paths?</source>
        <translation>Vild du fjerne alle delte stier?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>No</source>
        <translation>Nei</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="214"/>
        <source>Click to open %1</source>
        <translation>Klikk for å åpne %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="229"/>
        <source>%1 is already shared.</source>
        <translation>%1 er allerede delt.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="270"/>
        <source>File transfer is disabled. Open the option menu to enable it.</source>
        <translation>Filoverføring er slått av. Åpne innstillingsmeny for å slå på.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="334"/>
        <source>%1 shared files</source>
        <translation>%1 delte filer</translation>
    </message>
</context>
<context>
    <name>GuiShareLocalWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="87"/>
        <source>2</source>
        <translation>2</translation>
    </message>
</context>
<context>
    <name>GuiShareNetwork</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="98"/>
        <source>User</source>
        <translation>Bruker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="52"/>
        <source>Scan network</source>
        <translation>Skann nettverk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="39"/>
        <source>Folder and Files shared in your network</source>
        <translation>Mapper og filer delt i nettverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="53"/>
        <source>Search shared files in your network</source>
        <translation>Søk delte filer i nettverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="56"/>
        <source>Reload list</source>
        <translation>Les liste på nytt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="57"/>
        <source>Clear and reload list</source>
        <translation>Tøm og les liste på nytt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="74"/>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="83"/>
        <source>File Type</source>
        <translation>Filtype</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="89"/>
        <source>All Files</source>
        <translation>Alle filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="61"/>
        <source>Download</source>
        <translation>Last ned</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="62"/>
        <source>Download single or multiple files simultaneously</source>
        <translation>Last ned enkeltfil eller flere filer samtidig</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="68"/>
        <source>Filter</source>
        <translation>Filter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="116"/>
        <source>All Users</source>
        <translation>Alle brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="141"/>
        <source>%1 is searching shared files in your network</source>
        <translation>%1 søker etter delte filer i nettverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="195"/>
        <source>Double click to download %1</source>
        <translation>Dobbeltklikk for å laste ned %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="234"/>
        <source>%1 has shared %2 files (%3)</source>
        <translation>%1 har delt %2 filer (%3)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="315"/>
        <source>Double click to open %1</source>
        <translation>Dobbeltklikk for å åpne %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="317"/>
        <source>Transfer completed</source>
        <translation>Overføring fullført</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="331"/>
        <source>%1 files are shown in list (%2 are available in your network)</source>
        <translation>%1 filer vises i liste (%2 er tilgjengelig i nettverket)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="333"/>
        <source>%1 files shared in your network</source>
        <translation>%1 filer er delt i nettverket</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download single file</source>
        <translation>Last ned enkeltfil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download %1 selected files</source>
        <translation>Last ned %1 valgte filer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="377"/>
        <source>MAX</source>
        <translation>MAKS</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="380"/>
        <source>Clear selection</source>
        <translation>Tøm utvalg</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="384"/>
        <source>Expand all items</source>
        <translation>Ekspander alle elementer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="385"/>
        <source>Collapse all items</source>
        <translation>Slå sammen alle elementer</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="396"/>
        <source>Please select one or more files to download.</source>
        <translation>Velg en eller flere filer for nedlasting.</translation>
    </message>
</context>
<context>
    <name>GuiShareNetworkWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.ui" line="14"/>
        <source>Form</source>
        <translation>Skjema</translation>
    </message>
</context>
<context>
    <name>GuiShortcut</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="34"/>
        <source>Shortcuts</source>
        <translation>Snarveier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Key</source>
        <translation>Tast</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Action</source>
        <translation>Aksjon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="114"/>
        <source>Insert shorcut for the action: %1</source>
        <translation>Sett in snarvei for aksjonen: %1</translation>
    </message>
</context>
<context>
    <name>GuiShortcutDialog</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="17"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="23"/>
        <source>Use custom shortcuts</source>
        <translation>Bruk egne snarveier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="50"/>
        <source>Restore default shortcuts</source>
        <translation>Gjenopprett standard snarveier</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="53"/>
        <source>Restore</source>
        <translation>Gjenopprett</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="76"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="89"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiTransferFile</name>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>User</source>
        <translation>Bruker</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="116"/>
        <source>Completed</source>
        <translation>Fullført</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="122"/>
        <source>Cancel Transfer</source>
        <translation>Avbryt overføring</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="128"/>
        <source>Not Completed</source>
        <translation>Ikke fullført</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="161"/>
        <source>Transfer completed</source>
        <translation>Overføring fullført</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Downloading</source>
        <translation>Laster ned</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Uploading</source>
        <translation>Laster opp</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="218"/>
        <source>Do you really want to cancel the transfer of %1?</source>
        <translation>Vil du avbryte overføringa av %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="251"/>
        <source>Remove all transfers</source>
        <translation>Slett alle overføringer</translation>
    </message>
</context>
<context>
    <name>GuiUserList</name>
    <message>
        <location filename="../src/desktop/GuiUserList.cpp" line="55"/>
        <source>Search</source>
        <translation>Søk</translation>
    </message>
</context>
<context>
    <name>GuiVCard</name>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="59"/>
        <source>Birthday: %1</source>
        <translation>Fødselsdag: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="85"/>
        <source>unknown</source>
        <translation>ukjent</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="87"/>
        <source>old %1</source>
        <translation>gammel %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="89"/>
        <source>new %1</source>
        <translation>ny %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="105"/>
        <source>Remove from favorites</source>
        <translation>Slett fra favoritter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="110"/>
        <source>Add to favorites</source>
        <translation>Legg til favoritter</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="115"/>
        <source>Chat with all</source>
        <translation>Samtal med alle</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="121"/>
        <source>Open chat</source>
        <translation>Åpne samtale</translation>
    </message>
</context>
<context>
    <name>GuiVCardDialog</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="22"/>
        <source>User ID</source>
        <translation>Bruker-ID</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="150"/>
        <source>Nickname</source>
        <translation>Kallenavn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="207"/>
        <source>First name</source>
        <translation>Fornavn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="224"/>
        <source>Last name</source>
        <translation>Etternavn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="241"/>
        <source>Birthday</source>
        <translation>Fødselsdag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="274"/>
        <source>Email</source>
        <translation>Epost</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="291"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="356"/>
        <source>Add or change photo</source>
        <translation>Legg til eller endre foto</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="385"/>
        <source>Remove photo</source>
        <translation>Slett foto</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="417"/>
        <source>Change your nickname color</source>
        <translation>Endre farge på kallenavnet ditt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="456"/>
        <source>Other informations</source>
        <translation>Andre opplysninger</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="499"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="509"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
</context>
<context>
    <name>GuiVCardWidget</name>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="81"/>
        <source>Widget</source>
        <translation>Dings</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="291"/>
        <source>User Path</source>
        <translation>Brukersti</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="307"/>
        <source>User Name</source>
        <translation>Brukernavn</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="323"/>
        <source>Birthday</source>
        <translation>Fødselsdag</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="339"/>
        <source>Email</source>
        <translation>Epost</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="355"/>
        <source>Phone</source>
        <translation>Telefon</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="417"/>
        <source>S</source>
        <translation>S</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="452"/>
        <source>Open chat</source>
        <translation>Åpne samtale</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="490"/>
        <source>Send a file</source>
        <translation>Send ei fil</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="525"/>
        <source>Change the nickname color</source>
        <translation>Endre fargen på kallenav</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="583"/>
        <source>Remove user</source>
        <translation>Fjern bruker</translation>
    </message>
</context>
<context>
    <name>GuiWizard</name>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="36"/>
        <source>Welcome to &lt;b&gt;%1 Network&lt;/b&gt;.</source>
        <translation>Velkommen til &lt;b&gt;%1 nettverk&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="42"/>
        <source>Your system account is</source>
        <translation>Din systemkonto er</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="59"/>
        <source>Your nickname can not be empty.</source>
        <translation>Kallenavnet ditt kan ikke være blankt.</translation>
    </message>
</context>
<context>
    <name>GuiWizardDialog</name>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="14"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="56"/>
        <source>Welcome</source>
        <translation>Velkommen</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="94"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
</context>
<context>
    <name>NumberTextMarker</name>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="33"/>
        <source>Number Text Marker</source>
        <translation>Nummer tekstmarkering</translation>
    </message>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="48"/>
        <source>If you want to encode your message with numbers write a #text to encode# .</source>
        <translation>Hvis du vil kode meldinga di med nummer, skriv en #tekst som kodes# .</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="63"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3119"/>
        <location filename="../src/desktop/GuiUserItem.cpp" line="84"/>
        <source>All Lan Users</source>
        <translation>Alle LAN-brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="64"/>
        <source>Open chat with all local users</source>
        <translation>Åpne samtale med alle lokale brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="84"/>
        <source>Open chat with %1</source>
        <translation>Åpne samtale med %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatMessage.cpp" line="61"/>
        <source>You</source>
        <translation>Du</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="148"/>
        <source>Click to open chat with all local users</source>
        <translation>Klikk for å åpne samtale med alle lokale brukere</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="152"/>
        <source>%1 is %2</source>
        <translation>%1 er %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="167"/>
        <source>Click to send a private message</source>
        <translation>Klikk for å sende ei privat melding</translation>
    </message>
    <message>
        <source>Saved in</source>
        <translation type="obsolete">Lagra i</translation>
    </message>
    <message>
        <source>Started in</source>
        <translation type="obsolete">Starta i</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupItem.cpp" line="67"/>
        <source>Click to send message to group: %1</source>
        <translation>Klikk for å sende ei melding til gruppe: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="129"/>
        <source>Click to view chat history: %1</source>
        <translation>Klikk for å vise samtalehistorikk: %1</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="100"/>
        <source>Empty</source>
        <translation>Tom</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="102"/>
        <source>Send file</source>
        <translation>Send fil</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="104"/>
        <source>Show file transfers</source>
        <translation>Vis filoverføringer</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="106"/>
        <source>Set focus in message box</source>
        <translation>Gi meldingsboks fokus</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="108"/>
        <source>Minimize all chats</source>
        <translation>Minimer alle samtaler</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="110"/>
        <source>Show the next unread message</source>
        <translation>Vis neste uleste melding</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="112"/>
        <source>Send chat message</source>
        <translation>Send samtalemelding</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="114"/>
        <source>Print</source>
        <translation>Skriv</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="116"/>
        <source>Broadcast</source>
        <translation>Kringkast</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="118"/>
        <source>Find text in chat</source>
        <translation>Finn tekst i samtale</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="120"/>
        <source>Find next text in chat</source>
        <translation>Finn neste tekst i samtale</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="122"/>
        <source>Send folder</source>
        <translation>Send mappe</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="124"/>
        <source>Show emoticons panel</source>
        <translation>Vis panel for uttrykksikon</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="126"/>
        <source>Show all chats</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RainbowTextMarker</name>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="37"/>
        <source>Rainbow Text Marker</source>
        <translation>Regnbue tekstmarkering</translation>
    </message>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="52"/>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~ .</source>
        <translation>Hvis du vil ha en &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;e&lt;/font&gt;&lt;font color=#FFff00&gt;g&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;u&lt;/font&gt;&lt;font color=#00ffff&gt;e&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;k&lt;/font&gt;&lt;font color=#FF0080&gt;s&lt;/font&gt;&lt;font color=#ff0000&gt;t&lt;/font&gt;, skriv en ~regnbue tekst~ .</translation>
    </message>
</context>
<context>
    <name>RegularBoldTextMarker</name>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="32"/>
        <source>Regular Bold Text Marker</source>
        <translation>Vanlig feit tekstmarkering</translation>
    </message>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="47"/>
        <source>If you want to format your message with words in regular and bold font write a [text to format] .</source>
        <translation>Hvis du vil formattere meldinga med ord i vanlig og feit font, skriv en [tekst å formattere] .</translation>
    </message>
</context>
<context>
    <name>Tips</name>
    <message>
        <location filename="../src/Tips.h" line="31"/>
        <source>You can switch between chats with CTRL+TAB if there are new messages availables.</source>
        <translation>Du kan veksle mellom samtaler med CTRL+TAB hvis nye meldinger er tilgjengelig.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="32"/>
        <source>If you want a &lt;b&gt;bold text&lt;/b&gt; write a *bold text*.</source>
        <translation>Hvis du vil ha en &lt;b&gt;feit tekst&lt;/b&gt;, skriv en *feit tekst*.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="33"/>
        <source>If you want an &lt;i&gt;italic text&lt;/i&gt; write a /italic text/.</source>
        <translation>Hvis du vil ha en &lt;i&gt;kursiv tekst&lt;/i&gt;, skriv en /kursiv tekst/.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="34"/>
        <source>If you want an &lt;u&gt;underlined text&lt;/u&gt; write a _underlined text_.</source>
        <translation>Hvis du vil ha en &lt;u&gt;understreka tekst&lt;/u&gt;, skriv en _understreka tekst_.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="35"/>
        <source>You can search previous sent message in the history using the CTRL+Up e CTRL+Down keys.</source>
        <translation>Du kan søke i tildligere sendt melding i historikken med CTRL+Opp/CTRL+Ned taster.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="37"/>
        <source>You can drop files to active chat and send them to members.</source>
        <translation>Du kan dra filer til aktiv samtale og sende dem til medlemmer.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="38"/>
        <source>You can select multiple files from network shares and download them simultaneously with a right click.</source>
        <translation>Du kan velge flere filer fra nettverksdelinger og laste dem ned samtidig med et høyreklikk.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="39"/>
        <source>You can disable message notification from a group right clicking on its name on the list.</source>
        <translation>Du kan slå av meldingsvarsel fra en gruppe med høyreklikk på gruppenavnet i lista.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="47"/>
        <source>&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="48"/>
        <source>&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="49"/>
        <source>&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="50"/>
        <source>&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="51"/>
        <source>&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="53"/>
        <source>&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="54"/>
        <source>&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="55"/>
        <source>&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="56"/>
        <source>&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="57"/>
        <source>&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="59"/>
        <source>&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="60"/>
        <source>&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="61"/>
        <source>&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="62"/>
        <source>&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="63"/>
        <source>&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="65"/>
        <source>&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="66"/>
        <source>&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="67"/>
        <source>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="68"/>
        <source>&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="73"/>
        <source>&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="75"/>
        <source>&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="76"/>
        <source>&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="77"/>
        <source>&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="78"/>
        <source>&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="79"/>
        <source>&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="81"/>
        <source>&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="82"/>
        <source>&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="83"/>
        <source>&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="84"/>
        <source>&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="85"/>
        <source>&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="87"/>
        <source>&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="88"/>
        <source>&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="89"/>
        <source>&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="90"/>
        <source>&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="91"/>
        <source>&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="93"/>
        <source>&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="62"/>
        <source>offline</source>
        <translation>fraværende</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="63"/>
        <source>available</source>
        <translation>tilgjengelig</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="64"/>
        <source>busy</source>
        <translation>opptatt</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="65"/>
        <source>away</source>
        <translation>borte</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="66"/>
        <source>status error</source>
        <translation>statusfeil</translation>
    </message>
</context>
</TS>
