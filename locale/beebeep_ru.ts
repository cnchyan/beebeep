<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru" sourcelanguage="en">
<context>
    <name>ChatMessage</name>
    <message>
        <source>Undefined</source>
        <translation type="obsolete">Не определено</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="292"/>
        <source>Header</source>
        <translation>Заголовок</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="293"/>
        <source>System</source>
        <translation>Система</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="294"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="295"/>
        <source>Connection</source>
        <translation>Соединение</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="296"/>
        <source>User Status</source>
        <translation>Статус пользователя</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="297"/>
        <source>User Information</source>
        <translation>Информация о пользователе</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="298"/>
        <source>File Transfer</source>
        <translation>Передача файла</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="299"/>
        <source>History</source>
        <translation>История</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="300"/>
        <source>Other</source>
        <translation>Другое</translation>
    </message>
</context>
<context>
    <name>Core</name>
    <message>
        <location filename="../src/core/Core.cpp" line="127"/>
        <source>%1 Unable to connect to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Не удается соединиться с сетью %2. Пожалуйста, проверьте настройки вашего брандмауэра.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="152"/>
        <source>%1 Unable to broadcast to %2 Network. Please check your firewall settings.</source>
        <translation>%1 Не удается передать в сеть %2. Пожалуйста, проверьте настройки вашего брандмауэра..</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="165"/>
        <source>%1 You are connected to %2 Network.</source>
        <translation>%1 Вы соединены с сетью %2.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="176"/>
        <source>%1 You have selected to join only in these workgroups: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="230"/>
        <source>%1 Zero Configuration started with service name: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="245"/>
        <source>%1 Zero Configuration service closed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="277"/>
        <source>%1 You are disconnected from %2 Network.</source>
        <translation>%1 Вы отключены от сети %2.</translation>
    </message>
    <message>
        <source>%1 Looking for the available users in %2...</source>
        <translation type="obsolete">%1 Идет поиск доступных пользователей в %2...</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="81"/>
        <source>%1 User %2 cannot save settings in path: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="97"/>
        <source>%1 User %2 cannot save chat messages in path: %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 %2 will search users in these IP addresses: %3</source>
        <translation type="obsolete">%1 %2 будет искать пользователей в этих IP-адресах: %3</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="300"/>
        <source>%1 Zero Configuration is browsing network for service: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="307"/>
        <source>%1 Zero Configuration cannot browse network for service: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="325"/>
        <source>%1 Broadcasting to the %2 Network...</source>
        <translation>%1 Передает в сеть %2...</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="331"/>
        <source>%1 You are not connected to %2 Network.</source>
        <translation>%1 Вы не подключены к сети %2.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="337"/>
        <source>%1 %2 has found a filter on UDP port %3. Please check your firewall settings.</source>
        <translation>%1 %2 нашел фильтр UDP порта %3. Пожалуйста, проверьте настройки вашего брандмауэра.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="341"/>
        <source>View the log messages for more informations</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="511"/>
        <source>New version is available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="512"/>
        <source>Click here to download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 You cannot reach %2 Network.</source>
        <translation type="obsolete">%1 Вы не можете связаться с сетью %2.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="389"/>
        <source>is connected from external network (the new subnet is added to your broadcast address list).</source>
        <translation>присоединен от внешней сети (новая подсеть добавлена к вашему списку широковещательных адресов).</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="401"/>
        <source>%1 Checking %2 more addresses...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 Contacting %2 host addresses previously saved...</source>
        <translation type="obsolete">%1 Контактный адрес %2 узла предварительно сохранен...</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="46"/>
        <source>%1 Chat with all local users.</source>
        <translation>%1 Общается со всеми локальными пользователями.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="53"/>
        <source>Help me to know how many people are really using BeeBEEP.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="55"/>
        <source>Please add a like on Facebook.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="64"/>
        <source>Happy birthday to Marco Mastroddi: %1 years old today! Cheers!!!</source>
        <translation>С днем рождения Марко Мастродди: %1 лет исполнилось сегодня! Ваше здоровье!!!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="76"/>
        <source>Happy New Year!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="147"/>
        <source>%1 You have created the group %2.</source>
        <translation>%1 Вы создали группу %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="153"/>
        <source>%1 Welcome to the group %2.</source>
        <translation>%1 Добро пожаловать в группу %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="268"/>
        <source>Unable to send the message: this chat is disabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="394"/>
        <source>%1 %2 can not join to the group.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="457"/>
        <source>%1 saved chats are added to history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="573"/>
        <source>Offline messages sent to %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="99"/>
        <location filename="../src/core/CoreChat.cpp" line="164"/>
        <location filename="../src/core/CoreChat.cpp" line="244"/>
        <source>%1 Chat with %2.</source>
        <translation>%1 Общается с %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="198"/>
        <source>%1 The group has a new name: %2.</source>
        <translation>%1 Группа переименована: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="230"/>
        <source>%1 Members removed from the group: %2.</source>
        <translation>%1 Члены удалены из группы: %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="237"/>
        <source>%1 Members added to the group: %2.</source>
        <translation>%1 Члены добавлены в группу: %2.</translation>
    </message>
    <message>
        <source>%1 %2 can not be invited to the group.</source>
        <translation type="obsolete">%1 %2 не может быть приглашен в группу.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="410"/>
        <source>%1 %2 cannot be informed that you have left the group.</source>
        <translation>%1 %2 не может быть проинформирован, что вы ушли из группы.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="502"/>
        <source>%1 You have left the group.</source>
        <translation>%1 Вы покинули группу.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="504"/>
        <source>%1 %2 has left the group.</source>
        <translation>%1 %2 покинул группу.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="262"/>
        <source>Unable to send the message: you are not connected.</source>
        <translation>Не удается отослать сообщение: вы не подключены.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="302"/>
        <source>Unable to send the message to %1.</source>
        <translation>Не удается отослать сообщение для %1.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="330"/>
        <source>The message will be delivered to %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="335"/>
        <source>Nobody has received the message.</source>
        <translation>Никто не получил сообщение.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="48"/>
        <source>%1 Unable to start file transfer server: bind address/port failed.</source>
        <translation>%1 Не удается запустить сервер передачи файла: сбой присвоения адреса/порта.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="94"/>
        <source>%1 Unable to download %2 from %3: user is offline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="109"/>
        <source>%1 Unable to download %2 from %3: folder %4 cannot be created.</source>
        <translation>%1 Не удается загрузить %2 из %3: папка %4 не может быть создана.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="121"/>
        <source>%1 Downloading %2 from %3.</source>
        <translation>%1 Загружается %2 из %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>from</source>
        <translation>из</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="142"/>
        <source>to</source>
        <translation>в</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="184"/>
        <source>Open</source>
        <translation>Открыть</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="238"/>
        <source>%1 Unable to send %2 to %3: user is offline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="215"/>
        <source>%1 Unable to send %2. File transfer is disabled.</source>
        <translation>%1 Не удается отправить %2. Передача файлов выключена.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Upload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="141"/>
        <source>Download</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="187"/>
        <source>folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="247"/>
        <source>%1 %2: file not found.</source>
        <translation>%1 %2: файл не найден.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="257"/>
        <source>%1 %2 is a folder. You can share it.</source>
        <translation>%1 %2 - это папка. Вы можете открыть общий доступ к ней.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="278"/>
        <source>%1 Unable to send %2: %3 is not connected.</source>
        <translation>%1 Не удается отправить %2: %3 не подключен.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="272"/>
        <source>%1 You send %2 to %3.</source>
        <translation>%1 Вы отправили %2 в %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="303"/>
        <source>%1 You have refused to download %2 from %3.</source>
        <translation>%1 Вы отказались загружать %2 из %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="331"/>
        <source>%1 You have refused to download folder %2 from %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="394"/>
        <source>Adding to file sharing</source>
        <translation>Добавление общего доступа файла</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="449"/>
        <source>%1 is added to file sharing (%2)</source>
        <translation>%1 добавлен в файловый общий доступ (%2)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="451"/>
        <source>%1 is added to file sharing with %2 files, %3 (elapsed time: %4)</source>
        <translation>%1 добавлен в файловый общий доступ с %2 файлам, %3 (прошло времени: %4)</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="487"/>
        <source>All paths are removed from file sharing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="509"/>
        <source>%1 is removed from file sharing</source>
        <translation>%1 исключен из файлового общего доступа</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="511"/>
        <source>%1 is removed from file sharing with %2 files</source>
        <translation>%1 исключено из файлового общего доступа с %2 файлов</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="559"/>
        <source>%1 You are about to send %2 to %3. Checking folder...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="594"/>
        <source>%1 Unable to send folder %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="599"/>
        <source>the folder is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="606"/>
        <source>file transfer is not working.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="614"/>
        <source>%1 is not connected.</source>
        <translation>%1 не подключен.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="628"/>
        <source>internal error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreFileTransfer.cpp" line="633"/>
        <source>%1 You send folder %2 to %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="148"/>
        <source>%1 %2 has refused to download %3.</source>
        <translation>%1 %2 отказал загружать %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="175"/>
        <source>%1 %2 is sending to you the file: %3.</source>
        <translation>%1 %2 отсылает вам файл: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="245"/>
        <source>%1 An error occurred when %2 tries to add you to the group chat: %3.</source>
        <translation>%1 Ошибка произошла когда %2 попытался добавить вас в чат группы: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="251"/>
        <source>%1 %2 adds you to the group chat: %3.</source>
        <translation>%1 %2 добавляет вас в чат группы: %3.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="285"/>
        <source>%1 %2 has not shared files.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>%1 %2 has not file shared.</source>
        <translation type="obsolete">%1 %2 не содержит общедоступных файлов.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="290"/>
        <source>%1 %2 has shared %3 files.</source>
        <translation>%1 %2 открыл доступ к %3 файлам.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="319"/>
        <source>%1 %2 has refused to download folder %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="326"/>
        <source>unknown folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreParser.cpp" line="334"/>
        <source>%1 %2 is sending to you the folder: %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="86"/>
        <source>You are</source>
        <translation>Вы - </translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="88"/>
        <source>%1 is</source>
        <translation>%1 - </translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="101"/>
        <source>You have changed your nickname from %1 to %2.</source>
        <translation>Вы поменяли псевдоним с %1 на %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="103"/>
        <source>%1 has changed the nickname in %2.</source>
        <translation>%1 изменил псевдоним на %2.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="117"/>
        <source>The %1&apos;s profile has been received.</source>
        <translation>Профиль %1 получен.</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>You share this information</source>
        <translation>Вы делитесь этой информацией</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="131"/>
        <source>%1 shares this information</source>
        <translation>%1 делится этой информацией</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="235"/>
        <source>%1 You have created group from chat: %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="290"/>
        <source>%1 You have deleted group: %2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="377"/>
        <source>is removed from favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="383"/>
        <source>is added to favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreChat.cpp" line="83"/>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to you!</source>
        <translation>С днем рождения!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreUser.cpp" line="124"/>
        <source>Happy Birthday to %1!</source>
        <translation>С днем рождения %1!</translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="218"/>
        <source>%1 (%2) is disconnected from %3 network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/CoreConnection.cpp" line="338"/>
        <source>%1 (%2) is connected to %3 network.</source>
        <translation>%1 (%2) подключен к сети %3.</translation>
    </message>
    <message>
        <location filename="../src/core/Core.cpp" line="435"/>
        <source>%1 Network interface %2 is gone down.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>File</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="274"/>
        <source>Audio</source>
        <translation>Аудио</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="275"/>
        <source>Video</source>
        <translation>Видео</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="276"/>
        <source>Image</source>
        <translation>Изображение</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="277"/>
        <source>Document</source>
        <translation>Документ</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="279"/>
        <source>Executable</source>
        <translation>Исполняемый</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="280"/>
        <source>MacOSX</source>
        <translation>MacOSX</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="278"/>
        <source>Other</source>
        <translation>Другой</translation>
    </message>
</context>
<context>
    <name>FileTransferPeer</name>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="77"/>
        <source>invalid file header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="105"/>
        <source>Unable to open file</source>
        <translation>Невозможно открыть файл</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="112"/>
        <source>Unable to write in the file</source>
        <translation>Невозможно записать в файл</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferDownload.cpp" line="119"/>
        <source>%1 bytes downloaded but the file size is only %2 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="52"/>
        <source>Transfer cancelled</source>
        <translation>Передача отменена</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="113"/>
        <source>Transfer completed in %1</source>
        <translation>Передача завершена в %1</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="171"/>
        <source>Connection timeout</source>
        <translation>Вышло время соединения</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferPeer.cpp" line="179"/>
        <source>Transfer timeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="101"/>
        <source>unable to send file header</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="116"/>
        <source>%1 bytes uploaded but the file size is only %2 bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="123"/>
        <source>%1 bytes sent not confirmed (%2 bytes confirmed)</source>
        <translation>%1 байт передачи не подтверждено (%2 байт подтверждено)</translation>
    </message>
    <message>
        <location filename="../src/core/FileTransferUpload.cpp" line="154"/>
        <source>Unable to upload data</source>
        <translation>Невозможно отправить данные</translation>
    </message>
</context>
<context>
    <name>GuiAddUser</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="36"/>
        <source>Add user</source>
        <translation>Добавить пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="39"/>
        <source>your IP is %1 in LAN %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="45"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="104"/>
        <source>Please insert a valid IP address.</source>
        <translation>Пожалуйста, вставьте действительный IP-адрес.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="114"/>
        <source>Please insert a valid port or use the default one %1.</source>
        <translation>Пожалуйста, вставьте действительный порт или используйте по умолчанию %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="148"/>
        <source>These IP address and port are already inserted in list.</source>
        <translation>Эти IP-адреса и порты уже вставлены в список.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="182"/>
        <source>Remove user path</source>
        <translation>Убрать путь пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="184"/>
        <source>Clear all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="209"/>
        <source>Please select an user path in the list.</source>
        <translation>Пожалуйста, выберите путь пользователя в списке.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.cpp" line="234"/>
        <source>auto added</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiAddUserDialog</name>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="73"/>
        <source>Add an IP address and port of the user you want to connect</source>
        <translation>Добавьте IP-адрес и порт пользователя, с которым вы хотите соединиться</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="89"/>
        <source>IP Address</source>
        <translation>IP-адрес</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="121"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="169"/>
        <source>Split in IPv4 addresses</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="179"/>
        <source>Add</source>
        <translation>Добавить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="176"/>
        <source>Click here to add user path</source>
        <translation>Нажмите здесь для добавления пути пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="156"/>
        <source>Comment</source>
        <translation>Комментарий</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="25"/>
        <source>Auto add from LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="45"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAddUser.ui" line="52"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>GuiAskPassword</name>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="62"/>
        <source>Use standard session (encrypted but authentication is not required)</source>
        <translation>Использовать стандартную сессию (зашифровано, но аутентификация не требуется)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="69"/>
        <source>Use authentication password (spaces are removed) *</source>
        <translation>Использовать пароль аутентификации (пробелы будут удалены) *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="153"/>
        <source>Remember password (not recommended)</source>
        <translation>Запомнить пароль (не рекомендуется)</translation>
    </message>
    <message>
        <source>Use default chat password</source>
        <translation type="obsolete">Использовать пароль чата по умолчанию</translation>
    </message>
    <message>
        <source>Please enter the chat password (spaces are removed) *</source>
        <translation type="obsolete">Пожалуйста, введите пароль для чата (пробелы будут удалены) *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="124"/>
        <source>* Password must be the same for all user you want to connect</source>
        <translation>* Пароль должен быть один и тот же для всех пользователей, с которыми вы хотите соединиться</translation>
    </message>
    <message>
        <source>* Password must be the same for all client you want to reach</source>
        <translation type="obsolete">* Пароль должен быть одинаковым для всех клиентов, с которыми вы хотите связаться</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="160"/>
        <source>Show this dialog at connection startup</source>
        <translation>Показывать этот диалог при начале соединения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="30"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.ui" line="23"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="33"/>
        <source>Chat Password - %1</source>
        <translation>Пароль чата - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiAskPassword.cpp" line="103"/>
        <source>Password is empty. Please enter a valid one (spaces are removed).</source>
        <translation>Пароль пустой. Пожалуйста введите хотя бы один символ (пробелы будут удалены).</translation>
    </message>
</context>
<context>
    <name>GuiChat</name>
    <message>
        <source>%1 Secure Mode</source>
        <translation type="obsolete">%1 Безопасный режим</translation>
    </message>
    <message>
        <source>Emoticons</source>
        <translation type="obsolete">Смайлики</translation>
    </message>
    <message>
        <source>Add your preferred emoticon to the message</source>
        <translation type="obsolete">Добавьте ваш предпрочтительный смайлик в сообщение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="139"/>
        <source>Change font style</source>
        <translation>Изменить стиль шрифта</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="140"/>
        <source>Select your favourite chat font style</source>
        <translation>Выберите ваш любимый стиль шрифта для чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="141"/>
        <source>Change font color</source>
        <translation>Изменить цвет шрифта</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="142"/>
        <source>Select your favourite font color for the chat messages</source>
        <translation>Выберите ваш любимый цвет шрифта для сообщений чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="143"/>
        <source>Change background color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="144"/>
        <source>Select your favourite background color for the chat window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="149"/>
        <source>Spell checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="151"/>
        <source>Word completer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="153"/>
        <source>Use Return key to send message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="165"/>
        <location filename="../src/desktop/GuiChat.cpp" line="1076"/>
        <source>Find text in chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="166"/>
        <source>Send file</source>
        <translation>Отправить файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="167"/>
        <source>Send a file to a user or a group</source>
        <translation>Отправить файл пользователю или группе</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="168"/>
        <source>Send folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="169"/>
        <source>Save chat</source>
        <translation>Сохранить чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="170"/>
        <source>Save the messages of the current chat to a file</source>
        <translation>Сохранить сообщения текущего чата в файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="171"/>
        <source>Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="229"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="415"/>
        <source>unread messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="964"/>
        <source>Spell checking is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="971"/>
        <location filename="../src/desktop/GuiChat.cpp" line="995"/>
        <source>There is not a valid dictionary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="988"/>
        <source>Word completer is enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="990"/>
        <source>Word completer is disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="1114"/>
        <source>%1 not found in chat.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="966"/>
        <source>Spell checking is disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create group chat</source>
        <translation type="obsolete">Создать группу чата</translation>
    </message>
    <message>
        <source>Create a group chat with two or more users</source>
        <translation type="obsolete">Создать группу чата с двумя или более пользователями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="177"/>
        <source>Create group from chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="178"/>
        <source>Create a group from this chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="297"/>
        <source>Last message %1</source>
        <translation>Последнее сообщение %1</translation>
    </message>
    <message>
        <source>Edit group chat</source>
        <translation type="obsolete">Редактировать группу чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="180"/>
        <source>Change the name of the group or add and remove users</source>
        <translation>Изменить имя группы или добавить и удалить пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="173"/>
        <source>Clear messages</source>
        <translation>Очистить сообщения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="174"/>
        <source>Clear all the messages of the chat</source>
        <translation>Очистить все сообщения чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="181"/>
        <location filename="../src/desktop/GuiChat.cpp" line="182"/>
        <source>Leave the group</source>
        <translation>Покинуть группу</translation>
    </message>
    <message>
        <source>Create group</source>
        <translation type="obsolete">Создать группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="145"/>
        <source>Filter message</source>
        <translation>Фильтр сообщений</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="146"/>
        <source>Select the message types which will be showed in chat</source>
        <translation>Выберите тип сообщений, которые будут показаны в чате</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="147"/>
        <source>Chat settings</source>
        <translation>Настройки чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="148"/>
        <source>Click to show the settings menu of the chat</source>
        <translation>Нажмите для отображения меню настроек чата</translation>
    </message>
    <message>
        <source>Create a group with two or more users</source>
        <translation type="obsolete">Создать группу с двумя и более пользователями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="179"/>
        <source>Edit group</source>
        <translation type="unfinished">Редактировать группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="223"/>
        <source>Copy to clipboard</source>
        <translation>Копировать в буфер обмена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="225"/>
        <source>Select All</source>
        <translation>Выбрать все</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="257"/>
        <source>Show only messages in default chat</source>
        <translation>Показать только сообщения в чате по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="395"/>
        <source>All Lan Users</source>
        <translation>Все пользователи сети</translation>
    </message>
    <message>
        <source>(You have left)</source>
        <translation type="obsolete">(Вы вышли)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="948"/>
        <source>Use key Return to send message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="950"/>
        <source>Use key Return to make a carriage return</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="411"/>
        <source>You</source>
        <translation>Вы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="158"/>
        <source>Members</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="159"/>
        <source>Show the members of the chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="437"/>
        <source>offline</source>
        <translation>отключены</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="445"/>
        <source>Show profile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="455"/>
        <source>Show members</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>Nobody</source>
        <translation>Никто</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="459"/>
        <source>and</source>
        <translation>и</translation>
    </message>
    <message>
        <source>To</source>
        <translation type="obsolete">В</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="537"/>
        <source>last %1 messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="706"/>
        <source>Please select a file to save the messages of the chat.</source>
        <translation>Пожалуйста, выберите файл для сохранения сообщений чата.</translation>
    </message>
    <message>
        <source>%1: unable to save the messages.
Please check the file or the directories write permissions.</source>
        <translation type="obsolete">%1: не удается сохранить сообщения.
Пожалуйста, проверьте разрешения на запись файла или папок.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>%1: save completed.</source>
        <translation>%1: сохранение завершено.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="720"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="771"/>
        <source>Unable to save temporary file: %1</source>
        <translation type="unfinished">Не удается сохранить временный файл: %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="819"/>
        <source>Do you really want to send %1 %2 to the members of this chat?</source>
        <translation>Вы дейсвительно хотите отправить %1 %2 членам этого чата?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>file</source>
        <translation>файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="820"/>
        <source>files</source>
        <translation>файлы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="821"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.cpp" line="834"/>
        <source>Qt library for this OS doesn&apos;t support Drag and Drop for files. You have to select again the file to send.</source>
        <translation>Библиотека Qt для этой ОС не поддерживает перетаскивание для файлов. Вы должны выбрать снова файл для отправки.</translation>
    </message>
</context>
<context>
    <name>GuiChatList</name>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="49"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="52"/>
        <source>Clear</source>
        <translation>Очистить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="53"/>
        <source>Clear all chat messages</source>
        <translation>Очистить все сообщения чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatList.cpp" line="55"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
</context>
<context>
    <name>GuiChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="20"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="238"/>
        <source>Write to: &lt;b&gt;ALL&lt;/b&gt;</source>
        <translation>Написать: &lt;b&gt;ВСЕМ&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="260"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="288"/>
        <source>Save window&apos;s geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="326"/>
        <source>Detach chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChat.ui" line="136"/>
        <source>Click to send message or just hit enter</source>
        <translation>Нажмите для отправки сообщения или просто нажмите Enter</translation>
    </message>
</context>
<context>
    <name>GuiCreateGroup</name>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="26"/>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="42"/>
        <source>TextLabel</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="74"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.ui" line="81"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="37"/>
        <source>Group name</source>
        <translation>Имя группы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="38"/>
        <source>Please add member in the group:</source>
        <translation>Пожалуйста, добавьте члена в группу:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="41"/>
        <source>Users</source>
        <translation>Пользователи</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="65"/>
        <source>Create Group - %1</source>
        <translation>Создать группу - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="67"/>
        <source>Create Chat - %1</source>
        <translation>Создать чат - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="73"/>
        <source>Edit Group - %1</source>
        <translation>Редактировать группу - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="75"/>
        <source>Edit Chat - %1</source>
        <translation>Редактировать чат - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="113"/>
        <source>Please select two or more member for the group.</source>
        <translation>Пожалуйста, выберите два или более члена для группы.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="121"/>
        <source>Please insert a group name.</source>
        <translation>Пожалуйста, вставьте имя группы.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiCreateGroup.cpp" line="130"/>
        <source>%1 already exists as group name or chat name.
Please select a different name.</source>
        <translation>%1 уже существует как имя группы или имя чата.
Пожалуйста, выберите другое имя.</translation>
    </message>
</context>
<context>
    <name>GuiEditVCard</name>
    <message>
        <source>Profile - %1</source>
        <translation type="obsolete">Профиль - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="35"/>
        <source>Edit your profile</source>
        <translation>Редактировать ваш профиль</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="103"/>
        <source>%1 - Select your profile photo</source>
        <translation>%1 - Выберите ваше фото профиля</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="104"/>
        <source>Images</source>
        <translation>Изображения</translation>
    </message>
    <message>
        <source>Images (*.png *.xpm *.jpg *.jpeg)</source>
        <translation type="obsolete">Изображения (*.png *.xpm *.jpg *.jpeg)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <source>Unable to load image %1.</source>
        <translation>Не удается загрузить изображение %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="116"/>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="147"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.cpp" line="156"/>
        <source>Please insert your nickname.</source>
        <translation>Пожалуйста вставьте ваш псевдоним.</translation>
    </message>
</context>
<context>
    <name>GuiEmoticons</name>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="49"/>
        <source>Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="53"/>
        <source>Smiley</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="57"/>
        <source>Objects</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="61"/>
        <source>Nature</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="65"/>
        <source>Places</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEmoticons.cpp" line="69"/>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiFileInfoList</name>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Shared folders and files</source>
        <translation>Общедоступные папки и файлы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFileInfoList.cpp" line="42"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
</context>
<context>
    <name>GuiFloatingChat</name>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="43"/>
        <source>Show the bar of chat</source>
        <translation type="unfinished">Показать полосу чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="51"/>
        <source>Emoticons</source>
        <translation type="unfinished">Смайлики</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="63"/>
        <source>Show the emoticon panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="64"/>
        <source>Add your preferred emoticon to the message</source>
        <translation type="unfinished">Добавьте ваш предпрочтительный смайлик в сообщение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiFloatingChat.cpp" line="244"/>
        <source>Window&apos;s geometry and state saved</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiGroupList</name>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="48"/>
        <source>Create group</source>
        <translation>Создать группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="51"/>
        <source>Edit group</source>
        <translation>Редактировать группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="54"/>
        <source>Open chat</source>
        <translation>Открыть чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="57"/>
        <source>Enable notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="60"/>
        <source>Disable notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="63"/>
        <source>Delete group</source>
        <translation>Удалить группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupList.cpp" line="143"/>
        <source>Waiting for two or more connected user</source>
        <translation>Ожидание для двух или более подключенных пользователей</translation>
    </message>
</context>
<context>
    <name>GuiHome</name>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="39"/>
        <source>Home</source>
        <translation>Главная</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="49"/>
        <source>Select a user you want to chat with or</source>
        <translation>Выберите пользователя, с которым вы хотите общаться,  или</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="94"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Копировать в буфер обмена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="96"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="98"/>
        <source>Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="105"/>
        <source>Show the datestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.cpp" line="109"/>
        <source>Show the timestamp</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiHomeWidget</name>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>Show this page at startup</source>
        <translation type="obsolete">Показать эту страницу при запуске</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiHome.ui" line="107"/>
        <source>chat with all users</source>
        <translation>общаться со всеми пользователями</translation>
    </message>
</context>
<context>
    <name>GuiLanguage</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="35"/>
        <source>Select language</source>
        <translation>Выбор языка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="38"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="50"/>
        <source>For the latest language translations please visit the %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="51"/>
        <source>official website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="94"/>
        <source>Select a language folder</source>
        <translation>Выберите папку языка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.cpp" line="121"/>
        <source>Language &apos;%1&apos;&apos; not found.</source>
        <translation>Язык &apos;%1&apos;&apos; не найден.</translation>
    </message>
</context>
<context>
    <name>GuiLanguageDialog</name>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="17"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="45"/>
        <source>Language</source>
        <translation>Язык</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="96"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="127"/>
        <source>Select language folder</source>
        <translation>Выберите папку языка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="169"/>
        <source>Restore to default language</source>
        <translation>Восстановить язык по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="172"/>
        <source>Restore</source>
        <translation>Восстановить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="195"/>
        <source>Select</source>
        <translation>Выбор</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLanguage.ui" line="205"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>GuiLife</name>
    <message>
        <source>Paused (press space bar to continue)</source>
        <translation type="obsolete">На паузе (нажмите пробел для продолжения)</translation>
    </message>
    <message>
        <source>Alive: %1 - Died: %2 - Spaces: %3 - Step: %4</source>
        <translation type="obsolete">Живы: %1 - Умерло: %2 - Свободны: %3 - Шаг: %4</translation>
    </message>
    <message>
        <source>BeeLife</source>
        <translation type="obsolete">Bee Жизнь</translation>
    </message>
    <message>
        <source>Do you really want to randomize and restart?</source>
        <translation type="obsolete">Вы действительно хотите перемешать и перезапустить?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Do you really want to clear all?</source>
        <translation type="obsolete">Вы действительно хотите очистить все?</translation>
    </message>
    <message>
        <source>??? Evolution Completed ??? ... (or press space bar to continue)</source>
        <translation type="obsolete">??? Эволюция завершена ??? ... (или нажмите пробел для продолжения)</translation>
    </message>
</context>
<context>
    <name>GuiLifeWidget</name>
    <message>
        <source>Form</source>
        <translation type="obsolete">Форма</translation>
    </message>
    <message>
        <source>Press space bar to start</source>
        <translation type="obsolete">Нажмите пробел для запуска</translation>
    </message>
    <message>
        <source>BeeLife</source>
        <translation type="obsolete">Bee Жизнь</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="obsolete">Перезапустить</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation type="obsolete">Очистить</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="obsolete">Быстро</translation>
    </message>
    <message>
        <source>Slow</source>
        <translation type="obsolete">Медленно</translation>
    </message>
</context>
<context>
    <name>GuiLog</name>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="36"/>
        <source>System Log</source>
        <translation>Системный отчет</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="55"/>
        <source>Save log as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="56"/>
        <source>Save the log in a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="72"/>
        <source>Search</source>
        <translation type="unfinished">Поиск</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="78"/>
        <source>keyword</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="84"/>
        <source>Find</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="85"/>
        <source>Find keywords in the log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="97"/>
        <source>Case sensitive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="102"/>
        <source>Whole word</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="60"/>
        <source>Log to file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="109"/>
        <source>Block scrolling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="116"/>
        <source>Please select a file to save the log.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <source>Unable to save log in the file: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="125"/>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="151"/>
        <source>%1: save log completed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="152"/>
        <source>Open folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiLog.cpp" line="193"/>
        <source>%1 not found</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiLogWidget</name>
    <message>
        <location filename="../src/desktop/GuiLog.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>GuiMain</name>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="86"/>
        <source>Show the main tool bar</source>
        <translation>Показать главную панель инструментов</translation>
    </message>
    <message>
        <source>Show the bar of plugins</source>
        <translation type="obsolete">Показать панель расширений</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="283"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3291"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3292"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3293"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3294"/>
        <source>offline</source>
        <translation>отключен</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="326"/>
        <source>Do you really want to quit %1?</source>
        <translation>Вы действительно хотите выйти из %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="414"/>
        <source>No new message available</source>
        <translation>Новых сообщений нет</translation>
    </message>
    <message>
        <source>&amp;Disconnect</source>
        <translation type="obsolete">&amp;Отключить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="498"/>
        <source>Disconnect from %1 network</source>
        <translation>Отключить от сети %1</translation>
    </message>
    <message>
        <source>&amp;Connect</source>
        <translation type="obsolete">&amp;Подключить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="504"/>
        <source>Connect to %1 network</source>
        <translation>Подключить к сети %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="587"/>
        <source>Secure Lan Messenger</source>
        <translation>Безопасный сетевой мессенжер</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="588"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br&gt;it under the terms of the GNU General Public License as published&lt;br&gt;by the Free Software Foundation, either version 3 of the License&lt;br&gt;or (at your option) any later version.&lt;br&gt;&lt;br&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br&gt;See the GNU General Public License for more details.</source>
        <translation type="obsolete">BeeBEEP - это бесплатное приложение: вы можете распространять его и/или модифицировать&lt;br&gt;его по лицензии GNU General Public License опубликованной&lt;br&gt;сообществом бесплатного программного обеспечения (Free Software Foundation), или по версии 3 лицензии&lt;br&gt;или (на ваше усмотрение) любой более поздней версии.&lt;br&gt;&lt;br&gt;BeeBEEP распространяется в надежде, что оно будет полезно,&lt;br&gt;но БЕЗ ЛЮБЫХ ГАРАНТИЙ; даже без подразумеваемых гарантий&lt;br&gt;ПРИГОДНОСТИ или СООТВЕТСТВИЯ СПЕЦИФИЧЕСКИМ ТРЕБОВАНИЯМ.&lt;br&gt;Детали см. лицензию GNU General Public License.

Оригинал текста:
BeeBEEP is free software: you can redistribute it and/or modify&lt;br&gt;it under the terms of the GNU General Public License as published&lt;br&gt;by the Free Software Foundation, either version 3 of the License&lt;br&gt;or (at your option) any later version.&lt;br&gt;&lt;br&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br&gt;See the GNU General Public License for more details.</translation>
    </message>
    <message>
        <source>Add or search &amp;user...</source>
        <translation type="obsolete">&amp;Добавить или искать пользователя...</translation>
    </message>
    <message>
        <source>Add or search an user in the %1 network</source>
        <translation type="obsolete">Добавить или искать пользователя в сети %1</translation>
    </message>
    <message>
        <source>&amp;Quit</source>
        <translation type="obsolete">Вы&amp;ход</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="623"/>
        <source>Close the chat and quit %1</source>
        <translation>Закрыть чат и выйти из %1</translation>
    </message>
    <message>
        <source>Profile...</source>
        <translation type="obsolete">Профиль...</translation>
    </message>
    <message>
        <source>Change your profile data</source>
        <translation type="obsolete">Изменить данные вашего профиля</translation>
    </message>
    <message>
        <source>Show the tool bar with plugin shortcuts</source>
        <translation type="obsolete">Показать панель инструментов с ярлыками расширений</translation>
    </message>
    <message>
        <source>&amp;About %1...</source>
        <translation type="obsolete">&amp;О программе %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="635"/>
        <source>Show the informations about %1</source>
        <translation>Показать сведения о %1</translation>
    </message>
    <message>
        <source>&amp;Main</source>
        <translation type="obsolete">&amp;Главная</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="669"/>
        <source>Download folder...</source>
        <translation>Папка загрузки...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="670"/>
        <source>Select the download folder</source>
        <translation>Выберите папку загрузки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="677"/>
        <source>Select beep file...</source>
        <translation>Выберите звуковой файл...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="678"/>
        <source>Select the file to play on new message arrived</source>
        <translation>Выберите файл для воспроизведения после поступлении нового сообщения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="679"/>
        <source>Play beep</source>
        <translation>Воспроизвести звук</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="680"/>
        <source>Test the file to play on new message arrived</source>
        <translation>Протестировать файл, воспроизводимый после поступления нового сообщения</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="obsolete">&amp;Настройки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="933"/>
        <source>Show only the online users</source>
        <translation>Показать только активных пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="934"/>
        <source>If enabled only the online users are shown in the list</source>
        <translation>Если включено -  только активные пользователи будут отображены в списке</translation>
    </message>
    <message>
        <source>Beep on new message arrived</source>
        <translation type="obsolete">Сигнал на новое сообщение получен</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="881"/>
        <source>If enabled when a new message is arrived a sound is emitted</source>
        <translation>Если включено - будет воспроизведен звук после получения нового сообщения</translation>
    </message>
    <message>
        <source>Raise on new message arrived</source>
        <translation type="obsolete">Показать новое сообщение поверх</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="902"/>
        <source>If enabled when a new message is arrived %1 is shown on top of all other windows</source>
        <translation>Если включено - окно нового сообщения %1 будет показано поверх остальных окон</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="844"/>
        <source>Generate automatic filename</source>
        <translation>Генерировать автоматические имена файлов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="845"/>
        <source>If the file to be downloaded already exists a new filename is automatically generated</source>
        <translation>Если загруженный файл уже существует, то ему автоматически будет присвоено новое имя</translation>
    </message>
    <message>
        <source>Close to tray icon</source>
        <translation type="obsolete">Закрыть в область уведомлений</translation>
    </message>
    <message>
        <source>If enabled when the close button is clicked the window minimized to the system tray icon</source>
        <translation type="obsolete">Если включено - при нажатии кнопки закрыть окно будет свернуто в системную область уведомлений</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1094"/>
        <source>Enable tray icon notification</source>
        <translation>Включить уведомления в значке области уведомлений</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1095"/>
        <source>If enabled tray icon shows some notification about status and message</source>
        <translation>Если включено - значок в области уведомлений сообщает о статусе и сообщениях</translation>
    </message>
    <message>
        <source>Stay on top</source>
        <translation type="obsolete">Держать поверх</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="908"/>
        <source>If enabled %1 stays on top of the other windows</source>
        <translation>Если включено - %1 остается поверх других окон</translation>
    </message>
    <message>
        <source>Enable file sharing</source>
        <translation type="obsolete">Включить общий доступ</translation>
    </message>
    <message>
        <source>If enabled you can share files with the other users</source>
        <translation type="obsolete">Если включено - вы можете делиться файлами с другими пользователями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="790"/>
        <source>Prompts for network password on startup</source>
        <translation>Запрашивать сетевой пароль при старте</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="791"/>
        <source>If enabled the password dialog will be shown on connection startup</source>
        <translation>Если включено - диалог пароля будет показан при начале соединения</translation>
    </message>
    <message>
        <source>Set away status when idle</source>
        <translation type="obsolete">Устанавливать статус вышел при простое</translation>
    </message>
    <message>
        <source>If enabled %1 set your status to away after an idle of %2 minutes</source>
        <translation type="obsolete">Если включено - %1 устанавливает ваш статус в вышел после простоя в течение %2 минут</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="820"/>
        <source>Load %1 on Windows startup</source>
        <translation>Загружать %1 при старте Windows</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="821"/>
        <source>If enabled you can automatically load %1 at system startup</source>
        <translation>Если включено - вы можете автоматически загружать %1 при старте системы</translation>
    </message>
    <message>
        <source>&amp;Chat</source>
        <translation type="obsolete">&amp;Чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="709"/>
        <source>Enable the compact mode in chat window</source>
        <translation>Включить компактный режим в окне чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="710"/>
        <source>If enabled the sender&apos;s nickname and his message are in the same line</source>
        <translation>Если включено - псевдоним отправителя и его сообщение выводятся в одной строке</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="715"/>
        <source>Add a blank line between the messages</source>
        <translation>Добавлять пустую строку между сообщениями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="716"/>
        <source>If enabled the messages in the chat window are separated by a blank line</source>
        <translation>Если включено - сообщения в чате будут разделены пустой строкой</translation>
    </message>
    <message>
        <source>Show the messages&apos; timestamp</source>
        <translation type="obsolete">Показать метку времени сообщения </translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="722"/>
        <source>If enabled the message shows its timestamp in the chat window</source>
        <translation>Если включено - сообщение отображает метку времени в окне чата</translation>
    </message>
    <message>
        <source>Show the user&apos;s colors</source>
        <translation type="obsolete">Показать цвета пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="704"/>
        <source>If enabled the user&apos;s nickname in chat and in list is colored</source>
        <translation>Если включено - псевдоним пользователя в чате и в списке окрашивается</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="770"/>
        <source>Use HTML tags</source>
        <translation>Использовать теги HTML</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="771"/>
        <source>If enabled HTML tags are not removed from the message</source>
        <translation>Если включено - теги HTML не удаляются из сообщения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="776"/>
        <source>Use clickable links</source>
        <translation>Использовать нажимаемые ссылки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="777"/>
        <source>If enabled the links in the message are recognized and made clickable</source>
        <translation>Если включено - ссылки в сообщения распознаются и делаются нажимаемыми</translation>
    </message>
    <message>
        <source>Show emoticons</source>
        <translation type="obsolete">Показать смайлики</translation>
    </message>
    <message>
        <source>If enabled the emoticons will be recognized and shown as images</source>
        <translation type="obsolete">Если включено - смайлики будут распознаны и показаны как изображения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="751"/>
        <source>Show messages grouped by user</source>
        <translation>Показать сообщения, сгруппированные по пользователю</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="752"/>
        <source>If enabled the messages will be shown grouped by user</source>
        <translation>Если включено - сообщения будут показаны, сгруппированные по пользователю</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="695"/>
        <source>Save messages</source>
        <translation>Сохранить сообщения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="366"/>
        <source>&lt;b&gt;Chat messages can not be saved&lt;/b&gt;. Path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="497"/>
        <source>Disconnect</source>
        <translation>Разъединить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="503"/>
        <source>Connect</source>
        <translation>Соединить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="618"/>
        <source>Configure %1 network to search a user who is not in your local subnet</source>
        <translation>Настроить сеть %1 для поиска пользователя, который не в вашей локальной подсети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="621"/>
        <source>Quit</source>
        <translation>Выйти</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="626"/>
        <source>Edit your profile...</source>
        <translation>Редактировать ваш профиль...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="627"/>
        <source>Change your profile information like your picture or your email or phone number</source>
        <translation>Изменить информацию о вашем профиле такую как ваше фото или ваш электронный адрес или номер телефона</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="634"/>
        <source>About %1...</source>
        <translation>О программе %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="638"/>
        <source>Create chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="639"/>
        <source>Create a chat with two or more users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="642"/>
        <source>Create group</source>
        <translation>Создать группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="643"/>
        <source>Create a group with two or more users</source>
        <translation>Создать группу с двумя и более пользователями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="653"/>
        <source>Main</source>
        <translation>Главная</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="659"/>
        <source>Broadcast to network</source>
        <translation>Разослать в сеть</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="660"/>
        <source>Broadcast a message in your network to find available users</source>
        <translation>Разослать сообщение в вашей сети для поиска доступных пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="673"/>
        <source>Dictionary...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="674"/>
        <source>Select your preferred dictionary for spell checking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="684"/>
        <source>Open your resource folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="685"/>
        <source>Click to open your resource folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="687"/>
        <source>Open your data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="688"/>
        <source>Click to open your data folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="721"/>
        <source>Show the timestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="727"/>
        <source>Show the datestamp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="728"/>
        <source>If enabled the message shows its datestamp in the chat window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="733"/>
        <source>Show preview of the images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="734"/>
        <source>If enabled the preview of the downloaded images will be showed in the chat window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="739"/>
        <source>Parse Unicode and ASCII emoticons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="740"/>
        <source>If enabled the ASCII emoticons will be recognized and shown as images</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="745"/>
        <source>Use native emoticons</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="746"/>
        <source>If enabled the emoticons will be parsed by your system font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="763"/>
        <source>Use your name instead of &apos;You&apos;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="785"/>
        <source>Prompts for nickname on startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="814"/>
        <source>Check for new version at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="840"/>
        <source>If the file to be downloaded already exists it is automatically overwritten</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="849"/>
        <source>Ask me</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="884"/>
        <source>When the chat is not visible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="892"/>
        <source>Never</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="951"/>
        <source>Show status color in background</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="952"/>
        <source>If enabled the user in list has colored backrgound as status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="978"/>
        <source>Recently used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="983"/>
        <source>Change your status description...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="984"/>
        <source>Clear all status descriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="996"/>
        <source>Save main window geometry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1018"/>
        <source>Show new message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1048"/>
        <source>Add +1 user to anonymous usage statistics</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1049"/>
        <source>Help me to know how many users have BeeBEEP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1100"/>
        <source>Show only message notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1101"/>
        <source>If enabled tray icon shows only message notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1106"/>
        <source>Show chat message preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1639"/>
        <source>Show only last %1 messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1640"/>
        <source>If enabled only the last %1 messages will be shown in chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1954"/>
        <source>File transfer is not enabled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1960"/>
        <source>You are not connected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3982"/>
        <source>Window geometry and state saved</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="783"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="802"/>
        <source>Show minimized at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="803"/>
        <source>If enabled %1 is showed minimized at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="858"/>
        <source>Prompt before downloading file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="859"/>
        <source>If enabled you have to confirm the action before downloading a file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="808"/>
        <source>Reset window geometry at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="703"/>
        <source>Show colored nickname</source>
        <translation>Показать окрашенный псевдоним</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="796"/>
        <source>Show activities home page at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="797"/>
        <source>If enabled the activities home page instead of chat page will be showed at startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="923"/>
        <source>Options</source>
        <translation>Параметры</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="939"/>
        <source>Show the user&apos;s picture</source>
        <translation>Показывать фото пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="693"/>
        <source>Chat</source>
        <translation>Чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="696"/>
        <source>If enabled the messages are saved when the program is closed</source>
        <translation>Если включено - сообщения будут сохранены когда программа закрыта</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="663"/>
        <source>Add users manually...</source>
        <translation>Добавить пользователей вручную...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="664"/>
        <source>Add the IP address and the port of the users you want to connect</source>
        <translation>Добавить IP-адрес и порт пользователей, с которыми вы хотите соединиться</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="880"/>
        <source>Enable BEEP alert on new message</source>
        <translation>Добавить звуковой сигнал на новое сообщение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="901"/>
        <source>Raise on top on new message</source>
        <translation>Показывать на переднем плане новое сообщение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="907"/>
        <source>Always stay on top</source>
        <translation>Всегда поверх остальных</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1076"/>
        <source>Load on system tray at startup</source>
        <translation>Загружать в область уведомлений при старте</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1077"/>
        <source>If enabled %1 will be start hided in system tray</source>
        <translation>Если включено - %1 будет запускаться скрытым в области  уведомлений</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="995"/>
        <source>View</source>
        <translation>Вид</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1025"/>
        <source>?</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1026"/>
        <source>Tip of the day</source>
        <translation>Подсказка дня</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1028"/>
        <source>Fact of the day</source>
        <translation>Факт дня</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1029"/>
        <source>Show me the fact of the day</source>
        <translation>Показать мне факт дня</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1034"/>
        <source>Qt Library...</source>
        <translation>Библиотека Qt...</translation>
    </message>
    <message>
        <source>Please donate for %1 :-)</source>
        <translation type="obsolete">Пожалуйста поддержите %1 :-)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1055"/>
        <source>I&apos;m so grateful and pleased about that</source>
        <translation>Я так благодарен и признателен вам за это</translation>
    </message>
    <message>
        <source>Show online users</source>
        <translation type="obsolete">Показать активных пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1172"/>
        <source>Show the list of the connected users</source>
        <translation>Показать список подключенных пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1175"/>
        <source>Groups</source>
        <translation>Группы</translation>
    </message>
    <message>
        <source>Show your groups</source>
        <translation type="obsolete">Показать ваши группы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1184"/>
        <source>Show the list of your groups</source>
        <translation>Показать список ваших групп</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1195"/>
        <source>Show the chat panel</source>
        <translation>Показать панель чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1207"/>
        <source>Show the history panel</source>
        <translation>Показать панель истории</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1219"/>
        <source>Show the file transfer panel</source>
        <translation>Показать панель передачи файлов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1223"/>
        <source>Emoticons</source>
        <translation type="unfinished">Смайлики</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1235"/>
        <source>Show the emoticon panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1236"/>
        <source>Add your preferred emoticon to the message</source>
        <translation type="unfinished">Добавьте ваш предпрочтительный смайлик в сообщение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1268"/>
        <source>Show the bar of chat</source>
        <translation>Показать полосу чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1303"/>
        <source>Show the bar of log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="888"/>
        <source>Always</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="836"/>
        <source>If a file already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1507"/>
        <source>How many minutes of idle %1 can wait before changing status to away?</source>
        <translation>Сколько минут %1 может простаивать перед изменением статуса на вышел?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1543"/>
        <source>Please select the maximum number of messages to be showed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>%1 - Select a file</source>
        <translation>%1 - Выберите файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1935"/>
        <source>or more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2009"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2159"/>
        <source>File transfer is disabled. You cannot download %1.</source>
        <translation>Передача файлов выключена. Вы не можете загрузить %1.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2102"/>
        <source>You cannot download all these files at once. Do you want to download the first %1 files of the list?</source>
        <translation>Вы не можете загрузить все файлы за раз. Вы хотите загрузить первые %1 файлов из списка?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2110"/>
        <source>Downloading %1 files is a hard duty. Maybe you have to wait a lot of minutes. Do yo want to continue?</source>
        <translation>С трудом загружается %1 файлов. Возможно вам придется подождать несколько минут. Вы хотите продолжить?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2128"/>
        <source>%1 files are scheduled for download</source>
        <translation>%1 файлов запланировано для загрузки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2143"/>
        <source>File is not available for download.</source>
        <translation>Файл не доступен для загрузки.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2145"/>
        <source>%1 is not connected.</source>
        <translation>%1 не подключен.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2146"/>
        <source>Please reload the list of shared files.</source>
        <translation>Пожалуйста, перезагрузите список общедоступных файлов.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <source>Reload file list</source>
        <translation>Перезагрузить список файлов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2167"/>
        <source>Do you want to download folder %1 (%2 files) from %3?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2389"/>
        <source>Show the bar of games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2691"/>
        <source>The default BEEP will be used</source>
        <translation>Будет использован звуковой сигнал по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2737"/>
        <source>Group chat will be deleted when all members goes offline.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2738"/>
        <source>If you want a persistent chat please consider to make a Group instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2739"/>
        <source>Do you wish to continue or create group?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Continue</source>
        <translation type="unfinished">Продолжение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <source>Create Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2766"/>
        <source>Unable to add users in this chat. Please select a group one.</source>
        <translation>Не удается добавить пользователей в этот чат. Пожалуйста выберите группу.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2815"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2822"/>
        <source>Unable to add this key in the registry: permission denied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3037"/>
        <source>All the members of this chat are not online. The changes may not be permanent. Do you wish to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3122"/>
        <source>Chat with %1 is empty.</source>
        <translation>Чат с %1 пустой.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3126"/>
        <source>Do you really want to clear messages with %1?</source>
        <translation>Вы действительно хотите очистить сообщения %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3129"/>
        <source>Yes and delete history</source>
        <translation>Да и удалить историю</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3296"/>
        <source>inactive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3323"/>
        <source>active</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3410"/>
        <source>Please select the new size of the user picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3589"/>
        <source>at lunch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3598"/>
        <source>in a meeting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3634"/>
        <source>Do you really want to clear all saved status descriptions?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3811"/>
        <source>Select your dictionary path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3819"/>
        <source>Dictionary selected: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3821"/>
        <source>Unable to set dictionary: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3059"/>
        <source>%1 is a your group. You can not leave the chat.</source>
        <translation>%1 - это ваша группа. Вы не можете покинуть чат.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Delete this group</source>
        <translation>Удалить эту группу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3084"/>
        <source>You cannot leave this chat.</source>
        <translation>Вы не можете покинуть этот чат.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3096"/>
        <source>Do you really want to delete group &apos;%1&apos;?</source>
        <translation>Вы действительно хотите удалить группу &apos;%1&apos;?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3197"/>
        <source>Unable to delete this chat.</source>
        <translation>Не удается удалить этот чат.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3219"/>
        <source>%1 has shared %2 files</source>
        <translation>%1 открыл общий доступ к %2 файлам</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3316"/>
        <source>disabled</source>
        <translation>выключен</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3396"/>
        <source>%1 is online</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3398"/>
        <source>%1 is offline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="965"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="349"/>
        <source>&lt;b&gt;Settings can not be saved&lt;/b&gt;. Path:</source>
        <translation>&lt;b&gt;Настройки не могут быть сохранены&lt;/b&gt;. Путь:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="351"/>
        <location filename="../src/desktop/GuiMain.cpp" line="368"/>
        <source>&lt;b&gt;is not writable&lt;/b&gt; by user:</source>
        <translation>&lt;b&gt;не перезаписываемый&lt;/b&gt; пользователем:</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="353"/>
        <location filename="../src/desktop/GuiMain.cpp" line="370"/>
        <source>Do you want to close anyway?</source>
        <translation>Вы хотите закрыть в любом случае?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="590"/>
        <source>for</source>
        <translation>для</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="592"/>
        <source>developed by</source>
        <translation>разработано</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="601"/>
        <source>BeeBEEP is free software: you can redistribute it and/or modify&lt;br /&gt;it under the terms of the GNU General Public License as published&lt;br /&gt;by the Free Software Foundation, either version 3 of the License&lt;br /&gt;or (at your option) any later version.&lt;br /&gt;&lt;br /&gt;BeeBEEP is distributed in the hope that it will be useful,&lt;br /&gt;but WITHOUT ANY WARRANTY; without even the implied warranty&lt;br /&gt;of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.&lt;br /&gt;See the GNU General Public License for more details.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="617"/>
        <source>Search for users...</source>
        <translation>Поиск для пользователей...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="667"/>
        <source>Select language...</source>
        <translation>Выбрать язык...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="668"/>
        <source>Select your preferred language</source>
        <translation>Выберите ваш предпочтительный язык</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="940"/>
        <source>If enabled you can see a picture of the users in the list (if they have)</source>
        <translation>Если включено - вы будете видеть фото пользователей в списке (если они его имеют)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="829"/>
        <source>Enable file transfer</source>
        <translation>Включить передачу файлов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="830"/>
        <source>If enabled you can transfer files with the other users</source>
        <translation>Если включено - вы можете обмениваться файлами с другими пользователями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="966"/>
        <source>Select your status</source>
        <translation>Выберите ваш статус</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="972"/>
        <location filename="../src/desktop/GuiMain.cpp" line="988"/>
        <source>Your status will be %1</source>
        <translation>Ваш статус будет %1</translation>
    </message>
    <message>
        <source>Add a status description...</source>
        <translation type="obsolete">Добавить описание статуса...</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="obsolete">&amp;Вид</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1008"/>
        <source>Show the chat</source>
        <translation>Показать чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1009"/>
        <source>Show the chat view</source>
        <translation>Показать вид чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1010"/>
        <source>Show my shared files</source>
        <translation>Показать мои общедоступные файлы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1011"/>
        <source>Show the list of the files which I have shared</source>
        <translation>Показать список файлов, к которым я открыл общий доступ</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1012"/>
        <source>Show the network shared files</source>
        <translation>Показать общедоступные файлы в сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1013"/>
        <source>Show the list of the network shared files</source>
        <translation>Показать список общедоступных файлов в сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1022"/>
        <source>Plugins</source>
        <translation>Расширения</translation>
    </message>
    <message>
        <source>&amp;?</source>
        <translation type="obsolete">&amp;Справка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1027"/>
        <source>Show me the tip of the day</source>
        <translation>Показать мне подсказку дня</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1032"/>
        <source>Show %1&apos;s license...</source>
        <translation>Показать лицензию %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1033"/>
        <source>Show the informations about %1&apos;s license</source>
        <translation>Показать информацию о лицензии %1</translation>
    </message>
    <message>
        <source>About &amp;Qt...</source>
        <translation type="obsolete">О библиотеке &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1035"/>
        <source>Show the informations about Qt library</source>
        <translation>Показать информацию о библиотеке Qt</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1037"/>
        <source>Open %1 official website...</source>
        <translation>Открыть официальный вебсайт %1...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1038"/>
        <source>Explore %1 official website</source>
        <translation>Обзор официального вебсайта %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1039"/>
        <source>Check for new version...</source>
        <translation>Проверить новую версию...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1040"/>
        <source>Open %1 website and check if a new version exists</source>
        <translation>Открыть вебсайт %1 и проверить есть ли новая версия</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1041"/>
        <source>Download plugins...</source>
        <translation>Загрузить расширения...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1042"/>
        <source>Open %1 website and download your preferred plugin</source>
        <translation>Открыть вебсайт %1  и загрузить ваше предпочтительное расширение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1043"/>
        <source>Help online...</source>
        <translation>Справка в сети...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1044"/>
        <source>Open %1 website to have online support</source>
        <translation>Открыть вебсайт %1 для получения онлайн поддержки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1059"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <source>Used Ports</source>
        <translation type="obsolete">Использованы порты</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="obsolete">Закрыть</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="181"/>
        <source>Ready</source>
        <translation>Готов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1163"/>
        <source>Users</source>
        <translation>Пользователи</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="866"/>
        <source>Set status to away automatically</source>
        <translation>Устанавливать статус вышел автоматически</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="867"/>
        <source>If enabled %1 change your status to away after an idle of %2 minutes</source>
        <translation>Если включено - %1 изменяет ваш статус на вышел после простоя в течение %2 минут</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1187"/>
        <source>Chats</source>
        <translation>Чаты</translation>
    </message>
    <message>
        <source>Show the chat list</source>
        <translation type="obsolete">Показать список чатов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1196"/>
        <source>Show the list of the chats</source>
        <translation>Показать список чатов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1199"/>
        <source>History</source>
        <translation>История</translation>
    </message>
    <message>
        <source>Show the saved chat list</source>
        <translation type="obsolete">Показать сохраненный список чатов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1208"/>
        <source>Show the list of the saved chats</source>
        <translation>Показать список сохраненных чатов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1211"/>
        <source>File Transfers</source>
        <translation>Передача файлов</translation>
    </message>
    <message>
        <source>Show the file transfers</source>
        <translation type="obsolete">Показать передачу файлов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1220"/>
        <source>Show the list of the file transfers</source>
        <translation>Показать список передач файлов</translation>
    </message>
    <message>
        <source>Show the chat tool bar</source>
        <translation type="obsolete">Показать панель чата</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1317"/>
        <source>Show the bar of screenshot plugin</source>
        <translation>Показать панель расширения снимка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1342"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2406"/>
        <source>Play %1</source>
        <translation>Играть %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1346"/>
        <source>is a game developed by</source>
        <translation>игра разработана</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1349"/>
        <source>About %1</source>
        <translation>О программе %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1986"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2993"/>
        <source>User not found.</source>
        <translation>Пользователь не найден.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2030"/>
        <source>Do you want to download %1 (%2) from %3?</source>
        <translation>Вы хотите загрузить %1 (%2) из %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2685"/>
        <source>Sound module is not working. The default BEEP will be used.</source>
        <translation>Звуковой модуль не работает. Будет использован звуковой сигнал по умолчанию.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2690"/>
        <source>Sound file not found</source>
        <translation>Звуковой файл не найден</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3175"/>
        <source>Do you really want to delete chat with %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3241"/>
        <source>Default language is restored.</source>
        <translation>Язык по умолчанию восстановлен.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3243"/>
        <source>New language &apos;%1&apos; is selected.</source>
        <translation>Новый язык &apos;%1&apos; выбран.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="3246"/>
        <source>You must restart %1 to apply these changes.</source>
        <translation>Вы должны перезапустить %1 для применения изменений.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1485"/>
        <source>Please save the network password in the next dialog.</source>
        <translation>Пожалуйста, сохраните сетевой пароль в следующем диалоге.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="809"/>
        <source>If enabled the window geometry will be reset to default value at the next startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="945"/>
        <source>Show the user&apos;s vCard on right click</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="946"/>
        <source>If enabled you can see the user&apos;s vCard when right click on it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="925"/>
        <source>Save the users on exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="671"/>
        <source>Shortcuts...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="672"/>
        <source>Enable and edit your custom shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="874"/>
        <source>Always open a new floating chat window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="875"/>
        <source>If enabled when you always open chat in a new floating window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="915"/>
        <source>Prompt on quit (only when connected)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="916"/>
        <source>If enabled you will be asked if you really want to close %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="926"/>
        <source>If enabled the user list will be save on exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="959"/>
        <source>Change size of the user&apos;s picture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="960"/>
        <source>Click to change the picture size of the users in the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1006"/>
        <source>Show %1 home</source>
        <translation>Показать главное окно %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1007"/>
        <source>Show the homepage with %1 activity</source>
        <translation>Показать домашнюю страничку окна %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1046"/>
        <source>Like %1 on Facebook</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1054"/>
        <source>Donate for %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1082"/>
        <source>Close button minimize to tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1083"/>
        <source>If enabled when the close button is clicked the window will be minimized to the system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1088"/>
        <source>Escape key minimize to tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1089"/>
        <source>If enabled when the escape button is clicked the window will be minimized to the system tray icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1066"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1722"/>
        <source>New message from %1</source>
        <translation>Новое сообщение от %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1725"/>
        <source>New message arrived</source>
        <translation>Новое сообщение получено</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1829"/>
        <source>%1 is writing...</source>
        <translation>%1 набирает...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1842"/>
        <source>Do you want to disconnect from %1 network?</source>
        <translation>Вы хотите отключиться от сети %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1875"/>
        <source>You are %1%2</source>
        <translation>Вы - %1%2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1886"/>
        <source>Please insert the new status description</source>
        <translation>Пожалуйста, вставьте описание нового статуса</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1971"/>
        <source>There is no user connected.</source>
        <translation>Нет подключенных пользователей.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1977"/>
        <source>Please select the user to whom you would like to send a file.</source>
        <translation>Пожалуйста, выберите пользователя, которому вы хотите отослать файл.</translation>
    </message>
    <message>
        <source>Do you want to download %1 (%2) from the user %3?</source>
        <translation type="obsolete">Вы хотите загрузить %1 (%2) от пользователя %3?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="327"/>
        <location filename="../src/desktop/GuiMain.cpp" line="354"/>
        <location filename="../src/desktop/GuiMain.cpp" line="371"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2104"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2111"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2637"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3038"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3097"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3131"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3176"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3635"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2063"/>
        <source>%1 already exists. Please select a new filename.</source>
        <translation>%1 уже существует. Пожалуйста выберите новое имя файла.</translation>
    </message>
    <message>
        <source>File is not available for download. User is offline.</source>
        <translation type="obsolete">Файл не доступен для загрузки. Пользователь отключен.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2208"/>
        <source>%1 - Select the download folder</source>
        <translation>%1 - Выберите папку загрузки</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2361"/>
        <source>Plugin Manager...</source>
        <translation>Менеджер расширений...</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2362"/>
        <source>Open the plugin manager dialog and manage the installed plugins</source>
        <translation>Открыть диалог менеджера расширений и управлять установленными расширениями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2364"/>
        <source>is a plugin developed by</source>
        <translation>расширение разработано</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1016"/>
        <source>Make a screenshot</source>
        <translation>Сделать снимок</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1017"/>
        <source>Show the utility to capture a screenshot</source>
        <translation>Показать утилиту для захвата снимка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1014"/>
        <source>Show the %1 log</source>
        <translation>Показать журнал %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="631"/>
        <source>Show the main tool bar with settings</source>
        <translation>Показать главную панель с настройками</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1171"/>
        <source>Show the user panel</source>
        <translation>Показать панель пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1183"/>
        <source>Show the group panel</source>
        <translation>Показать панель группы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1281"/>
        <source>Show the bar of local file sharing</source>
        <translation>Показать панель с локальными общедоступными файлами</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1292"/>
        <source>Show the bar of network file sharing</source>
        <translation>Показать панель общедоступных файлов в сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2031"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2168"/>
        <source>Yes, and don&apos;t ask anymore</source>
        <translation>Да, и больше не спрашивать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="1015"/>
        <source>Show the application log to see if an error occurred</source>
        <translation>Показать журнал приложения для просмотра возникших ошибок</translation>
    </message>
    <message>
        <source>%1 will keep running in the background mode</source>
        <translation type="obsolete">%1 будет поддерживаться работащим в фоновом режиме</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2636"/>
        <source>Do you really want to open the file %1?</source>
        <translation>Вы действительно хотите открыть файл %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Unable to open %1</source>
        <translation>Не удается открыть %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2643"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2911"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2661"/>
        <source>Sound files (*.wav)</source>
        <translation>Звуковые файл (*.wav)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2672"/>
        <source>Sound is not enabled on a new message. Do you want to enable it?</source>
        <translation>Звук не включен для новых сообщений. Вы хотите включить его?</translation>
    </message>
    <message>
        <source>Sound file %1 not found.</source>
        <translation type="obsolete">Звуковой файл %1 не найден.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2813"/>
        <source>Now %1 will start on windows boot.</source>
        <translation>Теперь %1 будет запускаться при загрузке Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2820"/>
        <source>%1 will not start on windows boot.</source>
        <translation>%1 не будет запускаться при загрузке Windows.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2877"/>
        <source>Please select a chat you would like to link the saved text.</source>
        <translation>Пожалуйста, выберите чат, с которым вы хотите связать сохраненный текст.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2886"/>
        <source>The chat &apos;%1&apos; selected has already a saved text.&lt;br /&gt;What do you want to do with the selected saved text?</source>
        <translation>Выбранный чат &apos;%1&apos; уже содержит сохраненный текст.&lt;br /&gt;Что вы хотите сделать с выбранным сохраненным текстом?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="839"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Overwrite</source>
        <translation>Перезаписать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <source>Add in the head</source>
        <translation>Добавить в заголовок</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMain.cpp" line="2149"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2740"/>
        <location filename="../src/desktop/GuiMain.cpp" line="2888"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3060"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>GuiMessageEdit</name>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="322"/>
        <source>Undo</source>
        <translation>Отменить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="324"/>
        <source>Redo</source>
        <translation>Вернуть</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="327"/>
        <source>Cut</source>
        <translation>Вырезать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="328"/>
        <source>Copy</source>
        <translation>Копировать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="329"/>
        <source>Paste</source>
        <translation>Вставить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiMessageEdit.cpp" line="332"/>
        <source>Select All</source>
        <translation>Выбрать Все</translation>
    </message>
</context>
<context>
    <name>GuiPluginManager</name>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="100"/>
        <source>Enable All</source>
        <translation>Включить все</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="110"/>
        <source>Disable All</source>
        <translation>Выключить все</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.ui" line="133"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="36"/>
        <source>Plugin Manager - %1</source>
        <translation>Менеджер расширений - %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Plugin</source>
        <translation>Расширение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Version</source>
        <translation>Версия</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="41"/>
        <source>Author</source>
        <translation>Автор</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Disable %1</source>
        <translation>Выключить %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="96"/>
        <source>Enable %1</source>
        <translation>Включить %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is enabled</source>
        <translation>%1 включено</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="107"/>
        <source>%1 is disabled</source>
        <translation>%1 выключено</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="129"/>
        <source>Please select a plugin in the list.</source>
        <translation>Пожалуйста, выберите расширение в списке.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="152"/>
        <source>Text Markers</source>
        <translation>Маркеры текста</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="171"/>
        <source>Games</source>
        <translation>Игры</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="192"/>
        <source>%1 - Select the plugin folder</source>
        <translation>%1 - Выберите папку расширений</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiPluginManager.cpp" line="208"/>
        <source>Folder %1 not found.</source>
        <translation>Папка %1 не найдена.</translation>
    </message>
</context>
<context>
    <name>GuiSavedChat</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="56"/>
        <source>Saved chat</source>
        <translation>Сохраненный чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="58"/>
        <source>Empty</source>
        <translation>Пусто</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="72"/>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="143"/>
        <source>Find text in chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="77"/>
        <source>Copy to clipboard</source>
        <translation type="unfinished">Копировать в буфер обмена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="79"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="83"/>
        <source>Open selected text as url</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="86"/>
        <source>Print...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChat.cpp" line="181"/>
        <source>%1 not found in chat.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiSavedChatList</name>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="47"/>
        <source>Show</source>
        <translation>Показать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="50"/>
        <source>Link to chat</source>
        <translation>Ссылка на чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="52"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Do you really want to delete this saved chat?</source>
        <translation>Вы действительно хотите удалить сохраненный чат?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="103"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
</context>
<context>
    <name>GuiSavedChatWidget</name>
    <message>
        <location filename="../src/desktop/GuiSavedChat.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GuiScreenShot</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="35"/>
        <source>Make a Screenshot</source>
        <translation>Сделать снимок</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="44"/>
        <source>Delay</source>
        <translation>Задержка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="45"/>
        <source>Delay screenshot for selected seconds</source>
        <translation>Задержка снимка на выбранные секунды</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="53"/>
        <source>s</source>
        <translation>сек</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="61"/>
        <source>Hide this window</source>
        <translation>Скрыть это окно</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="62"/>
        <source>Hide this window before capture screenshot</source>
        <translation>Скрыть это окно перед захватом снимка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="73"/>
        <source>Enable high dpi</source>
        <translation>Включить высокое разрешение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="74"/>
        <source>Enable high dpi support to manage, for example, Apple Retina display</source>
        <translation>Включить поддержку высокого разрешения для управления, например, монитором Apple Retina</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="80"/>
        <source>Capture</source>
        <translation>Захват</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="81"/>
        <source>Capture a screenshot of your desktop</source>
        <translation>Сделать снимок вашего рабочего стола</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="82"/>
        <source>Send</source>
        <translation>Отправить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="83"/>
        <source>Send the captured screenshot to an user</source>
        <translation>Отправить захваченный снимок пользователю</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="84"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="85"/>
        <source>Save the captured screenshot as file</source>
        <translation>Сохранить захваченный снимок в файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="86"/>
        <source>Delete</source>
        <translation>Удалить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="87"/>
        <source>Delete the captured screenshot</source>
        <translation>Удалить захваченный снимок</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="107"/>
        <source>No screenshot available</source>
        <translation>Нет доступных снимков</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="179"/>
        <source>/beesshot-%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="182"/>
        <source>Save As</source>
        <translation>Сохранить как</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="184"/>
        <source>%1 Files (*.%2)</source>
        <translation>%1 Файлы (*.%2)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="199"/>
        <source>/beesshottmp-%1.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.cpp" line="206"/>
        <source>Unable to save temporary file: %1</source>
        <translation>Не удается сохранить временный файл: %1</translation>
    </message>
</context>
<context>
    <name>GuiScreenShotWidget</name>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiScreenShot.ui" line="84"/>
        <source>No Screenshot Available</source>
        <translation>Нет доступных снимков</translation>
    </message>
</context>
<context>
    <name>GuiSearchUser</name>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="20"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="77"/>
        <source>Local subnet address *</source>
        <translation>Адрес локальной подсети *</translation>
    </message>
    <message>
        <source>Enter the IP addresses or subnet of your local area network separed by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation type="obsolete">Введите IP-адреса или подсеть вашей области сети, разделенные запятой (например: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="204"/>
        <source>Addresses in beehosts.ini *</source>
        <translation>Адреса в beehosts.ini *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="35"/>
        <source>UDP Port in beebeep.rc *</source>
        <translation>UDP-порт в beebeep.rc *</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="64"/>
        <source>(the same for all clients)</source>
        <translation>(одинаковый для всех клиентов)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="106"/>
        <source>(search users here by default)</source>
        <translation>(искать пользователей здесь по умолчанию)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="119"/>
        <source>Multicast group in beebeep.rc *</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="155"/>
        <source>Enable broadcast interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="184"/>
        <source>seconds (0=disabled, 10=default)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="351"/>
        <source>Workgroups (enter your network groups separated by comma)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="239"/>
        <source>Accept connections only from your workgroups</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="258"/>
        <source>Enter the IP addresses or subnet of your local area network separated by comma (example: 192.168.0.123, 192.168.0.45, 192.168.1.255)</source>
        <translation>Введите IP-адреса или подсеть вашей области сети, разделенные запятой (например: 192.168.0.123, 192.168.0.45, 192.168.1.255)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="284"/>
        <source>Split subnet to IPV4 addresses</source>
        <translation>Разделить подсеть в IPV4-адресах</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="297"/>
        <source>Verbose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="328"/>
        <source>Max users to contact in a tick</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="337"/>
        <source>Automatically add external subnet</source>
        <translation>Автоматически добавлять внешнюю подсеть</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="344"/>
        <source>Enable Zero Configuration Networking</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="367"/>
        <source>* (read only section)</source>
        <translation>* (раздел только для чтения)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="374"/>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.ui" line="381"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <source>Broadcast</source>
        <translation type="obsolete">Передача</translation>
    </message>
    <message>
        <source>Please enter the IP addresses (separed by comma) where the lan users are available.</source>
        <translation type="obsolete">Пожалуйста введите IP-адреса (разделенные запятой), где есть доступные сетевые пользователи.</translation>
    </message>
    <message>
        <source>Save addresses</source>
        <translation type="obsolete">Сохранить адреса</translation>
    </message>
    <message>
        <source>Search Users</source>
        <translation type="obsolete">Искать пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="36"/>
        <source>Search for users</source>
        <translation>Поиск для всех пользователей</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="54"/>
        <source>Unknown address</source>
        <translation>Неизвестный адрес</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="62"/>
        <source>File is empty</source>
        <translation>Файл пуст</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="116"/>
        <source>Warning</source>
        <translation>Предупреждение</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSearchUser.cpp" line="117"/>
        <source>You have inserted an invalid host address:
%1 is removed from the list.</source>
        <translation>Вы вставили неправильный адрес узла:
%1 удален из списка.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocal</name>
    <message>
        <source>Filename</source>
        <translation type="obsolete">Имя файла</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="37"/>
        <source>Share your folders or files</source>
        <translation>Открыть общий доступ к вашим папкам или файлам</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Size</source>
        <translation>Размер</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>Path</source>
        <translation>Путь</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="46"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="77"/>
        <source>Share a file</source>
        <translation>Поделиться файлом</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="78"/>
        <source>Add a file to your local share</source>
        <translation>Добавить файл в ваш локальный общий доступ</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="80"/>
        <source>Share a folder</source>
        <translation>Поделиться папкой</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="81"/>
        <source>Add a folder to your local share</source>
        <translation>Добавить папку в ваш локальный общий доступ </translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="83"/>
        <source>Update shares</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load shared files</source>
        <translation type="obsolete">Загрузиь общедоступные файлы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="84"/>
        <source>Update shared folders and files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove shared files from the selected path</source>
        <translation type="obsolete">Удалить общедоступные файлы из выбранного пути</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="86"/>
        <source>Remove shared path</source>
        <translation>Удалить общедоступный путь</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="87"/>
        <source>Remove shared path from the list</source>
        <translation>Удалить общедоступный путь из списка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="89"/>
        <source>Clear all shares</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="90"/>
        <source>Clear all shared paths from the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="99"/>
        <source>Shared files</source>
        <translation>Общедоступные файлы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="101"/>
        <source>File transfer is disabled</source>
        <translation>Передача файлов выключена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="167"/>
        <source>Do you really want to remove all shared paths?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>Yes</source>
        <translation type="unfinished">Да</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="168"/>
        <source>No</source>
        <translation type="unfinished">Нет</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="214"/>
        <source>Click to open %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="270"/>
        <source>File transfer is disabled. Open the option menu to enable it.</source>
        <translation>Передача файлов выключена. Откройте меню настроек для включения.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="334"/>
        <source>%1 shared files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File sharing is disabled</source>
        <translation type="obsolete">Общий доступ файлов выключен</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="119"/>
        <source>Select a file to share</source>
        <translation>Выберите файл для общего доступа</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="132"/>
        <source>Select a folder to share</source>
        <translation>Выберите папку для общего доступа</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="150"/>
        <source>Please select a shared path.</source>
        <translation>Пожалуйста, выберите общедоступный путь.</translation>
    </message>
    <message>
        <source>Double click to open %1</source>
        <translation type="obsolete">Двойной щелчок для открытия %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.cpp" line="229"/>
        <source>%1 is already shared.</source>
        <translation>%1 уже общедоступен.</translation>
    </message>
    <message>
        <source>File sharing is disabled. Open the option menu to enable it.</source>
        <translation type="obsolete">Общий доступ выключен. Откройте меню настроек для включения его.</translation>
    </message>
</context>
<context>
    <name>GuiShareLocalWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareLocal.ui" line="87"/>
        <source>2</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GuiShareNetwork</name>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Размер</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="98"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Статус</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="39"/>
        <source>Folder and Files shared in your network</source>
        <translation>Папки и файлы открыты для общего доступа в вашей сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="52"/>
        <source>Scan network</source>
        <translation>Сканировать сеть</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="53"/>
        <source>Search shared files in your network</source>
        <translation>Искать общедоступные файлы в вашей сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="56"/>
        <source>Reload list</source>
        <translation>Перезагрузить список</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="57"/>
        <source>Clear and reload list</source>
        <translation>Очистить и перезагрузить список</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="68"/>
        <source>Filter</source>
        <translation>Фильтр</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="74"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
    <message>
        <source>Apply Filter</source>
        <translation type="obsolete">Применить фильтр</translation>
    </message>
    <message>
        <source>Filter the files in list using some keywords</source>
        <translation type="obsolete">Фильтровать файлы в списке используя некоторые ключевые слова</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="83"/>
        <source>File Type</source>
        <translation>Тип файла</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="89"/>
        <source>All Files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="61"/>
        <source>Download</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="62"/>
        <source>Download single or multiple files simultaneously</source>
        <translation>Загрузить один или несколько файлов одновременно</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="116"/>
        <source>All Users</source>
        <translation>Все пользователи</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="141"/>
        <source>%1 is searching shared files in your network</source>
        <translation>%1 ведет поиск общедоступных файлов в вашей сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="195"/>
        <source>Double click to download %1</source>
        <translation>Двойной щелчок для загрузки %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="234"/>
        <source>%1 has shared %2 files (%3)</source>
        <translation>%1 открыл доступ для %2 файлов (%3)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="315"/>
        <source>Double click to open %1</source>
        <translation>Двойной щелчок для открытия %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="317"/>
        <source>Transfer completed</source>
        <translation>Передача завершена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="331"/>
        <source>%1 files are shown in list (%2 are available in your network)</source>
        <translation>%1 файлов показано в списке (%2 доступно в вашей сети)</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="333"/>
        <source>%1 files shared in your network</source>
        <translation>%1 файлов доступно в вашей сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download single file</source>
        <translation>Загрузить один файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="375"/>
        <source>Download %1 selected files</source>
        <translation>Загрузить %1 выбранных файлов</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="377"/>
        <source>MAX</source>
        <translation>МАКС.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="380"/>
        <source>Clear selection</source>
        <translation>Отменить выбор</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="384"/>
        <source>Expand all items</source>
        <translation>Развернуть все элементы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="385"/>
        <source>Collapse all items</source>
        <translation>Свернуть все элементы</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.cpp" line="396"/>
        <source>Please select one or more files to download.</source>
        <translation>Пожалуйста, выберите один или несколько файлов для загрузки.</translation>
    </message>
</context>
<context>
    <name>GuiShareNetworkWidget</name>
    <message>
        <location filename="../src/desktop/GuiShareNetwork.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>GuiShortcut</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="34"/>
        <source>Shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="37"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.cpp" line="114"/>
        <source>Insert shorcut for the action: %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiShortcutDialog</name>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="17"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restore to default language</source>
        <translation type="obsolete">Восстановить язык по умолчанию</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="23"/>
        <source>Use custom shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="50"/>
        <source>Restore default shortcuts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="53"/>
        <source>Restore</source>
        <translation type="unfinished">Восстановить</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="76"/>
        <source>Ok</source>
        <translation type="unfinished">Ok</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiShortcut.ui" line="89"/>
        <source>Cancel</source>
        <translation type="unfinished">Отмена</translation>
    </message>
</context>
<context>
    <name>GuiSystemTray</name>
    <message>
        <source>1 new message</source>
        <translation type="obsolete">1 новое сообщение</translation>
    </message>
    <message>
        <source>%1 new messages</source>
        <translation type="obsolete">%1 новых сообщений</translation>
    </message>
</context>
<context>
    <name>GuiTetris</name>
    <message>
        <source>BeeTetris</source>
        <translation type="obsolete">Bee Тетрис</translation>
    </message>
    <message>
        <source>Next Piece</source>
        <translation type="obsolete">Следующая фигура</translation>
    </message>
    <message>
        <source>Score</source>
        <translation type="obsolete">Счет</translation>
    </message>
    <message>
        <source>Lines</source>
        <translation type="obsolete">Линии</translation>
    </message>
    <message>
        <source>Level</source>
        <translation type="obsolete">Уровень</translation>
    </message>
    <message>
        <source>Record</source>
        <translation type="obsolete">Запись</translation>
    </message>
    <message>
        <source>Start</source>
        <translation type="obsolete">Запуск</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Пауза</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="obsolete">Продолжение</translation>
    </message>
</context>
<context>
    <name>GuiTransferFile</name>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>User</source>
        <translation>Пользователь</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="36"/>
        <source>Status</source>
        <translation>Статус</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="116"/>
        <source>Completed</source>
        <translation>Завершено</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="122"/>
        <source>Cancel Transfer</source>
        <translation>Отменить передачу</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="128"/>
        <source>Not Completed</source>
        <translation>Не завершено</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="161"/>
        <source>Transfer completed</source>
        <translation>Передача завершена</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Downloading</source>
        <translation>Загрузка</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="167"/>
        <source>Uploading</source>
        <translation>Отправление</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="218"/>
        <source>Do you really want to cancel the transfer of %1?</source>
        <translation>Вы действительно хотите отменить передачу %1?</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiTransferFile.cpp" line="251"/>
        <source>Remove all transfers</source>
        <translation>Убрать все передачи</translation>
    </message>
</context>
<context>
    <name>GuiUserList</name>
    <message>
        <location filename="../src/desktop/GuiUserList.cpp" line="55"/>
        <source>Search</source>
        <translation>Поиск</translation>
    </message>
</context>
<context>
    <name>GuiVCard</name>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="59"/>
        <source>Birthday: %1</source>
        <translation>День рождения: %1</translation>
    </message>
    <message>
        <source>use old version</source>
        <translation type="obsolete">использовать старую версию</translation>
    </message>
    <message>
        <source>use old %1</source>
        <translation type="obsolete">использовать старый %1</translation>
    </message>
    <message>
        <source>use new %1</source>
        <translation type="obsolete">использовать новый %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="85"/>
        <source>unknown</source>
        <translation>неизвестный</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="87"/>
        <source>old %1</source>
        <translation>старый %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="89"/>
        <source>new %1</source>
        <translation>новый %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="105"/>
        <source>Remove from favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="110"/>
        <source>Add to favorites</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="115"/>
        <source>Chat with all</source>
        <translation>Общаться со всеми</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.cpp" line="121"/>
        <source>Open chat</source>
        <translation>Открыть чат</translation>
    </message>
</context>
<context>
    <name>GuiVCardDialog</name>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="22"/>
        <source>User ID</source>
        <translation>Идентификатор пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="150"/>
        <source>Nickname</source>
        <translation>Псевдоним</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="207"/>
        <source>First name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="224"/>
        <source>Last name</source>
        <translation>Фамилия</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="241"/>
        <source>Birthday</source>
        <translation>День рождения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="274"/>
        <source>Email</source>
        <translation>Эл. почта</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="291"/>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="356"/>
        <source>Add or change photo</source>
        <translation>Добавить или изменить фото</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="385"/>
        <source>Remove photo</source>
        <translation>Удалить фото</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="417"/>
        <source>Change your nickname color</source>
        <translation>Изменить цвет вашего псевдонима</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="456"/>
        <source>Other informations</source>
        <translation>Прочая информация</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="499"/>
        <source>Ok</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiEditVCard.ui" line="509"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
</context>
<context>
    <name>GuiVCardWidget</name>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="81"/>
        <source>Widget</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="291"/>
        <source>User Path</source>
        <translation>Путь пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="307"/>
        <source>User Name</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="323"/>
        <source>Birthday</source>
        <translation>День рождения</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="339"/>
        <source>Email</source>
        <translation>Эл. почта</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="355"/>
        <source>Phone</source>
        <translation>Телефон</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="417"/>
        <source>S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="452"/>
        <source>Open chat</source>
        <translation>Открыть чат</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="490"/>
        <source>Send a file</source>
        <translation>Отправить файл</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="525"/>
        <source>Change the nickname color</source>
        <translation>Изменить цвет псевдонима</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiVCard.ui" line="583"/>
        <source>Remove user</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuiWizard</name>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="36"/>
        <source>Welcome to &lt;b&gt;%1 Network&lt;/b&gt;.</source>
        <translation>Добро пожаловать в сеть&lt;b&gt;%1&lt;/b&gt;.</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="42"/>
        <source>Your system account is</source>
        <translation>Ваша системная учетная запись</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.cpp" line="59"/>
        <source>Your nickname can not be empty.</source>
        <translation>Ваш псевдоним не может быть пустым.</translation>
    </message>
</context>
<context>
    <name>GuiWizardDialog</name>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="56"/>
        <source>Welcome</source>
        <translation>Добро пожаловать</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiWizard.ui" line="94"/>
        <source>Ok</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LifeGame</name>
    <message>
        <source>BeeLife</source>
        <translation type="obsolete">Bee Жизнь</translation>
    </message>
    <message>
        <source>&lt;b&gt;The Game of Life&lt;/b&gt;, also known simply as Life, is a cellular automaton devised by the British mathematician &lt;b&gt;John Horton Conway&lt;/b&gt; in 1970. The universe of the Game of Life is an infinite two-dimensional orthogonal grid of square cells, each of which is in one of two possible states, alive or dead. Every cell interacts with its eight neighbours, which are the cells that are horizontally, vertically, or diagonally adjacent. At each step in time, the following transitions occur:&lt;ul&gt;&lt;li&gt;Any live cell with less than two live neighbours dies, as if caused by under-population.&lt;/li&gt;&lt;li&gt;Any live cell with two or three live neighbours lives on to the next generation.&lt;/li&gt;&lt;li&gt;Any live cell with more than three live neighbours dies, as if by overcrowding.&lt;/li&gt;&lt;li&gt;Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.&lt;/li&gt;&lt;/ul&gt;The rules continue to be applied repeatedly to create further generations.&lt;br /&gt;For more info please visit &lt;a href=http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life&gt;Conway&apos;s Game of Life&lt;/a&gt;.</source>
        <translation type="obsolete">&lt;b&gt;Игра Жизни&lt;/b&gt;, также известная просто как Жизнь, - это клеточный автомат разработан британским математиком &lt;b&gt;Джоном Гортоном Конвеем (John Horton Conway)&lt;/b&gt; в 1970. Пространство Игры Жизни - это бесконечная двухмерная ортогональная сетка квадратных ячеек, каждая из которых имеет одно из двух возможных состояний, живое или мертвое. Каждая ячейка взаимодействует со своими восемью соседями, которые являются ячейками расположеными по горизонтали, вертикали или диагонали. В каждый шаг во времени происходят следующие преобразования:&lt;ul&gt;&lt;li&gt;Каждая живая ячейка с менее чем двумя живыми соседями умирает по причине низкого заселения.&lt;/li&gt;&lt;li&gt;Каждая живая ячейка с двумя иили тремя живыми соседями живет до следующего поколения.&lt;/li&gt;&lt;li&gt;Каждая живая ячейка с более чем тремя соседями умирает, Any live cell with more than three live neighbours dies по причине перенаселения.&lt;/li&gt;&lt;li&gt;Каждая мертвая ячейка с точно тремя живыми соседями становится живой ячейкой по причине размножения.&lt;/li&gt;&lt;/ul&gt;Правила продолжения для примениния повторяются для создания дальнейших поколений.&lt;br /&gt;Для детальной информации посетите &lt;a href=http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life&gt;Conway&apos;s Game of Life&lt;/a&gt;.</translation>
    </message>
</context>
<context>
    <name>NumberTextMarker</name>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="33"/>
        <source>Number Text Marker</source>
        <translation>Маркер числового текста</translation>
    </message>
    <message>
        <location filename="../plugins/numbertextmarker/NumberTextMarker.cpp" line="48"/>
        <source>If you want to encode your message with numbers write a #text to encode# .</source>
        <translation>Если вы хотите закодировать ваше сообщение числами наберите #text to encode# .</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="63"/>
        <location filename="../src/desktop/GuiMain.cpp" line="3119"/>
        <location filename="../src/desktop/GuiUserItem.cpp" line="84"/>
        <source>All Lan Users</source>
        <translation>Все пользователи сети</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="64"/>
        <source>Open chat with all local users</source>
        <translation>Открыть чат со всеми локальными пользователями</translation>
    </message>
    <message>
        <source>Nobody</source>
        <translation type="obsolete">Никто</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatItem.cpp" line="84"/>
        <source>Open chat with %1</source>
        <translation>Открыть чат с %1</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiChatMessage.cpp" line="61"/>
        <source>You</source>
        <translation>Вы</translation>
    </message>
    <message>
        <source>Saved in</source>
        <translation type="obsolete">Сохранен в</translation>
    </message>
    <message>
        <source>Started in</source>
        <translation type="obsolete">Начато в</translation>
    </message>
    <message>
        <source>Double click to open chat with all local users</source>
        <translation type="obsolete">Двойной щелчок для открытия чата со всеми локальными пользователями</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="148"/>
        <source>Click to open chat with all local users</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="152"/>
        <source>%1 is %2</source>
        <translation>%1 - %2</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiUserItem.cpp" line="167"/>
        <source>Click to send a private message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Double click to send a private message</source>
        <translation type="obsolete">Двойной щелчок для отправки приватного сообщения</translation>
    </message>
    <message>
        <source>Double click to send message to group: %1</source>
        <translation type="obsolete">Двойной щелчок для отправки сообщения группе: %1</translation>
    </message>
    <message>
        <source>Double click to view chat history: %1</source>
        <translation type="obsolete">Двойной щелчок для просмотра истории чата: %1</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="obsolete">Файл</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="obsolete">Размер</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="obsolete">Статус</translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiGroupItem.cpp" line="67"/>
        <source>Click to send message to group: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/desktop/GuiSavedChatList.cpp" line="129"/>
        <source>Click to view chat history: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="100"/>
        <source>Empty</source>
        <translation type="unfinished">Пусто</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="102"/>
        <source>Send file</source>
        <translation type="unfinished">Отправить файл</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="104"/>
        <source>Show file transfers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="106"/>
        <source>Set focus in message box</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="108"/>
        <source>Minimize all chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="110"/>
        <source>Show the next unread message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="112"/>
        <source>Send chat message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="114"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="116"/>
        <source>Broadcast</source>
        <translation type="unfinished">Передача</translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="118"/>
        <source>Find text in chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="120"/>
        <source>Find next text in chat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="122"/>
        <source>Send folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="124"/>
        <source>Show emoticons panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/gui/ShortcutManager.cpp" line="126"/>
        <source>Show all chats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show emoticons</source>
        <translation type="obsolete">Показать смайлики</translation>
    </message>
</context>
<context>
    <name>RainbowTextMarker</name>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="37"/>
        <source>Rainbow Text Marker</source>
        <translation>Маркер радужного текста</translation>
    </message>
    <message>
        <location filename="../plugins/rainbowtextmarker/RainbowTextMarker.cpp" line="52"/>
        <source>If you want a &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; write a ~rainbow text~ .</source>
        <translation>Если вы хотите &lt;font color=#FF0000&gt;r&lt;/font&gt;&lt;font color=#FF8000&gt;a&lt;/font&gt;&lt;font color=#FFff00&gt;i&lt;/font&gt;&lt;font color=#7Fff00&gt;n&lt;/font&gt;&lt;font color=#00ff00&gt;b&lt;/font&gt;&lt;font color=#00ff80&gt;o&lt;/font&gt;&lt;font color=#00ffff&gt;w&lt;/font&gt;&lt;font color=#0080ff&gt; &lt;/font&gt;&lt;font color=#0000ff&gt;t&lt;/font&gt;&lt;font color=#7F00ff&gt;e&lt;/font&gt;&lt;font color=#FF00ff&gt;x&lt;/font&gt;&lt;font color=#FF0080&gt;t&lt;/font&gt; (радужный текст), наберите ~rainbow text~ .</translation>
    </message>
</context>
<context>
    <name>RegularBoldTextMarker</name>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="32"/>
        <source>Regular Bold Text Marker</source>
        <translation>Маркер обычного полужирного текста</translation>
    </message>
    <message>
        <location filename="../plugins/regularboldtextmarker/RegularBoldTextMarker.cpp" line="47"/>
        <source>If you want to format your message with words in regular and bold font write a [text to format] .</source>
        <translation>Если вы хотите отформатировать ваше сообщение со словами в обычный и полужирный шрифт, наберите [text to format] .</translation>
    </message>
</context>
<context>
    <name>TetrisBoard</name>
    <message>
        <source>Pause</source>
        <translation type="obsolete">Пауза</translation>
    </message>
    <message>
        <source>Game Over</source>
        <translation type="obsolete">Игра окончена</translation>
    </message>
</context>
<context>
    <name>TetrisGame</name>
    <message>
        <source>BeeTetris</source>
        <translation type="obsolete">Bee Тетрис</translation>
    </message>
    <message>
        <source>Use &lt;b&gt;Left&lt;/b&gt; and &lt;b&gt;Right Arrow&lt;/b&gt; to move the pieces on the board. &lt;b&gt;Up&lt;/b&gt; and &lt;b&gt;Down Arrow&lt;/b&gt; to rotate left and right. &lt;b&gt;Space&lt;/b&gt; to drop down the piece. &lt;b&gt;D&lt;/b&gt; to drop the piece only one line. &lt;b&gt;P&lt;/b&gt; to pause the game.</source>
        <translation type="obsolete">Используйте стрелки &lt;b&gt;Влево&lt;/b&gt; и &lt;b&gt;Вправо&lt;/b&gt; для движения фигур на доске. Стрелки&lt;b&gt;Вверх&lt;/b&gt; и &lt;b&gt;Вниз&lt;/b&gt; для поворота влево и вправо. &lt;b&gt;Пробел&lt;/b&gt; - для опускания фигуры вниз. &lt;b&gt;D&lt;/b&gt; - для опускания фигуры на одну линию. &lt;b&gt;P&lt;/b&gt; - для паузы игры.</translation>
    </message>
</context>
<context>
    <name>Tips</name>
    <message>
        <location filename="../src/Tips.h" line="31"/>
        <source>You can switch between chats with CTRL+TAB if there are new messages availables.</source>
        <translation type="unfinished">Вы можете переключаться между чатами с помощью CTRL+TAB, если есть новые доступные сообщения.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="32"/>
        <source>If you want a &lt;b&gt;bold text&lt;/b&gt; write a *bold text*.</source>
        <translation type="unfinished">Если вы хотите &lt;b&gt;полужирный текст&lt;/b&gt; напечатайте *bold text*.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="33"/>
        <source>If you want an &lt;i&gt;italic text&lt;/i&gt; write a /italic text/.</source>
        <translation type="unfinished">Если вы хотите &lt;i&gt;наклонный текст&lt;/i&gt; напечатайте /italic text/.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="34"/>
        <source>If you want an &lt;u&gt;underlined text&lt;/u&gt; write a _underlined text_.</source>
        <translation type="unfinished">Если вы хотите &lt;u&gt;подчеркнутый текст&lt;/u&gt; наберите _underlined text_.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="38"/>
        <source>You can select multiple files from network shares and download them simultaneously with a right click.</source>
        <translation type="unfinished">Вы можете выбрать несколько файлов из общедоступных в сети и загрузить их одновременно с помощью правого щелчка мышью</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="39"/>
        <source>You can disable message notification from a group right clicking on its name on the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="47"/>
        <source>&lt;i&gt;Free is that mind guided by the fantasy.&lt;/i&gt; (Marco Mastroddi)</source>
        <translation type="unfinished">&lt;i&gt;Свобода - это сознание направленное фантазией.&lt;/i&gt; (Marco Mastroddi)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="48"/>
        <source>&lt;i&gt;Stay hungry, stay foolish.&lt;/i&gt; (Steve Jobs)</source>
        <translation type="unfinished">&lt;i&gt;Оставайся голодным, оставайся безрассудным.&lt;/i&gt; (Стив Джобс)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="49"/>
        <source>&lt;i&gt;There is always one more bug.&lt;/i&gt; (Lubarsky&apos;s Law)</source>
        <translation type="unfinished">&lt;i&gt;Там всегда есть еще один баг.&lt;/i&gt; (Lubarsky&apos;s Law)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="50"/>
        <source>&lt;i&gt;If anything can go wrong, it will.&lt;/i&gt; (Murphy&apos;s Law)</source>
        <translation type="unfinished">&lt;i&gt;Если что-либо может идти неправильно, оно будет идти.&lt;/i&gt; (Закон Мерфи)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="51"/>
        <source>&lt;i&gt;If a program is useful, it will have to be changed.&lt;/i&gt; (Law of Computer Programming)</source>
        <translation type="unfinished">&lt;i&gt;Если программа полезна, она должна быть изменена.&lt;/i&gt; (Закон компьтерного программирования)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="53"/>
        <source>&lt;i&gt;Intellectuals solve problems; geniuses prevent them.&lt;/i&gt; (Albert Einstein)</source>
        <translation type="unfinished">&lt;i&gt;Интелектуалы решают проблемы; гении предупреждают их.&lt;/i&gt; (Альберт Эйнштейн)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="54"/>
        <source>&lt;i&gt;What does not destroy me, makes me strong.&lt;/i&gt; (Friedrich Nietzsche)</source>
        <translation type="unfinished">&lt;i&gt;Что не убивает меня, делает меня сильнее.&lt;/i&gt; (Фридрих Ницше)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="55"/>
        <source>&lt;i&gt;I am not young enough to know everything.&lt;/i&gt; (Oscar Wilde)</source>
        <translation type="unfinished">&lt;i&gt;Я не настолько молод чтобы знать все.&lt;/i&gt; (Оскар Уайлд)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="56"/>
        <source>&lt;i&gt;A lack of doubt leads to a lack of creativity.&lt;/i&gt; (Evert Jan Ouweneel)</source>
        <translation type="unfinished">&lt;i&gt;Недостаток сомнения ведет к недостатку творчества.&lt;/i&gt; (Evert Jan Ouweneel)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="57"/>
        <source>&lt;i&gt;Fear is the path to the dark side.&lt;/i&gt; (Joda)</source>
        <translation type="unfinished">&lt;i&gt;Страх - это путь на темную сторону.&lt;/i&gt; (Йода)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="59"/>
        <source>&lt;i&gt;I dream my painting and then paint my dream.&lt;/i&gt; (Vincent Van Gogh)</source>
        <translation type="unfinished">&lt;i&gt;Я мечтаю моим рисованием и затем рисую мою мечту.&lt;/i&gt; (Винцент Ван Гог)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="60"/>
        <source>&lt;i&gt;Everything you can imagine is real.&lt;/i&gt; (Pablo Picasso)</source>
        <translation type="unfinished">&lt;i&gt;Все что вы можете представить - реально.&lt;/i&gt; (Пабло Пикассо)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="61"/>
        <source>&lt;i&gt;All truths are easy to understand once they are discovered; the point is to discover them.&lt;/i&gt; (Galileo Galilei)</source>
        <translation type="unfinished">&lt;i&gt;Вся правда - это просто понять раз когда она открыта; вопрос в открытии ее.&lt;/i&gt; (Галилео Галилей)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="62"/>
        <source>&lt;i&gt;Truth prevails where opinions are free.&lt;/i&gt; (Thomas Paine)</source>
        <translation type="unfinished">&lt;i&gt;Правда преобладает там, где мнения свободны.&lt;/i&gt; (Thomas Paine)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="63"/>
        <source>&lt;i&gt;I&apos;ve seen things you people wouldn&apos;t believe...&lt;/i&gt; (Batty)</source>
        <translation type="unfinished">&lt;i&gt;Я вижу вещи, в которые вы люди не можете поверить...&lt;/i&gt; (Batty)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="67"/>
        <source>&lt;i&gt;Dum loquimur fugerit invida aetas: carpe diem, quam minimum credula postero&lt;/i&gt; (Orazio)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="93"/>
        <source>&lt;i&gt;Patience is also a form of action.&lt;/i&gt; (Auguste Rodin)</source>
        <translation type="unfinished">&lt;i&gt;Терпение - это также форма действия.&lt;/i&gt; (Auguste Rodin)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="66"/>
        <source>&lt;i&gt;A different language is a different vision of life.&lt;/i&gt; (Federico Fellini)</source>
        <translation type="unfinished">&lt;i&gt;Другой язык - другое видение жизни.&lt;/i&gt; (Федерико Феллини)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="68"/>
        <source>&lt;i&gt;Every day in Africa a gazelle wakes up. It knows it must run faster than the fastest lion or it will be killed. Every morning a lion wakes up. It knows that it must outrun the slowest gazelle or it will starve to death. It doesn&apos;t matter whether you are a lion or a gazelle. When the sun comes up, you better be running.&lt;/i&gt; (Abe Gubegna)</source>
        <translation type="unfinished">&lt;i&gt;Каждый день в Африке газель просыпается. Она знает что должна бежать быстрее чем самый быстрый лев или она будет убита. Каждое утро лев просыпается. Он знает что должен перегнать самую медленную газель или он умрет от голода. Неважно кто вы лев или газель. Когда встает сонце, вы лучше бегите.&lt;/i&gt; (Abe Gubegna)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="73"/>
        <source>&lt;i&gt;Okay, Houston, we&apos;ve had a problem here.&lt;/i&gt; (John L. Swigert)</source>
        <translation type="unfinished">&lt;i&gt;Окей, Хьюстон, у нас здесь проблема.&lt;/i&gt; (John L. Swigert)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="75"/>
        <source>&lt;i&gt;Second star to the right, and straight on till morning.&lt;/i&gt; (Peter Pan)</source>
        <translation type="unfinished">&lt;i&gt;Вторая звезда направо и прямо до утра.&lt;/i&gt; (Питер Пэн)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="35"/>
        <source>You can search previous sent message in the history using the CTRL+Up e CTRL+Down keys.</source>
        <translation type="unfinished">Вы можете найти отосланное сообщение в истории используя клавиши CTRL+Вверх и CTRL+Вниз.</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="65"/>
        <source>&lt;i&gt;A man&apos;s character is his fate.&lt;/i&gt; (Eraclitus)</source>
        <translation type="unfinished">&lt;i&gt;Характер человека - это его судьба.&lt;/i&gt; (Эраклитус)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="76"/>
        <source>&lt;i&gt;Necessity is the last and strongest weapon.&lt;/i&gt; (Titus Livius)</source>
        <translation type="unfinished">&lt;i&gt;Нужда - это последнее и сильнейшее оружие.&lt;/i&gt; (Titus Livius)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="77"/>
        <source>&lt;i&gt;Old-aged people are not wise, they are simply careful.&lt;/i&gt; (Ernest Hemingway)</source>
        <translation type="unfinished">&lt;i&gt;Древние люди не мудрые, они просто осторожные.&lt;/i&gt; (Эрнест Хемингуэй)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="78"/>
        <source>&lt;i&gt;A journey of a thousand miles begins with a single step.&lt;/i&gt; (Confucius)</source>
        <translation type="unfinished">&lt;i&gt;Путешествие в тысячу миль начинается с одного шага.&lt;/i&gt; (Конфуций)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="79"/>
        <source>&lt;i&gt;Life without the courage for death is slavery.&lt;/i&gt; (Lucius Annaeus Seneca)</source>
        <translation type="unfinished">&lt;i&gt;Жизнь без отваги до смерти - это рабство.&lt;/i&gt; (Lucius Annaeus Seneca)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="81"/>
        <source>&lt;i&gt;I can calculate the motion of heavenly bodies, but not the madness of people.&lt;/i&gt; (Isaac Newton)</source>
        <translation type="unfinished">&lt;i&gt;Я могу вычислить движение небесных тел, но не безумство людей.&lt;/i&gt; (Исаак Ньютон)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="82"/>
        <source>&lt;i&gt;Wonder is the beginning of wisdom.&lt;/i&gt; (Socrates)</source>
        <translation type="unfinished">&lt;i&gt;Удивление - это начало мудрости.&lt;/i&gt; (Сократ)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="83"/>
        <source>&lt;i&gt;No wise man ever wished to be younger.&lt;/i&gt; (Jonathan Swift)</source>
        <translation type="unfinished">&lt;i&gt;Неразумный человек постоянно желал быть моложе.&lt;/i&gt; (Джонатан Свифт)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="84"/>
        <source>&lt;i&gt;The only man who never makes a mistake is the man who never does anything.&lt;/i&gt; (Theodore Roosevelt)</source>
        <translation type="unfinished">&lt;i&gt;Человек, который никогда не делает ошибки, - это человек, который никогда ничего не делает.&lt;/i&gt; (Теодор Рузвельт)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="85"/>
        <source>&lt;i&gt;Attitude is a little thing that makes a big difference.&lt;/i&gt; (Winston Churchill)</source>
        <translation type="unfinished">&lt;i&gt;Позиция - это маленькая вещь, которая делает большой разрыв.&lt;/i&gt; (Винстон Черчилл)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="87"/>
        <source>&lt;i&gt;We become what we think.&lt;/i&gt; (Buddha)</source>
        <translation type="unfinished">&lt;i&gt;Мы делаем то, что мы думаем.&lt;/i&gt; (Будда)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="88"/>
        <source>&lt;i&gt;Difficulties are things that show a person what they are.&lt;/i&gt; (Epictetus)</source>
        <translation type="unfinished">&lt;i&gt;Трудности являются вещами, которые показывают то кем является личность.&lt;/i&gt; (Epictetus)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="89"/>
        <source>&lt;i&gt;Who will guard the guards themselves?&lt;/i&gt; (Decimus Junius Juvenal)</source>
        <translation type="unfinished">&lt;i&gt;Кто будет охранять самих охранников?&lt;/i&gt; (Decimus Junius Juvenal)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="90"/>
        <source>&lt;i&gt;A home without books is a body without soul.&lt;/i&gt; (Marcus Tullius Cicero)</source>
        <translation type="unfinished">&lt;i&gt;Дом без книг - это тело без души.&lt;/i&gt; (Marcus Tullius Cicero)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="91"/>
        <source>&lt;i&gt;We can not stop wishing our wishes.&lt;/i&gt; (Arthur Schopenhauer)</source>
        <translation type="unfinished">&lt;i&gt;Мы не можем перестать желать наши желания.&lt;/i&gt; (Артур Шопенгауэр)</translation>
    </message>
    <message>
        <location filename="../src/Tips.h" line="37"/>
        <source>You can drop files to active chat and send them to members.</source>
        <translation type="unfinished">Вы можете перетащить файлы в активный чат и отправить их членам.</translation>
    </message>
</context>
<context>
    <name>User</name>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="62"/>
        <source>offline</source>
        <translation>отключен</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="63"/>
        <source>available</source>
        <translation>доступен</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="64"/>
        <source>busy</source>
        <translation>занят</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="65"/>
        <source>away</source>
        <translation>вышел</translation>
    </message>
    <message>
        <location filename="../src/utils/BeeUtils.cpp" line="66"/>
        <source>status error</source>
        <translation>ошибка статуса</translation>
    </message>
</context>
</TS>
