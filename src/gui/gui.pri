
INCLUDEPATH += $$PWD

HEADERS +=  gui/AudioManager.h \
  gui/Avatar.h \
  gui/ColorManager.h \
  gui/Emoticon.h \
  gui/EmoticonManager.h \
  gui/ShortcutManager.h


SOURCES +=  gui/AudioManager.cpp \
  gui/Avatar.cpp \
  gui/ColorManager.cpp \
  gui/Emojis.cpp \
  gui/Emoticon.cpp \
  gui/EmoticonManager.cpp \
  gui/ShortcutManager.cpp
